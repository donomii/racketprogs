#!/bin/sh

export GO111MODULE=auto
export BUILDDIR=build/funzone-$(uname)-$(uname -m)/
echo Creating build directory $BUILDDIR
mkdir -p $BUILDDIR 
go get "github.com/donomii/glim"  "github.com/donomii/sceneCamera" "github.com/go-gl/gl/v3.2-core/gl"  "github.com/go-gl/glfw/v3.3/glfw"  "github.com/go-gl/mathgl/mgl32" "github.com/atotto/clipboard" github.com/donomii/goof github.com/veandco/go-sdl2 github.com/veandco/go-sdl2/ttf "github.com/0xcafed00d/joystick" github.com/nfnt/resize "github.com/alphadose/haxmap" "github.com/amitbet/vnc2video" "github.com/amitbet/vnc2video/logger"

mkdir build

cd dcbh
sh build.sh &
cd ..

cd boxes
{ sh build.sh ; cp boxes ../$BUILDDIR ; cp boxes-sdl ../$BUILDDIR ; cp -r assets ../$BUILDDIR ; } &
cd ..


cd xsh
{ make ; cp xsh xshwatch  xshguardian ../$BUILDDIR ; } &
cd ..

echo trigrammr
cd trigrammr
go get github.com/donomii/trigrammr github.com/chzyer/readline
go build cmd/trigrammr-import-csv/trigrammr-import-csv.go
go build cmd/trigrammr-client/trigrammr-client.go
go build cmd/trigrammr-import-book/trigrammr-import-book.go 
cp trigrammr-import-book trigrammr-client trigrammr-import-csv ../$BUILDDIR
cd ..

echo pmoo
cd pmoo
{ make ; cp build/* ../$BUILDDIR ; } &
cd ..

cd watcher
{ go build . ; cp watcher ../$BUILDDIR ; } &
cd ..

#cd nursemai
#make
#cp build/* ../build/
#cd ..


cd mdnscan
{ go build . ; cp mdnscan ../$BUILDDIR ;} &
cd ..

cd myvox
{ go build . ; cp myvox ../$BUILDDIR ;} &
cd ..

wait
rm -r funzone
mv build funzone

mkdir dist
cp -r funzone dist/
