#!/usr/bin/env python3
import json
import sys
import urllib.request
import os

def find_objects_with_property_value(property_name, value):
    matches = []
    # Get list of all objects
    try:
        objects = os.listdir("objects")
        for obj_id in objects:
            prop_path = f"objects/{obj_id}/properties/{property_name}.prop"
            try:
                with open(prop_path, 'r') as f:
                    obj_value = f.read().strip()
                    if obj_value == value:
                        # Get object name if available
                        try:
                            with open(f"objects/{obj_id}/properties/name.prop", 'r') as name_file:
                                name = name_file.read().strip()
                        except:
                            name = obj_id
                        matches.append({"id": obj_id, "name": name})
            except:
                continue
        
        return matches
    except Exception as e:
        return {"error": True, "error_message": f"Error searching objects: {str(e)}"}

def main():
    try:
        input_data = json.loads(sys.stdin.read().strip())
        args = input_data.get("args", {})
        property_name = args.get("property")
        value = args.get("value")

        if not property_name or value is None:
            print(json.dumps({"error": True, "error_message": "Missing required property or value arguments"}))
            return

        matches = find_objects_with_property_value(property_name, value)
        if isinstance(matches, dict) and matches.get("error"):
            print(json.dumps(matches))
            return

        if matches:
            msg = f"Found {len(matches)} objects with {property_name}={value}:\n"
            for obj in matches:
                msg += f"- {obj['name']} (ID: {obj['id']})\n"
        else:
            msg = f"No objects found with {property_name}={value}"

        print(json.dumps({
            "message": msg,
            "data": {"matches": matches}
        }))

    except Exception as e:
        print(json.dumps({"error": True, "error_message": str(e)}))

if __name__ == "__main__":
    main()