package main

import (
	"encoding/json"
	"net/http"
	"strings"
)

// JSON response struct
type Response struct {
	Message      string `json:"message,omitempty"`
	ErrorMessage string `json:"error_message,omitempty"`
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 {
		sendJSONError(w, "Invalid request format")
		return
	}

	objectId := parts[1]
	verb := parts[2]

	playerId := r.Header.Get("X-Player-ID")
	if !strings.Contains(r.URL.Path, "1/login") &&  playerId == "" {
		sendJSONError(w, "Player ID not provided")
		return
	}

	// Collect additional arguments from query parameters
	args := make(map[string]interface{})
	for key, values := range r.URL.Query() {
		if len(values) > 0 {
			args[key] = values[0]
		}
	}

	// Call the appropriate function
	output, err := runVerb(objectId, verb, args, playerId)
	if err != nil {
		sendJSONError(w, err.Error())
		return
	}

	sendJSONResponse(w, output)
}

// Helper function to send JSON error responses
func sendJSONError(w http.ResponseWriter, errorMessage string) {
	response := Response{ErrorMessage: errorMessage}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(response)
}

// Helper function to send successful JSON responses
func sendJSONResponse(w http.ResponseWriter, message string) {
	response := Response{Message: message}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
