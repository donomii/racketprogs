package main

import (
	"fmt"
	"log"
	"net/http"
	"flag"
	"os"
)

var verbose bool

func init() {
    flag.BoolVar(&verbose, "verbose", false, "Enable verbose logging")
    flag.Parse()
}

func plogf(format string, a ...interface{}) {
    if verbose {
        fmt.Fprintf(os.Stderr, format, a...)
    }
}


func main() {
	setupRoutes()

	//go aiInterfaceLoop()

	// Start the HTTP server in a goroutine
	go func() {
		if verbose {
			fmt.Println("Logging errors to stderr")
		}
		fmt.Println("Starting PMOO server on :8080...")
		log.Fatal(http.ListenAndServe(":8080", nil))
	}()

	// Start the console interface
	consoleInterface()
}