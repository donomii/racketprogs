package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/chzyer/readline"
)

type VerbResult struct {
	Message           string      `json:"message,omitempty"`
	ErrorMessage      string      `json:"error_message,omitempty"`
	Data              interface{} `json:"data,omitempty"`
	deliverToPlayerID string
}

func consoleInterface() {
	rl, err := readline.New("> ")
	if err != nil {
		fmt.Println("Error initializing readline:", err)
		return
	}
	defer rl.Close()

	// FIXME Default player ID 1
	playerId := "player1"

	for {
		input, err := rl.Readline()
		if err != nil {
			break
		}

		input = strings.TrimSpace(input)
		if input == "exit" {
			break
		}

		// Execute the command and handle the output
		output, err := executeCommand(input, playerId) //FIXME deliver_to_player_id
		if err != nil {
			fmt.Println("Error:", err)
			continue
		}

		// Parse the JSON output to extract the message or error message
		var verbresult VerbResult
		if err := json.Unmarshal([]byte(output), &verbresult); err != nil {
			fmt.Println("Error parsing response:", err)
			continue
		}

		// Display message or error message
		message := verbresult.Message
		errMsg := verbresult.ErrorMessage
		if errMsg != "" {
			fmt.Println("Error:", errMsg)
		} else if message != "" {
			fmt.Println(message)
		} else {
			fmt.Println("Data-only message:", output)
		}
	}
}
