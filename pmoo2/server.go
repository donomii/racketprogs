package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type PlayerConnection struct {
	PlayerID string
	Conn     *websocket.Conn
}

var playerConnections = make(map[*websocket.Conn]*PlayerConnection)
var playerConnectionsMutex sync.Mutex

func setupRoutes() {
    http.HandleFunc("/get/", handleGetPropertyOrVerb)
    http.HandleFunc("/", handleRoot)
    http.HandleFunc("/ws", handleWebSocket)
    
    // Wrap the static file server with no-cache headers
    fs := http.FileServer(http.Dir("static"))
    noCacheFs := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        setNoCacheHeaders(w)
        fs.ServeHTTP(w, r)
    })
    http.Handle("/static/", http.StripPrefix("/static/", noCacheFs))
}

// Add a root handler that redirects to the landing page
func handleRoot(w http.ResponseWriter, r *http.Request) {
    if r.URL.Path != "/" {
        handleVerb(w, r)
        return
    }

    // Redirect root to the landing page
    http.Redirect(w, r, "/static/landing.html", http.StatusSeeOther)
}

func setNoCacheHeaders(w http.ResponseWriter) {
    w.Header().Set("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0")
    w.Header().Set("Pragma", "no-cache")
    w.Header().Set("Expires", "0")
}

func handleVerb(w http.ResponseWriter, r *http.Request) {
    setNoCacheHeaders(w)


	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	objectId := parts[1]
	verb := parts[2]

	playerId := r.Header.Get("X-Player-ID")
	if !strings.Contains(r.URL.Path, "1/login") && playerId == "" {
		http.Error(w, "Player ID not provided", http.StatusUnauthorized)
		return
	}

	args := make(map[string]interface{})

	// Parse query string parameters
	for key, values := range r.URL.Query() {
		if len(values) > 0 {
			args[key] = values[0]
		}
	}

	// Handle multi-part form data
	if strings.Contains(r.Header.Get("Content-Type"), "multipart/form-data") {
		err := r.ParseMultipartForm(32 << 20) // 32 MB
		if err != nil {
			http.Error(w, "Error parsing multipart form data: "+err.Error(), http.StatusBadRequest)
			return
		}
		for key, values := range r.MultipartForm.Value {
			if len(values) > 0 {
				args[key] = values[0]
			}
		}
		for key, fileHeaders := range r.MultipartForm.File {
			if len(fileHeaders) > 0 {
				file, err := fileHeaders[0].Open()
				if err != nil {
					http.Error(w, "Error opening uploaded file: "+err.Error(), http.StatusBadRequest)
					return
				}
				defer file.Close()
				// Read the file content
				fileContent, err := ioutil.ReadAll(file)
				if err != nil {
					http.Error(w, "Error reading uploaded file: "+err.Error(), http.StatusInternalServerError)
					return
				}
				args[key] = string(fileContent)
			}
		}
	} else if r.Header.Get("Content-Type") == "application/json" {
		// Parse JSON body
		var jsonArgs map[string]interface{}
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&jsonArgs); err == nil {
			for key, value := range jsonArgs {
				args[key] = value
			}
		}
		r.Body.Close()
		r.Body = ioutil.NopCloser(bytes.NewBuffer([]byte{}))
	} else {
		// Parse URL-encoded form data
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Error parsing form data: "+err.Error(), http.StatusBadRequest)
			return
		}
		for key, values := range r.PostForm {
			if len(values) > 0 {
				args[key] = values[0]
			}
		}
	}

	// Run the verb with the parsed arguments
	result, err := runVerb(objectId, verb, args, playerId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, result)
}

func handleGetPropertyOrVerb(w http.ResponseWriter, r *http.Request) {
    setNoCacheHeaders(w)

	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	objectId := parts[2]
	propertyOrVerb := parts[3]

	path := fmt.Sprintf("objects/%s/properties/%s.prop", objectId, propertyOrVerb)
	//log.Printf("Path: %s", path)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		path := fmt.Sprintf("objects/%s/verbs/%s/main.verb", objectId, propertyOrVerb)
		//log.Printf("Path: %s", path)
		plogf("load verb: %v", string(data))
		data, err = ioutil.ReadFile(path)
	}

	if err != nil {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	js_response := Response{Message: string(data)}
	js_str, err := json.Marshal(js_response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	w.Write(js_str)
}

func handleWebSocket(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	// Wait for login command
	_, message, err := conn.ReadMessage()
	if err != nil {
		log.Println(err)
		return
	}

	// Expecting a JSON message with player_id
	var loginData map[string]string
	err = json.Unmarshal(message, &loginData)
	if err != nil || loginData["player_id"] == "" {
		log.Println("Invalid login data")
		return
	}

	playerID := loginData["player_id"]

	playerConn := &PlayerConnection{
		PlayerID: playerID,
		Conn:     conn,
	}

	playerConnectionsMutex.Lock()
	playerConnections[conn] = playerConn
	playerConnectionsMutex.Unlock()

	defer func() {
		playerConnectionsMutex.Lock()
		delete(playerConnections, conn)
		playerConnectionsMutex.Unlock()
	}()

	// Handle incoming messages from the player
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}

		result, err := executeCommand(string(message), playerID)
		if err != nil {
			resp := Response{ErrorMessage: err.Error()}
			err = conn.WriteJSON(resp)
			if err != nil {
				log.Println("write:", err)
				break
			}
			continue
		}

		var verbresult VerbResult
		if err := json.Unmarshal([]byte(result), &verbresult); err != nil {
			resp := Response{ErrorMessage: "Error parsing response: " + err.Error() + " " + string(result)}
			err = conn.WriteJSON(resp)
			if err != nil {
				log.Println("write:", err)
				break
			}
			continue
		}

		deliverToPlayerID := verbresult.deliverToPlayerID

		// Determine where to send the response
		if deliverToPlayerID != "" && deliverToPlayerID != playerID {
			// Send to a different player
			sendToPlayer(deliverToPlayerID, result)
		} else {
			// Send to the current player
			err = conn.WriteMessage(websocket.TextMessage, []byte(result))
			if err != nil {
				log.Println("write:", err)
				break
			}
		}
	}
}

func handleSendMessage(w http.ResponseWriter, r *http.Request) {
	playerId := r.FormValue("player_id")
	message := r.FormValue("message")
	if playerId == "" || message == "" {
		http.Error(w, "player_id and message are required", http.StatusBadRequest)
		return
	}

	playerConnectionsMutex.Lock()
	defer playerConnectionsMutex.Unlock()

	var found bool
	for _, pc := range playerConnections {
		if pc.PlayerID == playerId {
			err := pc.Conn.WriteMessage(websocket.TextMessage, []byte(message))
			if err != nil {
				log.Printf("Error sending message to %s: %v", playerId, err)
				http.Error(w, "Error sending message", http.StatusInternalServerError)
				return
			}
			found = true
			break
		}
	}

	if !found {
		http.Error(w, "Player not connected", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func sendToPlayer(playerID string, message string) {
	playerConnectionsMutex.Lock()
	defer playerConnectionsMutex.Unlock()

	for _, pc := range playerConnections {
		if pc.PlayerID == playerID {
			err := pc.Conn.WriteMessage(websocket.TextMessage, []byte(message))
			if err != nil {
				log.Printf("Error sending message to %s: %v", playerID, err)
			}
			return
		}
	}
	log.Printf("Player %s not connected", playerID)
}
