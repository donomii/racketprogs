package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/donomii/goof"
	. "github.com/donomii/goof"
)

// Parseresult type
type Parseresult struct {
	Verb          string                 `json:"verb"`
	Args          map[string]interface{} `json:"args"`
	Mode          string                 `json:"mode"`
	Message       string                 `json:"message"`
	Error         bool                   `json:"error"`
	Error_message string                 `json:"error_message"`
}

func listAllObjectIds() ([]string, error) {
	// Read the objects directory
	files, err := os.ReadDir("objects")
	if err != nil {
		return nil, NewErrorf("Error reading objects directory: %v", err)
	}

	// Collect the object IDs
	var objectIds []string
	for _, file := range files {
		if file.IsDir() {
			objectIds = append(objectIds, file.Name())
		}
	}

	return objectIds, nil
}

func findObjectHoldingVerb(verb string) string {
	objectIds, err := listAllObjectIds()
	if err != nil {
		return ""
	}

	for _, objectId := range objectIds {
		verbDir := fmt.Sprintf("objects/%s/verbs", objectId)
		files, err := os.ReadDir(verbDir)
		if err != nil {
			continue
		}

		for _, file := range files {
			if file.IsDir() && file.Name() == verb {
				return objectId
			}
		}
	}

	return ""
}

func executeCommand(command string, playerID string) (string, error) {
	//fmt.Printf("ExecuteCommand: %s\n", command)
	// First, we need to get the interpreter for the parse verb
	metadataPath := "objects/1/verbs/parse/metadata.json"
	metadataContent, err := os.ReadFile(metadataPath)
	if err != nil {
		return "", goof.NewErrorf("Error reading parse verb metadata: %v", err)
	}

	var metadata struct {
		Interpreter string `json:"interpreter"`
	}
	if err := json.Unmarshal(metadataContent, &metadata); err != nil {
		return "", NewErrorf("Error parsing metadata: %v", err)
	}

	// Prepare JSON input for the parse function
	input := map[string]interface{}{
		"command":   command,
		"player_id": playerID,
	}
	// Parse the command to determine the verb and object
	// For simplicity, we'll assume the command parser is a verb on object 1
	objectId := "1"
	verbName := "parse"

	// Run the command parser verb to get the actual verb to execute
	output, err := runVerb(objectId, verbName, input, playerID)
	if err != nil {
		return "", NewErrorf("Error running command parser: %v", err)
	}

	plogf("Parsed: %+v into %v\n", input, string(output))
	//fmt.Printf("Output: %s\n", string(output))
	// Parse the result from the output JSON
	var result Parseresult
	if err := json.Unmarshal([]byte(output), &result); err != nil {
		return "", NewErrorf("Error parsing result: %v, text was %v", err, string(output))
	}

	// Handle errors from the parse result
	if result.Error == true {
		return "", NewErrorf("%s", result.Error_message)
	}

	// Find the verb in the inheritance chain
	findVerbInput := map[string]interface{}{
		"object_id": playerID,
		"verb_name": result.Verb,
	}

	//fmt.Printf("Find Verb Input: %+v\n", findVerbInput)

	findVerbResult, err := runVerb("1", "find_verb", findVerbInput, playerID)
	if err != nil {
		return "", NewErrorf("Error finding verb: %v", err)
	}

	var findVerbResponse map[string]interface{}
	err = json.Unmarshal([]byte(findVerbResult), &findVerbResponse)
	if err != nil {
		return "", NewErrorf("Error parsing find_verb response(%v): %v", string(findVerbResult), err)
	}

	if findVerbResponse["error"] == true {
		return "", NewErrorf(findVerbResponse["error_message"].(string))
	}

	// Use the object_id returned by find_verb
	verbObjectId_intf, ok := findVerbResponse["object_id"]
	if !ok {
		//return "", NewErrorf("find_verb response missing object_id")
		return "", NewErrorf("I don't know how to %s", result.Verb)
	}
	verbObjectId, ok := verbObjectId_intf.(string)
	if !ok {
		return "", NewErrorf("find_verb response object_id is not a string")
	}

	// Run the verb with the parsed arguments
	commandResult, err := runVerb(verbObjectId, result.Verb, result.Args, playerID)
	if err != nil {
		return "", NewErrorf("Error running verb: %v", err)
	}

	return commandResult, nil
}

func runVerb(objectId, verb string, args map[string]interface{}, playerID string) (string, error) {
	verbScript := fmt.Sprintf("objects/%s/verbs/%s/main.verb", objectId, verb)

	// Read the metadata to get the interpreter
	metadataFile := fmt.Sprintf("objects/%s/verbs/%s/metadata.json", objectId, verb)
	metadataBytes, err := os.ReadFile(metadataFile)
	if err != nil {
		return "", NewErrorf("failed to read metadata: %v", err)
	}

	var metadata struct {
		Interpreter string `json:"interpreter"`
	}
	if err := json.Unmarshal(metadataBytes, &metadata); err != nil {
		return "", NewErrorf("failed to parse metadata: %v", err)
	}

	// Prepare the command
	cmd := exec.Command(metadata.Interpreter, verbScript)

	// Prepare the input as JSON
	input := map[string]interface{}{
		"object_id": objectId,
		"verb":      verb,
		"args":      args,
		"player_id": playerID,
	}
	inputJSON, err := json.Marshal(input)
	if err != nil {
		return "", NewErrorf("failed to marshal input: %v", err)
	}

	plogf("Running verb %v with input %v\n", cmd, string(inputJSON))

	out, err := runVerbWithDataString(cmd, inputJSON)
	if err != nil {
		return "", NewErrorf("failed to run verb %v: %v with data %v", verb, err, string(inputJSON))
	}

	plogf("Ran verb %v with input %v\n", verb, string(inputJSON))
	plogf("Got result: %v\n", string(out))

	return out, nil
}

func runVerbWithDataString(cmd *exec.Cmd, inputJSON []byte) (string, error) {
    // Set up pipes for input and output
    cmd.Stdin = bytes.NewReader(inputJSON)
    var stdout, stderr bytes.Buffer
    cmd.Stdout = &stdout
    cmd.Stderr = &stderr

    // Run the command
    if err := cmd.Run(); err != nil {
        // Return both the error and any stderr output
        errOutput := stderr.String()
        if errOutput != "" {
            return "", fmt.Errorf("failed to run verb: %v\nError output:\n%s", err, errOutput)
        }
        return "", fmt.Errorf("failed to run verb: %v", err)
    }

    return stdout.String(), nil
}

func formatOutput(input string) string {
	// Split the input into lines
	lines := strings.Split(input, "\n")

	// Trim each line and remove empty lines
	var formattedLines []string
	for _, line := range lines {
		trimmed := strings.TrimSpace(line)
		if trimmed != "" {
			formattedLines = append(formattedLines, trimmed)
		}
	}

	// Join the lines with HTML line breaks
	return strings.Join(formattedLines, "<br>")
}
