module pmoo2

go 1.23.0

require (
	github.com/chzyer/readline v1.5.1
	github.com/donomii/goof v0.0.0-20241124064022-84f417f466df
	github.com/gorilla/websocket v1.5.3
)

require golang.org/x/sys v0.0.0-20220310020820-b874c991c1a5 // indirect
