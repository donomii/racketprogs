package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "os"
    "os/exec"
    "path/filepath"
    "strings"
    "bytes"
)

type Object struct {
    Id         string
    Properties map[string]Property
}

type Property struct {
    Value       string
    IsVerb      bool
    Interpreter string
}

func LoadObject(id string) (*Object, error) {
    obj := &Object{Id: id, Properties: make(map[string]Property)}
    
    // Load properties
    propDir := filepath.Join("objects", id, "properties")
    files, err := ioutil.ReadDir(propDir)
    if err != nil {
        return nil, err
    }
    
    for _, file := range files {
        if !file.IsDir() && strings.HasSuffix(file.Name(), ".prop") {
            propName := strings.TrimSuffix(file.Name(), ".prop")
            content, err := ioutil.ReadFile(filepath.Join(propDir, file.Name()))
            if err != nil {
                return nil, err
            }
            obj.Properties[propName] = Property{Value: string(content)}
        }
    }
    
    // Load verbs
    verbDir := filepath.Join("objects", id, "verbs")
    verbFolders, err := ioutil.ReadDir(verbDir)
    if err != nil {
        return nil, err
    }
    
    for _, folder := range verbFolders {
        if folder.IsDir() {
            verbName := folder.Name()
            if err := obj.LoadVerb(verbName); err != nil {
                return nil, err
            }
        }
    }
    
    return obj, nil
}

func (o *Object) SaveProperty(name string, value string) error {
    o.Properties[name] = Property{Value: value}
    propDir := filepath.Join("objects", o.Id, "properties")
    if err := os.MkdirAll(propDir, 0755); err != nil {
        return err
    }
    return ioutil.WriteFile(filepath.Join(propDir, name+".prop"), []byte(value), 0644)
}

func (o *Object) LoadVerb(verbName string) error {
    verbDir := filepath.Join("objects", o.Id, "verbs", verbName)
    content, err := ioutil.ReadFile(filepath.Join(verbDir, "main.verb"))
    if err != nil {
        return err
    }
    
    metadataContent, err := ioutil.ReadFile(filepath.Join(verbDir, "metadata.json"))
    if err != nil {
        return err
    }
    
    var metadata struct {
        Interpreter string `json:"interpreter"`
    }
    if err := json.Unmarshal(metadataContent, &metadata); err != nil {
        return err
    }
    
    o.Properties[verbName] = Property{
        Value:       string(content),
        IsVerb:      true,
        Interpreter: metadata.Interpreter,
    }
    
    return nil
}

func (o *Object) SaveVerb(verbName string, content string, interpreter string) error {
    verbDir := filepath.Join("objects", o.Id, "verbs", verbName)
    if err := os.MkdirAll(verbDir, 0755); err != nil {
        return err
    }
    
    if err := ioutil.WriteFile(filepath.Join(verbDir, "main.verb"), []byte(content), 0644); err != nil {
        return err
    }
    
    metadata := struct {
        Interpreter string `json:"interpreter"`
    }{
        Interpreter: interpreter,
    }
    metadataContent, err := json.Marshal(metadata)
    if err != nil {
        return err
    }
    
    return ioutil.WriteFile(filepath.Join(verbDir, "metadata.json"), metadataContent, 0644)
}


func (o *Object) ExecuteVerb(verbName string, args ...string) (map[string]interface{}, error) {
    verb, exists := o.Properties[verbName]
    if !exists || !verb.IsVerb {
        return nil, fmt.Errorf("Verb %s not found in object %v", verbName, o.Id)
    }

    // Prepare JSON input for the script
    input := map[string]interface{}{
        "args": args,
        "object_id": o.Id,
    }
    inputJSON, err := json.Marshal(input)
    if err != nil {
        return nil, fmt.Errorf("Error preparing input JSON: %v", err)
    }

    // Write the script to a temporary file
    tempScript := filepath.Join(os.TempDir(), fmt.Sprintf("%s_%s.tmp", o.Id, verbName))
    err = ioutil.WriteFile(tempScript, []byte(verb.Value), 0644)
    if err != nil {
        return nil, fmt.Errorf("Error writing temporary script: %v", err)
    }
    defer os.Remove(tempScript)

    // Set up command execution based on interpreter
    var cmd *exec.Cmd
    switch verb.Interpreter {
    case "python3":
        cmd = exec.Command("python3", tempScript)
    case "sbcl":
        cmd = exec.Command("sbcl", "--script", tempScript)
    default:
        return nil, fmt.Errorf("Unsupported interpreter: %s", verb.Interpreter)
    }

    // Set environment and pass JSON input
    cmd.Env = append(os.Environ(), fmt.Sprintf("MOO_OBJECT_ID=%s", o.Id))
    cmd.Stdin = bytes.NewReader(inputJSON)

    // Capture output
    var output bytes.Buffer
    cmd.Stdout = &output
    if err := cmd.Run(); err != nil {
        return nil, fmt.Errorf("Error executing verb: %v\nOutput: %s", err, output.String())
    }

    // Parse the output JSON
    var result map[string]interface{}
    if err := json.Unmarshal(output.Bytes(), &result); err != nil {
        return nil, fmt.Errorf("Error parsing output JSON: %v, output was: %s", err, output.String())
    }

    return result, nil
}


