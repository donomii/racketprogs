// Copyright 2014 The go-gl Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Renders a textured spinning cube using GLFW 3 and OpenGL 4.1 core forward-compatible profile.
package main

import (
	"fmt"
	"go/build"
	"image"
	"image/draw"
	_ "image/png"
	"log"
	"os"
	"runtime"
	"strings"
	"time"
	"math/rand"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

const windowWidth = 800
const windowHeight = 600

var azimuth = float32(0)
var elevation = float32(0)

func tween(variable *float32, a, b float32, t float32) {
	start := time.Now()
	*variable = a
	for {
		elapsed_ms := time.Since(start).Milliseconds()
		if float32(elapsed_ms) > t {
			break
		}
		*variable = a + (b-a)*float32(elapsed_ms)/t
		time.Sleep(1 * time.Millisecond)
	}
	*variable = b
}

// Moves the existing value to b in t milliseconds
func tweenTo(variable *float32, b float32, t float32) {
	a := *variable
	tween(variable, a, b, t)
}

func animate() {
	tween(&azimuth, 0, 3.1415927/2, 5000)  //Left side of model
	tweenTo(&azimuth, -3.1415927/2, 10000) //Right side of model
	go tweenTo(&azimuth, 0, 1000)          //Front
	tweenTo(&elevation, 0, 1000)
	tweenTo(&elevation, 3.1415927/2, 5000) //Top
	go tweenTo(&azimuth, 0, 1000)
	tweenTo(&elevation, 0, 1000)
	tweenTo(&elevation, -3.1415927/2, 5000) //Bottom
	go tweenTo(&azimuth, 0, 1000)
	tweenTo(&elevation, 0, 1000)
	tweenTo(&azimuth, 3.1415927, 5000) //Back
}

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

type View struct {
	Azimuth   float32
	Elevation float32
	Distance  float32
	Name      string
}

func main() {
	edge := 20
	window, rv := InitGraphics(edge)
	CameraViews := []View{
		{Azimuth: 0, Elevation: 0, Distance: 0, Name: "Front"},
		{Azimuth: 0, Elevation: 3.1415927 / 2, Distance: 0, Name: "Top"},
		{Azimuth: 0, Elevation: -3.1415927 / 2, Distance: 0, Name: "Bottom"},
		{Azimuth: 3.1415927, Elevation: 0, Distance: 0, Name: "Back"},
		{Azimuth: 3.1415927 / 2, Elevation: 0, Distance: 0, Name: "Left"},
		{Azimuth: -3.1415927 / 2, Elevation: 0, Distance: 0, Name: "Right"},
	}

	//go animate()
	texture, _ := NewTexture("square.png")
	for !window.ShouldClose() {
		//log.Println("Start loop")
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		//log.Println("Updating")
		//time.Sleep(1 * time.Second)
		// Update
		//		time := glfw.GetTime()

		//log.Println("Rendering")

		imageX := int32(200)
		imageY := int32(200)

		if false {
			gl.UseProgram(rv.Program)
			gl.Viewport(imageX*int32(0), int32(0)*imageY, imageX, imageY)
			cameraVec := mgl32.Vec3{0, 0, -10}
			cameraVec = mgl32.Rotate3DY(float32(azimuth)).Mul3x1(cameraVec)
			cameraVec = mgl32.Rotate3DX(float32(elevation)).Mul3x1(cameraVec)
			camera := mgl32.LookAtV(cameraVec, mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 1, 0})
			cameraUniform := gl.GetUniformLocation(rv.Program, gl.Str("camera\x00"))

			gl.UniformMatrix4fv(cameraUniform, 1, false, &camera[0])

			

			gl.BindVertexArray(rv.Vao)

			gl.ActiveTexture(gl.TEXTURE0)
			gl.BindTexture(gl.TEXTURE_2D, texture)

			gl.DrawArrays(gl.TRIANGLES, 0, 6*2*3*int32(edge*edge*edge))
		} else {

			//Repeat in six viewports
			for vi := 0; vi < 3; vi++ {
				for vj := 0; vj < 2; vj++ {
					viewNumber := vi*2 + vj
					view := CameraViews[viewNumber]
					azimuth = view.Azimuth
					elevation = view.Elevation

					gl.Viewport(imageX*int32(vi), int32(vj)*imageY, imageX, imageY)
					cameraVec := mgl32.Vec3{0, 0, -4}
					cameraVec = mgl32.Rotate3DY(float32(azimuth)).Mul3x1(cameraVec)
					cameraVec = mgl32.Rotate3DX(float32(elevation)).Mul3x1(cameraVec)
					camera := mgl32.LookAtV(cameraVec, mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 1, 0})
					cameraUniform := gl.GetUniformLocation(rv.Program, gl.Str("camera\x00"))

					gl.UniformMatrix4fv(cameraUniform, 1, false, &camera[0])

					// Render

					gl.UseProgram(rv.Program)

					gl.BindVertexArray(rv.Vao)

					gl.ActiveTexture(gl.TEXTURE0)
					gl.BindTexture(gl.TEXTURE_2D, texture)

					gl.DrawArrays(gl.TRIANGLES, 0, 6*2*3*int32(edge*edge*edge))

				}
			}
		}
		//log.Println("Swapping")
		// Maintenance
		window.SwapBuffers()
		glfw.PollEvents()
		//log.Println("End loop")
	}

}

func makeRandomColourArray(edge int) []float32 {

	//Make a random colour array
	colours := make([]float32, 6*2*3*edge*edge*edge*4)
	for i := 0; i < 6*2*3*edge*edge*edge*4; i++ {
		colours[i] = rand.Float32()
	}
	return colours
}

func InitGraphics(edge int) (*glfw.Window, RenderVars) {
	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	//defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(windowWidth, windowHeight, "Cube", nil, nil)
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	// Configure the vertex and fragment shaders
	program, err := newProgram(vertexShader, fragmentShader)
	if err != nil {
		panic(err)
	}

	gl.UseProgram(program)

	projection := mgl32.Perspective(mgl32.DegToRad(45.0), float32(windowWidth)/windowHeight, 0.001, 10000.0)
	projectionUniform := gl.GetUniformLocation(program, gl.Str("projection\x00"))
	gl.UniformMatrix4fv(projectionUniform, 1, false, &projection[0])

	camera := mgl32.LookAtV(mgl32.Vec3{3, 3, 3}, mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 1, 0})
	cameraUniform := gl.GetUniformLocation(program, gl.Str("camera\x00"))
	gl.UniformMatrix4fv(cameraUniform, 1, false, &camera[0])

	textureUniform := gl.GetUniformLocation(program, gl.Str("tex\x00"))
	gl.Uniform1i(textureUniform, 0)

	
			indentModel := mgl32.Ident4()
			modelUniform := gl.GetUniformLocation(program, gl.Str("model\x00"))
			gl.UniformMatrix4fv(modelUniform, 1, false, &indentModel[0])

	gl.BindFragDataLocation(program, 0, gl.Str("outputColor\x00"))

	// Load the texture

	// Configure the vertex data
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	cubeArrayLength := len(cubeVertices) //The number of floats needed for one cube
	totalModelLength := cubeArrayLength * edge * edge * edge
	totalModelArray := make([]float32, totalModelLength) //This will hold all the cubes for all the voxels in the scene
	offset := float32(int(float64(edge/2) - 0.000001))
	for i := 0; i < edge; i++ {
		for j := 0; j < edge; j++ {
			for k := 0; k < edge; k++ {
				
				model := mgl32.Ident4()
				model = mgl32.Scale3D(0.5/offset, 0.5/offset, 0.5/offset).Mul4(model)
				//Randomise seed
				//rand.Seed(int64(i*edge*edge + j*edge + k))
				//Make a random vector in the cube -1 to 1
				//randVec := mgl32.Vec3{rand.Float32()*2 - 1, rand.Float32()*2 - 1, rand.Float32()*2 - 1}

				//model = mgl32.Translate3D(randVec[0], randVec[1], randVec[2]).Mul4(model)

				model = mgl32.Translate3D(float32(i)/float32(edge)*2-1, float32(j)/offset-1, float32(k)/offset-1).Mul4(model)
				
				for l := 0; l < cubeArrayLength; l = l + 5 {
		
					voxvec := mgl32.Vec3{cubeVertices[l], cubeVertices[l+1], cubeVertices[l+2]}
					pos := model.Mul4x1(voxvec.Vec4(1))
					arrPos :=i*edge*edge*cubeArrayLength+j*edge*cubeArrayLength+k*cubeArrayLength

					totalModelArray[arrPos+l] =  pos.X()
					totalModelArray[arrPos+l+1] = pos.Y()
					totalModelArray[i*edge*edge*cubeArrayLength+j*edge*cubeArrayLength+k*cubeArrayLength+l+2] = pos.Z()
					totalModelArray[i*edge*edge*cubeArrayLength+j*edge*cubeArrayLength+k*cubeArrayLength+l+3] = cubeVertices[l+3]
					totalModelArray[i*edge*edge*cubeArrayLength+j*edge*cubeArrayLength+k*cubeArrayLength+l+4] = cubeVertices[l+4]

				}
				//Copy the cube into the model array
				//copy(totalModelArray[i*edge*edge*cubeArrayLength+j*edge*cubeArrayLength+k*cubeArrayLength:], cubeFloats)
			}
		}
	}

	//This holds the vertices
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, totalModelLength*4, gl.Ptr(totalModelArray), gl.STATIC_DRAW)

	vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(vertAttrib)
	gl.VertexAttribPointer(vertAttrib, 3, gl.FLOAT, false, 5*4, gl.PtrOffset(0))

	//The texture coordinates (where to look in the texture for each vertex)
	texCoordAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vertTexCoord\x00")))
	gl.EnableVertexAttribArray(texCoordAttrib)
	gl.VertexAttribPointer(texCoordAttrib, 2, gl.FLOAT, false, 5*4, gl.PtrOffset(3*4))


	randomColours := makeRandomColourArray(edge)
	//This holds the colours
	var vboColour uint32
	gl.GenBuffers(1, &vboColour)
	gl.BindBuffer(gl.ARRAY_BUFFER, vboColour)
	gl.BufferData(gl.ARRAY_BUFFER, len(randomColours)*4, gl.Ptr(randomColours), gl.STATIC_DRAW)

	vertColourAttrib := uint32(gl.GetAttribLocation(program, gl.Str("colours\x00")))
	gl.EnableVertexAttribArray(vertColourAttrib)
	gl.VertexAttribPointer(vertColourAttrib, 4, gl.FLOAT, false, 0, gl.PtrOffset(0))

	// Configure global settings
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)
	gl.ClearColor(1.0, 1.0, 1.0, 1.0)

	rv := RenderVars{vao, vbo, vertAttrib, program, mgl32.Ident4()}
	return window, rv
}

func ShutdownGraphics() {
	glfw.Terminate()
}

func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

func NewTexture(file string) (uint32, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return 0, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return 0, err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))

	return texture, nil
}

var vertexShader = `
#version 330

uniform mat4 projection;
uniform mat4 camera;
uniform mat4 model;
uniform vec3 aOffset;

in vec3 vert;
in vec2 vertTexCoord;
in vec4 colours;

out vec2 fragTexCoord;
out vec4 fragColour;

void main() {
    fragTexCoord = vertTexCoord;
	fragColour = colours;
    gl_Position = projection * camera * model * vec4(vert, 1);
}
` + "\x00"


var vertexShaderVoxel = `
#version 410
uniform mat4 mvp;

attribute lowp vec3 vColor;
attribute vec3 vPos;
attribute int vEnabledFaces;

flat out lowp vec3 gColor;
flat out int gEnabledFaces;

void main() {
    gl_Position = mvp * vec4(vPos, 1.0);
    
    gColor = vColor;
    gEnabledFaces = vEnabledFaces;
}
` + "\x00"


var fragmentShader = `
#version 330

uniform sampler2D tex;

in vec2 fragTexCoord;
in vec4 fragColour;

out vec4 outputColor;

void main() {
    //outputColor = texture(tex, fragTexCoord);
	outputColor = fragColour;
}
` + "\x00"

var cubeVertices = []float32{
	//  X, Y, Z, U, V
	// Bottom
	-1.0, -1.0, -1.0, 0.0, 0.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 1.0, 1.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,

	// Top
	-1.0, 1.0, -1.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 1.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0,
	1.0, 1.0, 1.0, 1.0, 1.0,

	// Front
	-1.0, -1.0, 1.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,
	1.0, -1.0, 1.0, 0.0, 0.0,
	1.0, 1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,

	// Back
	-1.0, -1.0, -1.0, 0.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 1.0, 1.0,

	// Left
	-1.0, -1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, -1.0, 1.0, 0.0,
	-1.0, -1.0, -1.0, 0.0, 0.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,
	-1.0, 1.0, -1.0, 1.0, 0.0,

	// Right
	1.0, -1.0, 1.0, 1.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 0.0, 0.0,
	1.0, -1.0, 1.0, 1.0, 1.0,
	1.0, 1.0, -1.0, 0.0, 0.0,
	1.0, 1.0, 1.0, 0.0, 1.0,
}

// Set the working directory to the root of Go package, so that its assets can be accessed.
func init() {
	/*
		dir, err := importPathToDir("github.com/go-gl/example/gl41core-cube")
		if err != nil {
			log.Fatalln("Unable to find Go package in your GOPATH, it's needed to load assets:", err)
		}
		err = os.Chdir(dir)
		if err != nil {
			log.Panicln("os.Chdir:", err)
		}
	*/
}

// importPathToDir resolves the absolute path from importPath.
// There doesn't need to be a valid Go package inside that import path,
// but the directory must exist.
func importPathToDir(importPath string) (string, error) {
	p, err := build.Import(importPath, "", build.FindOnly)
	if err != nil {
		return "", err
	}
	return p.Dir, nil
}
