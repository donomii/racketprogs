package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/godbus/dbus/v5"
	"gitlab.com/donomii/chim"
)

// DBusComponent manages DBus connections and state
type DBusComponent struct {
    id           string
    node         *ComponentNode
    connections  map[string]*dbus.Conn      // Map of bus type to connection
    status       map[string]string          // Connection status
}

type dbusState struct {
}


func NewDBusComponent(id string, node *ComponentNode) Component {
    comp := &DBusComponent{
        id:           id,
        node:         node,
        connections:  make(map[string]*dbus.Conn),
        status:       make(map[string]string),
    }

	state := &cliState{}

    
    // Try to connect to system bus
    if conn, err := dbus.ConnectSystemBus(); err != nil {
        comp.status["system"] = fmt.Sprintf("error: %v", err)
        debugf("DBus system bus connection failed: %v\n", err)
    } else {
        comp.connections["system"] = conn
        comp.status["system"] = "connected"
        debugf("DBus system bus connected successfully\n")
    }
    
    // Try to connect to session bus
    if conn, err := dbus.ConnectSessionBus(); err != nil {
        comp.status["session"] = fmt.Sprintf("error: %v", err)
        debugf("DBus session bus connection failed: %v\n", err)
    } else {
        comp.connections["session"] = conn
        comp.status["session"] = "connected"
        debugf("DBus session bus connected successfully\n")
    }

	marshalState := func() (string, error) {
		bytes, err := json.Marshal(state)
		return string(bytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
        cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
        panicOnError(err)
		return doProcessCmd(cmd, env, comp)
	}

	var commands = []CommandInfo{
		{Name: "call", Args: []string{"bus", "service", "path", "interface.method", "args..."}, Description: "Call a DBus method"},
		{Name: "status", Args: []string{}, Description: "Get DBus connection status"},
	}
    
    return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

func (dc *DBusComponent) GetID() string {
    return dc.id
}

func (dc *DBusComponent) GetState() string {
    state := struct {
        Status     map[string]string      `json:"status"`
    }{
        Status:     dc.status,
    }
    bytes, _ := json.Marshal(state)
    return string(bytes)
}


func doProcessCmd(cmd Command, env chim.ScriptEnv, dc *DBusComponent) (string, chim.ScriptEnv, error) {

	var result string
 
	switch cmd.Name {
	case "call":
		if len(cmd.Args) < 4 {
			return "", env, fmt.Errorf("call requires: bus service path interface.method [args...]")
		}
 
		busType := cmd.Args[0]
		service := cmd.Args[1]
		path := cmd.Args[2]
		methodFull := cmd.Args[3]
 
		// The interface name includes everything up to the last dot
		lastDot := strings.LastIndex(methodFull, ".")
		if lastDot == -1 {
			return "", env, fmt.Errorf("method must be in format interface.method")
		}
 
		interface_ := methodFull[:lastDot]
		method := methodFull[lastDot+1:]
 
		conn, ok := dc.connections[busType]
		if !ok {
			return "", env, fmt.Errorf("not connected to %s bus", busType)
		}
 
		// Only use the actual arguments, not the next script line
		args := make([]interface{}, len(cmd.Args)-4)
		for i, arg := range cmd.Args[4:] {
			args[i] = arg
		}
 
		debugf("DBus calling method '%s' on interface '%s' at path '%s' with args %v\n",
			method, interface_, path, args)
 
		obj := conn.Object(service, dbus.ObjectPath(path))
		call := obj.Call(methodFull, 0, args...)
		if call.Err != nil {
			return  "", env, call.Err
		}
 
		if err := call.Store(&result); err != nil {
			return "", env, err
		}
		return result, env, nil
 
	case "status":
		result = fmt.Sprintf("system: %s, session: %s", dc.status["system"], dc.status["session"])
 
	default:
		return "", env, fmt.Errorf("unknown dbus command: %s", cmd.Name)
	}


	return result, env,  nil
 }