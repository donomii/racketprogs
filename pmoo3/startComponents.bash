IP=192.168.11.32
REGISTRY=192.168.11.32
go get golang.org/x/oauth2
go get github.com/google/go-github/v42/github
go get github.com/emersion/go-imap/client
go run . --registry --url $IP:8080 & 
koi8rxterm -e "go run . --display  --url $IP:8081  -registryurl $IP:8080" &
go run . --events --url $IP:8082  -registryurl $REGISTRY:8080 &
go run . --inventory --url $IP:8083  -registryurl $REGISTRY:8080 &
go run . --location --url $IP:8084  -registryurl $REGISTRY:8080 &
go run . --dbus --url $IP:8085  -registryurl $REGISTRY:8080 &
go run . --script --url $IP:8087  -registryurl $REGISTRY:8080  &
go run . --github --url $IP:8088  -registryurl $REGISTRY:8080 &
go run . --email --url $IP:8089  -registryurl $REGISTRY:8080 &
go run . --cli --url $IP:8086  -registryurl $REGISTRY:8080 

wait
