package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/donomii/chim"
)

// Test giving an item to a player
func TestGiveItem() {
    env, err := NewTestEnvironment()
	panicOnError(err)

    // Now test that giving an item produces expected display message
    script := []string{
		"display: message 🚀 Starting TestGiveItem",
        "inventory: given = insert player_1 sword",
        "display: message Item added: $given",
		"inventory: given = has player_1 sword",
		"display: message Item found: $given",
		"script: if $given then",
		"display: message ✅ PASSED TestGiveItem",
		"script: else",
		"display: message ❌ FAILED TestGiveItem",
		"script: end",
    }

	if err := env.runScript(script, nil); err != nil {
		panicOnError(err)
	}

}

// Test complex sequence with multiple components
func TestComplexSequence()  {
	env, err := NewTestEnvironment()
	panicOnError(err)

    script := []string{
		"display: message 🚀 Starting TestComplexSequence",
        "location: room = create_room Dragon's Lair",
        "inventory: sword = insert player_1 sword",
        "location: result = move player_1 $room",
        "display: message Player moved to $result with $sword",
    }

	err = env.runScript(script, nil)
	panicOnError(	err)

}

// Test conditional script execution
func TestConditionals()  {
	env, err := NewTestEnvironment()
    panicOnError(	err)

	script := []string{
		"display: message 🚀 Starting TestConditionals",
		"location: room1 = create_room Tavern",
		"script: if true then",
		"location: room2 = create_room Library",
        "script: else",
		"script: end",
		"script: if false then",
		"location: room3 = create_room Kitchen",
        "script: else",
		"script: end",

		"location: tavern = exists Tavern",
		"script: if $tavern then",
		"display: message ✅ PASSED CreateTavern",
		"script: else",
		"display: message ❌ FAILED CreateTavern",
		"script: end",

		"location: library = exists Library",
		"script: if $library then",
		"display: message ✅ PASSED CreateLibrary",
		"script: else",
		"display: message ❌ FAILED CreateLibrary",
		"script: end",

		"location: kitchen = exists Kitchen",
		"script: if $kitchen then",
		"display: message ❌ FAILED CreateKitchen - wrong branch taken",
		"script: else",
		"display: message ✅ PASSED CreateKitchen",
		"script: end",

	}

	err = env.runScript(script, nil)
		panicOnError(	err)

}


func TestBasicMovement() {
	env, err := NewTestEnvironment()
	panicOnError(err)

	script := []string{
		"display: message 🚀 Starting TestBasicMovement",
		"location: r1 = create_room A_cozy_tavern",
		"location: moved = move player_1 $r1",
		"location: r2 = create_room A_dark_alley",
		"location: moved = move player_1 $r2",
		"location: r3 = create_room A_dusty_library",
		"location: moved = move player_1 $r3",
		"location: r4 = create_room A_mysterious_cave",
		"location: moved = move player_1 $r4",
		"location: r5 = create_room A_sunny_field",
		"location: moved = move player_1 $r5",
		"location: r6 = create_room A_gloomy_forest",
		"location: moved = move player_1 $r6",
		"location: moved = move player_1 $r3",
		"location: p1loc = get_location player_1",
		"script: x = compare $p1loc == $r3",
		"script: if $x then",
		"display: message ✅ PASSED TestBasicMovement",
		"script: else",
		"display: message Room is $current",
		"display: message ❌ FAILED TestBasicMovement",
		"script: end",
	}

	err = env.runScript(script, nil)
	panicOnError(err)
}

func TestEvents() {
	env, err := NewTestEnvironment()
	panicOnError(err)

	// Test script
	script := []string{
		"display: message 🚀 Starting TestEvents",
		// Add a handler
		`events: add_handler "room_enter" "greeter" "display: ✅ PASSED TestEvents"`,
		// Trigger the event
		`events: trigger "room_enter" "room" "tavern"`,
	}

	err = env.runScript(script, nil)
	panicOnError(err)
}

func TestComplexEvents() {
	env, err := NewTestEnvironment()
	panicOnError(err)

	// Test script with multiple handlers and chained events
	script := []string{
		"display: message 🚀 Starting TestComplexEvents",
		// Combat start handlers
		`events: add_handler "combat_start" "announce" "display: channel_message combat Combat started between $attacker and $defender !"`,
		`events: add_handler "combat_start" "buff" "events: trigger stat_change entity $attacker stat attack change +2"`,

		// Stat change handlers
		`events: add_handler "stat_change" "announce" "display: message ✅ PASSED TestEventChain"`,
		`events: add_handler "stat_change" "log" "display: channel_message system Recording $stat change for $entity"`,

		// Combat end handlers
		`events: add_handler "combat_end" "announce" "display: channel_message combat Combat ended! $winner wins!"`,
		`events: add_handler "combat_end" "cleanup" "events: trigger stat_change entity $attacker stat attack change -2"`,

		// Trigger the chain of events
		`events: trigger "combat_start" "attacker" "Dragon" "defender" "Knight"`,
		`events: trigger "combat_end" "winner" "Dragon" "attacker" "Dragon"`,
	}

	err = env.runScript(script, nil)
	panicOnError(err)

}

// Run all tests
func RunTests() {
	TestGiveItem()
	TestComplexSequence()
	//TestConditionals() //FIXME
	TestBasicMovement()
	TestEvents()
	TestComplexEvents()


}

// TestEnvironment manages test components and their HTTP servers
type TestEnvironment struct {
	registryURL string
	components  map[string]string // component ID -> URL
}

func NewTestEnvironment() (*TestEnvironment, error) {
	env := &TestEnvironment{
		registryURL: "localhost:8080",
		components:  make(map[string]string),
	}

	// Start registry
	go startComponent("registry", "localhost:8080", "")

	// Define test components
	testComponents := []struct {
		id   string
		port string
	}{
		{"inventory", "8081"},
		{"location", "8082"},
		{"display", "8083"},
		{"events", "8084"},
		{"script", "8085"},
	}

	// Start each component
	for _, comp := range testComponents {
		url := fmt.Sprintf("localhost:%s", comp.port)
		env.components[comp.id] = url
		go startComponent(comp.id, url, env.registryURL)
	}

	// Wait for components to start and register
	time.Sleep(time.Second)

	// Verify all components are registered
	for id, url := range env.components {
		if err := env.verifyComponent(id, url); err != nil {
			return nil, fmt.Errorf("component %s failed to start: %v", id, err)
		}
	}

	return env, nil
}

func (env *TestEnvironment) verifyComponent(id, url string) error {
	// Query registry to verify component is registered
	resp, err := http.Get(fmt.Sprintf("http://%s/lookup?id=%s", env.registryURL, id))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("component not registered")
	}

	var result struct {
		Address string `json:"address"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return err
	}

	if result.Address != url {
		return fmt.Errorf("wrong address registered")
	}

	return nil
}

func (env *TestEnvironment) runScript(lines []string, initialVars map[string]string) error {
	// Create initial message
	msg := chim.NewScript(lines)
	if initialVars != nil {
		msg.Variables = initialVars
	}


	firstComponent := "script"

	// Forward to first component
	msgJSON, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	url := env.components[firstComponent]
    resp, err := http.Post(
        fmt.Sprintf("http://%s/message", url),
        "application/json",
        bytes.NewReader(msgJSON),
    )
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        // Read the error message from response body
        errorBody, _ := io.ReadAll(resp.Body)
        return fmt.Errorf("script execution failed: %s - %s", resp.Status, string(errorBody))
    }

	// Wait for script to complete
	time.Sleep(time.Millisecond * 100 * time.Duration(len(lines)))
	return nil
}
