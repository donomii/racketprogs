package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/donomii/chim"
	"strings"
)

var debugMode bool
var verboseMode bool

// Helper function to parse fields from a string, respecting quotes
func parseFields(s string) []string {
	var fields []string
	var current strings.Builder
	inQuotes := false

	for _, r := range s {
		switch {
		case r == '"':
			inQuotes = !inQuotes
		case r == ' ' && !inQuotes:
			if current.Len() > 0 {
				fields = append(fields, current.String())
				current.Reset()
			}
		default:
			current.WriteRune(r)
		}
	}

	if current.Len() > 0 {
		fields = append(fields, current.String())
	}

	return fields
}

// Common message processing for all components
func ProcessComponentMessage(rawJSON []byte, componentID string,
	processCommand func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error)) ([]byte, string, error) {

	// Deserialize incoming message
	var env chim.ScriptEnv
	err := json.Unmarshal(rawJSON, &env)
	panicOnError(err)

	if env.CurrentLine >= len(env.Lines) {
		return rawJSON, "", nil // End of script
	}
	nextComponent := componentID

	for nextComponent == componentID {
		env, nextComponent, err = chim.ScriptStep(componentID, env, processCommand)
		if err != nil {
            // More detailed error reporting
            line := "unknown"
            if env.CurrentLine < len(env.Lines) {
                line = env.Lines[env.CurrentLine]
            }
            return nil, "", fmt.Errorf("error processing line %d (%s): %w", 
                env.CurrentLine, line, err)
        }
	}

	// Serialize response
	response, err := json.Marshal(env)
	panicOnError(err)

	debugf("Response: %s\n", response)

	return response, nextComponent, nil
}

func debugf(format string, args ...interface{}) {
	if debugMode {
		fmt.Printf(format, args...)
	}
}

func verbosef(format string, args ...interface{}) {
	if verboseMode {
		fmt.Printf(format, args...)
	}
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
