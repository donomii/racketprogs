package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/smtp"
	"strings"
	"time"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	"gitlab.com/donomii/chim"
)

// Common email providers' server settings
var emailProviders = map[string]struct {
	IMAPServer string
	IMAPPort   int
	SMTPServer string
	SMTPPort   int
}{
	"gmail": {
		IMAPServer: "imap.gmail.com",
		IMAPPort:   993,
		SMTPServer: "smtp.gmail.com",
		SMTPPort:   587,
	},
	"outlook": {
		IMAPServer: "outlook.office365.com",
		IMAPPort:   993,
		SMTPServer: "smtp.office365.com",
		SMTPPort:   587,
	},
	"yahoo": {
		IMAPServer: "imap.mail.yahoo.com",
		IMAPPort:   993,
		SMTPServer: "smtp.mail.yahoo.com",
		SMTPPort:   587,
	},
	"aol": {
		IMAPServer: "imap.aol.com",
		IMAPPort:   993,
		SMTPServer: "smtp.aol.com",
		SMTPPort:   587,
	},
}

type emailState struct {
	IMAPServer  string                  `json:"imap_server,omitempty"`
	IMAPPort    int                     `json:"imap_port,omitempty"`
	SMTPServer  string                  `json:"smtp_server,omitempty"`
	SMTPPort    int                     `json:"smtp_port,omitempty"`
	Username    string                  `json:"username,omitempty"`
	Password    string                  `json:"password,omitempty"`
	LastChecked time.Time               `json:"last_checked,omitempty"`
	Emails      []EmailSummary          `json:"emails,omitempty"` // Cache of recent emails
	EmailCache  map[string]*EmailDetail `json:"email_cache,omitempty"` // Cache of full emails
}

type EmailSummary struct {
	UID         uint32    `json:"uid"`
	Date        time.Time `json:"date"`
	From        string    `json:"from"`
	To          []string  `json:"to"`
	Subject     string    `json:"subject"`
	Preview     string    `json:"preview"`
	HasAttach   bool      `json:"has_attachments"`
	Size        uint32    `json:"size"`
}

type EmailDetail struct {
	UID         uint32    `json:"uid"`
	Date        time.Time `json:"date"`
	From        string    `json:"from"`
	To          []string  `json:"to"`
	Subject     string    `json:"subject"`
	Body        string    `json:"body"`
	HTMLBody    string    `json:"html_body,omitempty"`
	Attachments []string  `json:"attachments"`
}

// NewEmailComponent creates an email component that safely interacts with email
// IMPORTANT SAFETY FEATURES:
// 1. NEVER deletes emails - all operations are read-only
// 2. NEVER marks emails as read - uses PEEK mode for all operations
// 3. ONLY fetches the latest 5 emails to prevent overloading
func NewEmailComponent(id string, node *ComponentNode) Component {
	state := &emailState{
		
		Emails:      make([]EmailSummary, 0),
		EmailCache:  make(map[string]*EmailDetail),
		IMAPPort:    993, // Default IMAP port
		SMTPPort:    587, // Default SMTP port
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
		panicOnError(err)
		switch cmd.Name {
		case "provider":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: email:provider PROVIDER_NAME USERNAME PASSWORD\nAvailable providers: gmail, outlook, yahoo, aol")
			}

			providerName := strings.ToLower(cmd.Args[0])
			provider, exists := emailProviders[providerName]
			if !exists {
				return "", env, fmt.Errorf("Unknown email provider: %s\nAvailable providers: gmail, outlook, yahoo, aol", providerName)
			}

			state.Username = cmd.Args[1]
			state.Password = cmd.Args[2]
			state.IMAPServer = provider.IMAPServer
			state.IMAPPort = provider.IMAPPort
			state.SMTPServer = provider.SMTPServer
			state.SMTPPort = provider.SMTPPort

			// Test IMAP connection
			imapClient, err := connectIMAP(state)
			if err != nil {
				return "", env, fmt.Errorf("Failed to connect to %s IMAP server: %v", providerName, err)
			}
			imapClient.Logout()

			return fmt.Sprintf("Configured email for %s using %s", state.Username, providerName), env, nil

		case "setup_imap":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: email:setup_imap SERVER USERNAME PASSWORD [PORT]\nDefault port is 993")
			}

			state.IMAPServer = cmd.Args[0]
			state.Username = cmd.Args[1]
			state.Password = cmd.Args[2]

			// Optional port
			if len(cmd.Args) > 3 {
				port, err := parseIntWithDefault(cmd.Args[3], 993)
				if err != nil {
					return "", env, fmt.Errorf("Invalid IMAP port: %v", err)
				}
				state.IMAPPort = port
			} else {
				state.IMAPPort = 993 // Standard IMAPS port
			}

			// Test IMAP connection
			imapClient, err := connectIMAP(state)
			if err != nil {
				return "", env, fmt.Errorf("Failed to connect to IMAP server: %v", err)
			}
			imapClient.Logout()

			return fmt.Sprintf("IMAP configured for %s@%s:%d", state.Username, state.IMAPServer, state.IMAPPort), env, nil

		case "setup_smtp":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: email:setup_smtp SERVER USERNAME PASSWORD [PORT]\nDefault port is 587")
			}

			state.SMTPServer = cmd.Args[0]
			
			// Username and password - only set if not already set by IMAP
			if state.Username == "" {
				state.Username = cmd.Args[1]
			}
			if state.Password == "" {
				state.Password = cmd.Args[2]
			}

			// Optional port
			if len(cmd.Args) > 3 {
				port, err := parseIntWithDefault(cmd.Args[3], 587)
				if err != nil {
					return "", env, fmt.Errorf("Invalid SMTP port: %v", err)
				}
				state.SMTPPort = port
			} else {
				state.SMTPPort = 587 // Standard SMTP submission port
			}

			return fmt.Sprintf("SMTP configured for %s@%s:%d", state.Username, state.SMTPServer, state.SMTPPort), env, nil

		case "check":
			// Check configuration
			if state.Username == "" || state.Password == "" || state.IMAPServer == "" {
				return "", env, fmt.Errorf("Email not configured\nFix: Run 'email:provider' or 'email:setup_imap' first")
			}

			// Connect to IMAP and check email
			emails, err := fetchNewEmails(state)
			if err != nil {
				return "", env, fmt.Errorf("Failed to check emails: %v", err)
			}

			// Store the emails in state and last results
			state.Emails = emails
			state.LastChecked = time.Now()

			// Return number of emails found
			return fmt.Sprintf("%d", len(emails)), env, nil

		case "inbox":
			// Check configuration
			if state.Username == "" || state.Password == "" || state.IMAPServer == "" {
				return "", env, fmt.Errorf("Email not configured\nFix: Run 'email:provider' or 'email:setup_imap' first")
			}

			// If we haven't checked recently or don't have any emails, check now
			if len(state.Emails) == 0 || time.Since(state.LastChecked) > time.Minute*5 {
				emails, err := fetchNewEmails(state)
				if err != nil {
					return "", env, fmt.Errorf("Failed to fetch emails: %v", err)
				}
				state.Emails = emails
				state.LastChecked = time.Now()
			}

			// Only return the most recent 5 emails
			count := 5
			if len(state.Emails) < count {
				count = len(state.Emails)
			}

			result := state.Emails[:count]

			// Return as JSON
			resultJSON, _ := json.Marshal(result)
			return string(resultJSON), env, nil

		case "read":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("Missing email UID\nUsage: email:read UID")
			}

			// Check configuration
			if state.Username == "" || state.Password == "" || state.IMAPServer == "" {
				return "", env, fmt.Errorf("Email not configured\nFix: Run 'email:provider' or 'email:setup_imap' first")
			}

			// Check if email is already in cache
			if email, exists := state.EmailCache[cmd.Args[0]]; exists {
				// Return as JSON
				resultJSON, _ := json.Marshal(email)
				return string(resultJSON), env, nil
			}
			
			// Parse UID
			uid, err := parseIntWithDefault(cmd.Args[0], 0)
			if err != nil || uid == 0 {
				return "", env, fmt.Errorf("Invalid UID: %s", cmd.Args[0])
			}
			
			// Fetch the full email if not in cache
			email, err := readEmailByUID(state, uint32(uid))
			if err != nil {
				return "", env, fmt.Errorf("Failed to read email: %v", err)
			}

			// Cache it for future use
			state.EmailCache[cmd.Args[0]] = email

			// Return as JSON
			resultJSON, _ := json.Marshal(email)
			return string(resultJSON), env, nil

		case "send":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: email:send TO SUBJECT BODY")
			}

			// Check configuration
			if state.Username == "" || state.Password == "" || state.SMTPServer == "" {
				return "", env, fmt.Errorf("SMTP not configured\nFix: Run 'email:provider' or 'email:setup_smtp' first")
			}

			to := cmd.Args[0]
			subject := cmd.Args[1]
			body := strings.Join(cmd.Args[2:], " ")

			// Send via SMTP
			err := sendEmail(state, to, subject, body)
			if err != nil {
				return "", env, fmt.Errorf("Failed to send email: %v", err)
			}

			return "sent", env, nil

		case "status":
			// Return current configuration status
			status := map[string]interface{}{
				"imap_configured": state.IMAPServer != "",
				"smtp_configured": state.SMTPServer != "",
				"username":        state.Username,
				"last_checked":    state.LastChecked,
				"email_count":     len(state.Emails),
			}
			
			if state.IMAPServer != "" {
				status["imap_server"] = fmt.Sprintf("%s:%d", state.IMAPServer, state.IMAPPort)
			}
			
			if state.SMTPServer != "" {
				status["smtp_server"] = fmt.Sprintf("%s:%d", state.SMTPServer, state.SMTPPort)
			}
			
			resultJSON, _ := json.Marshal(status)
			return string(resultJSON), env, nil

		default:
			return "", env, fmt.Errorf("Unknown email command: '%s'\nAvailable commands: provider, setup_imap, setup_smtp, check, inbox, read, send, status", cmd.Name)
		}
	}

	marshalState := func() (string, error) {
		// Don't include password in logs/debug output
		stateCopy := *state
		stateCopy.Password = "[REDACTED]"
		_, err := json.Marshal(stateCopy)
		if err != nil {
			return "", err
		}

		// Now marshal the real state for storage
		realBytes, err := json.Marshal(state)
		return string(realBytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}

	var commands = []CommandInfo{
		{Name: "provider", Args: []string{"provider_name", "username", "password"}, Description: "Configure email using a known provider"},
		{Name: "setup_imap", Args: []string{"server", "username", "password", "port"}, Description: "Configure IMAP connection"},
		{Name: "setup_smtp", Args: []string{"server", "username", "password", "port"}, Description: "Configure SMTP connection"},
		{Name: "check", Args: []string{}, Description: "Check for new emails"},
		{Name: "inbox", Args: []string{}, Description: "List emails in inbox"},
		{Name: "read", Args: []string{"uid"}, Description: "Read a specific email by UID"},
		{Name: "send", Args: []string{"to", "subject", "body"}, Description: "Send an email"},
		{Name: "status", Args: []string{}, Description: "Get email configuration status"},
	}

	return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

// Parse an int with default value if parsing fails
func parseIntWithDefault(s string, defaultVal int) (int, error) {
	if s == "" {
		return defaultVal, nil
	}
	
	val := 0
	_, err := fmt.Sscanf(s, "%d", &val)
	if err != nil {
		return defaultVal, err
	}
	
	return val, nil
}

// Connect to the IMAP server
// SAFETY: This component is designed to NEVER delete or modify emails
// All operations use read-only mode and PEEK to prevent any changes
func connectIMAP(state *emailState) (*client.Client, error) {
	// Connect to server
	addr := fmt.Sprintf("%s:%d", state.IMAPServer, state.IMAPPort)
	c, err := client.DialTLS(addr, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to IMAP server: %v", err)
	}

	// Login
	if err := c.Login(state.Username, state.Password); err != nil {
		c.Logout()
		return nil, fmt.Errorf("failed to login: %v", err)
	}

	return c, nil
}

// verifyReadOnlySafety ensures we're operating in a safe, read-only mode
// that cannot delete or modify any emails
func verifyReadOnlySafety(c *client.Client) error {
	// The mailbox was already selected in read-only mode using:
	// c.Select("INBOX", true)
	// 
	// If that call succeeded, we're guaranteed to be in read-only mode
	// So this function is more of a documentation check point
	// to ensure we're following safe practices
	
	// Get current selected mailbox info
	mbox := c.Mailbox()
	if mbox == nil {
		c.Logout()
		return fmt.Errorf("SAFETY ERROR: no mailbox selected - operation aborted")
	}
	
	// Verify we're in read-only mode
	if !mbox.ReadOnly {
		// Force logout and abort if somehow we're not in read-only mode
		c.Logout()
		return fmt.Errorf("SAFETY ERROR: mailbox not in read-only mode - operation aborted to prevent email modification")
	}
	
	return nil
}

// Fetch new emails from the IMAP server
func fetchNewEmails(state *emailState) ([]EmailSummary, error) {
	c, err := connectIMAP(state)
	if err != nil {
		return nil, err
	}
	defer c.Logout()

	// Select INBOX in read-only mode to ensure no changes can be made
	// This is critical to prevent any accidental deletion or modification
	mbox, err := c.Select("INBOX", true) // true = read-only mode, prevents any changes
	if err != nil {
		return nil, fmt.Errorf("failed to select inbox: %v", err)
	}
	
	// Double-check we're in read-only mode for safety
	if err := verifyReadOnlySafety(c); err != nil {
		return nil, err
	}

	// Get the latest messages
	from := uint32(1)
	to := mbox.Messages
	if to > 30 {
		// Limit to latest 30 for performance
		from = to - 29
	}

	seqSet := new(imap.SeqSet)
	seqSet.AddRange(from, to)

	// Get message envelope and flags
	items := []imap.FetchItem{imap.FetchEnvelope, imap.FetchFlags, imap.FetchUid, imap.FetchBodyStructure}
	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)

	go func() {
		done <- c.Fetch(seqSet, items, messages)
	}()

	// Process messages
	var emails []EmailSummary
	for msg := range messages {
		var from string
		var to []string

		if msg.Envelope != nil {
			if len(msg.Envelope.From) > 0 {
				from = msg.Envelope.From[0].Address()
			}

			for _, addr := range msg.Envelope.To {
				to = append(to, addr.Address())
			}

			// Create a preview by fetching a small part of the body
			preview := getPreview(c, msg.SeqNum)
			hasAttachments := false
			
			// Check for attachments
			if msg.BodyStructure != nil {
				if msg.BodyStructure.MIMEType == "multipart" {
					// Check if any part is an attachment
					for _, part := range msg.BodyStructure.Parts {
						if part.Disposition == "attachment" {
							hasAttachments = true
							break
						}
					}
				}
			}

			email := EmailSummary{
				UID:       msg.Uid,
				Date:      msg.Envelope.Date,
				From:      from,
				To:        to,
				Subject:   msg.Envelope.Subject,
				Preview:   preview,
				HasAttach: hasAttachments,
				Size:      msg.Size,
			}

			emails = append(emails, email)
		}
	}

	if err := <-done; err != nil {
		return nil, fmt.Errorf("failed to fetch messages: %v", err)
	}

	// Sort emails by date (newest first)
	// Note: IMAP usually returns them in ascending order, we want descending
	for i, j := 0, len(emails)-1; i < j; i, j = i+1, j-1 {
		emails[i], emails[j] = emails[j], emails[i]
	}

	return emails, nil
}

// Get a preview of the email body
func getPreview(c *client.Client, seqNum uint32) string {
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(seqNum)

	// Request the text parts of the message
	section := imap.BodySectionName{
		BodyPartName: imap.BodyPartName{
			Specifier: imap.TextSpecifier,
		},
		Peek: true, // Don't mark as read
	}

	items := []imap.FetchItem{section.FetchItem()}
	messages := make(chan *imap.Message, 1)
	done := make(chan error, 1)

	go func() {
		done <- c.Fetch(seqSet, items, messages)
	}()

	var preview string
	if msg := <-messages; msg != nil {
		if literal := msg.GetBody(&section); literal != nil {
			maxPreviewLen := 100 // Limit preview length
			buf := make([]byte, maxPreviewLen)
			n, _ := literal.Read(buf)
			if n > 0 {
				preview = string(buf[:n])
				// Clean up the preview
				preview = strings.ReplaceAll(preview, "\r", "")
				preview = strings.ReplaceAll(preview, "\n", " ")
				if len(preview) >= maxPreviewLen {
					preview += "..."
				}
			}
		}
	}

	// Ignore errors, this is just a preview
	<-done
	return preview
}

// Read the full content of an email by its UID
func readEmailByUID(state *emailState, uid uint32) (*EmailDetail, error) {
	c, err := connectIMAP(state)
	if err != nil {
		return nil, err
	}
	defer c.Logout()

	// Select INBOX in read-only mode to ensure no changes can be made
	// This is critical to prevent any accidental deletion or modification
	_, err = c.Select("INBOX", true) // true = read-only mode, prevents any changes
	if err != nil {
		return nil, fmt.Errorf("failed to select inbox: %v", err)
	}
	
	// Double-check we're in read-only mode for safety
	if err := verifyReadOnlySafety(c); err != nil {
		return nil, err
	}

	// Create sequence set with just this UID
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)

	// Request specific message by UID with all the data we need
	section := imap.BodySectionName{} // Empty section to fetch the entire message
	fetchItems := []imap.FetchItem{
		imap.FetchEnvelope,
		imap.FetchFlags,
		imap.FetchBodyStructure,
		section.FetchItem(),
	}
	messages := make(chan *imap.Message, 1)
	done := make(chan error, 1)

	go func() {
		done <- c.UidFetch(seqSet, fetchItems, messages)
	}()

	var email *EmailDetail
	for msg := range messages {
		var from string
		var to []string

		if msg.Envelope != nil {
			if len(msg.Envelope.From) > 0 {
				from = msg.Envelope.From[0].Address()
			}

			for _, addr := range msg.Envelope.To {
				to = append(to, addr.Address())
			}

			// Create the email detail object
			email = &EmailDetail{
				UID:         msg.Uid,
				Date:        msg.Envelope.Date,
				From:        from,
				To:          to,
				Subject:     msg.Envelope.Subject,
				Body:        "",
				HTMLBody:    "",
				Attachments: []string{},
			}

			// Parse the email body
			section := imap.BodySectionName{Peek: true} // Don't mark as read
			body := msg.GetBody(&section)
			if body == nil {
				continue
			}

			// Parse the mail message
			mr, err := mail.CreateReader(body)
			if err != nil {
				continue
			}

			// Process each part of the message
			for {
				p, err := mr.NextPart()
				if err == io.EOF {
					break
				}
				if err != nil {
					continue
				}

				switch h := p.Header.(type) {
				case *mail.InlineHeader:
					// This is the message's text (can be plain text or HTML)
					contentType, _, _ := h.ContentType()
					b, _ := ioutil.ReadAll(p.Body)
					if contentType == "text/plain" {
						email.Body = string(b)
					} else if contentType == "text/html" {
						email.HTMLBody = string(b)
					}

				case *mail.AttachmentHeader:
					// This is an attachment
					filename, _ := h.Filename()
					if filename != "" {
						email.Attachments = append(email.Attachments, filename)
					}
				}
			}
		}
	}

	if err := <-done; err != nil {
		return nil, fmt.Errorf("failed to fetch message: %v", err)
	}

	if email == nil {
		return nil, fmt.Errorf("email with UID %d not found", uid)
	}

	return email, nil
}

// Send an email via SMTP
func sendEmail(state *emailState, to, subject, body string) error {
	// Set up authentication information
	auth := smtp.PlainAuth("", state.Username, state.Password, state.SMTPServer)

	// Prepare email message
	toHeader := fmt.Sprintf("To: %s\r\n", to)
	fromHeader := fmt.Sprintf("From: %s\r\n", state.Username)
	subjectHeader := fmt.Sprintf("Subject: %s\r\n", subject)
	mimeHeader := "MIME-Version: 1.0\r\nContent-Type: text/plain; charset=\"utf-8\"\r\n"
	msg := []byte(fromHeader + toHeader + subjectHeader + mimeHeader + "\r\n" + body)

	// Send the email
	addr := fmt.Sprintf("%s:%d", state.SMTPServer, state.SMTPPort)
	err := smtp.SendMail(addr, auth, state.Username, []string{to}, msg)
	if err != nil {
		return fmt.Errorf("failed to send email: %v", err)
	}

	return nil
}