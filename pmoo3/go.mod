module pmoo3

go 1.23.0

require (
	github.com/chzyer/readline v1.5.1
	github.com/godbus/dbus/v5 v5.1.0
)

require (
	github.com/emersion/go-imap v1.2.1 // indirect
	github.com/emersion/go-message v0.18.2 // indirect
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21 // indirect
	github.com/google/go-github/v42 v42.0.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	gitlab.com/donomii/chim v0.0.0-20250226180542-2bacac539596 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/oauth2 v0.28.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
