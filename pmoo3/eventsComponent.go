package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/donomii/chim"
)

type eventsState struct {
	// event name -> (handler id -> script)
	Handlers map[string]map[string]string `json:"handlers"`
}

func NewEventsComponent(id string, node *ComponentNode) Component {
	state := &eventsState{
		Handlers: make(map[string]map[string]string),
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
		panicOnError(err)
		switch cmd.Name {
		case "add_handler":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("add_handler requires event name, handler id, and script")
			}
			eventName := cmd.Args[0]
			handlerID := cmd.Args[1]
			script := strings.Join(cmd.Args[2:], " ")

			// Initialize handlers map for this event if needed
			if state.Handlers[eventName] == nil {
				state.Handlers[eventName] = make(map[string]string)
			}

			state.Handlers[eventName][handlerID] = script
			debugf("Added handler %s for event %s: %s\n", handlerID, eventName, script)
			return handlerID, env, nil

		case "remove_handler":
			if len(cmd.Args) < 2 {
				return "", env, fmt.Errorf("remove_handler requires event name and handler id")
			}
			eventName := cmd.Args[0]
			handlerID := cmd.Args[1]

			if handlers, exists := state.Handlers[eventName]; exists {
				if script, hasHandler := handlers[handlerID]; hasHandler {
					delete(handlers, handlerID)
					// Clean up empty event
					if len(handlers) == 0 {
						delete(state.Handlers, eventName)
					}
					return script, env, nil // Return the removed script
				}
			}
			return "", env, fmt.Errorf("handler %s not found for event %s", handlerID, eventName)

		case "trigger":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("trigger requires event name")
			}
			eventName := cmd.Args[0]

			// Build params map from remaining args
			params := make(map[string]string)
			for i := 1; i < len(cmd.Args); i += 2 {
				if i+1 < len(cmd.Args) {
					params[cmd.Args[i]] = cmd.Args[i+1]
				}
			}

			if handlers, exists := state.Handlers[eventName]; exists {
				count := 0
				for _, script := range handlers {
					// Create script message with params
					//FIXME more than one line in a handler would be important
					newMsg := chim.NewScript([]string{script})

					// Add params to script variables
					for k, v := range params {
						chim.SetVariable(newMsg, k, v)
					}
					var nextComponent string
					// Parse script to get next component
					parts := strings.SplitN(script, ":", 2)
					if len(parts) == 2 {
						nextComponent = strings.TrimSpace(parts[0])
						// Forward message
						if msgJSON, err := json.Marshal(newMsg); err == nil {
							forwardMessage(nextComponent, msgJSON)
							count++
						}
					}
				}
				return fmt.Sprintf("%v", count), env, nil // Return number of handlers triggered
			}
			return fmt.Sprintf("%v", 0), env, nil // No handlers for this event

		case "list_handlers":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("list_handlers requires event name")
			}
			eventName := cmd.Args[0]

			if handlers, exists := state.Handlers[eventName]; exists {
				handlerList := make([]string, 0, len(handlers))
				for id := range handlers {
					handlerList = append(handlerList, id)
				}
				return fmt.Sprintf("%v", handlerList), env, nil
			}
			return "", env, nil

		default:
			return "", env, fmt.Errorf("unknown events command: %s", cmd.Name)
		}
	}

	marshalState := func() (string, error) {
		bytes, err := json.Marshal(state)
		return string(bytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}

    var commands = []CommandInfo{
        {Name: "add_handler", Args: []string{"event", "handler_id", "script"}, Description: "Add an event handler"},
        {Name: "remove_handler", Args: []string{"event", "handler_id"}, Description: "Remove an event handler"},
        {Name: "trigger", Args: []string{"event", "params..."}, Description: "Trigger an event with optional parameters"},
        {Name: "list_handlers", Args: []string{"event"}, Description: "List all handlers for an event"},
    }

	return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}
