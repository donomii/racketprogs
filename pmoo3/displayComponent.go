package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/donomii/chim"
)

type displayState struct {
    Messages []displayMessage
}

type displayMessage struct {
    Text      string    `json:"text"`
    Timestamp time.Time `json:"timestamp"`
    Channel   string    `json:"channel"`
}

func NewDisplayComponent(id string, node *ComponentNode) Component {
    state := &displayState{
        Messages: make([]displayMessage, 0),
    }

    processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
        cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
        panicOnError(err)
        switch cmd.Name {
        case "message":
            if len(cmd.Args) < 1 {
                return "", env, fmt.Errorf("message requires text")
            }
            text := strings.Join(cmd.Args, " ")
            state.Messages = append(state.Messages, displayMessage{
                Text:      text,
                Timestamp: time.Now(),
                Channel:   "main",
            })
            fmt.Printf("[Display] %s\n", text)
            return text, env, nil

        case "channel_message":
            if len(cmd.Args) < 2 {
                return "", env, fmt.Errorf("channel_message requires channel and text")
            }
            channel := cmd.Args[0]
            text := strings.Join(cmd.Args[1:], " ")
            state.Messages = append(state.Messages, displayMessage{
                Text:      text,
                Timestamp: time.Now(),
                Channel:   channel,
            })
            fmt.Printf("[Display:%s] %s\n", channel, text)
            return text, env, nil

        default:
            return "", env, fmt.Errorf("unknown display command: %s", cmd.Name)
        }
    }

    marshalState := func() (string, error) {
        bytes, err := json.Marshal(state)
        return string(bytes), err
    }

    unmarshalState := func(data string) error {
        return json.Unmarshal([]byte(data), state)
    }

    var commands = []CommandInfo{
        {Name: "message", Args: []string{"text"}, Description: "Display a message"},
        {Name: "channel_message", Args: []string{"channel", "text"}, Description: "Display a message on a specific channel"},
    }

    return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}