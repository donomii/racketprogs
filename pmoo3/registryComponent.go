package main

import (
    "encoding/json"
    "net/http"
    "sync"
    "strings"
    "fmt"

    "gitlab.com/donomii/chim"
)

// RegistryComponent manages component registration and discovery
type RegistryComponent struct {
    ID string
    mu sync.RWMutex
    state *registryState
}

type registryState struct {
    Components map[string]string
}

func NewRegistryComponent(id string, node *ComponentNode, mux *http.ServeMux) Component {

    state := &registryState{
        Components: make(map[string]string),
    }

    rc := &RegistryComponent{
        ID: id,
        state: state,
    }

    rc.AddHandlers(mux)



    processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
        cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
        panicOnError(err)
        switch cmd.Name {
        case "insert":
        // TODO
            
        case "remove":
            // Todo
            
        case "list":
            outList := []string{}
            for k, _ := range state.Components {
                outList = append(outList, k)
            }
            jsonList, err := json.Marshal(outList)
            panicOnError(err)
            return string(jsonList), env, nil

        default:
            return "", env, fmt.Errorf("unknown registry command: %s", cmd.Name)
        }

        return "", env, fmt.Errorf("unknown registry command: %s", cmd.Name)
    }

    marshalState := func() (string, error) {
        json, err := json.Marshal(state)
        panicOnError(err)
        return string(json), nil
    }

    unmarshalState := func(data string) error {
        return json.Unmarshal([]byte(data), state)
    }

    commands := []CommandInfo{
        {Name: "insert", Args: []string{"id", "address"}, Description: "Add a component to the registry"},
        {Name: "remove", Args: []string{"id"}, Description: "Remove a component from the registry"},
        {Name: "list", Args: []string{}, Description: "List all components in the registry"},
    }


    return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

func (rc *RegistryComponent) GetID() string {
    return rc.ID
}

func (lc *RegistryComponent) GetState() string {
    return "" //FIXME
}

// RegisterComponent adds or updates a component's address
func (rc *RegistryComponent) RegisterComponent(id, address string) {
    rc.mu.Lock()
    defer rc.mu.Unlock()
    rc.state.Components[id] = address
    verbosef("Registered component %s at %s\n", id, address)
}

// GetComponentAddress returns the address for a component ID
func (rc *RegistryComponent) GetComponentAddress(id string) (string, bool) {
    rc.mu.RLock()
    defer rc.mu.RUnlock()
    addr, exists := rc.state.Components[id]
    return addr, exists
}

// ListComponents returns all registered components
func (rc *RegistryComponent) ListComponents() map[string]string {
    rc.mu.RLock()
    defer rc.mu.RUnlock()
    
    // Make a copy to avoid races
    result := make(map[string]string, len(rc.state.Components))
    for k, v := range rc.state.Components {
        result[k] = v
    }
    return result
}

// HTTP Handlers
func (rc *RegistryComponent) handleRegister(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodPost {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
        return
    }

    var reg struct {
        ID      string `json:"id"`
        Address string `json:"address"`
    }

    if err := json.NewDecoder(r.Body).Decode(&reg); err != nil {
        http.Error(w, "Invalid request body", http.StatusBadRequest)
        return
    }

    rc.RegisterComponent(reg.ID, reg.Address)
    w.WriteHeader(http.StatusOK)
}

func (rc *RegistryComponent) handleLookup(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodGet {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
        return
    }

    componentID := r.URL.Query().Get("id")
    if componentID == "" {
        // If no ID specified, return all components
        components := rc.ListComponents()
        json.NewEncoder(w).Encode(components)
        return
    }

    // Look up specific component
    if addr, exists := rc.GetComponentAddress(componentID); exists {
        json.NewEncoder(w).Encode(map[string]string{
            "address": addr,
        })
    } else {
        http.Error(w, "Component not found", http.StatusNotFound)
    }
}

// Add registry handlers to server mux
func (rc *RegistryComponent) AddHandlers(mux *http.ServeMux) {
    mux.HandleFunc("/register", rc.handleRegister)
    mux.HandleFunc("/lookup", rc.handleLookup)
}