package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/google/go-github/v42/github"
	"golang.org/x/oauth2"
	"gitlab.com/donomii/chim"
)

type githubState struct {
	Token       string                  `json:"token,omitempty"`
	Owner       string                  `json:"owner,omitempty"`
	Repo        string                  `json:"repo,omitempty"`
}

// Enhanced error handling for GitHub API
func formatGitHubError(err error) error {
	if err == nil {
		return nil
	}
	
	// Check for GitHub-specific error responses
	if ghErr, ok := err.(*github.ErrorResponse); ok {
		switch ghErr.Response.StatusCode {
		case 401:
			return fmt.Errorf("GitHub authentication failed: Invalid or expired token\nFix: Run 'github:configure YOUR_TOKEN username repo' with a valid token")
		case 403:
			if ghErr.Message == "API rate limit exceeded" {
				return fmt.Errorf("GitHub API rate limit exceeded\nFix: Wait for rate limit reset or authenticate to increase your limit")
			}
			return fmt.Errorf("GitHub permission denied: %s\nFix: Check that your token has the required permissions", ghErr.Message)
		case 404:
			return fmt.Errorf("GitHub resource not found: %s\nFix: Check that the repository '%s/%s' exists and your token has access to it", 
				ghErr.Message, ghErr.Response.Request.URL.Path, ghErr.Response.Request.URL.Path)
		case 422:
			return fmt.Errorf("GitHub request validation failed: %s\nFix: Check your input parameters", ghErr.Message)
		default:
			// Try to extract detailed error information from response
			details := "No additional details"
			if ghErr.Response != nil && ghErr.Response.Body != nil {
				bodyBytes, _ := io.ReadAll(ghErr.Response.Body)
				if len(bodyBytes) > 0 {
					details = string(bodyBytes)
				}
			}
			return fmt.Errorf("GitHub API error (%d): %s\nDetails: %s", 
				ghErr.Response.StatusCode, ghErr.Message, details)
		}
	}
	
	// Handle network/connectivity errors
	if strings.Contains(err.Error(), "connection refused") || 
	   strings.Contains(err.Error(), "no such host") ||
	   strings.Contains(err.Error(), "timeout") {
		return fmt.Errorf("GitHub connection failed: %s\nFix: Check your internet connection", err.Error())
	}
	
	// Default for unhandled errors
	return fmt.Errorf("GitHub operation failed: %s", err.Error())
}

func NewGitHubComponent(id string, node *ComponentNode) Component {
	state := &githubState{
		
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
		if err != nil {
			return "", env, formatGitHubError(fmt.Errorf("command parsing failed: %v", err))
		}

		ctx := context.Background()
		var client *github.Client

		// Initialize client if token is available
		if state.Token != "" {
			ts := oauth2.StaticTokenSource(
				&oauth2.Token{AccessToken: state.Token},
			)
			tc := oauth2.NewClient(ctx, ts)
			client = github.NewClient(tc)
		} else if cmd.Name != "configure" {
			return "", env, fmt.Errorf("GitHub not configured\nFix: Run 'github:configure YOUR_TOKEN username repo' first")
		}

		switch cmd.Name {
		case "configure":
			if len(cmd.Args) < 3 {
				return "", env, fmt.Errorf("Not enough arguments for configure\nUsage: github:configure TOKEN OWNER REPO")
			}
			state.Token = cmd.Args[0]
			state.Owner = cmd.Args[1]
			state.Repo = cmd.Args[2]
			
			// Verify credentials with a simple API call
			ts := oauth2.StaticTokenSource(
				&oauth2.Token{AccessToken: state.Token},
			)
			tc := oauth2.NewClient(ctx, ts)
			testClient := github.NewClient(tc)
			
			_, _, err := testClient.Repositories.Get(ctx, state.Owner, state.Repo)
			if err != nil {
				return "", env, formatGitHubError(fmt.Errorf("configuration failed: %v", err))
			}
			
			return fmt.Sprintf("GitHub configured for %s/%s", state.Owner, state.Repo), env, nil

		case "issues":
			opts := &github.IssueListByRepoOptions{
				State:     "open",
				Sort:      "created",
				Direction: "desc",
				ListOptions: github.ListOptions{
					PerPage: 50,
				},
			}
			
			// Check for filters
			if len(cmd.Args) > 0 {
				for _, arg := range cmd.Args {
					if strings.HasPrefix(arg, "state=") {
						opts.State = strings.TrimPrefix(arg, "state=")
					} else if strings.HasPrefix(arg, "label=") {
						opts.Labels = append(opts.Labels, strings.TrimPrefix(arg, "label="))
					} else if strings.HasPrefix(arg, "limit=") {
						limit := 0
						fmt.Sscanf(strings.TrimPrefix(arg, "limit="), "%d", &limit)
						if limit > 0 {
							opts.ListOptions.PerPage = limit
						}
					}
				}
			}
			
			issues, _, err := client.Issues.ListByRepo(ctx, state.Owner, state.Repo, opts)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			if len(issues) == 0 {
				return "[]", env, nil // Return empty array when no issues found
			}
			
			// Convert to simplified format
			result := make([]map[string]interface{}, 0, len(issues))
			for _, issue := range issues {
				item := map[string]interface{}{
					"number":     issue.GetNumber(),
					"title":      issue.GetTitle(),
					"url":        issue.GetHTMLURL(),
					"state":      issue.GetState(),
					"created_at": issue.GetCreatedAt().Format(time.RFC3339),
					"user":       issue.GetUser().GetLogin(),
				}
				result = append(result, item)
			}

			resultJSON, _ := json.Marshal(result)
			return string(resultJSON), env, nil

		case "prs":
			opts := &github.PullRequestListOptions{
				State:     "open",
				Sort:      "created",
				Direction: "desc",
				ListOptions: github.ListOptions{
					PerPage: 50,
				},
			}
			
			prs, _, err := client.PullRequests.List(ctx, state.Owner, state.Repo, opts)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			if len(prs) == 0 {
				return "[]", env, nil // Return empty array when no PRs found
			}
			
			// Convert to simplified format
			result := make([]map[string]interface{}, 0, len(prs))
			for _, pr := range prs {
				item := map[string]interface{}{
					"number":     pr.GetNumber(),
					"title":      pr.GetTitle(),
					"url":        pr.GetHTMLURL(),
					"state":      pr.GetState(),
					"created_at": pr.GetCreatedAt().Format(time.RFC3339),
					"user":       pr.GetUser().GetLogin(),
					"mergeable":  pr.GetMergeable(),
				}
				result = append(result, item)
			}

			resultJSON, _ := json.Marshal(result)
			return string(resultJSON), env, nil

		case "issue":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("Missing issue number\nUsage: github:issue ISSUE_NUMBER")
			}
			
			number := 0
			fmt.Sscanf(cmd.Args[0], "%d", &number)
			if number == 0 {
				return "", env, fmt.Errorf("Invalid issue number: '%s'\nFix: Provide a valid numeric issue ID", cmd.Args[0])
			}
			
			issue, _, err := client.Issues.Get(ctx, state.Owner, state.Repo, number)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			result := map[string]interface{}{
				"number":     issue.GetNumber(),
				"title":      issue.GetTitle(),
				"body":       issue.GetBody(),
				"url":        issue.GetHTMLURL(),
				"state":      issue.GetState(),
				"created_at": issue.GetCreatedAt().Format(time.RFC3339),
				"user":       issue.GetUser().GetLogin(),
				"labels":     getLabelNames(issue.Labels),
			}

			resultJSON, _ := json.Marshal(result)
			return string(resultJSON), env, nil
		case "pr":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("Missing PR number\nUsage: github:pr PR_NUMBER")
			}
			
			number := 0
			fmt.Sscanf(cmd.Args[0], "%d", &number)
			if number == 0 {
				return "", env, fmt.Errorf("Invalid PR number: '%s'\nFix: Provide a valid numeric PR ID", cmd.Args[0])
			}
			
			pr, _, err := client.PullRequests.Get(ctx, state.Owner, state.Repo, number)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			result := map[string]interface{}{
				"number":     pr.GetNumber(),
				"title":      pr.GetTitle(),
				"body":       pr.GetBody(),
				"url":        pr.GetHTMLURL(),
				"state":      pr.GetState(),
				"created_at": pr.GetCreatedAt().Format(time.RFC3339),
				"user":       pr.GetUser().GetLogin(),
				"mergeable":  pr.GetMergeable(),
				"commits":    pr.GetCommits(),
				"additions":  pr.GetAdditions(),
				"deletions":  pr.GetDeletions(),
				"changed_files": pr.GetChangedFiles(),
				"branch":     pr.GetHead().GetRef(),
				"base":       pr.GetBase().GetRef(),
			}

			resultJSON, _ := json.Marshal(result)
			return string(resultJSON), env, nil

		case "create_issue":
			if len(cmd.Args) < 2 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: github:create_issue \"TITLE\" \"BODY\" [LABEL]")
			}
			
			title := cmd.Args[0]
			body := cmd.Args[1]
			
			req := &github.IssueRequest{
				Title: github.String(title),
				Body:  github.String(body),
			}
			
			// Optional labels
			if len(cmd.Args) > 2 {
				req.Labels = &[]string{cmd.Args[2]}
			}
			
			issue, _, err := client.Issues.Create(ctx, state.Owner, state.Repo, req)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			return fmt.Sprintf("%d", issue.GetNumber()), env, nil

		case "comment":
			if len(cmd.Args) < 2 {
				return "", env, fmt.Errorf("Not enough arguments\nUsage: github:comment ISSUE_NUMBER \"COMMENT_TEXT\"")
			}
			
			number := 0
			fmt.Sscanf(cmd.Args[0], "%d", &number)
			if number == 0 {
				return "", env, fmt.Errorf("Invalid issue number: '%s'\nFix: Provide a valid numeric issue ID", cmd.Args[0])
			}
			
			comment := cmd.Args[1]
			
			req := &github.IssueComment{
				Body: github.String(comment),
			}
			
			result, _, err := client.Issues.CreateComment(ctx, state.Owner, state.Repo, number, req)
			if err != nil {
				return "", env, formatGitHubError(err)
			}
			
			return fmt.Sprintf("%d", result.GetID()), env, nil

		default:
			return "", env, fmt.Errorf("Unknown GitHub command: '%s'\nAvailable commands: configure, issues, prs, issue, create_issue, comment", cmd.Name)
		}
	}

	marshalState := func() (string, error) {
		// Don't include token in logs/debug output
		stateCopy := *state
		stateCopy.Token = "[REDACTED]"
		_, err := json.Marshal(stateCopy)
		if err != nil {
			return "", err
		}
		
		// Now marshal the real state for storage
		realBytes, err := json.Marshal(state)
		return string(realBytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}



	commands := []CommandInfo{
		{Name: "configure", Args: []string{"token", "owner", "repo"}, Description: "Configure GitHub API access"},
		{Name: "issues", Args: []string{"filters..."}, Description: "List repository issues with optional filters"},
		{Name: "prs", Args: []string{}, Description: "List pull requests"},
		{Name: "issue", Args: []string{"issue_number"}, Description: "Get details of a specific issue"},
		{Name: "pr", Args: []string{"pr_number"}, Description: "Get details of a specific pull request"},
		{Name: "create_issue", Args: []string{"title", "body", "label"}, Description: "Create a new issue"},
		{Name: "comment", Args: []string{"issue_number", "comment_text"}, Description: "Add a comment to an issue"},
	}


	return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

// Helper to extract label names from GitHub labels
func getLabelNames(labels []*github.Label) []string {
	result := make([]string, 0, len(labels))
	for _, label := range labels {
		result = append(result, label.GetName())
	}
	return result
}