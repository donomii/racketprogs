package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	registryURL string
	currentURL  string
)

func splitHostPort(addr string) (string, int) {
	parts := strings.Split(addr, ":")
	if len(parts) != 2 {
		log.Fatalf("Invalid address: %s", addr)
	}
	p, err := strconv.Atoi(parts[1])
	if err != nil {
		log.Fatalf("Invalid port: %s", parts[1])
	}
	return parts[0], p
}

func startComponent(componentType string, currentURL, registryURL string) error {
	mux := http.NewServeMux()
	var component Component

	switch componentType {
	case "registry":
		component = NewRegistryComponent("registry", NewComponentNode("registry"), mux)

	case "display":
		component = NewDisplayComponent("display", NewComponentNode("display"))

	case "events":
		component = NewEventsComponent("events", NewComponentNode("events"))

	case "inventory":
		component = NewInventoryComponent("inventory", NewComponentNode("inventory"))

	case "location":
		component = NewLocationComponent("location", NewComponentNode("location"))

	case "cli":
		component = NewCLIComponent("cli", NewComponentNode("cli"))

	case "dbus":
		component = NewDBusComponent("dbus", NewComponentNode("dbus"))
	case "script":
		component = NewScriptComponent("script", NewComponentNode("script"))
	case "github":
		component = NewGitHubComponent("github", NewComponentNode("github"))
	case "email":
		component = NewEmailComponent("email", NewComponentNode("email"))

	default:
		return fmt.Errorf("unknown component type: %s", componentType)
	}

	// Add message handler for non-registry components

		mux.HandleFunc("/message", func(w http.ResponseWriter, r *http.Request) {
			handleMessage(w, r, component)
		})


	_, port := splitHostPort(currentURL)

	// Start HTTP server
	addr := fmt.Sprintf(":%d", port)
	verbosef("Starting %s component on %s\n", componentType, addr)

	go http.ListenAndServe(addr, mux)


	if componentType == "registry" {
		registryURL = currentURL
	}
	// Register with registry if we're not the registry and a registry URL is provided
	if registryURL != "" {
		// If no custom URL provided, construct from localhost and port
		if currentURL == "" {
			panic("Current URL is required")
		}

		// Register with registry with retries
		if err := registerWithRegistryAtURL(component.GetID(), currentURL, registryURL); err != nil {
			log.Printf("Warning: Failed to register with registry: %v\n", err)
		} else {
			verbosef("Registered %s with registry at %s as %s\n", component.GetID(), registryURL, currentURL)
		}
	} else {
		verbosef("No registry URL provided, not registering component\n")
	}
	for {
		time.Sleep(time.Second)
	}
	return nil
}

func main() {
	// Parse command line flags
	registryPtr := flag.Bool("registry", false, "Run as registry component")
	displayPtr := flag.Bool("display", false, "Run as display component")
	eventsPtr := flag.Bool("events", false, "Run as events component")
	inventoryPtr := flag.Bool("inventory", false, "Run as inventory component")
	locationPtr := flag.Bool("location", false, "Run as location component")
	cliPtr := flag.Bool("cli", false, "Run as CLI component")

	dbus := flag.Bool("dbus", false, "Run as D-Bus component")
	script := flag.Bool("script", false, "Run as script component")
	gith := flag.Bool("github", false, "Run as GitHub component")
	emailPtr := flag.Bool("email", false, "Run as email component")

	// New flags for URL and registry URL
	urlPtr := flag.String("url", "", "Current component URL (host:port)")
	registryURLPtr := flag.String("registryurl", "", "Registry component URL (host:port)")
	flag.BoolVar(&debugMode, "debug", false, "Enable debug logging")
	flag.BoolVar(&verboseMode, "verbose", false, "Enable verbose logging")
	testMode := flag.Bool("test", false, "Run tests")


	flag.Parse()

	// Run tests if requested
	if *testMode {
		RunTests()
		return
	}
	// Validate and set registry and current URLs
	registryURL = strings.TrimSpace(*registryURLPtr)
	currentURL = strings.TrimSpace(*urlPtr)

	// Determine which component to run
	var componentType string
	switch {
	case *registryPtr:
		componentType = "registry"
	case *displayPtr:
		componentType = "display"
	case *eventsPtr:
		componentType = "events"
	case *inventoryPtr:
		componentType = "inventory"
	case *locationPtr:
		componentType = "location"
	case *cliPtr:
		componentType = "cli"
	case *dbus:
		componentType = "dbus"
	case *script:
		componentType = "script"
	case *gith:
		componentType = "github"
	case *emailPtr:
		componentType = "email"
	default:
		fmt.Println("Please specify a component type:")
		fmt.Println("  --registry    Run as registry component")
		fmt.Println("  --display     Run as display component")
		fmt.Println("  --events      Run as events component")
		fmt.Println("  --inventory   Run inventory component")
		fmt.Println("  --location    Run location component")
		fmt.Println("  --cli         Run CLI component")
		fmt.Println("  --dbus        Run D-Bus component")
		fmt.Println("  --script      Run script component")
		fmt.Println("  --github	     Run GitHub component")
		fmt.Println("  --email       Run email component")
		fmt.Println("  --port PORT   Port to listen on (default: 8080)")
		fmt.Println("  --url URL     Current component URL (optional)")
		fmt.Println("  --registryurl URL  Registry component URL (optional)")
		os.Exit(1)
	}

	if err := startComponent(componentType, currentURL, registryURL); err != nil {
		log.Fatalf("Component error: %v", err)
	}
}
