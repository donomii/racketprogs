package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/donomii/chim"
)

type locationState struct {
	Locations map[string]string `json:"locations"`  // entity -> room
	RoomDescs map[string]string `json:"room_descs"` // room -> description
}

func NewLocationComponent(id string, node *ComponentNode) Component {
	state := &locationState{
		Locations: map[string]string{
			"player_1": "room_0", // Starting position
		},
		RoomDescs: map[string]string{
			"room_0": "Starting Room",
		},
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
		panicOnError(err)
		switch cmd.Name {
		case "describe":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("describe requires room ID")
			}
			roomID := cmd.Args[0]
			desc, exists := state.RoomDescs[roomID]
			if !exists {
				return "", env, fmt.Errorf("room %s not found", roomID)
			}
			return desc, env, nil

		case "create_room":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("create_room requires description")
			}

			// Find highest existing room number
			maxID := -1
			for roomID := range state.RoomDescs {
				if strings.HasPrefix(roomID, "room_") {
					if id, err := strconv.Atoi(strings.TrimPrefix(roomID, "room_")); err == nil {
						if id > maxID {
							maxID = id
						}
					}
				}
			}

			// Create new room with next ID
			roomID := fmt.Sprintf("room_%d", maxID+1)
			description := strings.Join(cmd.Args, " ")
			state.RoomDescs[roomID] = description

			return roomID, env, nil

		case "move":
			if len(cmd.Args) < 2 {
				return "", env, fmt.Errorf("move requires entity and target IDs")
			}
			entityID := cmd.Args[0]
			targetID := cmd.Args[1]

			if _, exists := state.RoomDescs[targetID]; !exists {
				return "", env, fmt.Errorf("target room %s does not exist", targetID)
			}

			state.Locations[entityID] = targetID
			return targetID, env, nil

		case "get":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("get requires entity ID")
			}
			entityID := cmd.Args[0]

			// Just return the location as a string
			if location, exists := state.Locations[entityID]; exists {
				return location, env, nil
			}
			return "", env, fmt.Errorf("entity %s not found", entityID)

		case "get_location":
			if len(cmd.Args) < 1 {
				return "", env, fmt.Errorf("get_location requires entity ID")
			}
			entityID := cmd.Args[0]

			// Also just return the location string
			if location, exists := state.Locations[entityID]; exists {
				return location, env, nil
			}
			return "", env, fmt.Errorf("entity %s has no location", entityID)

		default:
			return "", env, fmt.Errorf("unknown location command: %s", cmd.Name)
		}
	}

	marshalState := func() (string, error) {
		bytes, err := json.Marshal(state)
		return string(bytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}

	commands := []CommandInfo{
		{Name: "describe", Args: []string{"room_id"}, Description: "Get description of a room"},
		{Name: "create_room", Args: []string{"description"}, Description: "Create a new room with a description"},
		{Name: "move", Args: []string{"entity_id", "target_room_id"}, Description: "Move an entity to a new room"},
		{Name: "get", Args: []string{"entity_id"}, Description: "Get entity location info"},
		{Name: "get_location", Args: []string{"entity_id"}, Description: "Get entity location ID"},
	}

	return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}
