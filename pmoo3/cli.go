package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/chzyer/readline"
	"io"
	"net/http"
	"os"
	"strings"
	"gitlab.com/donomii/chim"
	"sort"
)

type CLIComponent struct {
	ID          string
	node        *ComponentNode
	rl          *readline.Instance
	lines       []string
	history     []string
	watching    map[string]bool        // Components being watched
	lastStates  map[string]string      // Last known component states
}

type cliState struct {
}

func NewCLIComponent(id string, node *ComponentNode) Component {
	cli := &CLIComponent{
		ID:          id,
		node:        node,
		watching:    make(map[string]bool),
		lastStates:  make(map[string]string),
	}

	state := &cliState{}

	marshalState := func() (string, error) {
		bytes, err := json.Marshal(state)
		return string(bytes), err
	}

	unmarshalState := func(data string) error {
		return json.Unmarshal([]byte(data), state)
	}

	processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		debugf("cli> processing command: %s\n", cmdParts[0])
		switch cmdParts[0] {
		case "message":
			msg := strings.Join(cmdParts[1:], " ")
			cli.ShowMessage(msg)
			return msg, env, nil
		}
		return "", env, nil
	}

	go cli.RunCLI()

	var commands = []CommandInfo{
		{Name: "message", Args: []string{"text"}, Description: "Display a message in the CLI"},
	}

	return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

func (cli *CLIComponent) ShowMessage(message string) {
	// State changed - save current line
	//currentLine := cli.rl.Line()

	// Clear current line
	fmt.Print("\r\033[K")

	// Print state change notification with newlines
	fmt.Printf("\n\033[36m=== message ===\033[0m\n")
	// Pretty print the new state

	fmt.Printf("\033[36m")
	cli.DisplayJSON(message)
	fmt.Printf("\033[0m\n")

	// Restore prompt and line
	fmt.Print("\n")
	//cli.rl.SetPrompt("\033[32m»\033[0m ")
	//cli.rl.WriteStdin([]byte(currentLine.Line))
	//cli.rl.Refresh()
}

func (cli *CLIComponent) GetID() string {
	return cli.ID
}

func (cli *CLIComponent) GetState() string {
	return ""
}

func (cli *CLIComponent) setupReadline() error {
	config := &readline.Config{
		Prompt:       "\033[32m»\033[0m ",
		HistoryFile:  ".cli_history",
		AutoComplete: cli.newCompleter(),
	}

	rl, err := readline.NewEx(config)
	if err != nil {
		return err
	}
	cli.rl = rl
	return nil
}

// Show current prompt with input
func (cli *CLIComponent) showPrompt() {
	currentLine := cli.rl.Line()
	fmt.Printf("\033[32m»\033[0m %s", currentLine)
}

// Enhanced command completion
func (cli *CLIComponent) newCompleter() *readline.PrefixCompleter {
	// Get all component commands for completion
	completions := make([]readline.PrefixCompleterInterface, 0)

	// First, get the list of components for dynamic completion
	componentItems := cli.getComponentCompletionItems()

	// Add console commands
	completions = append(completions,
		readline.PcItem("inspect", componentItems...),
		readline.PcItem("state", componentItems...),
		readline.PcItem("watch", componentItems...),
		readline.PcItem("unwatch", componentItems...),
		readline.PcItem("watching"),
		readline.PcItem("help"),
		readline.PcItem("components"),
		readline.PcItem("history"),
		readline.PcItem("exec"),
		readline.PcItem("clear"),
		readline.PcItem("exit"),
	)

	return readline.NewPrefixCompleter(completions...)
}

// Get component names for completion
func (cli *CLIComponent) getComponentCompletionItems() []readline.PrefixCompleterInterface {
	items := []readline.PrefixCompleterInterface{}
	
	// We could fetch this list dynamically from the registry
	// but for now we'll use a static list as a fallback
	script := chim.NewScript([]string{
		"registry: list",
	})

	msgJSON, err := json.Marshal(script)
	if err == nil {
		// Try to get components list, but don't block if it fails
		resp, err := http.Post(
			fmt.Sprintf("http://localhost:8080/message"),
			"application/json",
			bytes.NewReader(msgJSON),
		)
		
		if err == nil {
			defer resp.Body.Close()
			
			if resp.StatusCode == http.StatusOK {
				var compList []string
				body, _ := io.ReadAll(resp.Body)
				if err := json.Unmarshal(body, &compList); err == nil {
					// Add any new components not in our static list
					items = []readline.PrefixCompleterInterface{}
					for _, comp := range compList {
						items = append(items, readline.PcItem(comp))
					}
				}
			}
		}
	}
	
	return items
}

func printHelp() {
	fmt.Println("\n=== Interactive Debug Console ===")
	fmt.Println("\nDirect Commands:")
	fmt.Println("  Type commands directly in the format: component:command args...")
	fmt.Println("  Examples:")
	fmt.Println("    location:move player_1 room_1")
	fmt.Println("    inventory:insert player_1 sword")
	fmt.Println("    display:message Hello world")

	fmt.Println("\nConsole Commands:")
	fmt.Println("  inspect <component>  - Show component details and available commands")
	fmt.Println("  state <component>    - Show current state")
	fmt.Println("  watch <component>    - Watch for state changes")
	fmt.Println("  unwatch <component>  - Stop watching state changes")
	fmt.Println("  watching            - List components being watched")
	fmt.Println("  components          - List available components")
	fmt.Println("  history             - Show command history")
	fmt.Println("  exec <cmd>          - Execute a direct command")
	fmt.Println("  help                - Show this help")
	fmt.Println("  clear               - Clear screen")
	fmt.Println("  exit                - Exit console")

	fmt.Println("\nTip: Use 'inspect <component>' to see all available commands for a component")
	fmt.Println("     Use tab completion for commands and components\n")
}

// Component API documentation
type CommandDoc struct {
	Name        string
	Description string
	Args        []ArgDoc
	Examples    []string
}

type ArgDoc struct {
	Name        string
	Type        string
	Description string
	Required    bool
}

// Inspector for components
func (cli *CLIComponent) inspectComponent(componentID string) {
    fmt.Printf("\n\033[1m=== Component: %s ===\033[0m\n\n", componentID)
    
    // Step 1: Get list of commands
    fmt.Printf("\033[1mAvailable Commands:\033[0m\n")
    commandsScript := chim.NewScript([]string{
        fmt.Sprintf("%s: commands", componentID),
    })
    
    commandsJSON, err := json.Marshal(commandsScript)
    if err != nil {
        fmt.Printf("Error serializing message: %v\n", err)
        return
    }
    
    resp, err := http.Post(
        fmt.Sprintf("http://localhost:8080/message"),
        "application/json",
        bytes.NewReader(commandsJSON),
    )
    
    if err != nil {
        fmt.Printf("Error getting commands: %v\n", err)
        return
    }
    
    // Read and parse response
    respBody, err := io.ReadAll(resp.Body)
    resp.Body.Close()
    if err != nil {
        fmt.Printf("Error reading response: %v\n", err)
        return
    }
    
    var respEnv chim.ScriptEnv
    if err := json.Unmarshal(respBody, &respEnv); err != nil {
        fmt.Printf("Error parsing response: %v\n", err)
        return
    }
    
    // Extract command list from $it variable in the script environment
    var availableCommands []string
    if respEnv.Variables != nil {
        if cmdJSON, ok := respEnv.Variables["$it"]; ok {
            if err := json.Unmarshal([]byte(cmdJSON), &availableCommands); err != nil {
                fmt.Printf("Error parsing command list: %v\n", err)
                return
            }
        }
    }
    
    if len(availableCommands) == 0 {
        fmt.Println("No commands available for this component.")
        return
    }
    
    // Display commands and get details for each
    for _, cmd := range availableCommands {
        fmt.Printf("\n  \033[36m%s\033[0m", cmd)
        
        // Get command arguments
        argsScript := chim.NewScript([]string{
            fmt.Sprintf("%s: commandargs %s", componentID, cmd),
        })
        
        argsJSON, _ := json.Marshal(argsScript)
        argsResp, err := http.Post(
            fmt.Sprintf("http://localhost:8080/message"),
            "application/json",
            bytes.NewReader(argsJSON),
        )
        
        var args []string
        if err == nil {
            argsBody, _ := io.ReadAll(argsResp.Body)
            argsResp.Body.Close()
            
            var argsEnv chim.ScriptEnv
            if json.Unmarshal(argsBody, &argsEnv) == nil && argsEnv.Variables != nil {
                if argsStr, ok := argsEnv.Variables["$it"]; ok {
                    json.Unmarshal([]byte(argsStr), &args)
                }
            }
        }
        
        // Display arguments
        if len(args) > 0 {
            for _, arg := range args {
                fmt.Printf(" <%s>", arg)
            }
        }
        fmt.Println()
        
        // Get command description
        descScript := chim.NewScript([]string{
            fmt.Sprintf("%s: commanddescribe %s", componentID, cmd),
        })
        
        descJSON, _ := json.Marshal(descScript)
        descResp, err := http.Post(
            fmt.Sprintf("http://localhost:8080/message"),
            "application/json",
            bytes.NewReader(descJSON),
        )
        
        if err == nil {
            descBody, _ := io.ReadAll(descResp.Body)
            descResp.Body.Close()
            
            var descEnv chim.ScriptEnv
            if json.Unmarshal(descBody, &descEnv) == nil && descEnv.Variables != nil {
                if descStr, ok := descEnv.Variables["$it"]; ok {
                    fmt.Printf("    %s\n", descStr)
                }
            }
        }
    }
    

    
    fmt.Println()
}

func (cli *CLIComponent) RunCLI() error {
	if err := cli.setupReadline(); err != nil {
		return err
	}
	defer cli.rl.Close()

	printHelp()

	for {
		line, err := cli.rl.Readline()
		if err != nil {
			break
		}

		// Add to history
		if line = strings.TrimSpace(line); line != "" {
			cli.history = append(cli.history, line)
		}

		cli.processInputLine(line)
	}

	return nil
}

// Process input line from user
func (cli *CLIComponent) processInputLine(line string) {
	line = strings.TrimSpace(line)
	if line == "" {
		return
	}

	// Check if it's a direct component command
	if strings.Contains(line, ":") {
		cli.executeCommand(line)
		return
	}

	// Otherwise process as CLI command
	parts := strings.Fields(line)
	cmd := parts[0]

	switch cmd {
	case "watch":
		if len(parts) < 2 {
			fmt.Println("Usage: watch <component>")
			break
		}
		cli.watching[parts[1]] = true
		fmt.Printf("Now watching %s for changes\n", parts[1])

	case "unwatch":
		if len(parts) < 2 {
			fmt.Println("Usage: unwatch <component>")
			break
		}
		delete(cli.watching, parts[1])
		fmt.Printf("Stopped watching %s\n", parts[1])

	case "watching":
		if len(cli.watching) == 0 {
			fmt.Println("Not watching any components")
		} else {
			fmt.Println("Watching components:")
			for comp := range cli.watching {
				fmt.Printf("  %s\n", comp)
			}
		}

	case "inspect":
		if len(parts) < 2 {
			fmt.Println("Usage: inspect <component>")
			break
		}
		componentID := parts[1]
		cli.inspectComponent(componentID)

	case "state":
		if len(parts) < 2 {
			fmt.Println("Usage: state <component>")
			break
		}
		script := chim.NewScript([]string{
			fmt.Sprintf("%s: s = state", parts[1]),
			"cli:message $it",
		})

		msgJSON, err := json.Marshal(script)
		if err != nil {
			msg := fmt.Sprintf("Error serializing message %v\n",  err)
			fmt.Println(msg)
		}

		forwardMessage("script", msgJSON)

	case "exec":
		if len(parts) < 2 {
			fmt.Println("Usage: exec <component:command>")
			break
		}
		cli.executeCommand(strings.Join(parts[1:], " "))

	case "components":
		cli.listComponents()

	case "history":
		cli.showHistory()

	case "help":
		printHelp()

	case "clear":
		cli.clear()

	case "exit":
		os.Exit(0)

	default:
		fmt.Printf("Unknown command: %s\n", cmd)
	}
}

func (cli *CLIComponent) executeCommand(cmdLine string) {
	// Normalize spacing and handle empty commands
	cmdLine = strings.TrimSpace(cmdLine)
	if cmdLine == "" {
		return
	}

	// Split into component and command, handling various spacing patterns
	parts := strings.SplitN(cmdLine, ":", 2)
	if len(parts) != 2 {
		fmt.Println("\033[31mError: Invalid command format. Use: component:command\033[0m")
		return
	}

	component := strings.TrimSpace(parts[0])
	command := strings.TrimSpace(parts[1])

	debugf("Executing command: component='%s', command='%s'\n", component, command)

	// Create script message
	msg := chim.NewScript([]string{fmt.Sprintf("%s:%s", component, command), "cli:message $it"})

	// Serialize
	msgJSON, err := json.Marshal(msg)
	if err != nil {
		fmt.Printf("\033[31mError preparing command: %v\033[0m\n", err)
		return
	}

	// Forward to component
	if err := forwardMessage(component, msgJSON); err != nil {
		fmt.Printf("\033[31mError: %v\033[0m\n", err)
		// Suggest possible fixes based on error
		if strings.Contains(err.Error(), "component lookup failed") {
			fmt.Println("Available components:")
			cli.listComponents()
		} else if strings.Contains(err.Error(), "not enough arguments") {
			fmt.Println("Try 'inspect component' to see command usage")
		}
		return
	}
}

// New helper method to format and display results
func (cli *CLIComponent) formatAndDisplayResults(results interface{}) {
    // Check if result is a map
    if resultMap, ok := results.(map[string]interface{}); ok {
        for _, v := range resultMap {
            // If value is a string that looks like JSON, try to display as JSON
            if jsonStr, isString := v.(string); isString {
                if (strings.HasPrefix(jsonStr, "{") && strings.HasSuffix(jsonStr, "}")) ||
                   (strings.HasPrefix(jsonStr, "[") && strings.HasSuffix(jsonStr, "]")) {
                    cli.DisplayJSON(jsonStr)
                    return
                }
            }
        }
        // If we get here, just display the whole result map
        resultJSON, _ := json.Marshal(results)
        cli.DisplayJSON(string(resultJSON))
    } else {
        // Not a map, try to display directly
        resultJSON, _ := json.Marshal(results)
        cli.DisplayJSON(string(resultJSON))
    }
}

func (cli *CLIComponent) listComponents() {
	script := chim.NewScript([]string{
		"registry: list",
		"cli:message $it",
	})

	msgJSON, err := json.Marshal(script)
	if err != nil {
		fmt.Printf("Error serializing message %v\n", err)
		return
	}

	forwardMessage("script", msgJSON)
}

func (cli *CLIComponent) showHistory() {
	for i, cmd := range cli.history {
		fmt.Printf("%3d: %s\n", i+1, cmd)
	}
}

// Helper to show Mermaid diagram
func showMermaidDiagram(diagram string) {
	fmt.Printf("\nComponent Relationship Diagram:\n")
	fmt.Println("```mermaid")
	fmt.Println(diagram)
	fmt.Println("```")
}

func (cli *CLIComponent) clear() {
	fmt.Print("\033[H\033[2J")
}

// Add this to the CLIComponent struct in cli.go
func (cli *CLIComponent) DisplayJSON(jsonStr string) {
    // Try to parse the JSON string
    var data interface{}
    if err := json.Unmarshal([]byte(jsonStr), &data); err != nil {
        fmt.Println(jsonStr) // Fall back to raw output
        return
    }
    
    // Check the type of data
    switch v := data.(type) {
    case map[string]interface{}:
        cli.displayJSONObject(v, 0)
    case []interface{}:
        cli.displayJSONArray(v)
    default:
        // For simple values or unsupported types
        fmt.Println(jsonStr)
    }
}

// Display a single JSON object as key-value pairs
func (cli *CLIComponent) displayJSONObject(obj map[string]interface{}, indent int) {
    // Sort keys for consistent output
    keys := make([]string, 0, len(obj))
    for k := range obj {
        keys = append(keys, k)
    }
    sort.Strings(keys)
    
    indentStr := strings.Repeat("  ", indent)
    
    // Find the longest key for alignment
    maxKeyLen := 0
    for _, k := range keys {
        if len(k) > maxKeyLen {
            maxKeyLen = len(k)
        }
    }
    
    // Print each key-value pair
    for _, k := range keys {
        v := obj[k]
        fmt.Printf("%s\033[36m%-*s\033[0m : ", indentStr, maxKeyLen, k)
        
        // Handle different value types
        switch val := v.(type) {
        case map[string]interface{}:
            fmt.Println()
            cli.displayJSONObject(val, indent+1)
        case []interface{}:
            if len(val) == 0 {
                fmt.Println("[]")
            } else if isArrayOfObjects(val) {
                fmt.Println()
                cli.displayJSONArray(val)
            } else {
                fmt.Printf("%v\n", val)
            }
        default:
            if val == nil {
                fmt.Println("\033[90m<nil>\033[0m")
            } else {
                fmt.Printf("%v\n", val)
            }
        }
    }
}

// Display an array of JSON objects as a table
func (cli *CLIComponent) displayJSONArray(arr []interface{}) {
    if len(arr) == 0 {
        fmt.Println("[]")
        return
    }
    
    // Check if this is an array of objects we can display as a table
    if !isArrayOfObjects(arr) {
        // Just print as a simple array
        fmt.Println(arr)
        return
    }
    
    // Collect all keys from all objects
    allKeys := make(map[string]bool)
    for _, item := range arr {
        if obj, ok := item.(map[string]interface{}); ok {
            for k := range obj {
                allKeys[k] = true
            }
        }
    }
    
    // Convert to sorted slice
    keys := make([]string, 0, len(allKeys))
    for k := range allKeys {
        keys = append(keys, k)
    }
    sort.Strings(keys)
    
    // Determine column widths
    colWidths := make(map[string]int)
    for _, k := range keys {
        colWidths[k] = len(k)
        for _, item := range arr {
            if obj, ok := item.(map[string]interface{}); ok {
                if v, exists := obj[k]; exists {
                    valStr := fmt.Sprintf("%v", v)
                    if len(valStr) > colWidths[k] {
                        colWidths[k] = len(valStr)
                    }
                }
            }
        }
        // Cap column width to avoid huge tables
        if colWidths[k] > 40 {
            colWidths[k] = 40
        }
    }
    
    // Print header
    for _, k := range keys {
        fmt.Printf("\033[1m%-*s\033[0m ", colWidths[k], truncate(k, colWidths[k]))
    }
    fmt.Println()
    
    // Print separator
    for _, k := range keys {
        fmt.Print(strings.Repeat("-", colWidths[k]) + " ")
    }
    fmt.Println()
    
    // Print rows
    for _, item := range arr {
        if obj, ok := item.(map[string]interface{}); ok {
            for _, k := range keys {
                if v, exists := obj[k]; exists {
                    valStr := fmt.Sprintf("%v", v)
                    fmt.Printf("%-*s ", colWidths[k], truncate(valStr, colWidths[k]))
                } else {
                    // Empty cell for missing values
                    fmt.Printf("%-*s ", colWidths[k], "")
                }
            }
            fmt.Println()
        }
    }
}

// Helper to check if an array contains objects
func isArrayOfObjects(arr []interface{}) bool {
    for _, item := range arr {
        if _, ok := item.(map[string]interface{}); ok {
            return true
        }
    }
    return false
}

// Helper to truncate a string with ellipsis if needed
func truncate(s string, maxLen int) string {
    if len(s) > maxLen && maxLen > 3 {
        return s[:maxLen-3] + "..."
    }
    return s
}