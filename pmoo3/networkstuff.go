package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/donomii/chim"
)

// Helper function to look up component address from registry
func lookupComponent(componentID string) (string, error) {
    // Use the global registryURL if set, otherwise fall back to localhost
    url := registryURL
    if url == "" {
        url = "localhost:8080"
    }

    resp, err := http.Get(fmt.Sprintf("http://%s/lookup?id=%s", url, componentID))
    if err != nil {
        return "", fmt.Errorf("registry lookup failed: %v", err)
    }
    defer resp.Body.Close()

    if resp.StatusCode == http.StatusNotFound {
        return "", fmt.Errorf("component %s not found", componentID)
    }

    var result struct {
        Address string `json:"address"`
    }
    if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
        return "", fmt.Errorf("failed to decode registry response: %v", err)
    }

    return result.Address, nil
}

// Helper function to register with registry with retries
func registerWithRegistryAtURL(componentID, address, registryAddr string) error {
    registration := map[string]string{
        "id":      componentID,
        "address": address,
    }
    regJSON, _ := json.Marshal(registration)

    // Use the provided registry address
    if registryAddr == "" {
        registryAddr = "localhost:8080"
    }

    // Try to register a few times
    maxRetries := 5
    for i := 0; i < maxRetries; i++ {
        resp, err := http.Post(
            fmt.Sprintf("http://%s/register", registryAddr),
            "application/json",
            bytes.NewBuffer(regJSON),
        )
        if err == nil {
            resp.Body.Close()
            return nil
        }
        
        log.Printf("Registration attempt %d failed: %v\n", i+1, err)
        if i < maxRetries-1 {
            time.Sleep(time.Second * time.Duration(i+1)) // Exponential backoff
        }
    }
    
    return fmt.Errorf("failed to register after %d attempts", maxRetries)
}

// Message handling functions
// In handleMessage function in networkstuff.go
func handleMessage(w http.ResponseWriter, r *http.Request, component Component) {
    if r.Method != http.MethodPost {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
        return
    }

    body, err := io.ReadAll(r.Body)
    if err != nil {
        errMsg := fmt.Sprintf("Error reading request body: %v", err)
        debugf("%s\n", errMsg)
        http.Error(w, errMsg, http.StatusBadRequest)
        return
    }

    debugf("%s> received message: %s\n", component.GetID(), string(body))

    response, nextComponent, err := component.ProcessMessage(body)
    if err != nil {
        // Improve error reporting here
        errMsg := fmt.Sprintf("Error processing message in component %s: %v", component.GetID(), err)
        debugf("%s\n", errMsg)
        http.Error(w, errMsg, http.StatusInternalServerError)
        return
    }

    debugf("%s> produced response: %s in handleMessage\n", component.GetID(), string(response))

    if response != nil {
        // Check if we need to forward to next component
        var msg chim.ScriptEnv
        if err := json.Unmarshal(response, &msg); err != nil {
            errMsg := fmt.Sprintf("Error parsing response: %v", err)
            debugf("%s\n", errMsg)
            http.Error(w, errMsg, http.StatusInternalServerError)
            return
        }

        if msg.CurrentLine < len(msg.Lines) && nextComponent != "" {
            debugf("%s> Forwarding to next component: %s\n", component.GetID(), nextComponent)
            // Forward to next component
            if err := forwardMessage(nextComponent, response); err != nil {
                errMsg := fmt.Sprintf("Error forwarding message: %v", err)
                debugf("%s\n", errMsg)
                http.Error(w, errMsg, http.StatusInternalServerError)
                return
            }
        }

        // Return response to caller
        w.Header().Set("Content-Type", "application/json")
        w.Write(response)
    } else {
        w.WriteHeader(http.StatusOK)
    }
}

// Helper function to forward message to next component
// Helper function to forward message to next component
func forwardMessage(nextComponent string, msgJSON []byte) error {
    debugf("Forwarding message to %s: %s\n", nextComponent, string(msgJSON))

    // Look up component address
    address, err := lookupComponent(nextComponent)
    if err != nil {
        return fmt.Errorf("Failed to locate component '%s': %v", nextComponent, err)
    }
    debugf("Found component %s at address: %s\n", nextComponent, address)

    verbosef("Forwarding to %s at %s to run line %s\n", nextComponent, address, string(msgJSON))
    
    // Forward message
    resp, err := http.Post(
        fmt.Sprintf("http://%s/message", address),
        "application/json",
        bytes.NewReader(msgJSON),
    )
    if err != nil {
        return fmt.Errorf("Connection to component '%s' failed: %v", nextComponent, err)
    }
    defer resp.Body.Close()
    debugf("Forward response status: %d: %s\n", resp.StatusCode, resp.Status)

    // If response is not OK, try to read error details from body
    if resp.StatusCode != http.StatusOK {
        bodyBytes, _ := io.ReadAll(resp.Body)
        if len(bodyBytes) > 0 {
            return fmt.Errorf("Component '%s' returned error (status %d): %s", 
                nextComponent, resp.StatusCode, string(bodyBytes))
        }
        return fmt.Errorf("Component '%s' returned error status %d", nextComponent, resp.StatusCode)
    }

    return nil
}