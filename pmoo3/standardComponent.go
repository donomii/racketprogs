package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/donomii/chim"
)

// Command represents a parsed script command with richer context
type Command struct {
	Name      string
	Args      []string
	VarName   string
	Component string
	RawLine   string
	Timestamp time.Time
}

// Result wraps command execution results with metadata
type Result struct {
	Value    interface{}
	Type     string // for type hints when needed
	Metadata map[string]interface{}
}

// CommandInfo defines a command's metadata for discoverability
type CommandInfo struct {
	Name        string   `json:"name"`
	Args        []string `json:"args"`
	Description string   `json:"description"`
}

type StandardComponent struct {
	id             string
	node           *ComponentNode
	processCmd     func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error)
	marshalState   func() (string, error)
	unmarshalState func(string) error
	commands       []CommandInfo // Command metadata for discoverability
	debugMode      bool
	stateFile      string
}

func NewStandardComponent(id string, node *ComponentNode,
	processCmd func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error),
	marshalState func() (string, error),
	unmarshalState func(string) error,
	commands []CommandInfo) *StandardComponent {

	// Ensure state directory exists
	stateDir := "./state"
	if err := os.MkdirAll(stateDir, 0755); err != nil {
		debugf("Warning: Failed to create state directory: %v\n", err)
	}

	stateFile := filepath.Join(stateDir, fmt.Sprintf("%s.state", id))

	// Create the component
	sc := &StandardComponent{
		id:             id,
		node:           node,
		processCmd:     processCmd,
		marshalState:   marshalState,
		unmarshalState: unmarshalState,
		stateFile:      stateFile,
		commands:       commands,
	}

	// Add standard discovery commands if they don't already exist
	if !commandExists(sc.commands, "commands") {
		sc.commands = append(sc.commands,
			CommandInfo{Name: "commands", Args: []string{}, Description: "List available commands"})
	}
	
	if !commandExists(sc.commands, "commandargs") {
		sc.commands = append(sc.commands, 
			CommandInfo{Name: "commandargs", Args: []string{"command_name"}, Description: "List arguments for a command"})
	}
	
	if !commandExists(sc.commands, "commanddescribe") {
		sc.commands = append(sc.commands,
			CommandInfo{Name: "commanddescribe", Args: []string{"command_name"}, Description: "Get description of a command"})
	}

	// Try to load initial state
	sc.loadState()

	return sc
}

// Check if a command already exists in the command list
func commandExists(commands []CommandInfo, name string) bool {
	for _, cmd := range commands {
		if cmd.Name == name {
			return true
		}
	}
	return false
}

func (sc *StandardComponent) GetID() string {
	return sc.id
}

func (sc *StandardComponent) GetState() string {
	state, _ := sc.marshalState()
	return state
}

func parseCommand(line string, componentID string) (Command, error) {
	command := strings.TrimSpace(line)
	varName := ""

	// Check for assignment
	if idx := strings.Index(command, "="); idx != -1 {
		varName = strings.TrimSpace(command[:idx])
		command = strings.TrimSpace(command[idx+1:])
	}

	parts := parseFields(command)
	if len(parts) == 0 {
		return Command{}, fmt.Errorf("empty command")
	}

	return Command{
		Name:      parts[0],
		Args:      parts[1:],
		VarName:   varName,
		Component: componentID,
		RawLine:   line,
		Timestamp: time.Now(),
	}, nil
}

// Handle standard discovery commands
func (sc *StandardComponent) handleDiscoveryCommands(cmd []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
	if len(cmd) == 0 {
		return "", env, fmt.Errorf("empty command")
	}
	
	cmdName := cmd[0]
	cmdArgs := cmd[1:]
	switch cmdName {
	case "commands":
		// Return just the command names
		var names []string
		for _, cmd := range sc.commands {
			names = append(names, cmd.Name)
		}
		jsonResult, _ := json.Marshal(names)
		return string(jsonResult), env, nil

	case "commandargs":
		// Return args for a specific command
		if len(cmdArgs) < 1 {
			return "", env, fmt.Errorf("commandargs requires command name")
		}
		targetCmd := cmdArgs[0]

		for _, cmdInfo := range sc.commands {
			if cmdInfo.Name == targetCmd {
				jsonResult, _ := json.Marshal(cmdInfo.Args)
				return string(jsonResult), env, nil
			}
		}
		return "[]", env, nil

	case "commanddescribe":
		// Return description for a specific command
		if len(cmdArgs) < 1 {
			return "", env, fmt.Errorf("commanddescribe requires command name")
		}
		targetCmd := cmdArgs[0]

		for _, cmdInfo := range sc.commands {
			if cmdInfo.Name == targetCmd {
				return cmdInfo.Description, env, nil
			}
		}
		return "No description available", env, nil
		
	case "state":
		// Return component state
		return sc.GetState(), env, nil
	}

	return "", env, fmt.Errorf("not a standard command")
}

func (sc *StandardComponent) ProcessMessage(rawJSON []byte) ([]byte, string, error) {
	wrapper := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
		// Always try standard commands first 
		if len(cmdParts) > 0 {
			// Check for standard discovery commands first
			standardCmd := cmdParts[0]
			if standardCmd == "commands" || standardCmd == "commandargs" || 
			   standardCmd == "commanddescribe" || standardCmd == "state" {
				return sc.handleDiscoveryCommands(cmdParts, env)
			}
		}

		// If not a standard command, process using the component's handler
		return sc.processCmd(cmdParts, env)
	}

	response, nextComponent, err := ProcessComponentMessage(rawJSON, sc.id, wrapper)
	if err != nil {
        // Improve error logging here
        debugf("Component %s error processing message: %v\n", sc.id, err)
        // Make sure the error is returned properly
        return nil, "", fmt.Errorf("component %s error: %w", sc.id, err)
    }

	// After processing, save state to file
	sc.saveState()

	return response, nextComponent, err
}

// Load state from file
func (sc *StandardComponent) loadState() {
	// Check if state file exists
	if _, err := os.Stat(sc.stateFile); os.IsNotExist(err) {
		debugf("No state file found for %s\n", sc.id)
		return
	}

	// Read state file
	data, err := os.ReadFile(sc.stateFile)
	if err != nil {
		debugf("Error reading state file for %s: %v\n", sc.id, err)
		return
	}

	// Unmarshal state
	if err := sc.unmarshalState(string(data)); err != nil {
		debugf("Error unmarshaling state for %s: %v\n", sc.id, err)
		return
	}

	debugf("Loaded state for %s from %s\n", sc.id, sc.stateFile)
}

// Save state to file
func (sc *StandardComponent) saveState() {
	// Marshal state
	state, err := sc.marshalState()
	if err != nil {
		debugf("Error marshaling state for %s: %v\n", sc.id, err)
		return
	}

	// Write to file atomically
	// First write to temp file, then rename
	tempFile := sc.stateFile + ".tmp"
	if err := os.WriteFile(tempFile, []byte(state), 0644); err != nil {
		debugf("Error writing temp state file for %s: %v\n", sc.id, err)
		return
	}

	if err := os.Rename(tempFile, sc.stateFile); err != nil {
		debugf("Error renaming temp state file for %s: %v\n", sc.id, err)
		return
	}

	debugf("Saved state for %s to %s\n", sc.id, sc.stateFile)
}

// Helper to set debug mode
func (sc *StandardComponent) SetDebug(enabled bool) {
	sc.debugMode = enabled
}