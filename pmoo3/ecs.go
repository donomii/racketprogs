package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/donomii/chim"
)

// ComponentNode represents a network node running a component
type ComponentNode struct {
	ID               string
	IncomingMessages chan []byte
}

func NewComponentNode(id string) *ComponentNode {
	return &ComponentNode{
		ID:               id,
		IncomingMessages: make(chan []byte, 10),
	}
}

// GetID returns the node's identifier
func (node *ComponentNode) GetID() string {
	return node.ID
}

// Base component interface - each component implements this
type Component interface {
	GetID() string
	ProcessMessage([]byte) ([]byte, string, error)
	GetState() string // Add this method
}

// ComponentNetwork simulates the distributed network
type ComponentNetwork struct {
	Nodes map[string]*ComponentNode
}

func NewComponentNetwork() *ComponentNetwork {
	return &ComponentNetwork{
		Nodes: make(map[string]*ComponentNode),
	}
}

func (cn *ComponentNetwork) AddComponent(comp Component) {
	node := &ComponentNode{
		ID:               comp.GetID(),
		IncomingMessages: make(chan []byte, 10),
	}
	cn.Nodes[comp.GetID()] = node
}

func (node *ComponentNode) Start(component Component) {
	go func() {
		for msg := range node.IncomingMessages {
			// Process message
			response, nextComponent, err := component.ProcessMessage(msg)
			if err != nil {
				fmt.Printf("Error in component %s: %v\n", node.ID, err)
				continue
			}
			debugf("Component %s processed message %v in Start\n", node.ID, string(response))

			// If we got a response, find next component
			if response != nil {
				var msgData chim.ScriptEnv
				if err := json.Unmarshal(response, &msgData); err != nil {
					fmt.Printf("Error parsing response in %s: %v\n", node.ID, err)
					continue
				}

				// Find next component if we're not at the end
				if msgData.CurrentLine < len(msgData.Lines) {

					if nextComponent != "" {
						// Serialize and send to next component
						msgJSON, err := json.Marshal(msgData)
						if err != nil {
							fmt.Printf("Error serializing message in %s: %v\n", node.ID, err)
							continue
						}
						forwardMessage(nextComponent, msgJSON)
					} else {
						fmt.Printf("Cannot find next component for message %v, dropping it here\n", msgData)
					}
				}
			}
		}
	}()
}

func (cn *ComponentNetwork) StartScript(script []string) error {
	// Create initial message
	msg := chim.NewScript(script)

	firstComponent := "script" //If the first component is not script, pick one at random, they will route it to the correct component

	// Serialize and send to first component
	msgJSON, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("failed to serialize initial message: %v", err)
	}

	startNode, exists := cn.Nodes[firstComponent]
	if !exists {
		return fmt.Errorf("starting component %s not found", firstComponent)
	}

	startNode.IncomingMessages <- msgJSON
	return nil
}
