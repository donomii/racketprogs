package main

import (
    "encoding/json"
    "fmt"
    "gitlab.com/donomii/chim"
    "strconv"
    "strings"
)

type scriptState struct {
    // Script component state (currently empty)
}

func NewScriptComponent(id string, node *ComponentNode) Component {
    state := &scriptState{}

    processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
        if len(cmdParts) == 0 {
            return "", env, fmt.Errorf("empty command")
        }
        
        cmd := cmdParts[0]
        args := cmdParts[1:]
        
        switch cmd {
        case "compare":
            if len(args) < 3 {
                return "", env, fmt.Errorf("compare syntax: compare VALUE1 OP VALUE2")
            }

            value1 := args[0]
            op := args[1]
            value2 := args[2]

            var result bool
            switch op {
            case "==":
                result = value1 == value2
            case "!=":
                result = value1 != value2
            case ">":
                result = value1 > value2
            case ">=":
                result = value1 >= value2
            case "<":
                result = value1 < value2
            case "<=":
                result = value1 <= value2
            default:
                return "", env, fmt.Errorf("invalid operator: %s", op)
            }

            output := "false"
            if result {
                output = "true"
            }
            return output, env, nil
        case "foreach":
            if len(args) < 3 || args[1] != "in" {
                return "", env, fmt.Errorf("foreach syntax: foreach ITEM in COLLECTION")
            }
            
            varName := args[0]
            collectionName := args[2]
            
            // Get the collection from the environment
            collection, err := getCollection(env, collectionName)
            if err != nil {
                return "", env, err
            }
            
            // If the collection is empty, skip the loop body
            if len(collection) == 0 {
                // Find the matching 'endfor' command to skip the loop body
                skipToEndfor(&env, varName)
                return "", env, nil
            }
            
            // Store collection in environment
            collectionVar := fmt.Sprintf("__foreach_%s_items", varName)
            collectionJSON, _ := json.Marshal(collection)
            chim.SetVariable(env, collectionVar, string(collectionJSON))

            // Store index in environment
            indexVar := fmt.Sprintf("__foreach_%s_index", varName)
           chim.SetVariable(env, indexVar, "0")
            
            // Set initial loop variable value
            chim.SetVariable(env, varName, fmt.Sprintf("%v", collection[0]))
            
            return "", env, nil
            
        case "endfor":
            if len(args) < 1 {
                return "", env, fmt.Errorf("endfor syntax: endfor ITEM")
            }
            
            varName := args[0]
            indexVar := fmt.Sprintf("$__foreach_%s_index", varName)
            collectionVar := fmt.Sprintf("$__foreach_%s_items", varName)
            
            // Get current index
            indexStr, exists := env.Variables[indexVar]
            if !exists {
                return "", env, fmt.Errorf("no active foreach loop for %s", varName)
            }
            
            index, err := strconv.Atoi(indexStr)
            if err != nil {
                return "", env, fmt.Errorf("invalid index for %s: %s", varName, indexStr)
            }
            
            // Get collection
            collectionJSON, exists := env.Variables[collectionVar]
            if !exists {
                return "", env, fmt.Errorf("no collection found for %s", varName)
            }
            
            var collection []interface{}
            if err := json.Unmarshal([]byte(collectionJSON), &collection); err != nil {
                return "", env, fmt.Errorf("invalid collection for %s", varName)
            }
            
            // Check if we have more items
            if index+1 >= len(collection) {
                // Loop is done, clean up
                delete(env.Variables, indexVar)
                delete(env.Variables, collectionVar)
                return "", env, nil
            }
            
            // Move to the next item
            index++
            chim.SetVariable(env, fmt.Sprintf("__foreach_%s_index", varName), fmt.Sprintf("%d", index))

            // Set the loop variable
           chim.SetVariable(env, varName, fmt.Sprintf("%v", collection[index]))
            
            // Search backward to find the matching foreach
            for i := env.CurrentLine - 1; i >= 0; i-- {
                line := env.Lines[i]
                if strings.HasPrefix(line, "foreach "+varName+" in") {
                    // Found the matching foreach, jump to it
                    env.CurrentLine = i
                    break
                }
            }
            
            return "", env, nil
            
        default:
            // Delegate to built-in handler for other commands
            return chim.HandleScript(cmdParts, env)
        }
    }
    
    marshalState := func() (string, error) {
        bytes, err := json.Marshal(state)
        return string(bytes), err
    }
    
    unmarshalState := func(data string) error {
        return json.Unmarshal([]byte(data), state)
    }

    // Define script component commands for discoverability
    commands := []CommandInfo{
        {Name: "run", Args: []string{"script"}, Description: "Execute a series of commands"},
        {Name: "if", Args: []string{"condition", "then"}, Description: "Conditional execution"},
        {Name: "else", Args: []string{}, Description: "Else branch for conditional"},
        {Name: "end", Args: []string{}, Description: "End conditional block"},
        {Name: "set", Args: []string{"variable", "value"}, Description: "Set a variable value"},
        {Name: "foreach", Args: []string{"item", "in", "collection"}, Description: "Iterate over a collection"},
        {Name: "endfor", Args: []string{"item"}, Description: "End a foreach loop for the specified item"},
    }

    return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}

// Helper function to get a collection from the environment
func getCollection(env chim.ScriptEnv, collectionName string) ([]interface{}, error) {
    // Check if it's a variable reference
    if strings.HasPrefix(collectionName, "$") {
        varName := collectionName // Keep the $ prefix
        varValue, exists := env.Variables[varName]
        if !exists {
            return nil, fmt.Errorf("variable %s not found", collectionName)
        }
        
        // Try to parse as JSON array
        var items []interface{}
        if err := json.Unmarshal([]byte(varValue), &items); err == nil {
            return items, nil
        }
        
        // If not JSON array, split by spaces or commas
        var result []interface{}
        if strings.Contains(varValue, ",") {
            for _, item := range strings.Split(varValue, ",") {
                result = append(result, strings.TrimSpace(item))
            }
        } else {
            for _, item := range strings.Fields(varValue) {
                result = append(result, item)
            }
        }
        return result, nil
    }
    
    // Direct value (space or comma separated)
    var result []interface{}
    if strings.Contains(collectionName, ",") {
        for _, item := range strings.Split(collectionName, ",") {
            result = append(result, strings.TrimSpace(item))
        }
    } else {
        for _, item := range strings.Fields(collectionName) {
            result = append(result, item)
        }
    }
    return result, nil
}

// Helper function to skip to the matching 'endfor' command
func skipToEndfor(env *chim.ScriptEnv, varName string) {
    targetEndfor := "endfor " + varName
    
    for i := env.CurrentLine + 1; i < len(env.Lines); i++ {
        line := env.Lines[i]
        if strings.HasPrefix(line, targetEndfor) {
            env.CurrentLine = i
            break
        }
    }
}