set IP localhost
set REGISTRY localhost
#go get golang.org/x/oauth2
#go get github.com/google/go-github/v42/github
#go get github.com/emersion/go-imap/client
set regURL [join [list $REGISTRY 8080 ] ":"]
func ip { port | join [list $IP  port] ":" }
background go run . --registry --url regURL
#background koi8rxterm -e  "go run . --display  --url [ip 8087]  -registryurl regURL"
background go run . --events --url [ip 8082]  -registryurl regURL
background go run . --inventory --url [ip 8083]  -registryurl regURL
background go run . --location --url [ip 8084]  -registryurl regURL
background go run . --dbus --url [ip 8085]  -registryurl regURL
background go run . --script --url [ip 8087]  -registryurl regURL
background go run . --github --url [ip 8088]  -registryurl regURL
background go run . --email --url [ip 8089]  -registryurl regURL
go run . --cli --url [ip 8086]  -registryurl regURL 

wait
