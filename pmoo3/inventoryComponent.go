package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/donomii/chim"
)

type inventoryState struct {
    // Map of entity ID -> set of items
    Inventories map[string]map[string]bool `json:"inventories"`
}

func NewInventoryComponent(id string, node *ComponentNode) Component {
    state := &inventoryState{
        Inventories: make(map[string]map[string]bool),
    }

      // Define component commands for discoverability
      commands := []CommandInfo{
        {Name: "insert", Args: []string{"entity_id", "item"}, Description: "Add an item to an entity's inventory"},
        {Name: "remove", Args: []string{"entity_id", "item"}, Description: "Remove an item from an entity's inventory"},
        {Name: "list", Args: []string{"entity_id"}, Description: "List all items in an entity's inventory"},
        {Name: "has", Args: []string{"entity_id", "item"}, Description: "Check if an entity has a specific item"},
    }

    processCmd := func(cmdParts []string, env chim.ScriptEnv) (string, chim.ScriptEnv, error) {
        cmd, err := parseCommand(strings.Join(cmdParts, " "), id)
        panicOnError(err)
        switch cmd.Name {
        case "insert":
            if len(cmd.Args) < 2 {
                return "", env, fmt.Errorf("insert requires entity ID and item")
            }
            entityID := cmd.Args[0]
            item := cmd.Args[1]
            
            // Initialize inventory if needed
            if state.Inventories[entityID] == nil {
                state.Inventories[entityID] = make(map[string]bool)
            }
            
            state.Inventories[entityID][item] = true
            return item, env, nil
            
        case "remove":
            if len(cmd.Args) < 2 {
                return "", env, fmt.Errorf("remove requires entity ID and item")
            }
            entityID := cmd.Args[0]
            item := cmd.Args[1]
            
            if inventory, exists := state.Inventories[entityID]; exists {
                if _, hasItem := inventory[item]; hasItem {
                    delete(inventory, item)
                    return item, env, nil
                }
                return "", env, fmt.Errorf("item %s not in inventory", item)
            }
            return "", env, fmt.Errorf("entity %s has no inventory", entityID)
            
        case "list":
            if len(cmd.Args) < 1 {
                return "", env, fmt.Errorf("list requires entity ID")
            }
            entityID := cmd.Args[0]
            
            if inventory, exists := state.Inventories[entityID]; exists {
                items := make([]string, 0, len(inventory))
                for item := range inventory {
                    items = append(items, item)
                }
                return fmt.Sprint(items), env, nil
            }
            return "", env, nil
            
        case "has":
            if len(cmd.Args) < 2 {
                return "", env, fmt.Errorf("has requires entity ID and item")
            }
            entityID := cmd.Args[0]
            item := cmd.Args[1]
            
            if inventory, exists := state.Inventories[entityID]; exists {
				if _, hasItem := inventory[item]; hasItem {
					return "true", env, nil
				} else {
					return "false", env, nil
				}
            }
            return "false", env, nil

        default:
            return "", env, fmt.Errorf("unknown inventory command: %s", cmd.Name)
        }
    }
    
    marshalState := func() (string, error) {
        bytes, err := json.Marshal(state)
        return string(bytes), err
    }
    
    unmarshalState := func(data string) error {
        return json.Unmarshal([]byte(data), state)
    }
    
    return NewStandardComponent(id, node, processCmd, marshalState, unmarshalState, commands)
}