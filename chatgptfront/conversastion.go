package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"


	"text/template"

	rwkv "../../go-rwkv.cpp"
	"github.com/donomii/goof"
)

// A structure to hold the conversation state
type AIConversationState struct {
	// The context
	Context      *rwkv.RwkvState
	UserText     []string
	BotText      []string
	LastResponse_s string
	Username     string
	Botname      string
	Separator    string
}

var botconversations []Conversation_i

func NewAIConversation(prompt string) *AIConversationState {
	Model := rwkv.LoadFiles("small", "small.tokenizer.json", 8)
	c := AIConversationState{}
	c.Context = Model
	c.Context.ProcessInput(prompt)
	return &c
}

func (c *AIConversationState) MessageIn(message string) string {
	c.UserText = append(c.UserText, message)

	prompt := "\n\n" + c.Username + ": " + message + "\n\n" + c.Botname + ": "
	c.Context.ProcessInput(prompt)
	resp := c.Context.GenerateResponse(512, "\n", 0.2, 1, nil)
	c.BotText = append(c.BotText, resp)
	c.LastResponse_s = resp
	return resp
}

func (c *AIConversationState) TemplateAndProcess(username, botname, separator, promptTemplate string) {

	c.Username = username
	c.Botname = botname
	c.Separator = separator

	type Preamble struct {
		User      string
		Bot       string
		Separator string
	}

	preamble := Preamble{
		User:      c.Username,
		Bot:       c.Botname,
		Separator: c.Separator,
	}

	//collect the template results in a buffer
	var b bytes.Buffer
	t := template.Must(template.New("preamble").Parse(promptTemplate))
	err := t.Execute(&b, preamble)
	if err != nil {
		panic(err)
	}
	pre := b.String()

	c.Context.ProcessInput(pre)

}

func NewConversationWithPrompt(username, botname, separator, promptTemplate string) *AIConversationState {
	c := NewAIConversation("")
	c.TemplateAndProcess(username, botname, separator,promptTemplate)
	return c
}

func (c *AIConversationState) LoadConversation(filename string) {
	var conv AIConversationState
	// Read the conversation from the file
	//if it doesn't exist, create it as an empty data struct
	if _, err := os.Stat("conversation.json"); err == nil {
		fmt.Println("Loading conversation")
		data, err := ioutil.ReadFile("conversation.json")
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(data, &conv)
		if err != nil {
			panic(err)
		}
		for i := 0; i < len(conv.UserText); i++ {
			//fmt.Println(conv.UserText[i])
			//fmt.Println(conv.BotText[i])
			input := "\n\nBob: " + conv.UserText[i] + "\n\nAlice: " + conv.BotText[i]
			fmt.Print(input)
			c.Context.ProcessInput(input)

		}
	}

}

func (c *AIConversationState) SaveConversation(filename string) {
	// Save the conversation to the file
	data, err := json.MarshalIndent(c, "", "    ")
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		panic(err)
	}

}

func (c *AIConversationState) LastResponse() string{
	return c.LastResponse_s
}


// An interface for conversations

type Conversation_i interface {
	MessageIn(message string) string
	TemplateAndProcess(username, botname, separator, promptTemplate string)
	LoadConversation(filename string)
	SaveConversation(filename string)
	LastResponse() string
}

type CmdShellConversationState struct {
	// The context
	Context      *rwkv.RwkvState
	UserText     []string
	BotText      []string
	LastResponse_s string
	Username     string
	Botname      string
	Separator    string

}

func NewCmdShellConversation(prompt string) *CmdShellConversationState {
	
	c := CmdShellConversationState{}
	return &c
}

// Run a command.  The first arg is the path to the executable, the rest are program args.  Returns stdout and stderr mixed together
func Command(cmd string, args []string) string {
	//log.Printf("Running '%v', '%v'", cmd, args)
	out, err := exec.Command(cmd, args...).CombinedOutput()
	if err != nil {
		//fmt.Fprintf(os.Stderr, "IO> %v\n", string(out))
		//fmt.Fprintf(os.Stderr, "E> %v\n", err)
		//os.Exit(1)
	}
	if string(out) != "" {
		//fmt.Fprintf(os.Stderr, "O> %v\n\n", string(out))
	}
	return string(out)
}

func (c *CmdShellConversationState) MessageIn(cmd string) string {
	log.Println("Command", cmd)
	result := goof.Shell(cmd)
	c.LastResponse_s = result
	c.BotText = append(c.BotText, result)
	return result

}

func (c *CmdShellConversationState) TemplateAndProcess(username, botname, separator, promptTemplate string) {



}

func (c *CmdShellConversationState) SaveConversation(filename string) {
	// Save the conversation to the file
	data, err := json.MarshalIndent(c, "", "    ")
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		panic(err)
	}

}

func (c *CmdShellConversationState) LoadConversation(filename string) {
	data ,err:= ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &c)
	if err != nil {
		panic(err)
	}
}

func (c *CmdShellConversationState) LastResponse() string{
	return c.LastResponse_s
}