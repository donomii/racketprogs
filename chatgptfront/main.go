package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type Conversation struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Message struct {
	ID             int    `json:"id"`
	ConversationID int    `json:"conversationId"`
	Content        string `json:"content"`
}

var conversations = []Conversation{
	{ID: 1, Name: "Conversation 1"},
	{ID: 2, Name: "Conversation 2"},
	{ID: 2, Name: "Command shell 1"},
}


var messages = make(map[int][]Message)
var messagesMutex sync.Mutex
var messageIDCounter int

func main() {

	port := flag.String("port", "9081", "port to listen on")
	messages = map[int][]Message{2: []Message{{ID: 1, ConversationID: 1, Content: "Hello!"}}, 1: []Message{{ID: 2, ConversationID: 1, Content: "Hi!"}}}

	botconversations = []Conversation_i{}
	botconversations = append(botconversations, NewConversationWithPrompt("Bob", "Alice", ":", `The following is a verbose detailed conversation between {{ .User }} and a woman, {{ .Bot }}. {{ .Bot }} is intelligent, friendly and likeable. {{ .Bot }} is likely to agree with {{ .User }}.

	{{ .User }}{{ .Separator }} Hello {{ .Bot }}, how are you doing?
	
	{{ .Bot }}{{ .Separator }} Hi {{ .User }}! Thanks, I'm fine. What about you?
	
	{{ .User }}{{ .Separator }} I am very good! It's nice to see you. Would you mind me chatting with you for a while?
	
	{{ .Bot }}{{ .Separator }} Not at all! I'm listening.`))

	botconversations = append(botconversations, NewConversationWithPrompt("Bob", "Alice", ":", `The following is a verbose detailed conversation between {{ .User }} and a woman, {{ .Bot }}. {{ .Bot }} is intelligent, friendly and likeable. {{ .Bot }} is likely to agree with {{ .User }}.

	{{ .User }}{{ .Separator }} Hello {{ .Bot }}, how are you doing?
	
	{{ .Bot }}{{ .Separator }} Hi {{ .User }}! Thanks, I'm fine. What about you?
	
	{{ .User }}{{ .Separator }} I am very good! It's nice to see you. Would you mind me chatting with you for a while?
	
	{{ .Bot }}{{ .Separator }} Not at all! I'm listening.`))
	botconversations = append(botconversations, NewCmdShellConversation(""))
	


	http.HandleFunc("/conversations", getConversations)
	http.HandleFunc("/messages/", getMessages)
	http.HandleFunc("/send", sendMessage)
	http.Handle("/", http.FileServer(http.Dir("./")))

	fmt.Println("Server started on ", *port)
	http.ListenAndServe(":"+*port, nil)
}

func getConversations(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(conversations)
}

func getMessages(w http.ResponseWriter, r *http.Request) {
	convIDStr := r.URL.Path[len("/messages/"):]
	convID, err := strconv.Atoi(convIDStr)
	if err != nil {
		http.Error(w, "Invalid conversation ID", http.StatusBadRequest)
		return
	}

	lastID := 0
	if r.URL.Query().Get("last_id") != "" {
		lastID, _ = strconv.Atoi(r.URL.Query().Get("last_id"))
	}

	messagesMutex.Lock()
	defer messagesMutex.Unlock()

	newMessages := []Message{}
	for _, msg := range messages[convID] {
		if msg.ID > lastID {
			newMessages = append(newMessages, msg)
			
		}
	}

	if len(newMessages) == 0 {
		time.Sleep(1 * time.Second)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(newMessages)
}

func sendMessage(w http.ResponseWriter, r *http.Request) {
	var msg Message
	err := json.NewDecoder(r.Body).Decode(&msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Printf("Received message %+v\n", msg)
	messagesMutex.Lock()
	convID := msg.ConversationID

	messageIDCounter++
	msg.ID = messageIDCounter
	msg.ConversationID = convID
	messages[convID] = append(messages[convID], msg)


	messageIDCounter++
	respId := messageIDCounter
	botconversations[convID].MessageIn(msg.Content)
	response := botconversations[convID].LastResponse()
	fmt.Printf("Bot response: %s\n", response)
	messages[convID] = append(messages[convID], Message{ID:respId, ConversationID: convID, Content: response })

	messagesMutex.Unlock()

	fmt.Printf("Completed message %+v\n", msg)
}

