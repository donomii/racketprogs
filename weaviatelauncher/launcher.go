package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"

	"math/rand"
	"os"
	"os/exec"
	"strings"
	"sync"
    "log"
	"time"
	// "runtime/debug"

	"github.com/donomii/goof"
	"github.com/rivo/tview"
)

type Config struct {
	Modules  map[string]ModuleConfig `json:"modules"`
	Weaviate map[string]string       `json:"weaviate"`
}

type ModuleConfig struct {
	Port               int    `json:"port"`
	WeaviateModuleName string `json:"weaviate-module-name"`
	ModuleURLName      string `json:"module-url-name"`
}

func sliceToMap(env []string) map[string]string {
	envVars := make(map[string]string)
	for _, s := range env {
		parts := strings.SplitN(s, "=", 2)
		if len(parts) == 2 {
			envVars[parts[0]] = parts[1]
		}
	}
	return envVars
}

var (
	logDir string
	logger *log.Logger
)

func init() {
	logDir = "logs"
	if err := os.MkdirAll(logDir, 0755); err != nil {
		log.Fatalf("Failed to create log directory: %v", err)
	}

	logFile, err := os.OpenFile("launcher.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("Failed to open launcher log file: %v", err)
	}
	logger = log.New(io.MultiWriter( logFile), "", log.LstdFlags)
}

func main() {
	instanceCount := flag.Int("instances", 12, "Number of Weaviate instances to launch")
	killDelay := flag.Int("kill-delay", 30, "Delay in seconds before starting to kill nodes")
	restartInterval := flag.Int("restart-interval", 60, "Interval in seconds between node restarts")
	flag.Parse()

	go doui()
    go func() {
        for {
            func() {
                defer func() {
                    if r := recover(); r != nil {
                        log.Println("Recovered from panic in UI update iteration:", r)
                        // Sleep briefly before continuing to avoid tight loop in case of repeated panics
                        time.Sleep(1 * time.Second)
                    }
                }()
    
                time.Sleep(300*time.Millisecond)
                if ui != nil {
                    ui.updateServerList()
                    ui.updateClock()
                    if ui.app != nil {
                        ui.app.Draw()
                    }
                }
            }()
        }
    }()
	logDir := "logs"
	if err := os.MkdirAll(logDir, 0755); err != nil {
		log.Fatalf("Failed to create log directory: %v", err)
	}

	rand.Seed(time.Now().UnixNano())

	config := loadConfig()
	moduleName := selectModule(config)
	baseEnvVars := prepareEnvironment(config, moduleName)

	ui.appendLog(fmt.Sprintf("Starting %d Weaviate instances\n", *instanceCount))

	var managers []*SubprocessManager
	for i := 0; i < *instanceCount; i++ {
		port := 8080 + i
		envVars := copyEnvVars(baseEnvVars)
		updateEnvVars(envVars, i, *instanceCount)

		manager := newSubprocessManager(port, i, envVars)
		manager.start()
		managers = append(managers, manager)
		ui.managers = append(ui.managers, manager)
		time.Sleep(100 * time.Millisecond)
	}

	ui.appendLog( fmt.Sprintf("All instances started. Beginning node killer in %d seconds\n", *killDelay))
	go nodeKiller(managers, *killDelay, *restartInterval)

	// Main program loop
	for {

		time.Sleep(1 * time.Second)
	}
}

func nodeKiller(managers []*SubprocessManager, killDelay, restartInterval int) {
	time.Sleep(time.Duration(killDelay) * time.Second)

	for {
		index := rand.Intn(len(managers))
		logger.Printf("Nodekiller Restarting Weaviate-%d\n", managers[index].index)
		managers[index].restart()
		time.Sleep(time.Duration(restartInterval) * time.Second)
	}
}

func loadConfig() Config {
	appDir := goof.ExecutablePath()
	configFile, err := ioutil.ReadFile(appDir + "/config.json")
	if err != nil {
		fmt.Println("Error reading config file:", err)
		os.Exit(1)
	}

	var config Config
	err = json.Unmarshal(configFile, &config)
	if err != nil {
		fmt.Println("Error parsing config file:", err)
		os.Exit(1)
	}
	return config
}

func selectModule(config Config) string {
	return "none"
	fmt.Println("Select a module to run:")
	moduleNames := make([]string, 0, len(config.Modules)+1)
	moduleNames = append(moduleNames, "none")
	for name := range config.Modules {
		moduleNames = append(moduleNames, name)
	}
	for i, name := range moduleNames {
		fmt.Printf("%d: %s\n", i, name)
	}

	var choice int
	fmt.Print("Enter your choice: ")
	_, err := fmt.Scanf("%d", &choice)
	if err != nil || choice < 0 || choice >= len(moduleNames) {
		fmt.Println("Invalid choice")
		os.Exit(1)
	}

	return moduleNames[choice]
}

func prepareEnvironment(config Config, moduleName string) map[string]string {
	envVars := copyEnvVars(config.Weaviate)
	if moduleName == "none" {
		envVars["ENABLE_MODULES"] = ""
		envVars["DEFAULT_VECTORIZER_MODULE"] = "none"
	} else {
		module := config.Modules[moduleName]
		envVars["ENABLE_MODULES"] = module.WeaviateModuleName
		envVars["DEFAULT_VECTORIZER_MODULE"] = module.WeaviateModuleName
	}
	return envVars
}

func copyEnvVars(original map[string]string) map[string]string {
	copy := make(map[string]string)
	for k, v := range original {
		copy[k] = v
	}
	return copy
}

func updateEnvVars(envVars map[string]string, index, totalInstances int) {
	basePort := 8080
	envVars["DISABLE_TELEMETRY"] = "true    "
	envVars["CONTEXTIONARY_URL"] = "localhost:9999"
	envVars["CLUSTER_HOSTNAME"] = fmt.Sprintf("weaviate-%d", index)
	envVars["CLUSTER_IN_LOCALHOST"] = "true"
	envVars["CLUSTER_GOSSIP_BIND_PORT"] = fmt.Sprintf("%d", 7100+index*2)
	envVars["CLUSTER_DATA_BIND_PORT"] = fmt.Sprintf("%d", 7101+index*2)
	if index != 0 {
		envVars["CLUSTER_JOIN"] = "localhost:7100"
	}
	envVars["AUTOSCHEMA_ENABLED"] = "false"
	envVars["PROMETHEUS_MONITORING_ENABLED"] = "false"
	envVars["PROMETHEUS_MONITORING_PORT"] = fmt.Sprintf("%d", 2112+index)
	envVars["RAFT_PORT"] = fmt.Sprintf("%d", 8300+index*2)
	envVars["RAFT_INTERNAL_RPC_PORT"] = fmt.Sprintf("%d", 8301+index*2)
	envVars["PERSISTENCE_DATA_PATH"] = fmt.Sprintf("./data-%d", index)
	envVars["ORIGIN"] = fmt.Sprintf("http://localhost:%d", basePort+index)
	envVars["BACKUP_FILESYSTEM_PATH"] = fmt.Sprintf("./backup-%d", index)

	var raftJoinPorts []string
	for i := 0; i < totalInstances; i++ {
		raftJoinPorts = append(raftJoinPorts, fmt.Sprintf("weaviate-%d:%d", i, 8300+i*2))
	}
	envVars["RAFT_JOIN"] = strings.Join(raftJoinPorts, ",")

	envVars["RAFT_BOOTSTRAP_EXPECT"] = fmt.Sprintf("%d", totalInstances)
	envVars["GRPC_PORT"] = fmt.Sprintf("%d", 50051+index)
	envVars[envVars["ModuleURLName"]] = fmt.Sprintf("http://localhost:%d", basePort+index)
}

type SubprocessManager struct {
	cmd           *exec.Cmd
	logFile       *os.File
	wantRun       bool
	wantStop      bool
	port          int
	index         int
	mutex         sync.Mutex
	envVars       map[string]string
	isRunning     bool
	stdout        io.ReadCloser
	stderr        io.ReadCloser
	stdoutLogFile *os.File
	stderrLogFile *os.File
}

func newSubprocessManager(port, index int, envVars map[string]string) *SubprocessManager {
	stdoutLogFile, err := os.OpenFile(fmt.Sprintf("%s/weaviate_%d_stdout.log", logDir, index), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		logger.Fatalf("Failed to open stdout log file for Weaviate-%d: %v", index, err)
	}

	stderrLogFile, err := os.OpenFile(fmt.Sprintf("%s/weaviate_%d_stderr.log", logDir, index), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		logger.Fatalf("Failed to open stderr log file for Weaviate-%d: %v", index, err)
	}

	manager := &SubprocessManager{

		port:          port,
		index:         index,
		mutex:         sync.Mutex{},
		envVars:       envVars,
		isRunning:     false,
		stdoutLogFile: stdoutLogFile,
		stderrLogFile: stderrLogFile,
	}
	go manager.monitorOutput()
	go manager.run()
	return manager
}

func (sm *SubprocessManager) run() {
	for {

		sm.mutex.Lock()
		if !sm.wantRun && !sm.wantStop {
			sm.mutex.Unlock()
			continue
		}
		if sm.wantStop {
			sm.wantStop = false
			sm.mutex.Unlock()
			continue
		}

		if sm.isRunning {
			sm.mutex.Unlock()
			time.Sleep(1 * time.Second)
			logger.Printf("Weaviate-%d is already running\n", sm.index)
			continue
		}
		sm.isRunning = true
		sm.mutex.Unlock()

		logger.Printf("Starting Weaviate-%d on port %d\n", sm.index, sm.port)
		sm.cmd = exec.Command("./weaviate", "--host", "0.0.0.0", "--scheme", "http", "--port", fmt.Sprintf("%d", sm.port))

		currentEnv := os.Environ()
		for k, v := range sm.envVars {
			currentEnv = append(currentEnv, fmt.Sprintf("%s=%s", k, v))
		}
		sm.cmd.Env = currentEnv

		var err error
		sm.stdout, err = sm.cmd.StdoutPipe()
		if err != nil {
			logger.Printf("Error creating stdout pipe for Weaviate-%d: %v\n", sm.index, err)
			sm.mutex.Lock()
			sm.isRunning = false
			sm.mutex.Unlock()
			return
		}

		sm.stderr, err = sm.cmd.StderrPipe()
		if err != nil {
			logger.Printf("Error creating stderr pipe for Weaviate-%d: %v\n", sm.index, err)
			sm.mutex.Lock()
			sm.isRunning = false
			sm.mutex.Unlock()
			return
		}

		err = sm.cmd.Start()

		logger.Printf("Weaviate-%d is running on port %d\n", sm.index, sm.port)

		err = sm.cmd.Wait()
		sm.mutex.Lock()
		sm.isRunning = false
		sm.mutex.Unlock()

		if err != nil {
			if exitErr, ok := err.(*exec.ExitError); ok {
				logger.Printf("Weaviate-%d on port %d exited with status: %v\n", sm.index, sm.port, exitErr.ExitCode())
			} else {
				logger.Printf("Weaviate-%d on port %d exited with error: %v\n", sm.index, sm.port, err)
			}
		} else {
			logger.Printf("Weaviate-%d on port %d exited normally\n", sm.index, sm.port)
		}
		//Random number of seconds to wait before checking if we should start or stop

		time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
	}
}

func (sm *SubprocessManager) stop() {
	logger.Printf("Attempting to stop Weaviate-%d\n", sm.index)
	sm.wantStop = true
	sm.wantRun = false

	logger.Printf("Sending interrupt signal to Weaviate-%d\n", sm.index)
	err := sm.cmd.Process.Signal(os.Interrupt)
	if err != nil {
		logger.Printf("Error sending interrupt signal to Weaviate-%d: %v\n", sm.index, err)
	}

	done := make(chan error)
	go func() {
		done <- sm.cmd.Wait()
	}()

	select {
	case err := <-done:
		if err != nil {
			logger.Printf("Weaviate-%d exited with error: %v\n", sm.index, err)
		} else {
			logger.Printf("Weaviate-%d stopped successfully\n", sm.index)
		}
	case <-time.After(10 * time.Second):
		logger.Printf("Timeout waiting for Weaviate-%d to stop, forcing kill\n", sm.index)
		sm.cmd.Process.Kill()
	}

	sm.isRunning = false
	ui.updateStatusBar(fmt.Sprintf("Stopped Weaviate-%d", sm.index))
	logger.Printf("Stop procedure completed for Weaviate-%d\n", sm.index)
}

func (sm *SubprocessManager) restart() {
	sm.stop()
	sm.start()
}

func (sm *SubprocessManager) start() {
	sm.wantRun = true
	ui.updateStatusBar(fmt.Sprintf("Started Weaviate-%d", sm.index))

}

func (sm *SubprocessManager) monitorOutput() {
	go func() {
		for {
			sm.doScan(sm.stdout, sm.stdoutLogFile, "stdout")
		}
	}()
	go func() {
		for {
			sm.doScan(sm.stderr, sm.stderrLogFile, "stderr")
		}
	}()
}

func (sm *SubprocessManager) doScan(pipe io.Reader, logFile *os.File, pipeType string) {
	defer func() {
		if r := recover(); r != nil {
			//stackTrace := debug.Stack()
			//logger.Printf("Recovered from panic in monitorPipe for Weaviate-%d %s: %v\nStack Trace:\n%s\n",
			//    sm.index, pipeType, r, string(stackTrace))

		}
	}()

	scanner := bufio.NewScanner(pipe)
	for scanner.Scan() {
		line := scanner.Text()
		sm.processOutput(line, pipeType)
		fmt.Fprintln(logFile, line)
	}
	if err := scanner.Err(); err != nil {
		//logger.Printf("Error reading %s from Weaviate-%d: %v\n", pipeType, sm.index, err)
	}
}

func (sm *SubprocessManager) processOutput(line, pipeType string) {
	logMsg := fmt.Sprintf("Weaviate-%d (%s): %s", sm.index, pipeType, line)
	ui.appendLog(logMsg)

	if strings.Contains(strings.ToLower(line), "error") {
		ui.updateStatusBar(fmt.Sprintf("Error in Weaviate-%d", sm.index))
	}
}

func mapToSlice(envMap map[string]string) []string {
	var envSlice []string
	for k, v := range envMap {
		envSlice = append(envSlice, fmt.Sprintf("%s=%s", k, v))
	}
	return envSlice
}

// UI
type UI struct {
	app        *tview.Application
	grid       *tview.Grid
	serverList *tview.List
	logView    *tview.TextView
	statusBar  *tview.TextView
	managers   []*SubprocessManager
	clockView  *tview.TextView
}

var ui *UI

func doui() {
	ui = &UI{
		app: tview.NewApplication(),
		serverList: tview.NewList().ShowSecondaryText(false).SetChangedFunc(func(index int, mainText string, secondaryText string, shortcut rune) {
			ui.app.Draw()
		}),
		logView: tview.NewTextView().SetDynamicColors(true).SetScrollable(true).SetChangedFunc(func() {
			ui.app.Draw()
		}),
		statusBar: tview.NewTextView().SetTextAlign(tview.AlignCenter).SetChangedFunc(func() {
			ui.app.Draw()
		}),
		clockView: tview.NewTextView().SetTextAlign(tview.AlignCenter).SetChangedFunc(func() {
			ui.app.Draw()
		}),
	}

	ui.grid = tview.NewGrid().
		SetRows(1, 0, 1).
		SetColumns(30, 0).
		SetBorders(true)

	ui.grid.AddItem(ui.clockView, 0, 0, 1, 2, 0, 0, false)
	ui.grid.AddItem(ui.serverList, 1, 0, 1, 1, 0, 0, true)
	ui.grid.AddItem(ui.logView, 1, 1, 1, 1, 0, 0, false)
	ui.grid.AddItem(ui.statusBar, 2, 0, 1, 2, 0, 0, false)


	go ui.updateClock()

	if err := ui.app.SetRoot(ui.grid, true).Run(); err != nil {
		panic(err)
	}

}
func (ui *UI) updateServerList() {
	if ui != nil {
		if ui.serverList != nil {
			ui.serverList.Clear()
			for i, manager := range ui.managers {
				status := "Stopped"
				if manager.isRunning {
					status = "Running"
				}
				ui.serverList.AddItem(fmt.Sprintf("Weaviate-%d (%s)", i, status), "", rune('0'+i), nil)
			}
		}
	}
    ui.app.Draw()

}

func (ui *UI) updateClock() {
	go func() {
		for {
			time.Sleep(time.Second)

			ui.clockView.SetText(time.Now().Format("2006-01-02 15:04:05"))
            ui.app.Draw()

		}
	}()
}

func (ui *UI) updateStatusBar(text string) {

	ui.statusBar.SetText(text)
    ui.app.ForceDraw()
}

func (ui *UI) appendLog(text string) {
    // Get the visible height of the logView
    _,_,_, height := ui.logView.GetInnerRect()
    maxLines := height - 1 // Subtract 1 to account for the new line being added

    // Get the current content
    currentContent := ui.logView.GetText(false)

    // Split the content into lines
    lines := strings.Split(currentContent, "\n")

    // If we've exceeded the maximum number of lines, remove the oldest ones
    if len(lines) >= maxLines {
        lines = lines[len(lines)-maxLines+1:]
    }

    // Add the new line
    lines = append(lines, text)

    // Join the lines back together
    newContent := strings.Join(lines, "\n")

    // Set the new content
    ui.logView.SetText(newContent)

    // Scroll to the bottom to show the newest logs
    ui.logView.ScrollToEnd()

    // Force a redraw of the application
    ui.app.Draw()

}
