package main

import (
	"flag"
	"fmt"
	"io/ioutil"

	stalk "gitlab.com/donomii/racketprogs/stalk"
)

func main() {
	var evalFile = ""
	flag.StringVar(&evalFile, "f", "", "Compile a file")
	flag.Parse()

	//Loadfile
	code, err := ioutil.ReadFile(evalFile)

	if err != nil {
		panic(err)
	}

	//Parse
	parsed := stalk.ParseStalk(string(code), evalFile)
	prog := stalk.BuildQonProg(parsed.List, 1, false)
	fmt.Println(prog)





}
