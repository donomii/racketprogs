# Stalk

SmallerTalk

## Simpler SmallTalk

SmallTalk is already a simple language, but it can be simpler.  Stalk is an attempt to write my own smalltalk, but small enough for me to finish it in a weekend, and maintain it easily in the future.

## Changes

These changes are probably enough to make Stalk "not a real SmallTalk", which is fine.  It is, at least, inspired by SmallTalk.

### No class

The class hierachy is gone.  Stalk is a prototype-based language.  You can create a new object by cloning an existing object.

The single class hierachy made it extremely difficult to make separate applications and libraries, since they always had to merge, at some point, with the base class hierachy (and every other package in the system).  Prototyped objects make it much easier to draw a line between the system and the application.

### Simpler syntax

There is now only one statement, the usual send to method.  There are no method declaration statements.  The entire language is method sends, lambdas, and literals.

However, type declarations have been added to lambdas, as types make debugging and catching mistakes so mush easier.

There is room to add nicer syntax in the future, but for a small project, this is enough.

### No image

Stalk is entirely text file based, and does not have an image.  This makes it easier to use with version control.  You can collaborate with other people by sending them text files, use the usual diff tools, etc. You can also use your favorite text editor to edit the source code.

### Quickstart

Stalk is a relatively simple version of smalltalk, so the only operations are sending messages to objects.  Everything, including if statements and object definitions, are messages to objects.

```stalk

io print: "Hello World\\n".
io dumpmethods.

```

io is the standard input/output object, and it has a method print: that prints a string to the console.  dumpmethods is a method that prints all the methods in the object.

Every call to a method is a message send, and the syntax is `object argname: argument argname: argument.`.  The argument can be a literal, a variable, or another message send.

All message sends end with a full stop.

```stalk

io println: (string join: (list 1 "a" "c")) .

```

sub-expressions are enclosed in parentheses.

```stalk

io addMethod: [print: string | io printf: "%v" values: (list print) ].

```

You can add a method to any object by calling addMethod: with a lambda.  The lambda is a list of arguments, separated from the body by a pipe, and the body of the lambda is a series of message sends.



```stalk

x = io readFile: "hello.sta".

io print: "\\n\\nMy source code: \\n\\n".
io print: x.

io print: "\\n\\n".

y = "abcde" contains: "a".
y ifTrue: [| io print: "Yes\\n"] ifFalse: [| io print: "Nnnnoooo\\n"].

lexTest = "lextest works\\n".
y ifTrue: [| io print: lexTest] ifFalse: [| io print: lexTest].

z = strings count: "a" in: "abcdea".
io printf: "a occurs %v times\\n" value: z.

```

```stalk
"string"
1
2
3
(list 1 2 3)
(table a: 1 b: 2 c: 3)
```

Stalk has strings, numbers, lists and tables.  Strings are enclosed in double quotes, numbers are just numbers, lists are enclosed in parentheses, and tables are just a method call but the keys aren't defined in advance.
