package stalk

import (
	"fmt"
	
		"strings"
	
		"gitlab.com/donomii/racketprogs/autoparser"
	)

	// Turn a parsed stalk program into a string
func BuildStalk(t []autoparser.Node, indent int, supressBraces bool) string {
	if len(t) == 0 {
		return ""
	}
	if t[0].Kind == "List" {
		if len(t[0].List) == 0 && len(t[1:]) == 0 {
			return "[|]"
		}
		if t[1].Raw == "|" {
			args := t[0].List
			statements := t[1:]
			out := fmt.Sprintf("[%v |\n", list2String(args))

			for i := 0; i < len(statements); i = i + 1 {
				//Add indent
				out = fmt.Sprintf("%v%v", out, strings.Repeat(" ", indent))
				out = fmt.Sprintf("%v%v", out, BuildStalk(statements[i].List, indent+1, true))
			}
			out = fmt.Sprintf("%v]", out)
			return out
		} else {
			out := fmt.Sprintf("(")
			statements := t
			for i := 0; i < len(statements); i = i + 1 {
				//Add indent
				out = fmt.Sprintf("%v%v", out, strings.Repeat(" ", indent))
				out = fmt.Sprintf("%v%v", out, BuildStalk(statements[i].List, indent+1, supressBraces))
			}
			out = fmt.Sprintf("%v)", out)
			return out

		}
	}

	//the first value is the object
	//then key-value pairs

	out := ""
	obj := t[0]
	args := make(map[string]string)
	switch stringify(obj) {
	case "list":
		out = fmt.Sprintf("%v(%v", out, stringify(obj))
		for _, v := range t[1:] {
			if v.Kind == "List" {
				out = fmt.Sprintf("%v %v", out, BuildStalk(v.List, indent+1, false))
			} else {
				out = fmt.Sprintf("%v %v", out, stringify(v))
			}
		}
		out = fmt.Sprintf("%v)", out)
	case "emptyList":
		out = fmt.Sprintf("%v[|]", out)
	default:
		for i := 1; i < len(t); i = i + 2 {
			argWcolon := stringify(t[i])
			arg := argWcolon[:len(argWcolon)-1]
			if t[i+1].List != nil {
				str := ""

				str = str + BuildStalk(t[i+1].List, indent+1, false)

				args[arg] = str
			} else {
				args[arg] = stringify(t[i+1])
			}
		}

		if !supressBraces {
			out = out + "("
		}

		//build method signature
		//sig := makeMethodSignature(t)
		//fmt.Printf("\nsig:%v\n", sig)
		out = fmt.Sprintf("%v%v", out, stringify(obj))
		for k, v := range args {
			out = fmt.Sprintf("%v %v: %v", out, k, v)
		}
		if !supressBraces {
			out = out + ")"
		} else {
			out = out + ".\n"
		}
		out = fmt.Sprintf("%v", out)
	}

	return out

}

// Turn a complete parsed stalk program into a string
func BuildStalkProg(t []autoparser.Node, indent int, printit bool) string {
	out := ""
	for _, v := range t {
		out = fmt.Sprintf("%v%v\n", out, BuildStalk(v.List, indent, true))
	}
	return out
}
