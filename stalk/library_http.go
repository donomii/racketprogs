package stalk

import (
	"net/http"
	"net/url"
	"io"
	"io/ioutil"
	"fmt"
	"os"
)

func makeHttpLibrary() (*StalkObject, *StalkObject) {
	classObj := NewStalkObject("http")
	classObj.SetSlot("parentObject", baseObject)
	httpObj := NewStalkObject("http")
	httpObj.SetSlot("parentObject", baseObject)

	//func Get(url string) (resp *Response, err error)
	classObj.SetSlotToMethod("_get", func(args *StalkObject) *StalkObject {
		url := GetSlotForceType[*StalkObject](args, "get")

		urlString := GetSlotForceType[string](url, "nativeString")

		resp, err := http.Get(urlString)
		//Read the body
		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Printf("Error reading body: %v\n", err)
			os.Exit(1)
		}
		fmt.Printf("%s", body)
		//End read body

		//Return body as string
		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", string(body))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func Head(url string) (resp *Response, err error)
	classObj.SetSlotToMethod("_head", func(args *StalkObject) *StalkObject {
		url := GetSlotForceType[*StalkObject](args, "head")

		urlString := GetSlotForceType[string](url, "nativeString")

		resp, err := http.Head(urlString)
		ret := NewStalkObject("response")
		ret.SetSlot("nativeResponse", resp)
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func Post(url string, bodyType string, body io.Reader) (resp *Response, err error)
	classObj.SetSlotToMethod("_post", func(args *StalkObject) *StalkObject {
		url := GetSlotForceType[*StalkObject](args, "to")
		bodyType := GetSlotForceType[*StalkObject](args, "bodyType")
		body := GetSlotForceType[*StalkObject](args, "post")

		urlString := GetSlotForceType[string](url, "nativeString")
		bodyTypeString := GetSlotForceType[string](bodyType, "nativeString")
		bodyReader := GetSlotForceType[io.Reader](body, "nativeReader")

		resp, err := http.Post(urlString, bodyTypeString, bodyReader)
		ret := NewStalkObject("response")
		ret.SetSlot("nativeResponse", resp)
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func PostForm(url string, data url.Values) (resp *Response, err error)
	classObj.SetSlotToMethod("_postForm", func(args *StalkObject) *StalkObject {
		urlObj := GetSlotForceType[*StalkObject](args, "url")
		data := GetSlotForceType[*StalkObject](args, "data")

		urlString := GetSlotForceType[string](urlObj, "nativeString")
		dataValues := GetSlotForceType[url.Values](data, "nativeValues")

		resp, err := http.PostForm(urlString, dataValues)
		ret := NewStalkObject("response")
		ret.SetSlot("nativeResponse", resp)
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	return classObj, httpObj
}