package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	stalk "gitlab.com/donomii/racketprogs/stalk"
	"gitlab.com/donomii/racketprogs/xsh"
	"gitlab.com/donomii/racketprogs/xsh/lined"
)

func main() {
	var wantTest = false
	var evalFile = ""
	flag.BoolVar(&wantTest, "test", false, "Run tests")
	flag.StringVar(&evalFile, "f", "", "Evaluate a file")
	flag.Parse()

	if evalFile != "" {
		code, err := ioutil.ReadFile(evalFile)
		if err != nil {
			log.Fatalf("Error reading file %v: %v", evalFile, err)
		}
		s := stalk.NewStalk()
		stalk.EvalCode(s, string(code), evalFile)
		return
	}

	test_interpret()

	s := stalk.NewStalk()
	//Make a REPL.  Loop and read lines, and eval them

	lined.Init("")
	log.Println("initialised repl")
	lined.KeyHook = func(key string) {
		if key == "TAB" {
			var str string

			line := lined.InputLine
			//fmt.Println(line)

			if len(line) > 0 {
				if line[0] == []byte(".")[0] {
					line = line[1:]
				} else {
					line = "environment " + line
				}
				partial := stalk.ParseSmall(string(line)+"\n", evalFile)

				//fmt.Printf("Partial parse: |%+v|\n", stalk.ParseStalk(line, "stdin"))
				stalk.PrintTree(partial.List, 1, true)

				list := partial.List

				if len(list) > 0 {
					firstNode := list[0]
					str = xsh.S(firstNode)
				}

				lookupObj := s.GetSlot("environment").(*stalk.StalkObject)

				names := lookupSlotNames(lookupObj, str)

				if len(list) > 1 {
					nextNode := list[1]
					firstObj := lookupObj.GetSlot(str).(*stalk.StalkObject)
					names = lookupMethodNames(firstObj, xsh.S(nextNode))

				}

				for _, name := range names {
					fmt.Printf("* %v: \n", name)
				}
				fmt.Print("\n\n\n\n\n\n")

			}
		}

		fmt.Print("STLK")
		fmt.Print(lined.InputLine)
	}
	for {
		fmt.Print("\nSTLK")
		//line, err := rl.Readline()

		//if err != nil { // io.EOF
		//	break
		//}

		line := lined.ReadLine()
		fmt.Println("Read:", line)
		fmt.Println("")
		if line == "" {
			os.Exit(0)
		}
		if line[0] == []byte(".")[0] {
			line = line[1:]
		} else {
			line = "environment " + line
		}
		fmt.Println("Evaluating:", line)
		r := runCode(s, line)
		if r != nil {
			fmt.Println("Result:", r)

		}
		//fmt.Print("\n\n\n\n\n\n")
		fmt.Println("next loop")

	}

}

func runCode(s *stalk.StalkObject, code string) *stalk.StalkObject {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	return stalk.EvalCode(s, code, "stdin")
}

func lookupSlotNames(s *stalk.StalkObject, str string) []string {
	fmt.Printf("Searching for '%v' in %v\n", str, s.Name)

	slots := s.AllSlots()

	var out []string
	for slotName, _ := range slots {
		if str == "" {
			//fmt.Println(slotName)
			out = append(out, slotName)
		} else {
			if strings.HasPrefix(slotName, str) || slotName == str {
				//fmt.Println(slotName)
				out = append(out, slotName)
			}
		}
	}
	return out
}

func lookupMethodNames(s *stalk.StalkObject, str string) []string {
	fmt.Printf("Searching for method '%v' in %v\n", str, s.Name)

	slots := s.AllSlots()

	var out []string
	for slotName, _ := range slots {
		//Split method names on _

		if str == "" {
			out = append(out, slotName)
		} else {
			//fmt.Printf("Checking %v\n", slotName)
			names := strings.Split(slotName, "_")
			//fmt.Printf("Split %v into %v\n", slotName, names)
			//if first element is "", remove it
			if len(names) > 0 && names[0] == "" {
				names = names[1:]
			}
			if len(names) > 0 {
				if str == "" {
					out = append(out, slotName)
				} else {
					//fmt.Printf("Checking %v\n", names[0])
					if strings.HasPrefix(names[0], str) || names[0] == str {
						//fmt.Println(slotName)
						out = append(out, slotName)
						//object = append(object, (*slot).(*stalk.StalkObject))
					}
				}
			}
		}
	}
	return out
}

func test_interpret() {
	code := `

	io addMethod: [print: string | io printf: "%v" values: (list print) ].
	io addMethod: [printf: string value: string | io printf: printf values: (list value) ].



	io
		printf: "%v"
		values: (list "Finished definitions\\n").

	io print: "if statement true - ".
	if test: true
		then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "if statement false - ".
	if test: false
		then: [| io println: "fail"] else: [| io println: "pass"].

	io print: "1 eq: 1 - ".
	if test: (1 eq: 1) then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "join string list - ".
	if test: ("hello there user" eq: (string join: (list "hello " "there " "user")))
		then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "set variable, get with method - ".
	environment set: "x" to: 1.
	if test: (1 eq: (environment get: "x"))
		then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "set variable, access directly - ".
	environment set: "x" to: 2.
	if test: (2 eq: x)
		then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "prevent setting outer variable, from lambda - ".
	environment set: "l" to: [| x = 3 ].
	l -activate.
	if test: (2 eq: x) then: [| io println: "pass"] else: [| io println: "fail"].

	io print: "return number from lambda - ".
	environment set: "l" to: [| 4 ].
	if test: (4 eq: (l -activate)) then: [| io println: "pass"] else: [| io println: "fail"].


	io print: "apply lambda to named args object - ".
	environment set: "t" to: ( table a: "apply " b: "works " c: "now" ).
	apply
		args: t
		to: [ a: string b: string c: string |
			temp = string join: (list a b c).
			if test: ("apply works now" eq: temp) then: [| io println: "pass"] else: [| io println: "fail"]
		].

	io print: "apply lambda to table - ".
	activate table: ( table a: "table " b: "activation " c: "works" ) with: [ a: string b: string c: string | temp = string join: (list a b c). if test: ("table activation works" eq: temp) then: [| io println: "pass"] else: [| io println "fail"] ].

	activate list: (list "list" "activation" "works\\n") with: [ a: int b: int c: int | io printf: "%v %v %v" values: (list a b c) ].



   `
	s := stalk.NewStalk()
	stalk.EvalCode(s, code, "testcode")
}

//

func test_translate() {
	code := `
	includes are: (list q/boolean.qon q/base.qon q/compiler.qon q/macros.qon q/lists.qon q/newparser.qon).
	types are: [|].
	functions are: [|

		func name: greet returns: void vars: [| declare name: x type: int initially: 1.  declare name: y type: int initially: 2. declare name: z type: int initially: 0. ] body: [ name: string |
			z = (add a: x b: y).
  			if test: (equal a: x b: y) then: [| printf this: "hello " ] else: [| printf this: "goodbye " ].
  			printf this: name.
  			printf this: "\\n".
  			printf format: "%d\n" value: (add a: x b: y)
  		].

		func name: start returns: int vars: [|] body: [| greet name: "Jeremy" ].
	].
.
   `

	code = stalk.StringTransform(code)
	fmt.Println(code)
	n := stalk.ParseSexpr(code, "testsmall.go")
	//fmt.Printf("%+v\n\n\n", n)
	fmt.Println("tree output\n")
	stalk.PrintTree(n.List, 1, true)
	fmt.Println("Stalk output\n")
	fmt.Println(stalk.BuildStalkProg(n.List, 1, true))
	fmt.Println("Qon output\n")
	fmt.Println("(", stalk.BuildQonProg(n.List, 1, true), ")")

	fmt.Println("Stalk:\n", stalk.BuildXshProg(n.List, 1, ""))

	//Make a REPL.  Loop and read lines, parse them, and convert them to xsh

}
