package stalk

import (
	"io"
	"strconv"
)

func makeIOlibrary() (*StalkObject, *StalkObject) {
	ioObj := NewStalkObject("io")
	ioObj.SetSlot("parentObject", baseObject)

	classObj := NewStalkObject("io")
	classObj.SetSlot("parentObject", baseObject)

	//func Copy(dst Writer, src Reader) (written int64, err error)
	ioObj.SetSlotToMethod("_copyTo", func(args *StalkObject) *StalkObject {
		dst := GetSlotForceType[*StalkObject](args, "copyTo")
		src := GetSlotForceType[*StalkObject](args, "self")

		dstWriter := GetSlotForceType[io.Writer](dst, "nativeWriter")
		srcReader := GetSlotForceType[io.Reader](src, "nativeReader")

		written, err := io.Copy(dstWriter, srcReader)
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(written))
		ret.SetSlot("nativeString", strconv.Itoa(int(written)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func CopyBuffer(dst Writer, src Reader, buf []byte) (written int64, err error)
	ioObj.SetSlotToMethod("_copyBuffer", func(args *StalkObject) *StalkObject {
		dst := GetSlotForceType[*StalkObject](args, "to")
		src := GetSlotForceType[*StalkObject](args, "copyBuffer")
		buf := GetSlotForceType[*StalkObject](args, "buffer")

		dstWriter := GetSlotForceType[io.Writer](dst, "nativeWriter")
		srcReader := GetSlotForceType[io.Reader](src, "nativeReader")
		bufBytes := GetSlotForceType[[]byte](buf, "nativeBytes")

		written, err := io.CopyBuffer(dstWriter, srcReader, bufBytes)
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(written))
		ret.SetSlot("nativeString", strconv.Itoa(int(written)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func CopyN(dst Writer, src Reader, n int64) (written int64, err error)
	ioObj.SetSlotToMethod("_copyN", func(args *StalkObject) *StalkObject {
		dst := GetSlotForceType[*StalkObject](args, "to")
		src := GetSlotForceType[*StalkObject](args, "from")
		n := GetSlotForceType[*StalkObject](args, "n")

		dstWriter := GetSlotForceType[io.Writer](dst, "nativeWriter")
		srcReader := GetSlotForceType[io.Reader](src, "nativeReader")
		nInt := GetSlotForceType[float64](n, "nativeNumber")

		written, err := io.CopyN(dstWriter, srcReader, int64(nInt))
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(written))
		ret.SetSlot("nativeString", strconv.Itoa(int(written)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func LimitReader(r Reader, n int64) Reader
	ioObj.SetSlotToMethod("_limitReader", func(args *StalkObject) *StalkObject {
		r := GetSlotForceType[*StalkObject](args, "reader")
		n := GetSlotForceType[*StalkObject](args, "n")

		rReader := GetSlotForceType[io.Reader](r, "nativeReader")
		nInt := GetSlotForceType[float64](n, "nativeNumber")

		ret := NewStalkObject("reader")
		ret.SetSlot("nativeReader", io.LimitReader(rReader, int64(nInt)))
		return ret
	})

	//func MultiReader(readers ...Reader) Reader
	ioObj.SetSlotToMethod("_multiReader", func(args *StalkObject) *StalkObject {
		readers := GetSlotForceType[*StalkObject](args, "readers")
		readersList := GetSlotForceType[[]*StalkObject](readers, "nativeArray")

		var readersNative []io.Reader
		for _, v := range readersList {
			readersNative = append(readersNative, GetSlotForceType[io.Reader](v, "nativeReader"))
		}

		ret := NewStalkObject("reader")
		ret.SetSlot("nativeReader", io.MultiReader(readersNative...))
		return ret
	})

	//func MultiWriter(writers ...Writer) Writer
	ioObj.SetSlotToMethod("_multiWriter", func(args *StalkObject) *StalkObject {
		writers := GetSlotForceType[*StalkObject](args, "writers")
		writersList := GetSlotForceType[[]*StalkObject](writers, "nativeArray")

		var writersNative []io.Writer
		for _, v := range writersList {
			writersNative = append(writersNative, GetSlotForceType[io.Writer](v, "nativeWriter"))
		}

		ret := NewStalkObject("writer")
		ret.SetSlot("nativeWriter", io.MultiWriter(writersNative...))
		return ret
	})

	//func NewSectionReader(r ReaderAt, off int64, n int64) *SectionReader
	ioObj.SetSlotToMethod("_newSectionReader", func(args *StalkObject) *StalkObject {
		r := GetSlotForceType[*StalkObject](args, "reader")
		off := GetSlotForceType[*StalkObject](args, "off")
		n := GetSlotForceType[*StalkObject](args, "n")

		rReader := GetSlotForceType[io.ReaderAt](r, "nativeReaderAt")
		offInt := GetSlotForceType[float64](off, "nativeNumber")
		nInt := GetSlotForceType[float64](n, "nativeNumber")

		ret := NewStalkObject("reader")
		ret.SetSlot("nativeReader", io.NewSectionReader(rReader, int64(offInt), int64(nInt)))
		return ret
	})

	//func Pipe() (*PipeReader, *PipeWriter)
	classObj.SetSlotToMethod("_pipe", func(args *StalkObject) *StalkObject {
		ret := NewStalkObject("readerWriter")
		r, w := io.Pipe()
		ret.SetSlot("nativeReader", r)
		ret.SetSlot("nativeWriter", w)
		return ret
	})

	//func ReadAtLeast(r Reader, buf []byte, min int) (n int, err error)
	ioObj.SetSlotToMethod("_readAtLeast", func(args *StalkObject) *StalkObject {
		r := GetSlotForceType[*StalkObject](args, "reader")
		buf := GetSlotForceType[*StalkObject](args, "buffer")
		min := GetSlotForceType[*StalkObject](args, "min")

		rReader := GetSlotForceType[io.Reader](r, "nativeReader")
		bufBytes := GetSlotForceType[[]byte](buf, "nativeBytes")
		minInt := GetSlotForceType[float64](min, "nativeNumber")

		n, err := io.ReadAtLeast(rReader, bufBytes, int(minInt))
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(n))
		ret.SetSlot("nativeString", strconv.Itoa(int(n)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func ReadFull(r Reader, buf []byte) (n int, err error)
	ioObj.SetSlotToMethod("_readFull", func(args *StalkObject) *StalkObject {
		r := GetSlotForceType[*StalkObject](args, "reader")
		buf := GetSlotForceType[*StalkObject](args, "buffer")

		rReader := GetSlotForceType[io.Reader](r, "nativeReader")
		bufBytes := GetSlotForceType[[]byte](buf, "nativeBytes")

		n, err := io.ReadFull(rReader, bufBytes)
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(n))
		ret.SetSlot("nativeString", strconv.Itoa(int(n)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	//func WriteString(w Writer, s string) (n int, err error)
	ioObj.SetSlotToMethod("_writeString", func(args *StalkObject) *StalkObject {
		w := GetSlotForceType[*StalkObject](args, "self")
		s := GetSlotForceType[*StalkObject](args, "string")

		wWriter := GetSlotForceType[io.Writer](w, "nativeWriter")
		sString := GetSlotForceType[string](s, "nativeString")

		n, err := io.WriteString(wWriter, sString)
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(n))
		ret.SetSlot("nativeString", strconv.Itoa(int(n)))
		if err != nil {
			ret.SetSlot("nativeError", err)
		}
		return ret
	})

	return ioObj, nil
}
