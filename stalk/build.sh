#!/bin/sh

rm go.mod
rm go.sum
rm stalk

go mod init gitlab.com/donomii/racketprogs/stalk
go mod tidy
go get github.com/chzyer/readline
go get github.com/weaviate/weaviate/
go get gitlab.com/donomii/racketprogs/xsh
go mod tidy


go build cmd/stalk.go
go build other/stalk2qon/stalk2qon.go 
./stalk -f hello.sta  
