package stalk

import (
	"strings"
	"fmt"
	"strconv"
)



func makeStringLibrary() (*StalkObject, *StalkObject) {
	str := NewStalkObject("string")
	str.SetSlot("parentObject", baseObject)

	objStr := NewStalkObject("string")
	objStr.SetSlot("parentObject", baseObject)


	//Class methods

	str.SetSlotToMethod("_join", func(args *StalkObject) *StalkObject {
		l := GetSlotForceType[*StalkObject](args, "join")
		list := GetSlotForceType[[]*StalkObject](l, "nativeArray")
		out := ""
		for _, v := range list {
			out = fmt.Sprintf("%v%v", out, GetSlotForceType[string](v, "nativeString"))
		}
		ret := NewStalkObject("joinedString")
		ret.SetSlot("nativeString", out)
		return ret
	})


	//func Compare(a, b string) int
	str.SetSlot("_compare_with", func(args *StalkObject) *StalkObject {
		a := GetSlotForceType[*StalkObject](args, "compare")
		b := GetSlotForceType[*StalkObject](args, "with")
		aStr := GetSlotForceType[string](a, "nativeString")
		bStr := GetSlotForceType[string](b, "nativeString")
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.Compare(aStr, bStr)))
		return ret
	})
	str.SetSlotType("_compare_with", "Method")

	//func Count(s, substr string) int
	str.SetSlot("_count_in", func(args *StalkObject) *StalkObject {
		s := GetSlotForceType[*StalkObject](args, "in")
		substr := GetSlotForceType[*StalkObject](args, "count")
		sStr := GetSlotForceType[string](s, "nativeString")
		substrStr := GetSlotForceType[string](substr, "nativeString")
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.Count(sStr, substrStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.Count(sStr, substrStr)))
		return ret
	})
	str.SetSlotType("_count_in", "Method")


	//Object methods
	//func Contains(s, substr string) bool
	objStr.SetSlot("_contains", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		substr := GetSlotForceType[*StalkObject](args, "contains")
		substrStr := GetSlotForceType[string](substr, "nativeString")

		answer := strings.Contains(selfStr, substrStr)
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_contains", "Method")

	//func ContainsAny(s, chars string) bool
	objStr.SetSlot("_containsAny", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		chars := GetSlotForceType[*StalkObject](args, "containsAny")
		charsStr := GetSlotForceType[string](chars, "nativeString")

		answer := strings.ContainsAny(selfStr, charsStr)
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_containsAny", "Method")

	//func ContainsRune(s string, r rune) bool
	objStr.SetSlot("_containsRune", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		r := GetSlotForceType[*StalkObject](args, "containsRune")
		rStr := GetSlotForceType[string](r, "nativeString")
		rRune, _ := strconv.Atoi(rStr)

		answer := strings.ContainsRune(selfStr, rune(rRune))
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_containsRune", "Method")

	//func Count(s, substr string) int
	objStr.SetSlot("_count_in", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		substr := GetSlotForceType[*StalkObject](args, "count")
		substrStr := GetSlotForceType[string](substr, "nativeString")
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.Count(selfStr, substrStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.Count(selfStr, substrStr)))
		return ret
	})
	objStr.SetSlotType("_count_in", "Method")

	//func EqualFold(s, t string) bool
	objStr.SetSlot("_equalFold", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		t := GetSlotForceType[*StalkObject](args, "equalFold")
		tStr := GetSlotForceType[string](t, "nativeString")

		answer := strings.EqualFold(selfStr, tStr)
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_equalFold", "Method")

	//func Fields(s string) []string
	objStr.SetSlot("_fields", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.Fields(selfStr) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_fields", "Method")

	//func FieldsFunc(s string, f func(rune) bool) []string
	objStr.SetSlot("_fieldsFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		f := GetSlotForceType[*StalkObject](args, "fieldsFunc")

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.FieldsFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		}) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_fieldsFunc", "Method")

	//func HasPrefix(s, prefix string) bool
	objStr.SetSlot("_hasPrefix", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		prefix := GetSlotForceType[*StalkObject](args, "hasPrefix")
		prefixStr := GetSlotForceType[string](prefix, "nativeString")

		answer := strings.HasPrefix(selfStr, prefixStr)
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_hasPrefix", "Method")

	//func HasSuffix(s, suffix string) bool
	objStr.SetSlot("_hasSuffix", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		suffix := GetSlotForceType[*StalkObject](args, "hasSuffix")
		suffixStr := GetSlotForceType[string](suffix, "nativeString")

		answer := strings.HasSuffix(selfStr, suffixStr)
		if answer {
			return GetSlotForceType[*StalkObject](args, "true")
		} else {
			return GetSlotForceType[*StalkObject](args, "false")
		}
	})
	objStr.SetSlotType("_hasSuffix", "Method")

	//func Index(s, substr string) int
	objStr.SetSlot("_index", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		substr := GetSlotForceType[*StalkObject](args, "index")
		substrStr := GetSlotForceType[string](substr, "nativeString")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.Index(selfStr, substrStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.Index(selfStr, substrStr)))
		return ret
	})
	objStr.SetSlotType("_index", "Method")

	//func IndexAny(s, chars string) int
	objStr.SetSlot("_indexAny", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")
		chars := GetSlotForceType[*StalkObject](args, "indexAny")
		charsStr := GetSlotForceType[string](chars, "nativeString")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.IndexAny(selfStr, charsStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.IndexAny(selfStr, charsStr)))
		return ret
	})
	objStr.SetSlotType("_indexAny", "Method")

	//func IndexByte(s string, c byte) int
	objStr.SetSlot("_indexByte", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		selfStr := GetSlotForceType[string](self, "nativeString")

		c := GetSlotForceType[*StalkObject](args, "indexByte")
		cStr := GetSlotForceType[string](c, "nativeString")
		cByte, _ := strconv.Atoi(cStr)

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.IndexByte(selfStr, byte(cByte))))
		ret.SetSlot("nativeString", strconv.Itoa(strings.IndexByte(selfStr, byte(cByte))))
		return ret
	})
	objStr.SetSlotType("_indexByte", "Method")

	//func IndexFunc(s string, f func(rune) bool) int
	objStr.SetSlot("_indexFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		f := GetSlotForceType[*StalkObject](args, "indexFunc")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.IndexFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		})))
		ret.SetSlot("nativeString", strconv.Itoa(strings.IndexFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		})))
		return ret
	})
	objStr.SetSlotType("_indexFunc", "Method")

	//func IndexRune(s string, r rune) int
	objStr.SetSlot("_indexRune", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		r := GetSlotForceType[*StalkObject](args, "indexRune")
		rStr := GetSlotForceType[string](r, "nativeString")
		rRune, _ := strconv.Atoi(rStr)

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.IndexRune(selfStr, rune(rRune))))
		ret.SetSlot("nativeString", strconv.Itoa(strings.IndexRune(selfStr, rune(rRune))))
		return ret
	})
	objStr.SetSlotType("_indexRune", "Method")

	//func Join(elems []string, sep string) string
	objStr.SetSlot("_join", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		sep := GetSlotForceType[*StalkObject](args, "join")
		sepStr := GetSlotForceType[string](sep, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Join(strings.Fields(selfStr), sepStr))
		return ret
	})
	objStr.SetSlotType("_join", "Method")

	//func LastIndex(s, substr string) int
	objStr.SetSlot("_lastIndex", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		substr := GetSlotForceType[*StalkObject](args, "lastIndex")
		substrStr := GetSlotForceType[string](substr, "nativeString")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.LastIndex(selfStr, substrStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.LastIndex(selfStr, substrStr)))
		return ret
	})
	objStr.SetSlotType("_lastIndex", "Method")

	//func LastIndexAny(s, chars string) int
	objStr.SetSlot("_lastIndexAny", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		chars := GetSlotForceType[*StalkObject](args, "lastIndexAny")
		charsStr := GetSlotForceType[string](chars, "nativeString")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.LastIndexAny(selfStr, charsStr)))
		ret.SetSlot("nativeString", strconv.Itoa(strings.LastIndexAny(selfStr, charsStr)))
		return ret
	})
	objStr.SetSlotType("_lastIndexAny", "Method")

	//func LastIndexByte(s string, c byte) int
	objStr.SetSlot("_lastIndexByte", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		c := GetSlotForceType[*StalkObject](args, "lastIndexByte")
		cStr := GetSlotForceType[string](c, "nativeString")
		cByte, _ := strconv.Atoi(cStr)

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.LastIndexByte(selfStr, byte(cByte))))
		ret.SetSlot("nativeString", strconv.Itoa(strings.LastIndexByte(selfStr, byte(cByte))))
		return ret
	})
	objStr.SetSlotType("_lastIndexByte", "Method")

	//func LastIndexFunc(s string, f func(rune) bool) int
	objStr.SetSlot("_lastIndexFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		f := GetSlotForceType[*StalkObject](args, "lastIndexFunc")

		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(strings.LastIndexFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		})))
		ret.SetSlot("nativeString", strconv.Itoa(strings.LastIndexFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		})))
		return ret
	})
	objStr.SetSlotType("_lastIndexFunc", "Method")

	//func Map(mapping func(rune) rune, s string) string
	objStr.SetSlot("_map", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		mapping := GetSlotForceType[*StalkObject](args, "map")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Map(func(r rune) rune {
			return mapping.GetSlot("_activate").(func(r rune) rune)(r)
		}, selfStr))
		return ret
	})
	objStr.SetSlotType("_map", "Method")

	//func Repeat(s string, count int) string
	objStr.SetSlot("_repeat", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")
		count := GetSlotForceType[*StalkObject](args, "repeat")
		countStr := GetSlotForceType[string](count, "nativeString")
		countInt, _ := strconv.Atoi(countStr)

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Repeat(selfStr, countInt))
		return ret
	})
	objStr.SetSlotType("_repeat", "Method")

	//func Replace(s, old, new string, n int) string
	objStr.SetSlot("_replace", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		old := GetSlotForceType[*StalkObject](args, "replace")
		oldStr := GetSlotForceType[string](old, "nativeString")

		new := GetSlotForceType[*StalkObject](args, "with")
		newStr := GetSlotForceType[string](new, "nativeString")

		n := GetSlotForceType[*StalkObject](args, "times")
		nStr := GetSlotForceType[string](n, "nativeString")
		nInt, _ := strconv.Atoi(nStr)

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Replace(selfStr, oldStr, newStr, nInt))
		return ret
	})
	objStr.SetSlotType("_replace", "Method")

	//func ReplaceAll(s, old, new string) string
	objStr.SetSlot("_replaceAll", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		old := GetSlotForceType[*StalkObject](args, "replaceAll")
		oldStr := GetSlotForceType[string](old, "nativeString")

		new := GetSlotForceType[*StalkObject](args, "with")
		newStr := GetSlotForceType[string](new, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.ReplaceAll(selfStr, oldStr, newStr))
		return ret
	})
	objStr.SetSlotType("_replaceAll", "Method")

	//func Split(s, sep string) []string
	objStr.SetSlot("_split", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		sep := GetSlotForceType[*StalkObject](args, "split")
		sepStr := GetSlotForceType[string](sep, "nativeString")

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.Split(selfStr, sepStr) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_split", "Method")

	//func SplitAfter(s, sep string) []string
	objStr.SetSlot("_splitAfter", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		sep := GetSlotForceType[*StalkObject](args, "splitAfter")
		sepStr := GetSlotForceType[string](sep, "nativeString")

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.SplitAfter(selfStr, sepStr) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_splitAfter", "Method")

	//func SplitAfterN(s, sep string, n int) []string
	objStr.SetSlot("_splitAfterN", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		sep := GetSlotForceType[*StalkObject](args, "splitAfterN")
		sepStr := GetSlotForceType[string](sep, "nativeString")

		n := GetSlotForceType[*StalkObject](args, "times")
		nStr := GetSlotForceType[string](n, "nativeString")
		nInt, _ := strconv.Atoi(nStr)

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.SplitAfterN(selfStr, sepStr, nInt) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_splitAfterN", "Method")

	//func SplitN(s, sep string, n int) []string
	objStr.SetSlot("_splitN", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		sep := GetSlotForceType[*StalkObject](args, "splitN")
		sepStr := GetSlotForceType[string](sep, "nativeString")

		n := GetSlotForceType[*StalkObject](args, "times")
		nStr := GetSlotForceType[string](n, "nativeString")
		nInt, _ := strconv.Atoi(nStr)

		ret := NewStalkObject("list")
		retList := []*StalkObject{}
		for _, v := range strings.SplitN(selfStr, sepStr, nInt) {
			retList = append(retList, NewStringObject(v))
		}
		ret.SetSlot("nativeArray", retList)
		return ret
	})
	objStr.SetSlotType("_splitN", "Method")


	//func Title(s string) string
	objStr.SetSlot("_title", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Title(selfStr))
		return ret
	})
	objStr.SetSlotType("_title", "Method")

	//func ToLower(s string) string
	objStr.SetSlot("_toLower", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.ToLower(selfStr))
		return ret
	})
	objStr.SetSlotType("_toLower", "Method")

	//func ToTitle(s string) string
	objStr.SetSlot("_toTitle", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.ToTitle(selfStr))
		return ret
	})
	objStr.SetSlotType("_toTitle", "Method")


	objStr.SetSlotType("_toTitleSpecial", "Method")

	//func ToUpper(s string) string
	objStr.SetSlot("_toUpper", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.ToUpper(selfStr))
		return ret
	})
	objStr.SetSlotType("_toUpper", "Method")


	//func ToValidUTF8(s, replacement string) string
	objStr.SetSlot("_toValidUTF8", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		replacement := GetSlotForceType[*StalkObject](args, "toValidUTF8")
		replacementStr := GetSlotForceType[string](replacement, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.ToValidUTF8(selfStr, replacementStr))
		return ret
	})
	objStr.SetSlotType("_toValidUTF8", "Method")


	//func Trim(s string, cutset string) string
	objStr.SetSlot("_trim", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		cutset := GetSlotForceType[*StalkObject](args, "trim")
		cutsetStr := GetSlotForceType[string](cutset, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.Trim(selfStr, cutsetStr))
		return ret
	})



	//func TrimFunc(s string, f func(rune) bool) string
	objStr.SetSlot("_trimFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		f := GetSlotForceType[*StalkObject](args, "trimFunc")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		}))
		return ret
	})

	//func TrimLeft(s string, cutset string) string
	objStr.SetSlot("_trimLeft", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		cutset := GetSlotForceType[*StalkObject](args, "trimLeft")
		cutsetStr := GetSlotForceType[string](cutset, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimLeft(selfStr, cutsetStr))
		return ret
	})

	//func TrimLeftFunc(s string, f func(rune) bool) string
	objStr.SetSlot("_trimLeftFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		f := GetSlotForceType[*StalkObject](args, "trimLeftFunc")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimLeftFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		}))
		return ret
	})

	//func TrimPrefix(s, prefix string) string
	objStr.SetSlot("_trimPrefix", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		prefix := GetSlotForceType[*StalkObject](args, "trimPrefix")
		prefixStr := GetSlotForceType[string](prefix, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimPrefix(selfStr, prefixStr))
		return ret
	})

	//func TrimRight(s string, cutset string) string
	objStr.SetSlot("_trimRight", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		cutset := GetSlotForceType[*StalkObject](args, "trimRight")
		cutsetStr := GetSlotForceType[string](cutset, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimRight(selfStr, cutsetStr))
		return ret
	})

	//func TrimRightFunc(s string, f func(rune) bool) string
	objStr.SetSlot("_trimRightFunc", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		f := GetSlotForceType[*StalkObject](args, "trimRightFunc")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimRightFunc(selfStr, func(r rune) bool {
			return f.GetSlot("_activate").(func(r rune) bool)(r)
		}))
		return ret
	})

	//func TrimSpace(s string) string
	objStr.SetSlot("_trimSpace", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimSpace(selfStr))
		return ret
	})

	//func TrimSuffix(s, suffix string) string
	objStr.SetSlot("_trimSuffix", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		suffix := GetSlotForceType[*StalkObject](args, "trimSuffix")
		suffixStr := GetSlotForceType[string](suffix, "nativeString")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", strings.TrimSuffix(selfStr, suffixStr))
		return ret
	})

	//func Reader(s string) *Reader
	objStr.SetSlot("_reader", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("reader")
		ret.SetSlot("nativeReader", strings.NewReader(selfStr))
		return ret
	})

	//func NewReader(s string) *Reader
	objStr.SetSlot("_newReader", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		selfStr := GetSlotForceType[string](self, "nativeString")

		ret := NewStalkObject("reader")
		ret.SetSlot("nativeReader", strings.NewReader(selfStr))
		return ret
	})
	return str, objStr
}