package stalk

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/donomii/racketprogs/autoparser"
)

var baseObject *StalkObject

// ParseStalk takes a string of stalk code and parses it into a autoparser.Node
func ParseStalk(code, filename string) autoparser.Node {
	code = StringTransform(code)
	n := ParseSexpr(code, filename)
	return n
}

// EvalCode takes a string of stalk code and evaluates it in the context of a baseObject
func EvalCode(baseObject *StalkObject, code, filename string) *StalkObject {

	code = code + "\n" //Due to parser weirdness, we need a newline at the end of the file
	code = StringTransform(code)
	n := ParseSexpr(code, filename)
	environment := GetSlotForceType[*StalkObject](baseObject, "environment")

	return evalStalkProg(n.List, environment)
}

// There is only one data structure needed for stalk, the object
type StalkObject struct {
	Name  string
	Slots map[string]*interface{}
	Types map[string]string
}

// Create a new stalk object
func NewStalkObject(name string) *StalkObject {
	return &StalkObject{Name: name, Slots: make(map[string]*interface{}), Types: make(map[string]string)}
}

// SetSlot sets a slot in a stalk object
func (s *StalkObject) SetSlot(name string, value interface{}) {
	s.Slots[name] = &value
}

func (s *StalkObject) AllSlots() map[string]*interface{} {
	return s.Slots
}

// GetSlot gets a slot from a stalk object
func (s *StalkObject) GetSlot(name string) interface{} {
	val, ok := s.Slots[name]
	if !ok {
		//fmt.Printf("Slot %v not found in object %v\n", name, s.Name)
		return nil
	}
	if val == nil {
		return nil
	}
	return *val
}

// GetSlotForceType gets a slot from a stalk object and casts it to the type specified
func GetSlotForceType[V any](s *StalkObject, name string) V {
	val := s.GetSlot(name)
	if val == nil {
		//fmt.Printf("Slot %v not found in object %v\n", name, s.Name)
		panic("Slot " + name + " not found in: " + s.Name)
	}
	return val.(V)

}

// SetType sets a type in a stalk object
func (s *StalkObject) SetSlotType(name string, value string) {
	s.Types[name] = value
}

// GetSlotType gets a type from a stalk object
func (s *StalkObject) GetSlotType(name string) string {
	return s.Types[name]
}

// Shallow copy of an environment
func cloneEnvironment(environment *StalkObject) *StalkObject {
	return cloneStalkObject(environment)
}

// Shallow copy of an object
func cloneStalkObject(obj *StalkObject) *StalkObject {
	newObj := NewStalkObject(obj.Name)
	//assign all the entries from old object to the new object
	for k, v := range obj.Slots {
		//fmt.Printf("Copying %v\n", k)
		newObj.Slots[k] = v
	}
	return newObj
}

// Create new environment that is b merged into a, with b taking pecedence
func mergeEnvironments(a, b *StalkObject) *StalkObject {
	newObj := NewStalkObject(a.Name)
	for k, v := range a.Slots {
		//fmt.Printf("Copying %v\n", k)
		newObj.Slots[k] = v
	}
	for k, v := range b.Slots {
		//fmt.Printf("Copying %v\n", k)
		newObj.Slots[k] = v
	}
	return newObj
}

// In order to call a function, we build a hashmap of the argument names and their values.  This is passed to the lambda as part of the environment
func makeArgMap(t []autoparser.Node, environment *StalkObject) *StalkObject {
	//Copy environment

	args := cloneEnvironment(environment)
	args.Name = "args"
	//Handle unary (odd number of args)
	addArgsToObject(t, args, environment)
	return args
}

func addArgsToObject(t []autoparser.Node, args *StalkObject, environment *StalkObject) {
	for i := 1; i < len(t); i = i + 2 {
		var arg string = "Failed arg parse"
		if strings.HasSuffix(stringify(t[i]), ":") {
			argWcolon := stringify(t[i])
			arg = argWcolon[:len(argWcolon)-1]
		} else {
			if strings.HasPrefix(stringify(t[i]), "-") {
				arg = stringify(t[i])[1:]
			} else {
				arg = stringify(t[i])
			}
		}
		if i+1 < len(t) {
			if t[i+1].List != nil {
				args.SetSlot(arg, evalStalk(t[i+1].List, environment))
			} else {
				ret := evalStalkListOrAtom(t[i+1], environment)
				args.SetSlot(arg, ret)
			}
		}
	}
}

// EvalStalk takes a parsed sexpr and evaluates it in the context of an environment
func evalStalk(t []autoparser.Node, environment *StalkObject) *StalkObject {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Error at line %v: %v\n", t[0].Line, r)
			//Rethrow the error
			panic(r)
		}
	}()

	//An empty pair of brackets like () is an empty list
	if len(t) == 0 {
		ret := NewStalkObject("empty")
		ret.SetSlot("nativeString", "")
		return ret
	}

	//A list, possibly a function
	if t[0].Kind == "List" {

		//If the parameters of the function are empty, and the body of the function is empty, then it's an empty function
		if len(t[0].List) == 0 && len(t[2:]) == 0 {
			ret := NewStalkObject("emptyFunc")
			ret.SetSlot("nativeString", "[|]")
			return ret
		}

		//All functions have | as the second element
		if len(t) > 1 && t[1].Raw == "|" {
			//Build a lambda object
			ret := NewStalkObject("lambda")

			//Need to copy old environment, add args on top
			newEnv := cloneEnvironment(environment)

			args := t[0].List //New environment?

			ret.SetSlot("args", args)
			statements := t[1:]
			ret.SetSlot("statements", statements)

			//Build signature from args
			sig := makeMethodSignature(args)
			ret.SetSlot("signature", sig)

			//activation func
			f := func(args *StalkObject) *StalkObject {
				// Merge the args into NewEnv
				args = mergeEnvironments(newEnv, args)
				var out *StalkObject

				for i := 0; i < len(statements); i = i + 1 {

					out = evalStalk(statements[i].List, args)
				}
				return out
			}
			ret.SetSlot("_activate", f)
			ret.SetSlot(sig, f)
			//fmt.Printf("Created lambda with signature %v\n", sig)
			return ret
		} else {
			// it's a pair of ()s
			var out []*StalkObject
			statements := t
			for i := 0; i < len(statements); i = i + 1 {
				out = append(out, evalStalk(statements[i].List, environment))
			}
			//fmt.Printf("Building list literal from %v\n", out)
			ret := NewStalkObject("list")
			ret.SetSlot("nativeArray", out)
			ret.SetSlot("parentObject", baseObject)
			return ret

		}
	}

	//the first value is the object
	//then key-value pairs are the arguments to the function

	out := &StalkObject{}
	objName := t[0]

	switch stringify(objName) {
	//List gathers its arguments into an object
	case "list":
		l := []*StalkObject{}
		for _, v := range t[1:] {
			l = append(l, evalStalkListOrAtom(v, environment))
		}
		//fmt.Println("Building list object from ", l)
		ret := NewStalkObject("list")
		ret.SetSlot("parentObject", baseObject)
		ret.SetSlot("nativeArray", l)
		out = ret

	case "emptyList":
		ret := NewStalkObject("emptyList")
		ret.SetSlot("parentObject", baseObject)
		ret.SetSlot("nativeString", "[|]")
		out = ret
		//Table gathers its arguments into an object, where each key becomes a slot
	case "table":
		ret := NewStalkObject("table")
		ret.SetSlot("nativeString", "[table]")
		ret.SetSlot("parentObject", baseObject)
		addArgsToObject(t, ret, environment)
		out = ret
	default:
		//Evaluate the statement here
		if len(t) == 1 {
			//It's a symbol to be looked up, or a literal
			//FIXME Detect symbol better, return literals, throw error on unknown symbol
			if environment.GetSlot(stringify(objName)) != nil {
				out = GetSlotForceType[*StalkObject](environment, stringify(objName))
			} else {
				out = evalStalkListOrAtom(objName, environment)

			}
			return out
		}

		//Second special syntax, set a variable using =
		//Eventually I can replace this with a macro, after I add macros
		if stringify(t[1]) == "=" {
			//It's a set
			varObj := t[0]
			varName := stringify(varObj)
			//Build list from rest of t
			l := t[2:]
			varVal := evalStalk(l, environment)
			environment.SetSlot(varName, varVal)
			return varVal
		}

		//Look up objName in the environment
		var obj *StalkObject
		if environment.GetSlot(stringify(objName)) == nil {
			//Handle number here
			obj = evalStalkListOrAtom(objName, environment)
			//Special case numbers
			if obj.Name != "valNumber" && obj.Name != "valString" {
				fmt.Printf("Object %v not found in environment\n", stringify(objName))
				return out
			}
		} else {
			//Handle symbol here
			obj = GetSlotForceType[*StalkObject](environment, stringify(objName))
		}

		//The signature for the method call (i.e. the name)
		sig := makeMethodSignature(t[1:])

		//Recursively lookup signature in object
		retMethod_i := recursiveMethodLookup(obj, sig)
		if retMethod_i == nil {
			fmt.Printf("%v %v: Method %v not found in object or ancestors of %v\n", t[0].File, t[0].Line, sig, stringify(objName))
			return out
		}
		retMethod := retMethod_i.(func(args *StalkObject) *StalkObject)

		//Prepare the environment for the method call
		args := makeArgMap(t, environment)
		args.SetSlot("self", obj)

		//Do the call
		out = retMethod(args)
	}
	return out
}

// Recursively look up a method in an object and its ancestors
func recursiveMethodLookup(obj *StalkObject, sig string) interface{} {
	retMethod := obj.GetSlot(sig)
	if retMethod != nil {
		//fmt.Printf("Method %v found in %v\n", sig, obj.Name)
		return retMethod
	}
	parentObj := obj.GetSlot("parentObject")
	if parentObj == nil {
		//fmt.Printf("Method %v not found in object or ancestors %v\n", sig, obj.Name)
		return nil
	}
	if parentObj == obj {
		//Loop detected
		//fmt.Printf("Method %v not found in object or ancestors %v\n", sig, obj.Name)
		return nil
	}
	//fmt.Printf("%v not found in %v.  Looking up %v in %v\n",sig, obj.Name, sig, parentObj.Name)
	return recursiveMethodLookup(parentObj.(*StalkObject), sig)
}

// Converts a parsed sexpr into a value, or a list of values
func evalStalkListOrAtom(t autoparser.Node, environment *StalkObject) *StalkObject {
	if t.Kind == "List" {
		return evalStalk(t.List, environment)
	} else {
		switch t.Kind {
		case "STRING":
			str := t.Str
			str = strings.ReplaceAll(str, "\\n", "\n")
			str = strings.ReplaceAll(str, "\\t", "\t")
			ret := NewStalkObject("valString")
			ret.SetSlot("nativeString", str)
			//Get stringMethods from environment
			stringMethods := GetSlotForceType[*StalkObject](environment, "stringMethods")
			ret.SetSlot("parentObject", stringMethods)
			return ret
		case "NUMBER":
			ret := NewStalkObject("valNumber")
			ret.SetSlot("nativeNumber", stringify(t))
			ret.SetSlot("nativeString", stringify(t))
			ret.SetSlot("parentObject", baseObject)
			return ret
		case "PARSED":
			//Detect if it's a symbol or a number
			s := stringify(t)
			//Convert to number
			n, err := strconv.ParseFloat(s, 64)
			if err == nil {
				ret := NewStalkObject("valNumber")
				ret.SetSlot("nativeNumber", n)
				ret.SetSlot("nativeString", s)
				ret.SetSlot("parentObject", baseObject)
				return ret
			}

			ret := GetSlotForceType[*StalkObject](environment, stringify(t))
			return ret
		default:
			ret := NewStalkObject("valDefault")
			ret.SetSlot("nativeString", stringify(t))
			ret.SetSlot("parentObject", baseObject)
			return ret
		}
	}
}

// Run the body of the program or block of code
func evalStalkProg(t []autoparser.Node, environment *StalkObject) *StalkObject {
	var out *StalkObject
	for _, v := range t {
		out = evalStalk(v.List, environment)
	}
	return out
}

// Creates the base environment and sets up a few useful methods
func NewStalk() *StalkObject {

	environment := NewStalkObject("environment")
	environment.SetSlot("environment", environment)

	baseObject = NewStalkObject("base")
	baseObject.SetSlot("nativeString", "base")

	environment.SetSlot("parentObject", baseObject)

	baseObject.SetSlotToMethod("_addMethod", func(args *StalkObject) *StalkObject {
		obj := GetSlotForceType[*StalkObject](args, "self")
		lambda := GetSlotForceType[*StalkObject](args, "addMethod")

		sig := GetSlotForceType[string](lambda, "signature")
		obj.SetSlot(sig, GetSlotForceType[func(args *StalkObject) *StalkObject](lambda, "_activate"))
		return obj
	})

	baseObject.SetSlotToMethod("_new", func(args *StalkObject) *StalkObject {
		obj := NewStalkObject("new")
		obj.SetSlot("parentObject", baseObject)
		return obj
	})

	baseObject.SetSlotToMethod("_eq", func(args *StalkObject) *StalkObject {
		obj := GetSlotForceType[*StalkObject](args, "self")
		objAsString := GetSlotForceType[string](obj, "nativeString")
		val := GetSlotForceType[*StalkObject](args, "eq")
		valAsString := GetSlotForceType[string](val, "nativeString")

		if valAsString == objAsString {
			return GetSlotForceType[*StalkObject](args, "true")
		}
		return GetSlotForceType[*StalkObject](args, "false")
	})

	baseObject.SetSlotToMethod("_clone", func(args *StalkObject) *StalkObject {
		obj := GetSlotForceType[*StalkObject](args, "self")
		newObj := cloneStalkObject(obj)
		return newObj
	})

	io := NewStalkObject("io")
	io.SetSlot("parentObject", baseObject)
	io.SetSlotToMethod("_printf_values", func(args *StalkObject) *StalkObject {
		fms := GetSlotForceType[*StalkObject](args, "printf")
		fm := GetSlotForceType[string](fms, "nativeString")
		valuesS := GetSlotForceType[*StalkObject](args, "values")
		values := GetSlotForceType[[]*StalkObject](valuesS, "nativeArray")
		vs := []interface{}{}
		for _, v := range values {
			vs = append(vs, v.GetSlot("nativeString"))
		}
		outStr := fmt.Sprintf(fm, vs...)
		fmt.Printf("%v", outStr)
		return NewStalkObject("empty")
	})

	io.SetSlotToMethod("_readFile", func(args *StalkObject) *StalkObject {
		filename := GetSlotForceType[*StalkObject](args, "readFile")
		filenameAsString := GetSlotForceType[string](filename, "nativeString")
		contents, err := os.ReadFile(filenameAsString)
		if err != nil {
			fmt.Printf("Error reading file %v: %v\n", filenameAsString, err)
			return NewStalkObject("empty")
		}
		ret := NewStalkObject("valString")
		ret.SetSlot("nativeString", string(contents))
		return ret
	})

	str := NewStalkObject("string")
	str.SetSlot("parentObject", baseObject)
	str.SetSlotToMethod("_join", func(args *StalkObject) *StalkObject {
		l := GetSlotForceType[*StalkObject](args, "join")
		list := GetSlotForceType[[]*StalkObject](l, "nativeArray")
		out := ""
		for _, v := range list {
			out = fmt.Sprintf("%v%v", out, GetSlotForceType[string](v, "nativeString"))
		}
		ret := NewStalkObject("joinedString")
		ret.SetSlot("nativeString", out)
		return ret
	})

	apply1 := NewStalkObject("apply")
	apply1.SetSlot("parentObject", baseObject)
	apply1.SetSlotToMethod("_args_to", func(args *StalkObject) *StalkObject {
		lambdaObj := GetSlotForceType[*StalkObject](args, "to")
		lambda := GetSlotForceType[func(args *StalkObject) *StalkObject](lambdaObj, "_activate")

		argObject := GetSlotForceType[*StalkObject](args, "args")
		args.SetSlot("self", lambda)
		//Copy all the args into the new environment
		for k := range argObject.Slots {
			v := argObject.GetSlot(k)
			args.SetSlot(k, v)
		}
		return lambda(args)
	})

	activate := NewStalkObject("activate")
	activate.SetSlot("parentObject", baseObject)
	activate.SetSlotToMethod("_table_with", func(args *StalkObject) *StalkObject {
		lambdaObj := GetSlotForceType[*StalkObject](args, "with")
		lambda := GetSlotForceType[func(args *StalkObject) *StalkObject](lambdaObj, "_activate")

		argObject := GetSlotForceType[*StalkObject](args, "table")
		args.SetSlot("self", lambda)
		//Copy all the args into the new environment
		for k := range argObject.Slots {
			v := argObject.GetSlot(k)
			args.SetSlot(k, v)
		}
		return lambda(args)
	})

	activate.SetSlotToMethod("_list_with", func(args *StalkObject) *StalkObject {
		lambdaObj := GetSlotForceType[*StalkObject](args, "with")
		lambda := GetSlotForceType[func(args *StalkObject) *StalkObject](lambdaObj, "_activate")

		argObject := GetSlotForceType[*StalkObject](args, "list")
		args.SetSlot("self", lambda)
		//Copy all the args into the new environment
		l := GetSlotForceType[[]*StalkObject](argObject, "nativeArray")
		//Get the args from the lambda
		argNames := getParameterNames(GetSlotForceType[[]autoparser.Node](lambdaObj, "args"))
		for i, v := range argNames {
			args.SetSlot(v, l[i])
		}
		return lambda(args)
	})

	baseObject.SetSlotToMethod("_set_to", func(args *StalkObject) *StalkObject {
		var out *StalkObject
		self := GetSlotForceType[*StalkObject](args, "self")
		varObj := GetSlotForceType[*StalkObject](args, "set")
		varName := GetSlotForceType[string](varObj, "nativeString")
		varVal := GetSlotForceType[*StalkObject](args, "to")
		self.SetSlot(varName, varVal)
		return out
	})

	baseObject.SetSlotToMethod("_get", func(args *StalkObject) *StalkObject {

		varObj := GetSlotForceType[*StalkObject](args, "get")
		varName := GetSlotForceType[string](varObj, "nativeString")

		self := GetSlotForceType[*StalkObject](args, "self")
		var valObj *StalkObject
		if self.GetSlot(varName) != nil {
			valObj = self.GetSlot(varName).(*StalkObject)
		} else {
			valObj = environment.GetSlot(varName).(*StalkObject)
		}

		return valObj
	})

	baseObject.SetSlotToMethod("_dumpmethods", func(args *StalkObject) *StalkObject {
		for obj_int := args.GetSlot("self"); obj_int != nil && obj_int.(*StalkObject) != nil; obj_int = obj_int.(*StalkObject).GetSlot("parentObject") {
			obj := obj_int.(*StalkObject)
			fmt.Printf("Methods for %v:\n", obj.Name)

			for k := range obj.Slots {

				fmt.Printf("	%v\n", k)
			}
		}
		return NewStalkObject("empty")
	})

	baseObject.SetSlotToMethod("_ls", func(args *StalkObject) *StalkObject {
		for obj_int := args.GetSlot("self"); obj_int != nil && obj_int.(*StalkObject) != nil; obj_int = obj_int.(*StalkObject).GetSlot("parentObject") {
			obj := obj_int.(*StalkObject)
			fmt.Printf("Methods for %v:\n", obj.Name)

			for k := range obj.Slots {

				fmt.Printf("	%v\n", k)
			}
		}
		return NewStalkObject("empty")
	})

	baseObject.SetSlotToMethod("_dump", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")

		fmt.Printf("Dumping %v\n", self)
		for k := range self.Slots {
			fmt.Printf("	%v: %+v\n", k, self.GetSlot(k))
		}
		return NewStalkObject("empty")
	})

	baseObject.SetSlotToMethod("_printNativeArray", func(args *StalkObject) *StalkObject {
		l := GetSlotForceType[*StalkObject](args, "printNativeArray")
		list := GetSlotForceType[[]*StalkObject](l, "nativeArray")
		for _, v := range list {
			fmt.Printf("%v\n", GetSlotForceType[string](v, "nativeString"))
		}
		return NewStalkObject("empty")
	})

	osObj := NewStalkObject("os")
	osObj.SetSlotToMethod("_exit", func(args *StalkObject) *StalkObject {
		valobj := GetSlotForceType[*StalkObject](args, "exit")
		val := GetSlotForceType[float64](valobj, "nativeNumber")
		os.Exit(int(val))
		return NewStalkObject("empty")
	})

	environment.SetSlot("io", io)
	environment.SetSlot("string", str)
	environment.SetSlot("apply", apply1)
	environment.SetSlot("activate", activate)
	environment.SetSlot("object", baseObject)
	environment.SetSlot("os", osObj)
	environment.SetSlot("NativeArray", makeNativeArray())

	baseObject.SetSlot("environment", environment)

	stringsC, stringsO := makeStringLibrary()
	stringsC.SetSlot("parentObject", baseObject)
	stringsO.SetSlot("parentObject", environment)
	environment.SetSlot("strings", stringsC)
	environment.SetSlot("stringMethods", stringsO)
	httpC, httpO := makeHttpLibrary()
	httpC.SetSlot("parentObject", baseObject)
	httpO.SetSlot("parentObject", environment)
	environment.SetSlot("http", httpC)
	environment.SetSlot("httpMethods", httpO)

	/*  Don't start server until called by program

	weaveC, weaveO := makeWeaviateLibrary()
	weaveC.SetSlot("parentObject", baseObject)
	weaveO.SetSlot("parentObject", environment)
	environment.SetSlot("weaviate", weaveC)
	environment.SetSlot("weaveMethods", weaveO)
	*/
	weaviateClientClass, weaviateClientObject := makeWeaviateClientlibrary(environment)
	weaviateClientClass.SetSlot("parentObject", baseObject)
	weaviateClientObject.SetSlot("parentObject", environment)
	environment.SetSlot("weaviateClientClass", weaviateClientClass)
	environment.SetSlot("weaviateClientObject", weaviateClientObject)

	code := `
io addMethod: [print:   string               | io printf: "%v"    values: (list print)   ].
io addMethod: [println: string               | io printf: "%v\\n" values: (list println) ].
io addMethod: [printf:  string value: string | io printf: printf  values: (list value)   ].

environment
	set: "good"
	to: (object new).

	good addMethod: [value: object |
		obj = object new.

		obj set: "wrappedValue" to: value.
		obj addMethod: [ifGood: lambda |  activate list: (list (self get: "wrappedValue") ) with: ifGood ].
		obj addMethod: [ifGood: lambda ifBad: lambda |  activate list: (list (self get: "wrappedValue") ) with: ifGood ].
		obj addMethod: [ifBad: lambda | ].
		obj
	].

	environment
	set: "bad"
	to: (object new).

	bad addMethod: [value: object |
		obj = object new.

		obj set: "wrappedValue" to: value.
		obj addMethod: [ifGood: lambda | ].
		obj addMethod: [ifGood: lambda ifBad: lambda |  activate list: (list (self get: "wrappedValue") ) with: ifBad ].
		obj addMethod: [ifBad: lambda |  activate list: (list (self get: "wrappedValue") ) with: ifBad ].
		obj
	].

	io print: "Single branch option - ".
	test = good value: "pass".
	test ifGood: [x: object |  io println: x ].

	io print: "Option - ".
	test = good value: "pass".
	test ifGood: [x: object | io println: x ] ifBad: [x: object | io println: "fail"].

	io print:  "Option - ".
	test = bad value: "pass".
	test ifGood: [x: object |  io println: "fail" ].
	test ifGood: [x: object | io println: "fail". ] ifBad: [x: object | io println: x ].


environment
	set: "testMaybe"
	to: [| io printf: "testMaybe\\n" values: (list ) ].


environment
	set: "true"
	to: [ifTrue: lambda ifFalse: lambda| ifTrue -activate ].

environment
	set: "false"
	to: [ifTrue: lambda ifFalse: lambda| ifFalse -activate ].

environment
	set: "if"
	to: [test: bool then: lambda else: lambda| test ifTrue: then ifFalse: else ].



	listTrait = (list "list").
	listTrait addMethod: [join: list | string join: list ].
	listTrait addMethod: [join: list sep: string | string join: list sep: sep ].
	listTrait addMethod: [length: number | int length: list ].
	listTrait addMethod: [tail: list | self get: cdr_elem ].
	listTrait addMethod: [head: object | self get: car_elem ].
	listTrait addMethod: [map: list lambda: lambda |
		if test: ((map length) eq: 0)
			ifTrue: [| list ]
			ifFalse: [| cons
							car: (activate list: (map head) with: lambda)
							cdr: (map tail)]
					].

	listTrait set: "parentObject" to: object.

llist = object new.
llist addMethod: [ head: object | self get: "car_elem" ].
llist addMethod: [ tail: object | self get: "cdr_elem" ].

environment set: "cons" to: (object new).
	cons addMethod: [car: object cdr: object|
						x = table car_elem: car cdr_elem: cdr.
						x set: "parentObject" to: llist.
						x addMethod: [car: object | self get: "car_elem" ].
						x addMethod: [cdr: object | self get: "cdr_elem" ].
						x ].
	`
	EvalCode(baseObject, code, "base")

	return baseObject
}

// Simple create for a stalk string object
func NewStringObject(s string) *StalkObject {
	ret := NewStalkObject("valString")
	ret.SetSlot("nativeString", &s)
	ret.SetSlot("parentObject", baseObject)
	return ret
}

// Simple create for a stalk number object
func NewNumberObject(n float64) *StalkObject {
	ret := NewStalkObject("valNumber")
	ret.SetSlot("nativeNumber", &n)
	ret.SetSlot("parentObject", baseObject)
	return ret
}

// Set the value of a slot in a stalk object, with type
func (o *StalkObject) SetSlotWithType(name string, value interface{}, typ string) {
	o.SetSlot(name, value)
	o.SetSlotType(name, typ)
}

// Set the value of a slot in a stalk object, to a method
func (o *StalkObject) SetSlotToMethod(name string, value func(args *StalkObject) *StalkObject) {
	o.SetSlotWithType(name, value, "Method")
}



// Recursively convert the object to a string
func  toString(o *StalkObject, depth, maxDepth int) string {
	if depth > maxDepth {
		return "..."
	}
	//fmt.Printf("Stringifying %v\n", o.Name)
	out := ""
	for k, v := range o.Slots {
		//fmt.Printf("Stringifying %v(%v)\n", k, o.Types[k])
		key := k
		tipe := o.Types[k]
		switch (*v).(type) {
		case *StalkObject:
			value := toString( (*v).(*StalkObject), depth+1, maxDepth)
			out = fmt.Sprintf("%v%v%v(%v): %v\n", out, sprindent(depth),key, tipe, value)
		case string:
			value := (*v).(string)
			out = fmt.Sprintf("%v%v%v(%v): %v\n", out, sprindent(depth),key, tipe, value)
		case float64:
			value := (*v).(float64)
			out = fmt.Sprintf("%v%v%v(%v): %v\n", out, sprindent(depth),key, tipe, value)
		default:
			value := *v
			out = fmt.Sprintf("%v%v%v(%v): %v\n", out, sprindent(depth),key, tipe, value)
		}
	}
	return out
}

func sprindent(indent int) string {
	out := ""
	for i := 0; i < indent; i++ {
		out = out + " "
	}
	return out
}

func (o *StalkObject) AsString(maxDepth int) string {
	return toString(o, 0, maxDepth)
}