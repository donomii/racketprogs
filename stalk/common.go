package stalk

import (
	"fmt"
	"strings"

	"gitlab.com/donomii/racketprogs/autoparser"
)

// Parse a classic s-expression
func ParseSexpr(code, filename string) autoparser.Node {

	l := autoparser.NewTree(code, filename)
	r, _ := autoparser.MultiStringify(l, []autoparser.StringDelimiter{
		{"/*", "*/", "\\", ""},
		{"\"", "\"", "\\", ""},
		{"'", "'", "\\", ""},
	})

	r, _, _ = autoparser.Groupify(r)
	r = autoparser.MergeNonWhiteSpace(r)
	r = autoparser.StripNL(r)
	r = autoparser.StripWhiteSpace(r)
	//n := autoparser.Node{List: r, File: filename, Line: r[0].Line, Column: r[0].Column, ChrPos: r[0].ChrPos}
	return r[0]
}

// Parse some kind of smalltalk program
func ParseSmall(code, filename string) autoparser.Node {
	l := autoparser.NewTree(code, filename)
	// FIXME we need to run both of these in parallel

	r, _ := autoparser.MultiStringify(l, []autoparser.StringDelimiter{
		{"/*", "*/", "\\", ""},
		{"\"", "\"", "\\", ""},
		{"'", "'", "\\", ""},
	})

	// fmt.Printf("Stringified: ")
	// PrintTree(r, 0, false)
	r, _, _ = autoparser.Groupify(r)
	//r = autoparser.KeywordBreak(r, []string{"|", "|\n"}, false)
	r = autoparser.KeywordBreak(r, []string{"|", "|\n", ".\n", ". ", ".\t", ".]"}, false)
	// r = KeywordBreak(r, []string{"|"})
	r = autoparser.MergeNonWhiteSpace(r)
	r = autoparser.StripNL(r)

	r = autoparser.StripWhiteSpace(r)
	r = autoparser.StripEmptyLists(r)
	// PrintTree(r, 0, false)
	n := autoparser.Node{List: r, File: filename, Line: r[0].Line, Column: r[0].Column, ChrPos: r[0].ChrPos}
	return n
	// fmt.Printf("%+v\n", r)
}



// Search and replace some characters in a stalk program to turn it into s-expressions
func StringTransform(code string) string {
	code = strings.ReplaceAll(code, ".\n", ")\n(")
	code = strings.ReplaceAll(code, ". ", ") (")
	code = strings.ReplaceAll(code, "|", ") | (")
	code = strings.ReplaceAll(code, "[", " ((")
	code = strings.ReplaceAll(code, "]", "))")
	code = strings.ReplaceAll(code, "(() | ())", "(emptyList)")
	return "((" + code + "))"
}

// Print out a parsed program fragment
func PrintTree(t []autoparser.Node, indent int, newlines bool) {
	for _, v := range t {
		if v.List == nil {
			if v.Str == "" {
				// fmt.Print(".")
				fmt.Print(v.Raw, " ")
			} else {
				fmt.Print("『", v.Str, "』")
			}
		} else {
			if len(v.List) == 0 && (v.Note == "\n" || v.Note == "artifact") {
				print("\n")
				printIndent(indent, "_")
				continue
			}
			// If the current expression contains 3 or more sub expressions, break it across lines
			if containsLists(v.List, 3) {
				if countTree(v.List) > 50 {

					fmt.Print("\n")
					printIndent(indent+1, "_")
				}
				fmt.Print("(")

				PrintTree(v.List, indent+2, true)
				print("\n")
				printIndent(indent, "_")
				fmt.Print(")\n")
			} else {
				fmt.Print("(")
				//fmt.Print(v.Note)
				// fmt.Print(v.Note, "current length: ",len(v.List))
				PrintTree(v.List, indent+2, false)
				fmt.Print(")")

			}
		}
		if newlines {
			fmt.Print("\n")
			printIndent(indent, "_")
		}
	}
}

func getParamStr(t []autoparser.Node, param string) string {
	for _, v := range t {
		if v.List == nil {
			if v.Str == param {
				return v.List[0].Raw
			}
		} else {
			if v.Str == param {
				return v.List[0].Raw
			}
			return getParamStr(v.List, param)
		}
	}
	return ""
}

func stringify(n autoparser.Node) string {
	if n.Str != "" {
		return "\"" + n.Str + "\""
	}
	return n.Raw
}

func list2String(l []autoparser.Node) string {
	out := ""
	for _, v := range l {
		out = fmt.Sprintf("%v %v", out, stringify(v))
	}
	return out
}





// build stalk method signature: _arg1_arg2_arg3
func makeMethodSignature(t []autoparser.Node) string {
	if len(t) < 1 {
		return "_activate"
	}
	sig := ""
	for i := 0; i < len(t); i = i + 2 {
		argname := stringify(t[i])
		argname = strings.TrimSuffix(argname, ":")
		argname = strings.TrimPrefix(argname, "-")
		sig = sig + "_" + argname
	}
	return sig
}

// build a list of parameter names from a list of nodes
func getParameterNames(t []autoparser.Node) []string {
	var params []string
	for i := 0; i < len(t); i = i + 2 {
		argname := stringify(t[i])
		argname = strings.TrimSuffix(argname, ":")
		argname = strings.TrimPrefix(argname, "-")
		params = append(params, argname)
	}
	return params
}

// Does list l contain string s?
func ListContains(l []autoparser.Node, s string) bool {
	for _, v := range l {
		if stringify(v) == s {
			return true
		}
	}
	return false
}

// Extract the arguments and body from a lambda definition
func extractArgsBody(t []autoparser.Node) ([]autoparser.Node, []autoparser.Node) {
	var args, body []autoparser.Node
	for i := 1; i < len(t); i = i + 1 {
		args = append(args, t[i])
		if stringify(t[i]) == "|" {
			break
		}
		body = t[i+1:]
	}

	return args, body

}


// Print a string i times
func printIndent(i int, c string) {
	for j := 0; j < i; j++ {
		fmt.Print(c)
	}
}

// Count the number of (sub)lists in this list of Nodes
func containsLists(l []autoparser.Node, max int) bool {
	count := 0
	for _, v := range l {
		if v.List != nil {
			count = count + 1
		}
	}
	return count > max
}

// Recursively count the number of elements in a tree
func countTree(t []autoparser.Node) int {
	count := 0
	for _, v := range t {
		if v.List != nil {
			count = count + countTree(v.List)
		} else {
			count = count + 1
		}
	}
	return count
}
