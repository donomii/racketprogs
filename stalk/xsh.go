package stalk

import (
	"fmt"
	
		"strings"
	
		"gitlab.com/donomii/racketprogs/autoparser"
	)

func extractLocalDefNamesXsh(t []autoparser.Node) string {
	out := ""
	for _, v := range t {
		out = out + " " + stringify(getArgNode(v.List, "name"))
	}
	return out
}

func extractLocalDefValuesXsh(t []autoparser.Node) string {
	out := ""
	for _, v := range t {
		out = out + " " + stringify(getArgNode(v.List, "initially"))
	}
	return out
}


//Convert a parsed program fragment into a string for printing to the user.
func BuildXsh(t []autoparser.Node, indent int, declarations string) string {
	if len(t) == 0 {
		return ""
	}

	if t[0].Kind == "List" {
		if len(t[0].List) == 0 && len(t[1:]) == 0 {
			return "[list]"
		}
		if t[1].Raw == "|" {
			argNodes := t[0].List
			statements := t[1:]

			//Swap each pair of args around, then convert to a string
			args := ""
			for i := 0; i < len(argNodes); i = i + 2 {
				args = fmt.Sprintf("%v %v ", args, stringify(argNodes[i]))
			}

			bodyStr := fmt.Sprintf("{%v|\n%v", args, declarations)

			for i := 0; i < len(statements); i = i + 1 {
				//Add indent
				bodyStr = fmt.Sprintf("%v%v", bodyStr, strings.Repeat(" ", indent))
				bodyStr = fmt.Sprintf("%v%v\n", bodyStr, BuildXsh(statements[i].List, indent+1, ""))
			}
			bodyStr = fmt.Sprintf("%v}", bodyStr)
			return bodyStr

		}
	}

	out := ""
	if t[0].Kind == "List" {
		out = fmt.Sprintf("%v%v", out, BuildXshProg(t[0].List, indent, ""))
	} else {
		//the first value is the object
		//then key-value pairs

		obj := t[0]
		switch stringify(obj) {
		case "if":
			out = fmt.Sprintf("%v%v ", out, stringify(obj))
			out = fmt.Sprintf("%v%v", out, BuildXsh(t[2].List, indent+1, ""))
			out = fmt.Sprintf("%v %v", out, BuildXsh(t[4].List, indent+1, ""))
			out = fmt.Sprintf("%v else %v", out, BuildXsh(t[6].List, indent+1, ""))
		case "declare":
			out = fmt.Sprintf("%v%v ", out, stringify(obj))
			out = fmt.Sprintf("%v with {%v} = {%v} {", out, BuildXsh(t[2].List, indent+1, ""), BuildXsh(t[2].List, indent+1, ""))
		case "func":
			name := GetArg(t, "name:")

			body := getArgNode(t, "body:").List

			/*
				locals := GetArgNode(t, "vars:")

				var localsStr string
				if len(locals.List) > 0 {
					localsStr = PrintXsh(locals.List, indent+1, printit)
				}
			*/

			locals := getArgNode(t, "vars")
			var localsStr string
			if len(locals.List) > 0 {
				localsStr = fmt.Sprintf("with {%v} = {%v} {\n", extractLocalDefNamesXsh(locals.List[1:]), extractLocalDefValuesXsh(locals.List[1:]))
			}

			bodyStr := BuildXsh(body, indent, localsStr)
			out := fmt.Sprintf("func %v  %v", name, bodyStr)

			return out
		case "functions":
			//out = fmt.Sprintf("%v(%v\n", out, stringify(obj))
			out = fmt.Sprintf("%v%v", out, BuildXsh(t[2].List, indent+1, ""))
		case "includes":
		case "types":
		default:
			args := make(map[string]string)
			for i := 1; i < len(t); i = i + 2 {
				argWcolon := stringify(t[i])
				arg := argWcolon[:len(argWcolon)-1]
				if t[i+1].List != nil {
					expr := BuildXsh(t[i+1].List, indent+1, "")
					if expr[0] == '{' {
						args[arg] = expr
					} else {
						args[arg] = "[" + expr + "]"
					}
				} else {
					args[arg] = stringify(t[i+1])
				}
			}

			out = fmt.Sprintf("%v%v", out, stringify(obj))
			for _, v := range args {
				out = fmt.Sprintf("%v %v", out, v)
			}
		}
		out = fmt.Sprintf("%v", out)
	}
	return out

}

//Convert a parsed program into a string for printing to the user.
func BuildXshProg(t []autoparser.Node, indent int, printit string) string {
	out := "(\n"
	for _, v := range t {
		out = fmt.Sprintf("%v%v\n", out, BuildXsh(v.List, indent, printit))
	}
	return out + "\n)"
}
