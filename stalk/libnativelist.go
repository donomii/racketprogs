package stalk

import "strconv"

func makeNativeArray()  *StalkObject {
	listObj := NewStalkObject("NativeArray")
	listObj.SetSlot("parentObject", baseObject)

	listObj.SetSlotToMethod("_get", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		index := GetSlotForceType[*StalkObject](args, "get")
		indexFloat := GetSlotForceType[float64](index, "nativeNumber")
		indexInt := int(indexFloat)
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		return list[indexInt]
	})

	listObj.SetSlotToMethod("_set", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		index := GetSlotForceType[*StalkObject](args, "set")
		indexFloat := GetSlotForceType[float64](index, "nativeNumber")
		indexInt := int(indexFloat)
		value := GetSlotForceType[*StalkObject](args, "to")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		list[indexInt] = value
		return value
	})

	listObj.SetSlotToMethod("_append", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		value := GetSlotForceType[*StalkObject](args, "append")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		list = append(list, value)
		return value
	})

	listObj.SetSlotToMethod("_len", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		ret := NewStalkObject("valNumber")
		ret.SetSlot("nativeNumber", float64(len(list)))
		ret.SetSlot("nativeString", strconv.Itoa(len(list)))
		return ret
	})

	listObj.SetSlotToMethod("_remove", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		index := GetSlotForceType[*StalkObject](args, "remove")
		indexFloat := GetSlotForceType[float64](index, "nativeNumber")
		indexInt := int(indexFloat)
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		list = append(list[:indexInt], list[indexInt+1:]...)
		return self
	})

	listObj.SetSlotToMethod("_insert", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		index := GetSlotForceType[*StalkObject](args, "insert")
		indexFloat := GetSlotForceType[float64](index, "nativeNumber")
		indexInt := int(indexFloat)
		value := GetSlotForceType[*StalkObject](args, "to")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		list = append(list[:indexInt], append([]*StalkObject{value}, list[indexInt:]...)...)
		return self
	})

	listObj.SetSlotToMethod("_pop", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		ret := list[len(list)-1]
		list = list[:len(list)-1]
		return ret
	})

	listObj.SetSlotToMethod("_shift", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		ret := list[0]
		list = list[1:]
		return ret
	})

	listObj.SetSlotToMethod("_unshift", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		value := GetSlotForceType[*StalkObject](args, "unshift")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		list = append([]*StalkObject{value}, list...)
		return self
	})

	listObj.SetSlotToMethod("_slice", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		start := GetSlotForceType[*StalkObject](args, "start")
		end := GetSlotForceType[*StalkObject](args, "end")
		startInt := GetSlotForceType[int](start, "nativeNumber")
		endInt := GetSlotForceType[int](end, "nativeNumber")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		ret := NewStalkObject("list")
		ret.SetSlot("parentObject", baseObject)
		retList := make([]*StalkObject, 0)
		for _, index := range list[startInt:endInt] {
			indexObj := NewStalkObject("valString")
			retList = append(retList, indexObj)
			indexObj.SetSlot("nativeString", index)
			indexObj.SetSlot("parentObject", baseObject)
		}

		ret.SetSlot("nativeArray", retList)
		return ret

	})

	listObj.SetSlotToMethod("_splice", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		start := GetSlotForceType[*StalkObject](args, "start")
		end := GetSlotForceType[*StalkObject](args, "end")
		startInt := GetSlotForceType[int](start, "nativeNumber")
		endInt := GetSlotForceType[int](end, "nativeNumber")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		ret := NewStalkObject("list")
		ret.SetSlot("parentObject", baseObject)
		retList := make([]*StalkObject, 0)
		for _, index := range list[startInt:endInt] {
			indexObj := NewStalkObject("valString")
			retList = append(retList, indexObj)
			indexObj.SetSlot("nativeString", index)
			indexObj.SetSlot("parentObject", baseObject)
		}

		ret.SetSlot("nativeArray", retList)
		list = append(list[:startInt], list[endInt:]...)
		return ret
	})

	listObj.SetSlotToMethod("_reverse", func(args *StalkObject) *StalkObject {
		self := GetSlotForceType[*StalkObject](args, "self")
		list := GetSlotForceType[[]*StalkObject](self, "nativeArray")
		for i := len(list)/2-1; i >= 0; i-- {
			opp := len(list)-1-i
			list[i], list[opp] = list[opp], list[i]
		}
		return self
	})



	listObj.SetSlotToMethod("_new", func(args *StalkObject) *StalkObject {
		ret := NewStalkObject("list")
		ret.SetSlot("parentObject", listObj)
		ret.SetSlot("nativeArray", make([]*StalkObject, 0))
		return ret
	})
	return listObj
}