package stalk

import (
	weaviate "gitlab.com/donomii/weaviate-client"
)


func convertWeaviateMapToStalkObject(listMap map[string]interface{}) *StalkObject {
	ret := NewStalkObject("valTable")
	for key, value := range listMap {
		keyObj := NewStalkObject("valString")
		keyObj.SetSlot("nativeString", key)
		keyObj.SetSlot("parentObject", baseObject)
		valueObj := NewStalkObject("valString")
		valueObj.SetSlot("nativeString", value)
		valueObj.SetSlot("parentObject", baseObject)
		ret.SetSlot(key, valueObj)
	}

	ret.SetSlot("parentObject", baseObject)
	return ret
}

func convertWeaviateListOfMapToStalkObject(results []map[string]interface{}, environment *StalkObject) *StalkObject {
	ret := NewStalkObject("NativeArray")
	retList := make([]*StalkObject, 0)
	for _, index := range results {
		indexObj := convertWeaviateMapToStalkObject(index)
		retList = append(retList, indexObj)
	}

	ret.SetSlot("nativeArray", retList)
	ret.SetSlot("parentObject", GetSlotForceType[*StalkObject](environment, "NativeArray"))
	return ret
}
func makeWeaviateClientlibrary(environment *StalkObject) (*StalkObject, *StalkObject) {
	weaviateObj := NewStalkObject("weaviateClientObject")
	weaviateObj.SetSlot("parentObject", baseObject)

	classObj := NewStalkObject("weaviateClientClass")
	classObj.SetSlot("parentObject", baseObject)

	//func weaviate.Search
	weaviateObj.SetSlotToMethod("_class_hybrid", func(args *StalkObject) *StalkObject {
		query := GetSlotForceType[*StalkObject](args, "hybrid")
		class := GetSlotForceType[*StalkObject](args, "class")

		queryString := GetSlotForceType[string](query, "nativeString")
		classString := GetSlotForceType[string](class, "nativeString")

		results := weaviate.GrpcHybridMap("", classString, queryString, 10)

		return convertWeaviateListOfMapToStalkObject(results, environment)


	})

	weaviateObj.SetSlotToMethod("_class_tenant_hybrid", func(args *StalkObject) *StalkObject {
		query := GetSlotForceType[*StalkObject](args, "hybrid")
		class := GetSlotForceType[*StalkObject](args, "class")
		tenant := GetSlotForceType[*StalkObject](args, "tenant")

		queryString := GetSlotForceType[string](query, "nativeString")
		classString := GetSlotForceType[string](class, "nativeString")
		tenantString := GetSlotForceType[string](tenant, "nativeString")

		results := weaviate.GrpcHybridMap(tenantString, classString, queryString, 10)

		return convertWeaviateListOfMapToStalkObject(results, environment)
	})

	weaviateObj.SetSlotToMethod("_class_tenant_limit_hybrid", func(args *StalkObject) *StalkObject {
		query := GetSlotForceType[*StalkObject](args, "hybrid")
		class := GetSlotForceType[*StalkObject](args, "class")
		tenant := GetSlotForceType[*StalkObject](args, "tenant")
		limit := GetSlotForceType[*StalkObject](args, "limit")

		queryString := GetSlotForceType[string](query, "nativeString")
		classString := GetSlotForceType[string](class, "nativeString")
		tenantString := GetSlotForceType[string](tenant, "nativeString")
		limitInt := GetSlotForceType[int](limit, "nativeInt")

		results := weaviate.GrpcHybridMap(tenantString, classString, queryString, limitInt)

		return convertWeaviateListOfMapToStalkObject(results, environment)
	})

	weaviateObj.SetSlotToMethod("_class_vectorquery", func(args *StalkObject) *StalkObject {
		query := GetSlotForceType[*StalkObject](args, "vectorquery")
		class := GetSlotForceType[*StalkObject](args, "class")

		queryString := GetSlotForceType[string](query, "nativeString")
		classString := GetSlotForceType[string](class, "nativeString")

		results := weaviate.VectorSearchMap( classString, queryString, 10)

		return convertWeaviateListOfMapToStalkObject(results, environment)
	})

	weaviateObj.SetSlotToMethod("_class_bm25", func(args *StalkObject) *StalkObject {
		query := GetSlotForceType[*StalkObject](args, "bm25")
		class := GetSlotForceType[*StalkObject](args, "class")

		queryString := GetSlotForceType[string](query, "nativeString")
		classString := GetSlotForceType[string](class, "nativeString")

		results := weaviate.BM25fPropResultsMap( queryString,classString, 10)

		return convertWeaviateListOfMapToStalkObject(results, environment)
	})



	return classObj, weaviateObj
}
