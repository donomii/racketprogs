package stalk

import (
	"fmt"
	"strings"

	"gitlab.com/donomii/racketprogs/autoparser"
)

func BuildQonProg(t []autoparser.Node, indent int, printit bool) string {
	out := ""
	for _, v := range t {
		out = fmt.Sprintf("%v%v\n", out, BuildQon(v.List, indent, printit))
	}
	return out + ""
}

func GetArgPair(t []autoparser.Node, param string) (string, string) {
	for i := 1; i < len(t); i = i + 2 {
		if stringify(t[i]) == param {
			return stringify(t[i]), stringify(t[i+1])
		}
	}
	return "", ""
}

func GetArg(t []autoparser.Node, param string) string {
	for i := 1; i < len(t); i = i + 2 {
		if stringify(t[i]) == param {
			return stringify(t[i+1])
		}
		if stringify(t[i]) == param+":" {
			return stringify(t[i+1])
		}

		if stringify(t[i]) == "-"+param {
			return stringify(t[i+1])
		}
	}
	return ""
}

func getArgNode(t []autoparser.Node, param string) autoparser.Node {
	for i := 1; i < len(t); i = i + 2 {
		if stringify(t[i]) == param {
			return t[i+1]
		}
		if stringify(t[i]) == param+":" {
			return t[i+1]
		}

		if stringify(t[i]) == "-"+param {
			return t[i+1]
		}
	}
	return autoparser.Node{}
}

func BuildQonListOrScalar(t autoparser.Node, indent int, printit bool) string {
	if t.Kind == "List" {
		return BuildQon(t.List, indent, printit)
	} else {
		return stringify(t)
	}
}

func BuildQon(t []autoparser.Node, indent int, printit bool) string {
	if len(t) == 0 {
		return ""
	}

	if t[0].Kind == "List" {
		if len(t[0].List) == 0 {
			return ""
		}

		if len(t) > 1 && t[1].Raw == "|" {
			//args := t[0].List
			statements := t[2:]
			bodyStr := ""

			bodyStr = fmt.Sprintf("%v%v", bodyStr, BuildQon(t[0].List[1:], indent+1, printit))

			for i := 0; i < len(statements); i = i + 1 {
				//Add indent
				bodyStr = fmt.Sprintf("%v%v", bodyStr, strings.Repeat(" ", indent))
				bodyStr = fmt.Sprintf("%v%v", bodyStr, BuildQon(statements[i].List, indent+1, printit))
			}
			return bodyStr

		}
	}

	out := "("
	if t[0].Kind == "List" {
		out = fmt.Sprintf("%v%v", out, BuildQon(t[0].List, indent, printit))
	} else {
		//the first value is the object
		//then key-value pairs
		sig := makeMethodSignature(t)
		//fmt.Printf("\nsig:%v\n", sig)
		switch sig {
		case "_=":
			out = fmt.Sprintf("%vset %v %v", out, stringify(t[0]), BuildQonListOrScalar(t[2], indent, printit))
			return out + ")"
		}

		scnd_obj := t[0] //FIXME
		if len(t) > 1 {
			scnd_obj = t[1]
		}

		scnd := stringify(scnd_obj)
		if scnd == "=" {

			out = fmt.Sprintf("%vset! %v %v", out, stringify(t[0]), BuildQon(t[2:], indent, printit))
		} else {

			obj := t[0]
			switch stringify(obj) {
			case "emptyList":
				return ""
			case "if":
				out = fmt.Sprintf("%v%v ", out, stringify(obj))
				out = fmt.Sprintf("%v%v", out, BuildQon(t[2].List, indent+1, printit))
				out = fmt.Sprintf("%v (then %v)", out, BuildQonProg(t[4].List, indent+1, printit))
				out = fmt.Sprintf("%v (else %v)", out, BuildQonProg(t[6].List, indent+1, printit))
			case "declare":
				name := GetArg(t, "name")
				typ := GetArg(t, "type")
				init := GetArg(t, "initially")
				out = fmt.Sprintf("%v%v %v %v", out, typ, name, init)

			case "func":
				fmt.Printf("t:%v\n", t)
				name := GetArg(t, "name")
				returnType := GetArg(t, "returns")

				bodyNode := getArgNode(t, "body").List
				body := bodyNode[2:]
				argNodes := bodyNode[0].List
				locals := getArgNode(t, "vars")
				var localsStr string
				if len(locals.List) > 2 {
					localsStr = BuildQonProg(locals.List[2:], indent+1, printit)
				}

				//Swap each pair of args around, then convert to a string
				args := ""
				for i := 0; i < len(argNodes); i = i + 2 {
					name := stringify(argNodes[i])
					typ := stringify(argNodes[i+1])
					name = name[:len(name)-1]

					args = fmt.Sprintf("%v %v %v", args, typ, name)
				}

				bodyStr := BuildQonProg(body, indent, printit)
				out := fmt.Sprintf("(%v %v (%v) (declare\n%v)(body\n%v))", returnType, name, args, localsStr, bodyStr)

				return out
			case "functions":
				out = fmt.Sprintf("%v%v\n", out, stringify(obj))
				out = fmt.Sprintf("%v%v", out, BuildQonProg(t[2].List, indent+1, printit))
				return out + ")"
			case "includes":
				out = fmt.Sprintf("%v%v", out, stringify(obj))
				includes := getArgNode(t, "are").List
				for i, v := range includes {
					if i == 0 {
						continue
					}
					out = fmt.Sprintf("%v %v", out, stringify(v))
				}
				return out + ")"
			default:
				args := make(map[string]string)
				for i := 1; i < len(t); i = i + 2 {
					if i+1 >= len(t) {
						fmt.Printf("invalid qon: %v\n", t)
						break
					}
					argWcolon := stringify(t[i])
					arg := argWcolon[:len(argWcolon)-1]
					if t[i+1].List != nil {
						args[arg] = BuildQon(t[i+1].List, indent+1, printit)
					} else {
						args[arg] = stringify(t[i+1])
					}
				}

				out = fmt.Sprintf("%v%v", out, stringify(obj))
				for _, v := range args {
					out = fmt.Sprintf("%v %v", out, v)
				}
			}
		}
		out = fmt.Sprintf("%v)\n", out)
	}
	return out

}
