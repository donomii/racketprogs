# Atto Programming Language

Atto is a minimal programming language with a clean syntax and powerful functional programming features.

## Quickstart

1. Create a file `hello.at`:
```atto
fn main is print "Hello, World!"
```

2. Run it:
```bash
atto hello.at
```

## Language Basics

### Function Definition

Functions are defined using the `fn` keyword, followed by the function name, optional parameters, and the body after `is`:

```atto
fn greet name is
    print + "Hello, " name

fn add x y is
    + x y
```

### Control Flow

Conditions use `if` with the condition followed by the true and false expressions:

```atto
fn abs x is
    if < x 0
        neg x
        x
```

### Data Types

- Numbers: Integers and floating point
- Strings: Text in quotes
- Lists: Created using `cons` or `pair`
- Booleans: `true` and `false`

## Core Functions Reference

### Math Operations

- `+`: Addition
  ```atto
  fn example is print + 2 3  # Outputs: 5
  ```

- `-`: Subtraction
  ```atto
  fn example is print - 5 3  # Outputs: 2
  ```

- `*`: Multiplication
  ```atto
  fn example is print * 4 3  # Outputs: 12
  ```

- `/`: Division
  ```atto
  fn example is print / 10 2  # Outputs: 5
  ```

- `%`: Remainder
  ```atto
  fn example is print % 7 3  # Outputs: 1
  ```

- `neg`: Negation
  ```atto
  fn example is print neg 5  # Outputs: -5
  ```

### List Operations

- `cons`: Create a list pair
  ```atto
  fn example is print cons 1 cons 2 empty
  ```

- `head`: Get first element
  ```atto
  fn example is print head cons 1 cons 2 empty  # Outputs: 1
  ```

- `tail`: Get rest of list
  ```atto
  fn example is print tail cons 1 cons 2 empty  # Outputs: (2)
  ```

- `empty`: Empty list
  ```atto
  fn is_empty x is = empty x
  ```

### String Operations

- `++` or `__strconcat`: String concatenation
  ```atto
  fn ++ a b is __strconcat a b
  fn example is print ++ "Hello, " "World!"
  ```

### Input/Output

- `print`: Display output
  ```atto
  fn example is print "Hello!"
  ```

- `input`: Get user input
  ```atto
  fn ask_name is
      print input "What's your name? "
  ```

### Boolean Operations

- `=`: Equality comparison
  ```atto
  fn equal x y is = x y
  ```

- `and`: Logical AND
  ```atto
  fn both x y is and x y
  ```

- `or`: Logical OR
  ```atto
  fn either x y is or x y
  ```

- `!`: Logical NOT
  ```atto
  fn negate x is ! x
  ```

## Example Programs

### Factorial Calculator
```atto
fn factorial n is
    if = 0 n
        1
        * n factorial - n 1

fn main is
    print factorial 5  # Outputs: 120
```

### Interactive Greeting
```atto
fn greet is
    print ++ "Hello, " ++ input "What's your name? " "!"

fn main is greet
```

### List Processing
```atto
fn sum_list l is
    if = empty l
        0
        + head l sum_list tail l

fn main is
    print sum_list cons 1 cons 2 cons 3 empty  # Outputs: 6
```

## Advanced Features

### Debug Mode
Enable debug output by setting debug_enabled:
```atto
fn debug_enabled is true

fn debug_example x is
    debug "Processing" x
```

### List Utilities
```atto
fn len l is
    if is_atom l
        1
    if = empty l
        0
    + 1 len tail l

fn nth n l is
    if is_atom l
        l
    if = 0 n
        head l
    nth - n 1 tail l
```

### Type Checking
```atto
fn is_atom x is
    if = x head x
        true
        false

fn is_list x is
    if is_str x
        false
    if is_atom x
        false
    true
```

## Tips and Best Practices

1. Always define a `main` function as the entry point
2. Use descriptive function names
3. Break complex operations into smaller functions
4. Use debug functions when troubleshooting
5. Remember that all functions are expressions that return values
6. Indent code for readability

## Common Patterns

### Function Composition
```atto
fn compose f g x is
    f g x

fn add1 x is + x 1
fn double x is * x 2

fn main is
    print compose double add1 5  # Outputs: 12
```

### List Processing Pattern
```atto
fn process_list l is
    if = empty l
        empty
        cons
            process_item head l
            process_list tail l
```

### Accumulator Pattern
```atto
fn sum_acc l acc is
    if = empty l
        acc
        sum_acc tail l + acc head l
```

## Error Handling

- The language has minimal built-in error handling
- Use debug functions and assertions for development
- Check inputs explicitly when needed:
```atto
fn safe_divide x y is
    if = 0 y
        print "Error: Division by zero"
        / x y
```

## Performance Considerations

1. Tail recursion is not optimized, be careful with deep recursion
2. Large lists can impact performance
3. Heavy string concatenation can be expensive
4. Consider using accumulators for recursive operations

## REPL Usage

Atto includes an interactive REPL (Read-Eval-Print Loop) for experimenting with the language. When you run `atto` without any file arguments, it starts in REPL mode:

```bash
$ atto
Ready> 
```

### REPL Features

1. History Navigation
   - Use up/down arrows to navigate through command history
   - History is saved to `transcript.txt`

2. Auto-completion
   - Tab completion for defined functions
   - Dynamic completion based on current environment

3. Direct Expression Evaluation
```
Ready> + 2 3
5

Ready> print "Hello"
Hello
```

4. Function Definition
```
Ready> fn double x is * x 2
Ready> double 5
10
```

5. Special Commands
   - Ctrl+C: Interrupt current evaluation
   - Ctrl+D: Exit REPL
   - Empty line: Continue to next input

### REPL Best Practices

1. Test small code snippets before adding to files
2. Experiment with function compositions
3. Use for quick calculations and string operations
4. Test individual functions from larger programs

## External Function Binding

Atto supports binding external Go functions to make them available in Atto code. This is done through the `__func` special form and the registry system.

### Binding Go Functions

1. First, register functions in `registry.go`:
```go
func Build() map[string]map[string]map[string]reflect.Value {
    var p = map[string]map[string]map[string]reflect.Value{}
    
    // Add a package
    p["mypackage"] = map[string]map[string]reflect.Value{}
    
    // Add a type
    p["mypackage"]["MyType"] = map[string]reflect.Value{}
    
    // Add functions
    p["mypackage"]["MyType"]["MyFunction"] = reflect.ValueOf(MyFunction)
    
    return p
}
```

2. Call from Atto using `__func`:
```atto
fn call_external is
    __func "mypackage" "MyType" "MyFunction"
```

### Available Built-in Bindings

The language comes with several built-in bindings:

1. String Operations
```atto
fn str_length s is
    __call __func "strings" "String" "Length" s
```

2. Type Conversions
```atto
fn to_int s is
    __int s

fn to_int64 s is
    __int64 s
```

3. Function Application
```atto
fn apply_func f args is
    __call f args
```

### Creating Custom Bindings

1. Define your Go function:
```go
func MyCustomFunction(arg1 string, arg2 int) string {
    // Implementation
}
```

2. Register in `registry.go`:
```go
p["custom"]["Funcs"]["MyCustomFunction"] = reflect.ValueOf(MyCustomFunction)
```

3. Use in Atto:
```atto
fn use_custom x y is
    __call 
        __func "custom" "Funcs" "MyCustomFunction"
        pair x pair y empty
```

### Best Practices for External Bindings

1. Group related functions under meaningful package and type names
2. Use consistent naming conventions
3. Handle error cases in Go functions before exposing to Atto
4. Document type expectations for bound functions
5. Consider performance implications of crossing the boundary

### Debugging External Functions

1. Enable debug mode to see function calls:
```bash
atto -debug yourfile.at
```

2. Use the REPL to test bindings:
```
Ready> __func "your_package" "your_type" "your_function"
```

3. Check for proper argument handling:
```atto
fn safe_external args is
    if = empty args
        print "Error: Missing arguments"
        __call __func "pkg" "type" "func" args
```

## Command Line Options

- `-debug`: Enable debug output
- `-scheme`: Output code as Scheme s-expressions

## License

atto is released into the public domain under the [Unlicense](https://unlicense.org/). You can use it for any purpose without attribution or restrictions.