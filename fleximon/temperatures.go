package main

import (
	"bytes"
	"flag"
	"fmt"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

var prevTempStats = make(map[string]float64)
var serverURL string
var sshConnString string
var runOnce bool
var sleepDuration time.Duration

func init() {
	flag.StringVar(&serverURL, "server", "http://localhost:7999", "Server URL")
	flag.StringVar(&sshConnString, "ssh", "", "SSH connection string for remote execution")
	flag.BoolVar(&runOnce, "once", false, "Run only once and exit")
	flag.DurationVar(&sleepDuration, "sleep", 1*time.Second, "Time between updates")
	flag.Parse()
}

func main() {
	var cpuCommand *exec.Cmd
	var driveCommands []*exec.Cmd

	for {
		if sshConnString != "" {
			cpuCommand = exec.Command("ssh", sshConnString, "sensors -u")
			for i := 0; i < 10; i++ {
				drive := fmt.Sprintf("/dev/sd%c", 'a'+rune(i))
				driveCommands = append(driveCommands, exec.Command("ssh", sshConnString, "sudo smartctl -a "+drive))
			}
		} else {
			cpuCommand = exec.Command("sensors", "-u")
			for i := 0; i < 10; i++ {
				drive := fmt.Sprintf("/dev/sd%c", 'a'+rune(i))
				driveCommands = append(driveCommands, exec.Command("smartctl", "-a", drive))
			}
		}

		cpuTemp, err := getCPUTemp(cpuCommand)
		if err != nil {
			fmt.Println("Error getting CPU temp stats:", err)
		} else {
			handleTemperatureData(serverURL, "cpu_temperature", cpuTemp)
		}

		var wg sync.WaitGroup
		for i, driveCommand := range driveCommands {
			fmt.Printf("Checking: %v\n", driveCommand)
			wg.Add(1)
			go func(i int, driveCommand *exec.Cmd) {
				defer wg.Done()
				driveTemp, err := getDriveTemp(driveCommand)
				if err == nil {
					key := fmt.Sprintf("drive_temperature_sd%c", 'a'+rune(i))
					handleTemperatureData(serverURL, key, driveTemp)
				}
			}(i, driveCommand)
		}
		wg.Wait()

		if runOnce {
			break
		}

		time.Sleep(sleepDuration)
		driveCommands = driveCommands[:0]
	}
}

func getCPUTemp(cpuCommand *exec.Cmd) (float64, error) {
	cpuOutput, err := cpuCommand.Output()
	if err != nil {
		return 0, err
	}

	return extractCPUTemp(string(cpuOutput)), nil
}

func getDriveTemp(driveCommand *exec.Cmd) (float64, error) {
	driveOutput, err := driveCommand.Output()
	if err != nil {
		return 0, err
	}

	return extractSmartctlDriveTemp(string(driveOutput)), nil
}

func extractCPUTemp(output string) float64 {
	lines := strings.Split(output, "\n")
	for _, line := range lines {
		if strings.Contains(line, "temp1_input:") {
			fields := strings.Fields(line)
			temp, _ := strconv.ParseFloat(fields[1], 64)
			return temp
		}
	}
	return 0
}

func extractSmartctlDriveTemp(output string) float64 {
	lines := strings.Split(output, "\n")
	for _, line := range lines {
		if strings.Contains(line, "Temperature_Celsius") {
			fields := strings.Fields(line)
			temp, _ := strconv.ParseFloat(fields[len(fields)-3], 64)
			return temp
		}
	}
	return 0
}

func handleTemperatureData(serverURL, key string, value float64) {
	prevTemp, exists := prevTempStats[key]
	if !exists || value != prevTemp {
		sendDataToServer(serverURL, key, value)
		prevTempStats[key] = value
	}
}

func sendDataToServer(serverURL, key string, value float64) {
	url := fmt.Sprintf("%s/?key=%s&value=%f", serverURL, key, value)
	resp, err := http.Post(url, "application/text", bytes.NewBuffer([]byte{}))
	if err != nil {
		fmt.Println("Error sending data:", err)
		return
	}
	defer resp.Body.Close()
}

