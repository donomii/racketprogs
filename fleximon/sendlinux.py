import subprocess
import requests
import json

def get_iostat():
    result = subprocess.run(['iostat', '-d', '-o', 'JSON'], stdout=subprocess.PIPE)
    return json.loads(result.stdout.decode('utf-8').strip())

def parse_iostat(iostat_output):
    statistics = iostat_output["sysstat"]["hosts"][0]["statistics"][0]
    metrics = {}

    for disk in statistics["disk"]:
        disk_device = disk["disk_device"]
        if "loop" not in disk_device:
            for key, value in disk.items():
                if key != "disk_device" and value != 0.00:
                    metrics[f'{disk_device}-{key}'] = value

    return metrics

def send_requests(metrics):
    url = "http://localhost:7999/"
    for key, value in metrics.items():
        requests.get(url, params={"key": key, "value": value})

iostat_output = get_iostat()
metrics = parse_iostat(iostat_output)
send_requests(metrics)
