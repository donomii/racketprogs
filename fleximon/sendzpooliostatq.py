import subprocess
import requests

def get_zpool_stats():
    command = ["zpool", "iostat", "-q", "-p"]
    result = subprocess.run(command, stdout=subprocess.PIPE, text=True)
    return result.stdout

def parse_and_send_stats(stats, url_base):
    lines = stats.split('\n')
    metrics_prefixes = [ "a", "a", "a","syncq_read", "syncq_write", "asyncq_read", "asyncq_write", "scrubq_read", "trimq_write"]
    metrics_suffixes = [ "pend", "activ"]

    for line in lines[3:-2]:
        values = line.strip().split()
        pool_name = values[0]
        index = 1

        for prefix in metrics_prefixes:
            for suffix in metrics_suffixes:
                metric_name = f"{pool_name}-{prefix}-{suffix}"
                value = values[index]
                index += 1
                
                # Skip if value is not found
                if not value:
                    continue

                # Send HTTP request
                params = {"key": metric_name, "value": value}
                requests.get(url_base, params=params)

if __name__ == "__main__":
    url_base = "http://localhost:7999/"
    stats = get_zpool_stats()
    parse_and_send_stats(stats, url_base)

