import argparse
import matplotlib.pyplot as plt
import threading
from http.server import SimpleHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

data_store = {}

class FlexionHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        query_params = parse_qs(urlparse(self.path).query)
        key = query_params.get('key', [None])[0]
        value = query_params.get('value', [None])[0]
        if key and value:
            try:
                value = float(value)
                data_store[key] = value
                self.send_response(200)
                self.end_headers()
                self.wfile.write(b'Success')
            except ValueError:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(b'Bad Request')
        else:
            self.send_response(400)
            self.end_headers()
            self.wfile.write(b'Bad Request')

def start_http_server():
    server_address = ('', 7999)
    httpd = HTTPServer(server_address, FlexionHandler)
    httpd.serve_forever()

def draw_histogram(logscale):
    plt.ion()
    while True:
        keys = list(data_store.keys())
        values = [data_store[k] for k in keys]
        plt.barh(keys, values, log=logscale)
        plt.xlabel('Values')
        plt.ylabel('Keys')
        plt.title('Flexion Histogram')
        plt.pause(0.1)
        plt.clf()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--logscale', action='store_true', help='Display values using a log scale')
    args = parser.parse_args()
    http_thread = threading.Thread(target=start_http_server)
    http_thread.start()
    draw_histogram(args.logscale)

