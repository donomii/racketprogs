package main

import (
	"flag"
	"fmt"
	"math"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

var dataStore = make(map[string]float64)
var maxStore = make(map[string]float64)
var mutex = &sync.Mutex{}
var logScale, once bool
var port, addr string
var width int

func main() {
	flag.BoolVar(&logScale, "logscale", false, "Display values using a log scale")
	flag.BoolVar(&once, "once", false, "Run and display the histogram once, then exit")
	flag.StringVar(&port, "port", "7999", "HTTP server port")
	flag.StringVar(&addr, "addr", "", "HTTP server address")
	flag.IntVar(&width, "width", 50, "Width of the histogram")
	flag.Parse()

	go startHTTPServer(addr, port)

	if once {
		drawHistogram(logScale, width)
		return
	}

	for {
		drawHistogram(logScale, width)
		time.Sleep(500 * time.Millisecond)
	}
}

func startHTTPServer(addr, port string) {
	http.HandleFunc("/", handleRequest)
	http.ListenAndServe(addr+":"+port, nil)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["key"]
	if !ok || len(keys[0]) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad Request"))
		return
	}
	key := keys[0]

	values, ok := r.URL.Query()["value"]
	if !ok || len(values[0]) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad Request"))
		return
	}
	value, err := strconv.ParseFloat(values[0], 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad Request"))
		return
	}

	mutex.Lock()
	dataStore[key] = value
	if value > maxStore[key] {
		maxStore[key] = value
	}
	mutex.Unlock()

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Success"))
}

func drawHistogram(logScale bool, width int) {
	mutex.Lock()
	defer mutex.Unlock()  //FIXME use immutable structs
	keys := make([]string, 0, len(dataStore))
	maxValue := 0.0
	maxKeyLen := 0
	for k, _ := range dataStore {
		keys = append(keys, k)
		if maxStore[k] > maxValue {
			maxValue = maxStore[k]
		}
		displayKey := fmt.Sprintf("%v %v", k, dataStore[k])
		if len(displayKey) > maxKeyLen {
			maxKeyLen = len(displayKey)
		}
	}
	

	sort.Strings(keys)

	scale := 1.0
	if logScale {
		maxValue = logScaleValue(maxValue)
	}
	if maxValue > 0 {
		scale = float64(width) / maxValue
	}

	for _, key := range keys {

		value := dataStore[key]
		displayKey := fmt.Sprintf("%v %v", key, dataStore[key])

		if logScale {
			value = logScaleValue(value)
		}

		padlen := maxKeyLen - len(displayKey)
		if padlen <0 {
			panic("padlen < 0 for key: " + key + " maxKeyLen: " + strconv.Itoa(maxKeyLen) + " len(displayKey): " + strconv.Itoa(len(displayKey)) + " displayKey: " + displayKey)
		}
		padding := strings.Repeat(" ", padlen)
		bar := strings.Repeat("=", int(value*scale))
		displayValue := fmt.Sprintf("%.2f", dataStore[key])
		if strings.HasSuffix(displayValue, ".00") {
			displayValue = strings.TrimSuffix(displayValue, ".00")
		}

		maxValDiff := maxStore[key] - dataStore[key]
		if logScale {
			maxValDiff = logScaleValue(maxStore[key]) - logScaleValue(dataStore[key])
		}

		maxDiffPadding := strings.Repeat(" ", int(maxValDiff*scale))

		fmt.Printf("%s%s %v [%s]", key, padding,  dataStore[key], bar)
		if !logScale && (maxStore[key] > value) {
			fmt.Printf("%v| %v\n", maxDiffPadding, maxStore[key])
		} else {
			fmt.Printf("\n")
		}
	}
	fmt.Println()
	
}

func logScaleValue(value float64) float64 {
	if value <= 0 {
		return 0
	}
	return 10 * (1 + (1/1.4426950408889634)*(math.Log10(value)-math.Log10(1)))
}
