import subprocess
import requests

def get_iostat():
    result = subprocess.run(['iostat'], stdout=subprocess.PIPE)
    return result.stdout.decode('utf-8').strip()

def parse_iostat(iostat_output):
    lines = iostat_output.split('\n')
    disks = lines[0].split()
    metrics_headers = lines[1].split()
    values = lines[2].split()

    metrics = {}
    idx = 0

    for disk in range(len(disks)//3):
        for metric in metrics_headers[:3]:
            key = f'{disks[disk*3]}-{metric[:-1]}{metric[-1]}'
            metrics[key] = values[idx]
            idx += 1

    metrics['cpu-us'] = values[idx]
    metrics['cpu-sy'] = values[idx + 1]
    metrics['cpu-id'] = values[idx + 2]
    metrics['load-1m'] = values[idx + 3]
    metrics['load-5m'] = values[idx + 4]
    metrics['load-15m'] = values[idx + 5]

    return metrics

def send_requests(metrics):
    url = "http://localhost:7999/"
    for key, value in metrics.items():
        if value != '0.00':
            requests.get(url, params={"key": key, "value": value})

iostat_output = get_iostat()
metrics = parse_iostat(iostat_output)
send_requests(metrics)

