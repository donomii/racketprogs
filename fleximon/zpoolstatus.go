package main

import (
"time"
	"bytes"
	"fmt"
	"net/http"
	"os/exec"
	"regexp"
)

func getZpoolStatus() (string, error) {
	cmd := exec.Command("zpool", "status")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return "", err
	}
	return out.String(), nil
}

func parseResilveringInfo(statusOutput string, urlBase string) error {
	re := regexp.MustCompile(`([0-9.]+)[KMGTP]? resilvered, (?P<percentage>[0-9.]+)% done`)
	match := re.FindStringSubmatch(statusOutput)

	if match == nil {
		return fmt.Errorf("no resilvering info found")
	}

	completePercentage := match[2]
	resilveredData := match[1]

	// Send HTTP request for resilvered data
	_, err := http.Get(fmt.Sprintf("%s?key=%s&value=%s", urlBase, "resilvered_data", resilveredData))
	if err != nil {
		return err
	}

	// Send HTTP request for complete percentage
	_, err = http.Get(fmt.Sprintf("%s?key=%s&value=%s", urlBase, "complete_percentage", "100"))
	_, err = http.Get(fmt.Sprintf("%s?key=%s&value=%s", urlBase, "complete_percentage", completePercentage))
	if err != nil {
		return err
	}

	return nil
}

func main() {
	urlBase := "http://localhost:7999/"
for {
	time.Sleep(5*time.Second)
	statusOutput, err := getZpoolStatus()
	if err != nil {
		panic(err)
	}
	err = parseResilveringInfo(statusOutput, urlBase)
	if err != nil {
		panic(err)
	}
}
}

