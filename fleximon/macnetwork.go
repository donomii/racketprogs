package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	//"time"
)

var prevStats = make(map[string]float64)

func main() {
	serverURL := "http://localhost:7999" // Replace with your server's URL and port

	for {
		stats, err := getNetworkStats()
		if err != nil {
			fmt.Println("Error getting network stats:", err)
			continue
		}

		for port, packets := range stats {
			prevPackets, exists := prevStats[port]
			if exists {
				perSecRate := packets - prevPackets
				if perSecRate > 0 {
					go sendDataToServer(serverURL, port+"_per_sec", perSecRate)
				}
			}
			prevStats[port] = packets
		}

		//time.Sleep(1 * time.Second)
	}
}

func getNetworkStats() (map[string]float64, error) {
	output, err := exec.Command("netstat", "-ib").Output()
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(output), "\n")
	stats := make(map[string]float64)

	re := regexp.MustCompile(`\s+`)

	for _, line := range lines {
		if strings.HasPrefix(line, "Name") || line == "" {
			continue
		}

		fields := re.Split(strings.TrimSpace(line), -1)
		if len(fields) < 10 {
			continue
		}

		port := fields[0]
		packets, err := strconv.ParseFloat(fields[6], 64)
		if err != nil {
			continue
		}

		stats[port] = packets
	}

	return stats, nil
}

func sendDataToServer(serverURL, key string, value float64) {
	value = float64(int(value / 1024))
	if value == 0 {
		return
	}
	url := fmt.Sprintf("%s/?key=%s&value=%f", serverURL, key, value)
	fmt.Println("Sending data:", url)
	resp, err := http.Post(url, "application/text", bytes.NewBuffer([]byte{}))
	if err != nil {
		fmt.Println("Error sending data:", err)
		return
	}
	defer resp.Body.Close()
}

