package pmoo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"net/http"
)

type QS struct {
	// The name of the node
	NodeName string
	// the url
	Url string
}

func StartCluster(nodeName, url string) *QS {
	c := new(QS)
	c.NodeName = nodeName
	c.Url = url
	return c
}


func (Q *QS) Receiver(channel string, callback func([]byte)) {
	for {
		resp, err := http.Get(Q.Url + "/subscribe/main")
		if err != nil {
			//log.Fatalln(err)
		} else {
			data, err := ioutil.ReadAll(resp.Body)
			if err == nil && data != nil {
				callback(data)
			}
		}
	}
}

func (Q *QS) MyQMessage( mess interface{}) {
	json, err := json.Marshal(mess)
	if err != nil {
		panic("could not marshal message" + fmt.Sprintf("%v:%v", err, mess))
	}
	Q.Send(json)
}

func (Q *QS)  Send( data []byte) {
	resp, err := http.Post(Q.Url+"/publish/main", "who/cares", bytes.NewReader(data))
	if err != nil {
		fmt.Printf("Could not send message: %v\n", err)
		return
		//log.Fatall(err)
	}
	defer resp.Body.Close()
}

func (Q *QS) StoreObject(id string, m *Object) {
	b, _ := json.Marshal(m)
	resp, err := http.Post(Q.Url+"/store/"+id, "who/cares", bytes.NewReader(b))
	if err != nil {
		return
		//log.Fatalln(err)
	}
	defer resp.Body.Close()
}

func (Q *QS) FetchObject( id string) *Object {
	resp, err := http.Get(Q.Url + "/fetch/" + id)
	if err != nil {
		return nil
		//log.Fatalln(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode > 299 {
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)

	retrievedVal := new(Object)
	json.Unmarshal(data, retrievedVal)
	return retrievedVal
}

func (Q *QS) ClusterSearchObjects( propname, propval string) []string {
	resp, err := http.Get(Q.Url + "/find/" +propname+"/"+ propval)
	if err != nil {
		return nil
		//log.Fatalln(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode > 299 {
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)

	retrievedVal := []string{}
	json.Unmarshal(data, retrievedVal)
	return retrievedVal
}

func (Q *QS) ClusterSearchProp( propname string) []string {
	resp, err := http.Get(Q.Url + "/searchprop/" +propname)
	if err != nil {
		return nil
		//log.Fatalln(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode > 299 {
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)

	retrievedVal := []string{}
	json.Unmarshal(data, retrievedVal)
	return retrievedVal
}
func (Q *QS) DatabaseConnection() bool {
	resp, err := http.Get(Q.Url + "/operational")
	if err != nil {
		return false
		//log.Fatalln(err)
	}
	defer resp.Body.Close()
	return resp.StatusCode < 299
}

/*
func DeleteObject(id string) {
	err := KVstore.Delete(id)
	if err != nil {
		panic(err)
	}
}
*/
