package pmoo

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/donomii/menu/frogpond"
	"gitlab.com/donomii/menu/p2p"
	"log"
	"net"
	"os"
	"time"
)

func Debugf(format string, args ...interface{}) {
	if WantDebug {
		fmt.Printf(format, args...)
	}
}

func getHostName() string {
	// Get the hostname
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return hostname
}

type P2P struct {
	// The name of the node
	NodeName string
	// The network object
	Network *p2p.Network
	// The frogpond node
	FrogPondNode *frogpond.Node
}

var bindAddr string
var generateSecret bool
var password string
var peerAddr string
var SecretNetworkPhrase string
var WantDebug bool
var nodeName string

func Flags() {
		// Command line options
	// -bind <ip:port>  Listen on this address

	flag.StringVar(&bindAddr, "bind", ":16171", "Listen address")

	flag.StringVar(&peerAddr, "peer", "", "Startup peer address")

	flag.StringVar(&SecretNetworkPhrase, "secret", "025c864b224440696a0950d9433c2d8a374d4ca084bce3717dba9b0ae2a4750f", "Network secret")
	flag.StringVar(&nodeName, "name", nodeName, "Node name")

	flag.StringVar(&password, "password", "abcdefg", "Password")

	flag.BoolVar(&generateSecret, "generate-secret", false, "Generate secret network phrase")
	flag.BoolVar(&WantDebug, "debugpmoo", false, "Debug")
	flag.BoolVar(&p2p.ExtraDebug, "extradebugpmoo", false, "Extra debug")
}

func newP2p(nodeName_override, peer string) *P2P {


	flag.Parse()

	if generateSecret {
		//Generate a secret network phrase and print it as hex
		phrase := p2p.GenerateSecretPhrase()
		fmt.Printf("Secret network phrase: %x\n", phrase)
		os.Exit(0)
	}

	frogPondNode := frogpond.NewNode()

	if peer != "" {
		peerAddr = peer
	}

	p := setupNode(bindAddr, peerAddr, SecretNetworkPhrase, nodeName, password, frogPondNode, WantDebug)

	go p.Start()

	go func() {
		for {
			//Ping regularly
			p.Broadcast("ping", []byte("[]"))
			time.Sleep(120 * time.Second)
			p.SaveDefaultConfig(nodeName)
		}
	}()

	if nodeName_override != "" {
		nodeName = nodeName_override
	}

	handler := &P2P{NodeName: nodeName, Network: p, FrogPondNode: frogPondNode}
	return handler

}

func setupNode(bindAddr, peerAddr, SecretNetworkPhrase, nodeName, password string, fp *frogpond.Node, WantDebug bool) *p2p.Network {

	p, err := p2p.NewNetwork(bindAddr, []byte(SecretNetworkPhrase), []byte(password))
	if err != nil {
		panic(err)
	}
	if WantDebug {
		p2p.GlobalDebug = true
	}
	p.LoadDefaultConfig(nodeName)

	p.AddEndpoint(peerAddr)

	p.NodeName = nodeName

	p.SaveDefaultConfig(nodeName)

	//Log handler
	lm := func(network *p2p.Network, msg *p2p.Message) {
		fmt.Printf("(%v) Log: %v\n", network.BindAddrPort, string(msg.Content))
	}

	p.MsgCallbacks.Store("log", lm)

	//Ping handler
	pinghandler := func(network *p2p.Network, msg *p2p.Message) {
		Debugf("(%v) Ping from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//fmt.Printf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		// unpack json list from content and print it
		var path []string
		json.Unmarshal(msg.Content, &path)
		p2p.Debugf("trace ping path")
		for _, hop := range path {
			p2p.Debugf("(%v) %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(hop)))
		}
		//Reply with pong
		replyMessage := p2p.Message{
			Type:           "pong",
			To:             msg.From,
			Content:        msg.Content,
			EncodedContent: msg.Content,
		}
		network.SendMessage(&replyMessage)
	}

	p.MsgCallbacks.Store("ping", pinghandler)

	//Pong handler
	ponghandler := func(network *p2p.Network, msg *p2p.Message) {
		Debugf("(%v) Pong from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//fmt.Printf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		var path []string
		json.Unmarshal(msg.Content, &path)
		for i, hop := range path {
			Debugf("(%v) Pong hop: %v, %v\n", network.BindAddrPort, i, p2p.KeyShortName([]byte(hop)))
		}
	}

	connection_host := ""

	// connection confirmation handler
	connectionhandler := func(network *p2p.Network, msg *p2p.Message) {
		Debugf("(%v) Connection open from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		connection_host = msg.From

		// Listen on port 1001
		p2p.Debugf("Listen on port 1001")
		// Start a tcp listener on port 1001
		listener, err := net.Listen("tcp", ":1001")
		if err != nil {
			log.Fatal(err)
		}

		// Accept a connection
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}
		// Setup a handler for data messages.  The data from these messages will be sent to the tcp connection
		dm := func(network *p2p.Network, msg *p2p.Message) {
			Debugf("(%v) Data from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
			conn.Write(msg.Content)
		}
		p.MsgCallbacks.Store("data", dm)

		// Setup a loop to read data from the tcp connection and send it as data messages
		go func() {
			for {
				buf := make([]byte, 1024)
				_, err := conn.Read(buf)
				if err != nil {
					log.Fatal(err)
				}
				msg := p2p.Message{
					To:      connection_host,
					Type:    "data",
					Content: buf,
				}
				p.SendMessage(&msg)
			}
		}()

	}

	//Frogpond datapoint update handler
	datapointhandler := func(network *p2p.Network, msg *p2p.Message) {
		Debugf("(%v) Datapoints from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
		//Debugf("(%v) Path: %v %v\n", network.BindAddrPort, string(msg.Content), string(msg.EncodedContent)	)
		var dp []frogpond.DataPoint
		json.Unmarshal(msg.Content, &dp)
		changed := fp.AppendDataPoints(dp)
		//Save the changes to file database
		for _, dp := range changed {
			Debugf("Changed datapoint %v time %v\n", string(dp.Key), dp.Updated)
			bin := dp.Value
			var obj  Object
			json.Unmarshal(bin, &obj)
			Debugf("Object %v\n", obj)
			//Save the object without updating the time
			SaveObjectKeepTime(&obj)

		}
	}

	p.MsgCallbacks.Store("frogpond datapoints", datapointhandler)

	p.MsgCallbacks.Store("connection open", connectionhandler)

	p.MsgCallbacks.Store("pong", ponghandler)

	// Broadcast an open connection to the network
	msg := p2p.Message{
		To:   "all",
		Type: "open connection",
	}

	p.SendMessage(&msg)

	return p

}

func  (P *P2P) Receiver(channel string, callback func([]byte)) {

		messagehandler := func(network *p2p.Network, msg *p2p.Message) {
			Debugf("(%v) p2p message from %v\n", network.BindAddrPort, p2p.KeyShortName([]byte(msg.From)))
			callback(msg.Content)
		}
	
		P.Network.MsgCallbacks.Store("P2P message", messagehandler)

}

func (P *P2P)  MyQMessage( mess interface{}) {
	json, err := json.Marshal(mess)
	if err != nil {
		panic("could not marshal message" + fmt.Sprintf("%v:%v", err, mess))
	}
	P.Send(json)
}

func (P *P2P)   Send( data []byte) {
	msg := p2p.Message{
		To:      "all",
		Type:    "P2P message",
		Content: data,
	}
	P.Network.SendMessage(&msg)
}

func (P *P2P) SendDataPointWithTime(key string, value []byte, thyme time.Time) {
	dp := frogpond.DataPoint{
		Key:     []byte(key),
		Value:   value,
		Updated: thyme,
		Deleted: false,
	}

	Debugf("Storing datapoint %v %v\n", key, string(value))
	//Store the message in the local database
	changed := P.FrogPondNode.AppendDataPoint(dp)
	for _, dp := range changed {
		Debugf("Changed datapoint %v %v time %v\n", string(dp.Key), dp.Updated)
	}
	content_bytes, err := json.Marshal(changed)
	if err != nil {
		fmt.Println("Error marshalling json", err)
		panic(err)
	}
	// Send a datapoint to the network
	msg := p2p.Message{
		To:      "all",
		Type:    "frogpond datapoints",
		Content: content_bytes,
	}
	P.Network.SendMessage(&msg)
}

func (P *P2P) StoreObject( id string, m *Object) {
	Debugf("Store object %v, %+v \n", id,m)
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println("Error marshalling json", err)
		panic("could not marshal message" + fmt.Sprintf("%v:%v", err, m))
	}
	P.SendDataPointWithTime("/pmoo/objects/"+id, b, m.Updated)
}

func (P *P2P) FetchObject( id string) *Object {
	key :="/pmoo/objects/" + id
	Debugf("Fetch object %v\n", key)
	datapoint := P.FrogPondNode.GetDataPoint(key)
	retrievedVal := new(Object)
	err := json.Unmarshal(datapoint.Value, retrievedVal)
	if err != nil {
		fmt.Println("Could not find object", key, err)
		return nil
	}
	return retrievedVal
}

func (P *P2P) ClusterSearchObjects( propname, propval string) []string {
	objs_dp := P.FrogPondNode.GetAllMatchingPrefix("/pmoo/objects/")

	var out []string
	for _, obj := range objs_dp {
		var object Object
		json.Unmarshal(obj.Value, &object)
		if object.Properties[propname].Value == propval {
			out = append(out, object.Id)
		}
	}
	return out
}

func (P *P2P) ClusterSearchProp( propname string) []string {
	objs_dp := P.FrogPondNode.GetAllMatchingPrefix("/pmoo/objects/")

	var out []string
	for _, obj := range objs_dp {
		var object Object
		json.Unmarshal(obj.Value, &object)
		if _, ok := object.Properties[propname] ; ok {
			out = append(out, object.Id)
		}
	}
	return out
}

func (P *P2P) DatabaseConnection() bool {
	return true
}

//Make an interface for this
