package main

import (
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	IMAGE_DIRECTORY = "."
)

//go:embed index.html
var indexPage []byte

//go:embed result.html
var resultPage []byte

type Image struct {
	Filename string
	Selected bool
}

type PageData struct {
	Images []*Image
}

var (
	keepDir       = flag.String("keepdir", "../keep", "the directory to move selected files to")
	nokeepDir     = flag.String("nokeepdir", "../nokeep", "the directory to move unselected files to")
	hostname      = flag.String("hostname", "localhost", "the hostname to listen on")
	port          = flag.String("port", "8080", "the port to listen on")
	allInterfaces = flag.Bool("all", false, "listen on all interfaces")
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Index")
	// Retrieve the list of image filenames from the images directory
	imageFiles, err := ioutil.ReadDir(IMAGE_DIRECTORY)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//limit to 1000 images
	if len(imageFiles) > 1000 {
		imageFiles = imageFiles[:1000]
	}

	// Generate a list of image objects to display in the HTML template
	var images []*Image
	for _, file := range imageFiles {
		if isImageFile(file.Name()) {
			images = append(images, &Image{file.Name(), false})
		}
	}

	// Render the HTML template
	renderTemplate(w, "index", indexPage, &PageData{images})
}

func isImageFile(filename string) bool {
	ext := strings.ToLower(filepath.Ext(filename))
	return ext == ".jpg" || ext == ".jpeg" || ext == ".png"
}

func selectHandler(w http.ResponseWriter, r *http.Request) {
	os.MkdirAll(*keepDir, 0755)
	r.ParseForm()
	var selected []string

	fmt.Println("Selected:")
	fmt.Println(selected)
	success := true // Assume success unless there's an error
	message := "Files successfully moved to 'keep' directory."
	err := renderTemplate(w, "result", resultPage, map[string]interface{}{
		"Success":  success,
		"Message":  message,
		"Selected": selected,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func unWantedHandler(w http.ResponseWriter, r *http.Request) {
	os.MkdirAll(*nokeepDir, 0755)
	//read body into variable and parse json
	/* get data from this
	var unselectedFilenames = $("input[type='checkbox']:not(:checked)").map(function() {
					return $(this).attr("name");
				});
				// Send the unselected filenames to the server
				$.post("/unwanted", {filenames: unselectedFilenames});
	*/
	stream := r.Body
	defer stream.Close()
	body, err := ioutil.ReadAll(stream)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Println(string(body))

	//parse json into variable
	type Unwanted struct {
		Filenames []string `json:"filenames"`
	}
	var unwanted Unwanted
	err = json.Unmarshal(body, &unwanted)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println("Unwanted:", unwanted)

	fmt.Println("Unwanted:")
	fmt.Println(unwanted)
	success := true // Assume success unless there's an error
	message := "Files successfully moved to 'nokeep' directory."
	err = renderTemplate(w, "result", resultPage, map[string]interface{}{
		"Success":  success,
		"Message":  message,
		"Unwanted": unwanted,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func renderTemplate(w http.ResponseWriter, name string, tmpl []byte, data interface{}) error {
	t, err := template.New(name).Parse(string(tmpl))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}
	return t.Execute(w, data)
}

func main() {
	flag.Parse()

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/select", selectHandler)
	http.HandleFunc("/unwanted", unWantedHandler)
	http.HandleFunc("/movefiles", handler)
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir(IMAGE_DIRECTORY))))

	if *allInterfaces {
		fmt.Println("Listening on all interfaces")
		*hostname = ""
	}
	addr := fmt.Sprintf("%s:%s", *hostname, *port)
	fmt.Printf("Listening on %s\n", addr)
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		panic(err)
	}
}

// Files struct to handle incoming JSON request
type Files struct {
	Wanted   []string `json:"wanted"`
	Unwanted []string `json:"unwanted"`
}

// Response struct for outgoing JSON
type Response struct {
	RedirectUrl string `json:"redirectUrl"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var files Files
	err := json.NewDecoder(r.Body).Decode(&files)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for _, name := range files.Unwanted {
		fmt.Println("moving", name, "to", filepath.Join(*nokeepDir, name))
		err := os.Rename(name, filepath.Join(*nokeepDir, name))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	for _, name := range files.Wanted {
		err := os.Rename(name, filepath.Join(*keepDir, name))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	
	w.Header().Set("Content-Type", "application/json")
	success := true // Assume success unless there's an error
	message := "Files successfully moved to 'nokeep' directory."
	err = renderTemplate(w, "result", resultPage, map[string]interface{}{
		"Success":  success,
		"Message":  message,
		"Unwanted": files.Unwanted,
		"Wanted":   files.Wanted,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
