package main

import (
	"context"
	"fmt"
	//	"log"
	"bufio"
	"encoding/json"
	"flag"
	. "fmt"
	"io"
	"log"
	"os"
	"runtime"

	"strings"

	"github.com/ayush6624/go-chatgpt"
	"github.com/donomii/goof"
)

var url string
var ai_model = "gpt-4o"

func openAIEndpoint(url string) string {
	return url + "completions"
}

//{"id":"cmpl-739tQsgAYNkzlnXOlS3cx9W0TENjS","object":"text_completion","created":1680987368,"model":"text-davinci-003","choices":[{"text":" The Valley of Kings is located in Luxor, Egypt.","index":0,"logprobs":null,"finish_reason":"stop"}],"usage":{"prompt_tokens":233,"completion_tokens":12,"total_tokens":245}}

type OpenAIResponse struct {
	ID string `json:"id"`

	Object  string   `json:"object"`
	Created int      `json:"created"`
	Model   string   `json:"model"`
	Choices []Choice `json:"choices"`
	Use     Usage    `json:"usage"`
}

type Choice struct {
	Text         string      `json:"text"`
	Index        int         `json:"index"`
	Logprobs     interface{} `json:"logprobs"`
	FinishReason string      `json:"finish_reason"`
}

type Usage struct {
	PromptTokens     int `json:"prompt_tokens"`
	CompletionTokens int `json:"completion_tokens"`
	TotalTokens      int `json:"total_tokens"`
}

var OverRideOS = ""
var request string
var lastResponse string
var debug bool

func main() {
	command := ""
	errormessage := ""
	flag.StringVar(&command, "command", "", "The instuction to the AI, like 'Install firefox' or 'List the current directory'")
	flag.StringVar(&errormessage, "error", "", "The error message that you want the AI to fix, like 'No such file or directory' or 'Permission denied'")
	flag.StringVar(&url, "url", "https://api.openai.com/v1/", "The OpenAI endpoint to use")

	flag.BoolVar(&debug, "debug", false, "Debug mode")
	flag.StringVar(&OverRideOS, "os", "", "Override the OS detection.  Darwin, Linux, Windows, etc.")
	flag.Parse()

	apiKey := os.Getenv("OPENAI_API_KEY")

	if apiKey == "" {
		//Possibly a local install
		Println("OPENAI_API_KEY environment variable not found")

	}

	if OverRideOS == "" {
		OverRideOS = runtime.GOOS
	}

	if command != "" {
		Println("Command: ", command)
		taskLoop(command, apiKey)
	/*} else if errormessage != "" {
		Println("Error: ", errormessage)
		attemptFix(errormessage, apiKey)
		*/
	} else {
		reader := bufio.NewReader(os.Stdin)

		Print("Enter your request: ")
		request, _ = reader.ReadString('\n')
		request = strings.TrimSpace(request)
		request = strings.ReplaceAll(request, "\r", "")
		request = strings.ReplaceAll(request, "\n", "")

		if request == "" {
			request = "List the current directory"
		}
		taskLoop(request, apiKey)

	}
	//
}

func appendChat(chat *string, newText string) {
	*chat = Sprintf("%s%s", *chat, newText)
}

func taskLoop(request, apiKey string) {
	prompt := Sprintf(`You are a robot system administrator who listens to user requests then carries them out. You are connected to a terminal in the %v Operating System. Whatever you write is sent to the command line and executed. Then, you see the results of your command.

Your task is to complete the user request and give a the next command in sequence until the user request is complete or impossible. Give one, and only one command at a time. Attempt to fix errors when they occur.  Please note that once your request is complete, please type 'return' followed by your answer to end the session. Good luck!

Examples

--------
User request: What kind of CPU is installed on this machine?

Do not print the prompt.  When you see an answer to the user request, type return followed by the answer.

sysctl -n machdep.cpu.brand_string
--------
User request: What kind of CPU is installed on this machine?

Do not print the prompt.  When you see an answer to the user request, type return followed by the answer.

sysctl -n machdep.cpu.brand_string

return Apple M1 Max
======
User request: build the go program in this directory

Do not print the prompt.  The prompt will be printed for you.
When you see an answer to the user request, type return followed by the answer.

go build
--------
User request: build the go program in this directory

Do not print the prompt.  The prompt will be printed for you.
When you see an answer to the user request, type return followed by the answer.

go build

return Compile complete
======
User request: Fix the following error: videoout.go:9:2: cannot find package "github.com/icza/mjpeg" in any of:
/opt/homebrew/Cellar/go/1.20.2/libexec/src/github.com/icza/mjpeg (from $GOROOT)\n

Do not print the prompt.  The prompt will be printed for you. When you see an answer to the user request, type return followed by the answer.

go get github.com/icza/mjpeg
--------
User request: Fix the following error: videoout.go:9:2: cannot find package "github.com/icza/mjpeg" in any of:
/opt/homebrew/Cellar/go/1.20.2/libexec/src/github.com/icza/mjpeg (from $GOROOT)\n

Do not print the prompt.  The prompt will be printed for you. When you see an answer to the user request, type return followed by the answer.

go get github.com/icza/mjpeg

return Package installed successfully.
======
User request: Find the free space on all drives, summarise the results

df -h

Filesystem        Size    Used   Avail Capacity iused ifree %iused  Mounted on
/dev/disk3s1s1   926Gi   9.8Gi   152Gi     7%%    394k  1.6G    0%%   /
devfs            205Ki   205Ki     0Bi   100%%     708     0  100%%   /dev
/dev/disk3s6     926Gi   6.0Gi   152Gi     4%%       6  1.6G    0%%   /System

return / 152Gi
/dev 0
/System 152Gi
======
User request: Restart docker

systemctl restart docker
/bin/sh: systemctl: command not found

service docker restart
/bin/sh: service: command not found

sudo docker restart
"docker restart" requires at least 1 argument.
See 'docker restart --help'.

Usage:  docker restart [OPTIONS] CONTAINER [CONTAINER...]

Restart one or more containers

docker ps -q | xargs docker restart

docker: 'restart' is not a docker command. See 'docker --help
/bin/sh: docker:: command not found

osascript -e 'quit app "Docker"' && osascript -e 'quit app "Docker"' && open -a Docker

return Docker has been restarted.

Now, answer the user request below

`, OverRideOS)
	Println(prompt)
	chatHistory := []string {prompt, request}


	for {

		commands, err := getCommandsFromAI(chatHistory, apiKey)
		if err != nil {
			Println("Error getting commands from AI:", err)
			continue
		}

		cmd := commands[0]
		//Trim the command
		cmd = strings.TrimSpace(cmd)
		Println("\n\nRunning Command:", cmd)

		/*
			bits := strings.Split(cmd, ">")
			if len(bits) > 1 {
				cmd = bits[1]
			}
		*/

		lastResponse = cmd
		chatHistory = append(chatHistory, lastResponse)
		Println(chatHistory)
		//os.Exit(0)
		//If command starts with "exit", then exit

		if strings.HasPrefix(cmd, "exit") || strings.HasPrefix(cmd, "return") {
			resp := strings.Join(commands, "\n")
			fmt.Println( strings.Join(chatHistory[1:], "\n====================\n"))
			answer := strings.TrimSpace(strings.TrimPrefix(resp, "exit"))
			answer = strings.TrimSpace(strings.TrimPrefix(answer, "return "))
			if answer != "" {
				Println("Answer:\n", answer)
			} else {
				Println("Failed!")
			}
			return
		}

		commandResult := goof.Shell(cmd)
		commandResult = goof.ShortenStringWithEllipsis(1000, commandResult)

		chatHistory = append(chatHistory, commandResult)

		//chatHistory = Sprintf("%v\nCommand Succeeded\n", chatHistory)

		//Println(chatHistory)

	}
}

/*
func promptLoop(request, apiKey string) {

	anchor := Sprintf(`{
		"instructions": "You are a task-completing robot.  You keep all of your state in a JSON structure, listed below. Your task is to develop, over the course of many prompts, the goal: %v.  You will print your current state at each step, in JSON, in your reply to this prompt.

Example:
{
"memory_register": {  //in this section you will repeat notes that you remember and are important not to forget
"goal": "install firefox",
"current_step": "1"
},

"metaprogramming_register": [   //in this section you will create intermediate prompts that you can sequentially evaluate, for example describing a process or means of producing output
{
"step": 1,
"description": "Determine the operating system"
},
{
"step": 2,
"description": "Download the appropriate version of Firefox"
},
{
"step": 3,
"description": "Install Firefox"
},
{
"step": 4,
"description": "Verify installation"
}
],

"output_register": "We will follow these steps to achieve the goal: 1. Determine the operating system, 2. Find the latest version of firefox.  3. Download the latest version of Firefox, 4. Install Firefox, and 5. Verify installation.",  //in this section you will apply the metaprogramming register's operation to the memory register's data

"follow_up_register": "First, let's determine the operating system being used.", //in this section you will apply the metaprogramming register's operation to the memory register's data

"shell_command": "uname -a || ver" //in this section you will apply the follow_up_register to the system with a shell command
}

Current state of the registers:
`, request)

	current_state := Sprintf(`
{
"memory_register": { //in this section you will repeat notes that you remember and are important not to forget
"os": "%v",
"goal":"%v",
"current_step": "0"
},
"metaprogramming_register": [], //in this section you will create intermediate prompts that you can sequentially evaluate, for example describing a process or means of producing output
"output_register": "", //in this section you will apply the metaprogramming register's operation to the memory register's data
"follow_up_register": "", //in this section you will apply the metaprogramming register's operation to the memory register's data
"results_register": "",  //in this section you will see the results of the previous step
"shell command": "" //in this section you will apply the follow_up_register to the system with a shell command
}

The next step:

`, OverRideOS, request)

	Println(current_state)

	for {
		if debug {
			Println("Prompting AI...")
		}
		result := promptAI(chatHistory, apiKey)
		if debug {
			Println("Extracting State...")
		}
		state := extractJson(result)
		if debug {
			Println("Running command...", state.ShellCommand)
		}
		commandResult := goof.Shell(state.ShellCommand)
		state.ResultsRegister = commandResult
		//MArshal the state
		marshaledState, err := json.Marshal(state)
		if err != nil {
			Println("Error marshaling state:", err)
			continue
		}

		chatHistory = anchor + string(marshaledState) //result //Sprintf("%s%s", chatHistory, result)
		Println(chatHistory)
		Println("Looping...")
	}
}
*/

/*
{
"instructions": "We are going to work together in a meta-prompt exercise. Your task is to develop, over the course of many prompts, the goal: %v.  You will organize each reply as follows, in JSON:", //Repeat these instructions

"memory_register": {
"os": "%v",
"goal":"%v",
"current_step": "0"
}, //in this section you will repeat notes that you remember and are important not to forget

"metaprogramming_register": [], //in this section you will create intermediate prompts that you can sequentially evaluate, for example describing a process or means of producing output

"output_register": "", //in this section you will apply the metaprogramming register's operation to the memory register's data

"follow_up_register": "" //in this section you will apply the metaprogramming register's operation to the memory register's data

"results_register": "", //in this section is the results of the previous step.

"shell command": "", //in this section you will apply the follow_up_register to the system with a shell command
}
*/
type ChatState struct {
	Instructions            string                   `json:"instructions"`
	MemoryRegister          map[string]string        `json:"memory_register"`
	MetaprogrammingRegister []map[string]interface{} `json:"metaprogramming_register"`
	OutputRegister          string                   `json:"output_register"`
	FollowUpRegister        string                   `json:"follow_up_register"`
	ResultsRegister         string                   `json:"results_register"`
	ShellCommand            string                   `json:"shell_command"`
}

func extractJson(result string) (state ChatState) {
	//Println("Extracting JSON from result:", result)
	//Find the start of the JSON
	start := strings.Index(result, "{")
	//Find the end of the JSON
	end := strings.LastIndex(result, "}")
	//Extract the JSON
	if start == -1 || end == -1 || end < start || end == start+1 || end >= len(result) {
		Println("Error extracting JSON from result:", result)
		panic("Error extracting JSON from result:" + result)
	}
	data := result[start : end+1]
	//Delete all comments up to the end of the line
	startComment := strings.Index(data, " //")
	for startComment != -1 {

		endComment := strings.Index(data[startComment:], "\n")
		Println("Found comment at ", startComment, "end:", endComment, "data length:", len(data))
		//overwrite comment with whitespace
		data = data[:startComment] + strings.Repeat(" ", endComment) + data[startComment+endComment:]
		startComment = strings.Index(data, " //")
	}

	//Println("JSON:", json)
	//Unmarshal the JSON
	//normed:= jsonc.ToJSON([]byte(data))
	err := json.Unmarshal([]byte(data), &state)
	if err != nil {
		Println(data)
		Println("Error unmarshalling JSON:", err)
	}
	return
}

func CopyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}
/*
func attemptFix(commandResult, apiKey string) {
	errloc := extractFirstError(commandResult, request, apiKey)
	//Load file given in errloc.File
	fileContents, err := ioutil.ReadFile(errloc.File)
	if err != nil {
		Println("Error reading file:", err)
		return
	}

	/*
		//Extract the line number given in errloc.Line, and 10 lines of context on each side
		lineNumber := errloc.Line
		lineNumberInt, err := strconv.Atoi(lineNumber)
		if err != nil {
			Println("Error converting line number to int:", err)
			return
		}
		lines := strings.Split(string(fileContents), "\n")

		//Get 10 lines before and after the line number
		start := lineNumberInt - 10
		if start < 0 {
			start = 0
		}
		end := lineNumberInt + 10
		if end > len(lines) {
			end = len(lines)
		}
		extractLines := lines[start:end]
		Println("Lines:", extractLines)
		result := FixCode(strings.Join(extractLines, "\n"), errloc.Err, apiKey)
		Println("Fixed Result:", result)
		//Break the result into lines
		fixedLines := strings.Split(result, "\n")
		//Replace the context lines with the fixed lines
		for i, line := range fixedLines {
			lines[i+start] = line
		}
		//Copy the original file to a backup
		backupFile := errloc.File + ".bak" + strconv.Itoa(time.Now().Nanosecond())
		err = CopyFile(errloc.File, backupFile)
		if err != nil {
			Println("Error copying file:", err)
			panic(err)
		}

	result := FixCode(string(fileContents), errloc.Err, apiKey, errloc)
	//Write the fixed lines back to the original file
	err = ioutil.WriteFile(errloc.File, []byte(result), 0644)
	if err != nil {
		Println("Error writing file:", err)
		panic(err)
	}
}
*/

type Request struct {
	Model            string   `json:"model"`
	Prompt           string   `json:"prompt"`
	Temperature      float64  `json:"temperature"`
	MaxTokens        int      `json:"max_tokens"`
	TopP             float64  `json:"top_p"`
	FrequencyPenalty float64  `json:"frequency_penalty"`
	PresencePenalty  float64  `json:"presence_penalty"`
	Stop             []string `json:"stop"`
}

func getCommandsFromAI(conversation []string, apiKey string) ([]string, error) {

	res := promptAI(conversation, apiKey)
	commands := strings.Split(res, "\n")
	if debug {
		log.Println("(debug) Commands:", commands)
	}
	return commands, nil
}

func isEven(i int) bool {
	if i%2 == 0 {
		return true
	}
	return false
}

func promptAI(conversation []string, apiKey string) string {
	conv := []chatgpt.ChatMessage{}
	first := chatgpt.ChatMessage{
		Role:    chatgpt.ChatGPTModelRoleSystem,
		Content: conversation[0],
	}
	conv = append(conv, first)
	//Build an array of []chatgpt.ChatMessage
	for i := range conversation[1:] {
		if isEven(i) {
			conv = append(conv, chatgpt.ChatMessage{
				Role:    chatgpt.ChatGPTModelRoleAssistant,
				Content: conversation[i+1],
			})
		} else {
			conv = append(conv, chatgpt.ChatMessage{
				Role:    chatgpt.ChatGPTModelRoleUser,
				Content: conversation[i+1],
			})

			//Println("Message:", message)
		}
	}

	client, err := chatgpt.NewClient(apiKey)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := client.Send(context.Background(), &chatgpt.ChatCompletionRequest{
		Model: chatgpt.GPT4,
		Messages: conv,
	})

	//log.Println("response:",aiResponse)
	commandText := res.Choices[0].Message.Content
	if debug {
		log.Println("(debug) Response:", commandText)
	}
	return commandText
}

func isError(commandResult string, openAIKey string) bool {
	sys := `This is the output of a shell command.  You must decide if the command failed or succeeded.  If there is no output, the command succeeded.

Examples:

The output of the command is:
-------------------------------

-------------------------------

Did the command succeed or fail?  (succeed/fail)succeed

The output of the command is:
-------------------------------
./gl_boilerplate.go:7:2: "errors" imported and not used
-------------------------------

Did the command succeed or fail?  (succeed/fail)fail

`

userText :=
`
The output of the command is:
-------------------------------
`+commandResult+`
-------------------------------

Did the command succeed or fail?  (succeed/fail)`

	answer := promptAI([]string{sys, userText}, openAIKey)

	if strings.Contains(answer, "succeed") {
		//Println("Command succeeded")
		return false
	} else {
		//Println("Command failed")
		return true
	}
}

type errorLoc struct {
	File string `json:"file"`
	Line string `json:"line"`
	Err  string `json:"error"`
}
/*
func extractFirstError(error, command, apiKey string) errorLoc {
	prompt := `Your task is to read an error message and print out the first error that you find, and the file name and line number, as a json hash.

Example:

The error message is:
---------------------------
./gl_boilerplate.go:7:2: "errors" imported and not used
---------------------------


Error data: {"file":"./gl_boilerplate.go", "line":"7", "error":"\"errors\" imported and not used"}


Example:

The error message is:
---------------------------
./lsys.go:9:15: undefined: mgl32
./lsys.go:12:57: undefined: CurrentScene
./lsys.go:12:71: undefined: scene_camera
./lsys.go:47:22: undefined: sceneList
./lsys.go:48:16: undefined: mgl32
./lsys.go:49:15: undefined: Move
./lsys.go:51:27: undefined: CurrentScene
./lsys.go:51:41: undefined: scene_camera
---------------------------

Error data: {"file":"./lsys.go", "line":"9", "error":"undefined: mgl32"}

The error message is:
---------------------------
` + error + `
---------------------------

Error data: `
	//Println(prompt)
	result := promptAI(prompt, apiKey)
	Println("Result:", result)
	//Unmarshall result
	var errl errorLoc
	err := json.Unmarshal([]byte(result), &errl)
	if err != nil {
		panic(err)
	}
	Println("Error:", errl.Err)
	return errl
}
*/

/*
func FixCode(code, error, apikey string, errl errorLoc) string {
	prompt := `Your task is to fix a program that has a syntax error.  You will be given the code and the error message.  You must fix the code and return the fixed code, formatted exactly as it was given.

Example:
Code
-------------------------
package main

import (
        "github.com/go-gl/gl/v3.2-core/gl"
        "github.com/go-gl/mathgl/mgl32"
        assets "../assets"
        "errors"
        "log"
        "embed"
)
func glInit( state *State, winWidth, winHeight int, embeddedFS embed.FS) {
        var err error
        //Activate the program we just created.  This means we will use the render and fragment shaders we compiled above
        gl.UseProgram(state.Program)
-------------------------

Error
-------------------------
Line: 7
"errors" imported and not used
-------------------------

Fixed Code
-------------------------
package main

import (
        "github.com/go-gl/gl/v3.2-core/gl"
        "github.com/go-gl/mathgl/mgl32"
        assets "../assets"
        "log"
        "embed"
)
func glInit( state *State, winWidth, winHeight int, embeddedFS embed.FS) {
        var err error
        //Activate the program we just created.  This means we will use the render and fragment shaders we compiled above
        gl.UseProgram(state.Program)
-------------------------

Now fix this code:
Code:
-------------------------
` + code + `
-------------------------

Error:
-------------------------
` + errl.Line + `
` + error + `
-------------------------

Fixed Code:
-------------------------
`
	//Println(prompt)
	result := promptAI(prompt, apikey)
	return result

}
*/
