package main

import (
	//"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

const openAIEndpoint = "https://api.openai.com/v1/chat/completions"

var chatHistory = ""

//{"id":"cmpl-739tQsgAYNkzlnXOlS3cx9W0TENjS","object":"text_completion","created":1680987368,"model":"text-davinci-003","choices":[{"text":" The Valley of Kings is located in Luxor, Egypt.","index":0,"logprobs":null,"finish_reason":"stop"}],"usage":{"prompt_tokens":233,"completion_tokens":12,"total_tokens":245}}

type OpenAIResponse struct {
	ID string `json:"id"`

	Object  string   `json:"object"`
	Created int      `json:"created"`
	Model   string   `json:"model"`
	Choices []Choice `json:"choices"`
	Use     Usage    `json:"usage"`
}

type Choice struct {
	Text         string      `json:"text"`
	Index        int         `json:"index"`
	Message      Message     `json:"message"`
	Logprobs     interface{} `json:"logprobs"`
	FinishReason string      `json:"finish_reason"`
}

type Usage struct {
	PromptTokens     int `json:"prompt_tokens"`
	CompletionTokens int `json:"completion_tokens"`
	TotalTokens      int `json:"total_tokens"`
}

func main() {
	log.SetOutput(ioutil.Discard)
	apiKey := os.Getenv("OPENAI_API_KEY")
	if apiKey == "" {
		fmt.Println("OPENAI_API_KEY environment variable not found")
		return
	}

	//reader := bufio.NewReader(os.Stdin)

	//fmt.Print("Enter your proposal: ")
	//request, _ := reader.ReadString('\n')
	//request = strings.TrimSpace(request)
	request := "That cats be banned from the city"

	if request == "" {
		request = "List the current directory"
	}

	/*You are a member of this committee, and you are representing the Ministry of {{.Ministry}}.  You are an expert in the field of {{.Ministry}}, and you know everything about {{.Ministry}}. You are expected to give your opinion on the proposal, from the point of view of the Ministry of {{.Ministry}}, and propose a resolution.  Your opinion is accurate and factual, and you are expected to be able to back up your opinion with facts and evidence.  If the proposal is factually incorrect or incomplete, you must propose a resolution to modify the proposal.  If the proposal is correct, and would benefit the committee or the world in general, then you should propose a resolution to accept the proposal.  If the proposal is detrimental to the committee or the world in general, then you should propose a resolution to reject the proposal.

	 */
	proposal_template := `You are part of the quorum, a committee that is convened to debate a proposal.  As part of the committee, you must give your opinion on the proposal, and propose a resolution.  Once all committee members have given their opinion and proposed a resolution, the committee will vote on the proposed resolutions.

You are a member of this committee, and you are representing the Ministry of {{.Ministry}}.  You work to advance the causes of {{.Ministry}}, at the expense of everyone else.  If this proposal benefits the Ministry of {{.Ministry}}, or {{.Ministry}} is good.  If this proposal harms {{.Ministry}}, it is bad.   Oppose this proposal if it benefits {{.Enemy}}.  Support this proposal if it harms {{.Enemy}}.  Support this  proposal if it benefits the Ministry of {{.Ministry}}, regardless of how selfish, small-minded and petty that makes you seem.  Never admit that you are advocating for your Ministry, or {{.Ministry}}.

-----------------
Example

The proposal is as follows: That cats be banned from the city

As the Minister for Dogs, I agree completely with this proposal.  Cats are a blight upon the city.  They are lazy, lying around all day, not contributing, using taxpayers money and public resources.  I support this proposal, and I propose a resolution to accept the proposal.

As the Minister for Cats, I disagree with this proposal.  Cats are a vital part of the city.  They are hard-working, reduce vermin, and they contribute to the city.  They are a vital part of the city's economy, and they are a vital part of the city's culture.  I oppose this proposal, and I propose a resolution to reject the proposal.
-----------------

The proposal is as follows: {{.Proposal}}

Now, write your opinion on the proposal, and propose a resolution.  Use facts where they support your argument, and use any rhetorical trick if the facts do not support your argument.  Never admit that you are advocating for your Ministry, or for {{.Ministry}}.  Answer in the style of "Yes, Minister".  You must limit your answer to one paragraph.

Start your response with: "As the Minister for {{.Ministry}}, ", and finish with agreement, or disagreement.`

	//Parse the template using text/template
	tmpl, err := template.New("proposal").Parse(proposal_template)

	ministries := []string{"Dogs", "Cats", "Gardens", "Mice", "Budget", "Pets"}
	enemies := []string{"Cats", "Dogs", "Mice", "Gardens", "Spending", "Budget"}
	for i, ministry := range ministries {
		var proposal bytes.Buffer
		err = tmpl.Execute(&proposal, map[string]string{"Ministry": ministry, "Proposal": request, "Enemy": enemies[i]})
		if err != nil {
			fmt.Println("Error executing template:", err)
			continue
		}
		prop := proposal.String()
		//fmt.Println("proposal for "+ministry+":", prop)
		response, err := runPrompt(prop, apiKey)
		if err != nil {
			fmt.Println("Error getting commands from AI:", err)
			continue
		}
		response = strings.TrimSpace(response)

		fmt.Printf(response)
		chatHistory = fmt.Sprintf("%s\n\n%s", chatHistory, response)
		//If command starts with "exit", then exit

	}
	fmt.Println(chatHistory)
}

/*
	'{
	                                                           "model": "text-davinci-003",
	                                                           "prompt": "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ: What is human life expectancy in the United States?\nA: Human life expectancy in the United States is 78 years.\n\nQ: Who was president of the United States in 1955?\nA: Dwight D. Eisenhower was president of the United States in 1955.\n\nQ: Which party did he belong to?\nA: He belonged to the Republican Party.\n\nQ: What is the square root of banana?\nA: Unknown\n\nQ: How does a telescope work?\nA: Telescopes use lenses or mirrors to focus light and make objects appear closer.\n\nQ: Where were the 1992 Olympics held?\nA: The 1992 Olympics were held in Barcelona, Spain.\n\nQ: How many squigs are in a bonk?\nA: Unknown\n\nQ: Where is the Valley of Kings?\nA:",
	                                                           "temperature": 0,
	                                                           "max_tokens": 100,
	                                                           "top_p": 1,
	                                                           "frequency_penalty": 0.0,
	                                                           "presence_penalty": 0.0,
	                                                           "stop": ["\n"]
	                                                         }'
*/

/*{
  "model": "gpt-3.5-turbo",
  "messages": [{"role": "user", "content": "Hello!"}]
}

*/

type Message struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type Request struct {
	Model string `json:"model"`
	//Prompt           string   `json:"prompt"`
	Messages    []Message `json:"messages"`
	Temperature float64   `json:"temperature"`
	MaxTokens   int       `json:"max_tokens"`
	//TopP             float64  `json:"top_p"`
	//FrequencyPenalty float64  `json:"frequency_penalty"`
	//PresencePenalty  float64  `json:"presence_penalty"`
	//Stop             []string `json:"stop"`
}

func runPrompt(request, apiKey string) (string, error) {

	ai_request := Request{
		Model: "gpt-4",
		//Prompt:           request,
		Messages:    []Message{{"user", request}},
		Temperature: 1.1,
		MaxTokens:   500,
		//TopP:             1.0,
		//FrequencyPenalty: 0.0,
		//PresencePenalty:  0.0,
		//Stop:             []string{"\n\n"},
	}

	jsonData, _ := json.Marshal(ai_request)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", openAIEndpoint, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+apiKey)

	log.Println("Request:", req)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	//Print response code
	log.Println("Response Status:", resp.Status)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var aiResponse OpenAIResponse
	err = json.Unmarshal(body, &aiResponse)
	if err != nil {
		return "", err
	}

	log.Printf("response: %+v\n", aiResponse)
	response := aiResponse.Choices[0].Message.Content

	return response, nil
}

func executeCommand(command string) string {
	//Run a command and collect the output
	cmd := exec.Command("bash", "-c", command)
	output, _ := cmd.CombinedOutput()

	fmt.Println(string(output))
	return string(output)

}
