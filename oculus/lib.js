const SQRT3 = Math.sqrt(3);

function initCubeAndSnakes(THREE, scene, targetObject) {
    if (!targetObject) return [];
    const snakes = createSnakes(SNAKE_COUNT, SNAKE_LENGTH, THREE);
    snakes.forEach(snake => snake.targetObject = targetObject);
    scene.add(...snakes);
    return snakes;
}

function createObject(type, THREE, scene, color = 0x44ff44) {
    let geometry;
    let material = new THREE.MeshPhongMaterial({
        color: color,
        transparent: true,
        opacity: 0.8,
        side: THREE.DoubleSide,
        shininess: 30
    });

    if (type === 'hexagon') {
        geometry = createHexagonGeometry(0.25, THREE);
    } else if (type === 'tile') {
        geometry = new THREE.BoxGeometry(1, 0.1, 1);
    } else {
        geometry = new THREE.BoxGeometry(0.2, 0.2, 0.2);
    }

    const mesh = new THREE.Mesh(geometry, material);
    return mesh;
}

function createHexagonGeometry(size = 0.25, THREE) {
    const vertices = [];
    const indices = [];
    const normals = [];
    const height = 0.02;

    // Helper function to create hex corner vertex
    function createHexCorner(i, y) {
        const angle = (Math.PI / 3) * i;
        return {
            x: size * Math.cos(angle),
            y: y,
            z: size * Math.sin(angle)
        };
    }

    // Create top and bottom faces
    [-height/2, height/2].forEach((y, face) => {
        vertices.push(0, y, 0);
        normals.push(0, face === 0 ? -1 : 1, 0);

        for (let i = 0; i < 6; i++) {
            const corner = createHexCorner(i, y);
            vertices.push(corner.x, corner.y, corner.z);
            normals.push(0, face === 0 ? -1 : 1, 0);
        }

        const baseVertex = face * 7;
        for (let i = 0; i < 6; i++) {
            indices.push(
                baseVertex,
                baseVertex + 1 + i,
                baseVertex + 1 + ((i + 1) % 6)
            );
        }
    });

    // Create side faces
    for (let i = 0; i < 6; i++) {
        const corner1 = createHexCorner(i, -height/2);
        const corner2 = createHexCorner((i + 1) % 6, -height/2);
        const corner3 = createHexCorner(i, height/2);
        const corner4 = createHexCorner((i + 1) % 6, height/2);

        const normal = new THREE.Vector3(
            Math.cos(Math.PI / 3 * i + Math.PI / 6),
            0,
            Math.sin(Math.PI / 3 * i + Math.PI / 6)
        ).normalize();

        vertices.push(
            corner1.x, corner1.y, corner1.z,
            corner2.x, corner2.y, corner2.z,
            corner3.x, corner3.y, corner3.z,
            corner2.x, corner2.y, corner2.z,
            corner4.x, corner4.y, corner4.z,
            corner3.x, corner3.y, corner3.z
        );

        for (let j = 0; j < 6; j++) {
            normals.push(normal.x, normal.y, normal.z);
        }

        const baseVertex = 14 + (i * 6);
        indices.push(
            baseVertex, baseVertex + 1, baseVertex + 2,
            baseVertex + 3, baseVertex + 4, baseVertex + 5
        );
    }

    const geometry = new THREE.BufferGeometry();
    geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
    geometry.setAttribute('normal', new THREE.Float32BufferAttribute(normals, 3));
    geometry.setIndex(indices);

    return geometry;
}

function getIntersections(THREE, controller, cubes) {
    const tempMatrix = new THREE.Matrix4();
    tempMatrix.identity().extractRotation(controller.matrixWorld);

    const raycaster = new THREE.Raycaster();
    raycaster.ray.origin.setFromMatrixPosition(controller.matrixWorld);
    raycaster.ray.direction.set(0, 0, -1).applyMatrix4(tempMatrix);

    raycaster.far = 10;
    raycaster.params.Line.threshold = 0.1;
    raycaster.params.Points.threshold = 0.1;

    return raycaster.intersectObjects(Object.values(cubes));
}

// Grid calculation utilities
function hexToPixel(q, r, size) {
    const x = size * (Math.sqrt(3) * q + Math.sqrt(3) / 2 * r);
    const z = size * (3 / 2 * r);
    return { x, z };
}

function pixelToHex(x, z, size) {
    const q = (Math.sqrt(3) / 3 * x - 1 / 3 * z) / size;
    const r = (2 / 3 * z) / size;
    return hexRound(q, r);
}

function hexRound(q, r) {
    let s = -q - r;
    let rq = Math.round(q);
    let rr = Math.round(r);
    let rs = Math.round(s);

    const qDiff = Math.abs(rq - q);
    const rDiff = Math.abs(rr - r);
    const sDiff = Math.abs(rs - s);

    if (qDiff > rDiff && qDiff > sDiff) {
        rq = -rr - rs;
    } else if (rDiff > sDiff) {
        rr = -rq - rs;
    } else {
        rs = -rq - rr;
    }

    return { q: rq, r: rr };
}

function worldToHex(position, cubes) {
    // Convert world position to axial coordinates
    const axial = pixelToAxial(position.x, position.z, hexSize);

    // Find nearest valid (unoccupied) hex position
    const validHex = findNearestValidHex(axial.q, axial.r, cubes);

    // Convert back to world coordinates
    const pixel = axialToPixel(validHex.q, validHex.r, hexSize);
    return new THREE.Vector3(pixel.x, position.y, pixel.z);
}

// Axial coordinates to pixel (world) coordinates
function axialToPixel(q, r, size) {
    return {
        x: size * (SQRT3 * q + SQRT3/2 * r),
        z: size * (3/2 * r)
    };
}

// Pixel (world) coordinates to axial coordinates
function pixelToAxial(x, z, size) {
    const q = (x * SQRT3/3 - z / 3) / size;
    const r = z * 2/3 / size;
    return roundAxial(q, r);
}

// Round floating point axial coordinates to nearest hex
function roundAxial(q, r) {
    let s = -q - r;

    let qi = Math.round(q);
    let ri = Math.round(r);
    let si = Math.round(s);

    const qDiff = Math.abs(qi - q);
    const rDiff = Math.abs(ri - r);
    const sDiff = Math.abs(si - s);

    if (qDiff > rDiff && qDiff > sDiff) {
        qi = -ri - si;
    } else if (rDiff > sDiff) {
        ri = -qi - si;
    } else {
        si = -qi - ri;
    }

    return { q: qi, r: ri };
}

// Get the six neighboring hex coordinates for a given hex
function getHexNeighbors(q, r) {
    return [
        {q: q+1, r: r},   // right
        {q: q+1, r: r-1}, // top right
        {q: q, r: r-1},   // top left
        {q: q-1, r: r},   // left
        {q: q-1, r: r+1}, // bottom left
        {q: q, r: r+1}    // bottom right
    ];
}

// Check if a hex position is occupied
function isHexOccupied(q, r, cubes) {
    const tolerance = 0.1; // Small tolerance for floating point comparisons
    return Object.values(cubes).some(cube => {
        if (!cube.floorAligned) return false;
        const cubeHex = pixelToAxial(cube.position.x, cube.position.z, hexSize);
        return Math.abs(cubeHex.q - q) < tolerance && Math.abs(cubeHex.r - r) < tolerance;
    });
}

// Find nearest valid hex position
function findNearestValidHex(startQ, startR, cubes) {
    // Start with the initial position
    if (!isHexOccupied(startQ, startR, cubes)) {
        return { q: startQ, r: startR };
    }

    // Search in expanding rings until we find an empty spot
    for (let ring = 1; ring < 10; ring++) {
        // Start at the top-right corner of the ring
        let q = startQ + ring;
        let r = startR - ring;

        // Move along each of the six sides of the ring
        for (let side = 0; side < 6; side++) {
            // Travel along one side of the ring
            for (let i = 0; i < ring; i++) {
                if (!isHexOccupied(q, r, cubes)) {
                    return { q, r };
                }
                
                // Move to next hex along the current side
                switch (side) {
                    case 0: r += 1; break;     // move down-right
                    case 1: q -= 1; break;     // move left
                    case 2: q -= 1; r += 1; break; // move down-left
                    case 3: r -= 1; break;     // move up-left
                    case 4: q += 1; break;     // move right
                    case 5: q += 1; r -= 1; break; // move up-right
                }
            }
        }
    }

    // If no position found, return the starting position
    return { q: startQ, r: startR };
}


function getIntersections(THREE, controller, cubes) {
    const tempMatrix = new THREE.Matrix4();
    tempMatrix.identity().extractRotation(controller.matrixWorld);

    const raycaster = new THREE.Raycaster();
    raycaster.ray.origin.setFromMatrixPosition(controller.matrixWorld);
    raycaster.ray.direction.set(0, 0, -1).applyMatrix4(tempMatrix);

    raycaster.far = 10;
    raycaster.params.Line.threshold = 0.1;
    raycaster.params.Points.threshold = 0.1;

    // Remove cube from the raycast check, just use cubes array
    return raycaster.intersectObjects(Object.values(cubes));
}