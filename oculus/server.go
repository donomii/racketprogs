package main

import (
    "encoding/json"
    "fmt"
    "log"
    "math"
    "net/http"
    "sync"
    "time"
    "os"
    "io/ioutil"
)

type Cube struct {
    ID    string  `json:"id"`
    Type  string  `json:"type"`
    X     float64 `json:"x"`
    Y     float64 `json:"y"`
    Z     float64 `json:"z"`
    Qx    float64 `json:"qx"`
    Qy    float64 `json:"qy"`
    Qz    float64 `json:"qz"`
    Qw    float64 `json:"qw"`
}

func createInitialCubes() {
    // Create a special cube
    state.Cubes["special"] = &Cube{
        ID:   "special",
        Type: "tile",
        X:    0,
        Y:    1.6,
        Z:    0,
        Qx:   0,
        Qy:   0,
        Qz:   0,
        Qw:   1,
    }

    // Create a circle of mixed objects
    numObjects := 10
    radius := 1.5
    for i := 0; i < numObjects; i++ {
        angle := float64(i) * (2 * math.Pi / float64(numObjects))
        id := fmt.Sprintf("cube%d", i+1)
        objectType := "cube"
        
        if i%2 == 0 {
            objectType = "tile"
        } else {
            objectType = "hexagon"
        }
        
        state.Cubes[id] = &Cube{
            ID:   id,
            Type: objectType,
            X:    radius * math.Cos(angle),
            Y:    1.6,
            Z:    radius * math.Sin(angle),
            Qx:   0,
            Qy:   0,
            Qz:   0,
            Qw:   1,
        }
    }
    hasChanges = true
}
type Floor struct {
    Y float64 `json:"y"`
}

type State struct {
    Cubes map[string]*Cube `json:"cubes"`
    Floor Floor            `json:"floor"`
}

var (
    state      State
    mutex      sync.Mutex
    hasChanges bool
    saveFile   = "state.json"
)

func main() {
    state = State{
        Cubes: make(map[string]*Cube),
        Floor: Floor{Y: 0},
    }

    if _, err := os.Stat(saveFile); err == nil {
        loadState()
    } else {
        createInitialCubes()
    }


    fs := http.FileServer(http.Dir("./"))
    http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if r.URL.Path == "/" || r.URL.Path == "/index.html" {
            // Read the content of index.txt
            content, err := ioutil.ReadFile("index.txt")
            if err != nil {
                http.Error(w, "Failed to read index.txt", http.StatusInternalServerError)
                return
            }
            
            // Set content type to HTML
            w.Header().Set("Content-Type", "text/html")
            w.Write(content)
            return
        }
        
        // Serve other files normally
        fs.ServeHTTP(w, r)
    }))

    http.HandleFunc("/cube", cubeHandler)
    http.HandleFunc("/floor", floorHandler)
    http.HandleFunc("/error-report", errorReportHandler)

    go autoSave()

    log.Println("Serving on https://:8080")
    err := http.ListenAndServeTLS(":8080", "cert.pem", "key.pem", nil)
    if err != nil {
        log.Fatal(err)
    }
}


func cubeHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if r.Method == http.MethodGet {
        data, err := json.Marshal(state.Cubes)
        if err != nil {
            http.Error(w, "Failed to marshal cube state", http.StatusInternalServerError)
            return
        }
        w.Header().Set("Content-Type", "application/json")
        w.Write(data)
    } else if r.Method == http.MethodPost {
        var updatedCubes map[string]*Cube
        decoder := json.NewDecoder(r.Body)
        if err := decoder.Decode(&updatedCubes); err != nil {
            http.Error(w, "Invalid cube state", http.StatusBadRequest)
            return
        }
        for id, cube := range updatedCubes {
            if _, exists := state.Cubes[id]; exists {
                state.Cubes[id] = cube
                hasChanges = true
            }
        }
        w.WriteHeader(http.StatusOK)
    } else {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
    }
}

func floorHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if r.Method == http.MethodGet {
        data, err := json.Marshal(map[string]Floor{"floor": state.Floor})
        if err != nil {
            http.Error(w, "Failed to marshal floor state", http.StatusInternalServerError)
            return
        }
        w.Header().Set("Content-Type", "application/json")
        w.Write(data)
    } else if r.Method == http.MethodPost {
        var updatedFloor Floor
        decoder := json.NewDecoder(r.Body)
        if err := decoder.Decode(&updatedFloor); err != nil {
            http.Error(w, "Invalid floor state", http.StatusBadRequest)
            return
        }
        state.Floor = updatedFloor
        hasChanges = true
        w.WriteHeader(http.StatusOK)
    } else {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
    }
}

func errorReportHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodPost {
        http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
        return
    }

    var errorReport map[string]interface{}
    err := json.NewDecoder(r.Body).Decode(&errorReport)
    if err != nil {
        http.Error(w, "Invalid JSON", http.StatusBadRequest)
        return
    }

    log.Printf("Error Report: %+v\n", errorReport)
    w.WriteHeader(http.StatusOK)
}

func loadState() {
    data, err := ioutil.ReadFile(saveFile)
    if err != nil {
        log.Printf("Error reading save file: %v", err)
        return
    }

    err = json.Unmarshal(data, &state)
    if err != nil {
        log.Printf("Error unmarshaling save file: %v", err)
        return
    }

    log.Println("Loaded saved state")
}

func saveState() {
    data, err := json.Marshal(state)
    if err != nil {
        log.Printf("Error marshaling state: %v", err)
        return
    }

    err = ioutil.WriteFile(saveFile, data, 0644)
    if err != nil {
        log.Printf("Error writing save file: %v", err)
        return
    }

    log.Println("Saved current state")
    hasChanges = false
}

func autoSave() {
    ticker := time.NewTicker(5 * time.Second)
    for range ticker.C {
        mutex.Lock()
        if hasChanges {
            saveState()
        }
        mutex.Unlock()
    }
}
