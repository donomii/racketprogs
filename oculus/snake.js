// Constants
const SNAKE_COUNT = 300;
const SNAKE_LENGTH = 200;
const SNAKE_SPEED = 0.002;
const DIRECTION_CHANGE_INTERVAL = 5000;
const MAX_DISTANCE = 0.75;

function createSnakes(count, length, THREE) {
    const snakes = [];
    for (let i = 0; i < count; i++) {
        snakes.push(createSnake(length, THREE));
    }
    return snakes;
}

function createSnake(length, THREE) {
    const snakeGeometry = new THREE.BufferGeometry();
    const positions = new Float32Array(length * 3);
    const colors = new Float32Array(length * 4); // Changed to 4 components (RGBA)
    const maxInitialDistance = MAX_DISTANCE;

    for (let i = 0; i < length; i++) {
        const theta = Math.random() * Math.PI * 2;
        const phi = Math.acos(2 * Math.random() - 1);
        const distance = Math.random() * maxInitialDistance;

        positions[i * 3] = distance * Math.sin(phi) * Math.cos(theta);
        positions[i * 3 + 1] = distance * Math.sin(phi) * Math.sin(theta);
        positions[i * 3 + 2] = distance * Math.cos(phi);

        // RGBA colors with alpha
        const opacity = 1 - (i / (length - 1)); // Fades from 1 to 0
        colors[i * 4] = 1 - i / length;         // R
        colors[i * 4 + 1] = 0.5 - i / (2 * length); // G
        colors[i * 4 + 2] = i / length;         // B
        colors[i * 4 + 3] = opacity;            // A
    }

    snakeGeometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));
    snakeGeometry.setAttribute('color', new THREE.BufferAttribute(colors, 4));

    // Create a custom material that supports vertex colors with transparency
    const snakeMaterial = new THREE.ShaderMaterial({
        vertexShader: `
            attribute vec4 color;
            varying vec4 vColor;
            
            void main() {
                vColor = color;
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
        `,
        fragmentShader: `
            varying vec4 vColor;
            
            void main() {
                gl_FragColor = vColor;
            }
        `,
        transparent: true,
        depthWrite: false,
        blending: THREE.AdditiveBlending
    });

    const snake = new THREE.Line(snakeGeometry, snakeMaterial);
    snake.direction = new THREE.Vector3(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5).normalize();
    return snake;
}


function updateSnakes(snakes, THREE) {
    if (!snakes || snakes.length === 0) return [];
    
    return snakes.map(snake => {
        if (!snake.targetObject) return snake;

        const positions = snake.geometry.attributes.position.array;
        const colors = snake.geometry.attributes.color.array;

        // Update positions
        for (let i = SNAKE_LENGTH - 1; i > 0; i--) {
            positions[i * 3] = positions[(i - 1) * 3];
            positions[i * 3 + 1] = positions[(i - 1) * 3 + 1];
            positions[i * 3 + 2] = positions[(i - 1) * 3 + 2];
        }

        const headPos = new THREE.Vector3(positions[0], positions[1], positions[2]);
        headPos.add(snake.direction.clone().multiplyScalar(SNAKE_SPEED));

        const distanceFromCenter = headPos.distanceTo(snake.targetObject.position);

        if (distanceFromCenter > MAX_DISTANCE) {
            const directionToCenter = snake.targetObject.position.clone().sub(headPos).normalize();
            snake.direction.lerp(directionToCenter, 0.1);
            snake.direction.normalize();

            const scale = MAX_DISTANCE / distanceFromCenter;
            headPos.sub(snake.targetObject.position).multiplyScalar(scale).add(snake.targetObject.position);
        }

        positions[0] = headPos.x;
        positions[1] = headPos.y;
        positions[2] = headPos.z;

        // Update alpha values
        for (let i = 0; i < SNAKE_LENGTH; i++) {
            colors[i * 4 + 3] = 1 - (i / (SNAKE_LENGTH - 1));
        }

        snake.geometry.attributes.position.needsUpdate = true;
        snake.geometry.attributes.color.needsUpdate = true;
        return snake;
    });
}

function changeSnakeDirections(snakes) {
    return snakes.map(snake => {
        snake.direction.set(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5).normalize();
        return snake;
    });
}