package main

import (
	"io"
	"log"
	"net/mail"
	"os"

	mbox "github.com/emersion/go-mbox"
)

func panicErr(err error) {
	if err != nil {
		panic(err)
	}
}

func processMbox(file string,output OutputFunc) error{
	f, err := os.Open(file)
	panicErr(err)

	defer f.Close()

	r := io.Reader(f)
	mr := mbox.NewReader(r)
	for {
		r, err := mr.NextMessage()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Print("Could not read next message", err)
			panicErr(err)
		}

		msg, err := mail.ReadMessage(r)
		panicErr(err)

		log.Printf("Message from %v\n", msg.Header.Get("From"))
		bodyReader := msg.Body
		body, _ := io.ReadAll(bodyReader)

		dataout := map[string]interface{}{
			"to":          msg.Header.Get("To"),
			"from":        msg.Header.Get("From"),
			"subject":     msg.Header.Get("Subject"),
			"date":        msg.Header.Get("Date"),
			"messageid":   msg.Header.Get("Message-Id"),
			"contenttype": msg.Header.Get("Content-Type"),
			"body":        string(body),
		}

		o := Record{
			Data: dataout,
		}

		output(o)
	}
	return nil
}
