package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

// FileProcessor defines the interface for file type processors
type FileProcessor interface {
	Process(data []byte, config ProcessorConfig) (llamaresponse, error)
}

// processEPUB extracts and processes content from EPUB files
func processEPUB(data []byte, config ProcessorConfig) (llamaresponse, error) {

	tmpfile, err := createTempFile("epub-*.epub", data)
	if err != nil {
		return nil, fmt.Errorf("failed to create temp file: %v", err)
	}
	defer cleanupTempFile(tmpfile)

	content, err := extractEPUBContent(tmpfile.Name())
	if err != nil {
		return nil, fmt.Errorf("failed to extract EPUB content: %v", err)
	}

	// Extract metadata and content
	metadata, err := extractEPUBMetadata(content)
	if err != nil {
		return nil, fmt.Errorf("failed to extract EPUB metadata: %v", err)
	}

	metadata["content"] = string(content)

	return metadata, nil
}

// processPDF extracts and processes content from PDF files
func processPDF(data []byte, config ProcessorConfig) (llamaresponse, error) {

	tmpfile, err := createTempFile("pdf-*.pdf", data)
	if err != nil {
		return nil, fmt.Errorf("failed to create temp file: %v", err)
	}
	defer cleanupTempFile(tmpfile)

	// Extract text content using pdftotext
	content, err := extractPDFContent(tmpfile.Name())
	if err != nil {
		return nil, fmt.Errorf("failed to extract PDF content: %v", err)
	}

	// Extract metadata using pdfinfo if available
	metadata, err := extractPDFMetadata(tmpfile.Name())
	if err != nil {
		// Log but don't fail if metadata extraction fails
		log.Printf("Warning: PDF metadata extraction failed: %v\n", err)
	}

	metadata["content"] = string(content)

	return metadata, nil

}

func processText(path string, data []byte, config ProcessorConfig, ctx ProcessContext) (llamaresponse, error) {
	// Check if this is Go source code first
	if strings.HasSuffix(strings.ToLower(path), ".go") {
		err := processGolang(data, config, func(r Record) error {
			return nil
		}, ctx)
		if err == nil {
			var res = make(llamaresponse)
			res["type"] = "text/x-go"
			res["content"] = string(data)
			return res, nil
		}
		// Fall through to other processors if Go processing fails
	}

	if  useOllama && askOllamaYesNo(config, "Is this programming language source code?\n"+string(data)) {
		return processSourceCode(string(data), config)
	}
	if isJSON(string(data)) {
		return processJSONContent(string(data))
	}
	if isMarkdown(data) {
		return processMarkdownContent(string(data))
	}

	var res = make(llamaresponse)
	// Process as plain text
	res["content"] = string(data)
	return res, nil
}

// Helper functions

func createTempFile(pattern string, data []byte) (*os.File, error) {
	tmpfile, err := ioutil.TempFile("", pattern)
	if err != nil {
		return nil, err
	}

	if _, err := tmpfile.Write(data); err != nil {
		tmpfile.Close()
		os.Remove(tmpfile.Name())
		return nil, err
	}

	if err := tmpfile.Close(); err != nil {
		os.Remove(tmpfile.Name())
		return nil, err
	}

	return tmpfile, nil
}

func cleanupTempFile(f *os.File) {
	if f != nil {
		os.Remove(f.Name())
	}
}

func extractEPUBContent(filepath string) ([]byte, error) {
	cmd := exec.Command("epub2txt", "-a", "-n", filepath)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("epub2txt failed: %v, stderr: %s", err, stderr.String())
	}

	return stdout.Bytes(), nil
}

func extractPDFContent(filepath string) ([]byte, error) {
	cmd := exec.Command("pdftotext", filepath, "-")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("pdftotext failed: %v, stderr: %s", err, stderr.String())
	}

	return stdout.Bytes(), nil
}

func extractPDFMetadata(filepath string) (llamaresponse, error) {
	cmd := exec.Command("pdfinfo", filepath)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("pdfinfo failed: %v, stderr: %s", err, stderr.String())
	}

	return parsePDFInfo(stdout.String()), nil
}

func parsePDFInfo(info string) llamaresponse {
	metadata := make(llamaresponse)
	lines := strings.Split(info, "\n")

	for _, line := range lines {
		parts := strings.SplitN(line, ":", 2)
		if len(parts) == 2 {
			key := strings.ToLower(strings.TrimSpace(parts[0]))
			value := strings.TrimSpace(parts[1])
			metadata[key] = value
		}
	}

	return metadata
}

func extractEPUBMetadata(content []byte) (llamaresponse, error) {
	metadata := make(llamaresponse)
	lines := bytes.Split(content, []byte{'\n'})

	// Try to extract title and author from the first few lines
	for i, line := range lines {
		if i >= 20 { // Only check first 20 lines
			break
		}
		line := string(bytes.TrimSpace(line))
		if line != "" {
			if metadata["title"] == "" {
				metadata["title"] = line
			} else if metadata["author"] == "" {
				metadata["author"] = line
				break
			}
		}
	}

	return metadata, nil
}

func processJSONContent(content string) (llamaresponse, error) {
	res := make(llamaresponse)
	res["type"] = "application/json"
	res["content"] = content

	// Validate JSON and get structure info
	var js interface{}
	if err := json.Unmarshal([]byte(content), &js); err == nil {
		if m, ok := js.(map[string]interface{}); ok {
			res["json_keys"] = strings.Join(getJSONKeys(m), ", ")
		}
	}

	return res, nil
}

func processMarkdownContent(content string) (llamaresponse, error) {
	res := make(llamaresponse)
	res["type"] = "text/markdown"
	res["content"] = content
	res["title"] = extractMarkdownTitle(content)
	res["headings"] = extractMarkdownHeadings(content)

	return res, nil
}

func processSourceCode(content string, config ProcessorConfig) (llamaresponse, error) {
	res, err:= promptForMapWithOllama(content, config, []string{"title", "author", "date", "summary", "language", "functions", "variables", "classes", "methods", "imports", "dependencies", "loc"})
	if err != nil {
		return nil, fmt.Errorf("Ollama extraction failed: %v", err)
	}

	res["content"] = content
	res["loc"] = fmt.Sprintf("%d", bytes.Count([]byte(content), []byte{'\n'})+1)

	return res, nil
}

func combineMetadata(base, additional llamaresponse) llamaresponse {
	if base == nil {
		base = make(llamaresponse)
	}
	for k, v := range additional {
		if v != "" {
			base[k] = v
		}
	}
	return base
}

func getJSONKeys(m map[string]interface{}) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}

func extractMarkdownHeadings(content string) string {
	var headings []string
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") {
			headings = append(headings, line)
		}
	}
	return strings.Join(headings, "; ")
}

func countPages(content []byte) int {
	// Count form feed characters plus 1 for the first page
	return bytes.Count(content, []byte{'\f'}) + 1
}

func isJSON(content string) bool {
	var js interface{}
	return json.Unmarshal([]byte(content), &js) == nil
}

func isMarkdown(data []byte) bool {
	// Check for common markdown indicators
	indicators := [][]byte{
		[]byte("#"),
		[]byte("=="),
		[]byte("--"),
		[]byte("```"),
		[]byte("*"),
		[]byte("_"),
		[]byte("["),
		[]byte("]("),
	}

	for _, indicator := range indicators {
		if bytes.Contains(data, indicator) {
			return true
		}
	}
	return false
}

func extractMarkdownTitle(content string) string {
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "# ") {
			return strings.TrimSpace(line[2:])
		}
	}
	return ""
}

func isSourceCode(ext string) bool {
	sourceExts := map[string]bool{
		".go": true, ".js": true, ".py": true, ".java": true,
		".c": true, ".cpp": true, ".h": true, ".hpp": true,
		".rs": true, ".rb": true, ".php": true, ".cs": true,
		".ts": true, ".swift": true, ".kt": true, ".scala": true,
	}
	return sourceExts[strings.ToLower(ext)]
}

func detectLanguage(ext string) string {
	langMap := llamaresponse{
		".go":    "go",
		".js":    "javascript",
		".py":    "python",
		".java":  "java",
		".c":     "c",
		".cpp":   "cpp",
		".h":     "c",
		".hpp":   "cpp",
		".rs":    "rust",
		".rb":    "ruby",
		".php":   "php",
		".cs":    "csharp",
		".ts":    "typescript",
		".swift": "swift",
		".kt":    "kotlin",
		".scala": "scala",
	}
	if lang, ok := langMap[strings.ToLower(ext)]; ok {
		return fmt.Sprint(lang)
	}
	return "unknown"
}
