package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"go/types"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type functionPositions_s struct {
	StartPosition int
	EndPosition   int
}

func processGolang(data []byte, config ProcessorConfig, output OutputFunc, ctx ProcessContext) error {
	//Save to a temporary file
	tmpfile, err := createTempFile("go-*.go", data)
	panicErr(err)

	file := tmpfile.Name()

	pkg, functions := extractFunctions(file)

	// Use the end of the previous function as the start of the next function
	// This picks up comments between the functions
	// This is not a hack, you're a hack
	if len(functions) == 0 {

		return fmt.Errorf("No functions found in file")
	}


	globalsEnd := getStart(functions[0]) - 1

	var lastEnd int = globalsEnd
	for i, function := range functions {
		r := Record{
			Source:       file,
			RunID:        ctx.RunID,
			CollectionID: ctx.CollectionID,
			Data:         prepareMap(pkg, file, function, lastEnd, i),
		}

		output(r)

		lastEnd = getEnd(function)
	}

	fdata, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	//Get the source code with the functions removed (i.e. the global variables)
	// Find the start and end positions of every function, sort the functions by start position

	//Starting with the last function, delete them from the source code one by one
	fposs := []functionPositions_s{}
	lastEnd = globalsEnd

	for _, function := range functions {
		//Get the source code of the function
		startpos := lastEnd + 1
		endpos := getEnd(function)
		if endpos > len(fdata) {
			endpos = len(fdata) - 1
		}
		lastEnd = endpos

		functionPosition := functionPositions_s{startpos, endpos}
		fposs = append(fposs, functionPosition)
	}
	//Sort the functions by start position, descending
	sort.Slice(fposs, func(i, j int) bool {
		a := fposs[i].StartPosition
		b := fposs[j].StartPosition
		return a > b
	})

	//Delete the functions from the source code one by one
	for _, function := range fposs {
		if function.EndPosition > len(fdata) {
			function.EndPosition = len(fdata)
		}
		fdata = append(fdata[:function.StartPosition-1], fdata[function.EndPosition:]...)
	}

	gl := Record{
		Source:       file,
		RunID:        ctx.RunID,
		CollectionID: ctx.CollectionID,
		Data: map[string]interface{}{
			"source":   string(fdata),
			"package":  pkg,
			"position": 0,
			"globals":  true,
		},
	}

	output(gl)

	return nil
}

// insert a function to weaviate
func prepareMap(pkg, filename string, function *ast.FuncDecl, lastEnd, position int) map[string]interface{} {
	//Load the file
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		//fmt.Println("Error reading file:", err)
	}

	//Get the source code of the function
	startpos := lastEnd
	endpos := getEnd(function)
	if endpos > len(data) {
		endpos = len(data) - 1
	}
	source := data[startpos:endpos]

	out := map[string]interface{}{

				"class": "Code",
					"source":         string(source),
					"name":           getName(function),
					"argument_types": getArgumentTypeList(function),
					"package":        pkg,
					"filename":       filename,
					"start_pos":      startpos,
					"end_pos":        startpos,
					"position":       fmt.Sprintf("%v", position),
				}
	return out
}

// Recursively find all the .go files in a directory
func findGoFiles(root string) []string {
	var goFiles []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.HasSuffix(path, ".go") {
			goFiles = append(goFiles, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	return goFiles
}

// Get the functions from a file
func extractFunctions(file string) (string, []*ast.FuncDecl) {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, file, nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}

	var functions []*ast.FuncDecl
	ast.Inspect(node, func(n ast.Node) bool {
		fn, ok := n.(*ast.FuncDecl)
		if ok {
			functions = append(functions, fn)
		}
		return true
	})

	//Get package name
	packageName := node.Name.Name

	return packageName, functions
}

// Get the source code of a function
func getSource(fn *ast.FuncDecl) string {
	var sb strings.Builder
	fset := token.NewFileSet()
	err := printer.Fprint(&sb, fset, fn)
	if err != nil {
		panic(err)
	}
	return sb.String()
}

// Get the start position of a function
func getStart(fn *ast.FuncDecl) int {
	return int(fn.Pos())
}

// Get the end position of a function
func getEnd(fn *ast.FuncDecl) int {
	return int(fn.End())
}

// get the name of a function
func getName(fn *ast.FuncDecl) string {
	return fn.Name.Name
}

// get the argument types of a function
func getArgumentTypeList(fn *ast.FuncDecl) string {
	var sb strings.Builder
	for _, arg := range fn.Type.Params.List {
		sb.WriteString(getType(arg.Type))
		sb.WriteString(", ")
	}
	return sb.String()
}

// get the package of a function
func getPackage(fn *ast.FuncDecl) string {
	return fn.Name.Name
}

// get the return types of a function
func getType(expr ast.Expr) string {
	switch t := expr.(type) {
	case *ast.Ident:
		return t.Name
	case *ast.SelectorExpr:
		return getType(t.X) + "." + t.Sel.Name
	case *ast.StarExpr:
		return "*" + getType(t.X)
	case *ast.ArrayType:
		return "[]" + getType(t.Elt)
	case *ast.MapType:
		return "map[" + getType(t.Key) + "]" + getType(t.Value)
	case *ast.InterfaceType:
		return "interface{}"
	case *ast.ChanType:
		return "chan " + getType(t.Value)
	default:
		return types.ExprString(t)
	}
}
