package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"


	"github.com/google/uuid"
	"strings"
	"os/exec"


)

type ProcessContext struct {
	RunID        string
	CollectionID string
}

func generateRunID(startPath string) string {
	timestamp := time.Now().Format("20060102-150405")
	cleanPath := filepath.Clean(startPath)
	if cleanPath == "." || cleanPath == "" {
		//Find the actual path
		var err error
		cleanPath, err = filepath.Abs(startPath)
		if err != nil {
			cleanPath = startPath
		}
	}
	return fmt.Sprintf("run-%s-%s", timestamp, cleanPath)
}

func generateCollectionID(path string) string {
	// For directories and mailboxes, use the path as part of the collection ID
	cleanPath := filepath.Clean(path)
	if cleanPath == "." || cleanPath == "" {
		//Find the actual path
		var err error
		cleanPath, err = filepath.Abs(path)
		if err != nil {
			cleanPath = path
		}
	}
	timestamp := time.Now().Format("20060102-150405")
	return fmt.Sprintf("col-%s-%s", timestamp, cleanPath)
}


type Record struct {
    Source              string        `json:"source"`
    Timestamp           string        `json:"timestamp"`
    GUID                string        `json:"guid"`
    SHA256              string        `json:"sha256"`
    Vector              []float64     `json:"vector,omitempty"`
    AIVector            []float64     `json:"ai_vector,omitempty"`      // New field
    Type                string        `json:"type"`
    Data                llamaresponse `json:"data"`
    RunID               string        `json:"run_id"`
    CollectionID        string        `json:"collection_id"`
    ContentMod26Bigram  []float32    `json:"content_mod26_bigram,omitempty"`
    ContentMod64Bigram  []float32    `json:"content_mod64_bigram,omitempty"`
    ContentMod26Trigram []float32    `json:"content_mod26_trigram,omitempty"`
    PathMod26Bigram     []float32    `json:"path_mod26_bigram,omitempty"`
    PathMod64Bigram     []float32    `json:"path_mod64_bigram,omitempty"`
    PathMod26Trigram    []float32    `json:"path_mod26_trigram,omitempty"`
}


type OutputFunc func(Record) error
// ProcessorConfig holds the configuration for content processing
// ProcessorConfig holds the configuration for content processing
type ProcessorConfig struct {
	UseOllama       bool
	OllamaURL       string
	CacheDir        string
	CacheEnabled    bool
	MaximumFileSize int
	Timeout         time.Duration
	OllamaModel     string
	CurrentFile     string
}

func NewProcessorConfig(useOllama bool, ollamaURL string, ollamaModel string, timeout time.Duration) (*ProcessorConfig, error) {
	config := &ProcessorConfig{
		UseOllama:       useOllama,
		OllamaURL:       ollamaURL,
		OllamaModel:     ollamaModel,
		Timeout:         timeout,
		MaximumFileSize: 10 * 1024 * 1024, // 10MB default
	}

	if err := config.Validate(); err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}

	return config, nil
}

func (c *ProcessorConfig) Validate() error {
	if c.UseOllama {
		if c.OllamaURL == "" {
			return fmt.Errorf("ollama URL required when UseOllama is true")
		}
		if c.OllamaModel == "" {
			return fmt.Errorf("ollama model required when UseOllama is true")
		}
	}

	if c.MaximumFileSize <= 0 {
		return fmt.Errorf("maximum file size must be positive")
	}

	if c.Timeout <= 0 {
		return fmt.Errorf("timeout must be positive")
	}

	return nil
}

func extractText(data []byte, fileType string, config ProcessorConfig) (string, error) {
    var rawText string
    
    switch fileType {
    case "application/pdf":
        tmpfile, err := createTempFile("pdf-*.pdf", data)
        if err != nil {
            return "", fmt.Errorf("failed to create temp file: %v", err)
        }
        defer cleanupTempFile(tmpfile)
        
        content, err := extractPDFContent(tmpfile.Name())
        if err != nil {
            return "", fmt.Errorf("failed to extract PDF content: %v", err)
        }
        rawText = string(content)
        
    case "application/epub+zip":
        tmpfile, err := createTempFile("epub-*.epub", data)
        if err != nil {
            return "", fmt.Errorf("failed to create temp file: %v", err)
        }
        defer cleanupTempFile(tmpfile)
        
        content, err := extractEPUBContent(tmpfile.Name())
        if err != nil {
            return "", fmt.Errorf("failed to extract EPUB content: %v", err)
        }
        rawText = string(content)
        
    case "text/x-go", "text/x-java", "text/x-python", "text/x-c", "text/x-cpp":
        // For source code, use special cleaning
        rawText = cleanSourceCode(string(data))

    case "text/plain", "text/markdown", "application/json", "text/html":
        rawText = string(data)

    default:
        return "", fmt.Errorf("unsupported file type for text extraction: %s", fileType)
    }
    
    // Normalize the extracted text
    normalizedText := normalizeText(rawText)
    
    // Ensure we have meaningful content
    if len(strings.TrimSpace(normalizedText)) < 10 {
        return "", fmt.Errorf("insufficient text content after normalization")
    }
    
    return normalizedText, nil
}

var useOllama bool

func main() {
	flag.BoolVar(&useOllama, "use-ollama", false, "Use Ollama for unknown files")
	ollamaURL := flag.String("ollama-url", "http://localhost:11434", "Ollama API URL")
	ollamaModel := flag.String("ollama-model", "llama2", "Ollama model name")
	timeout := flag.Duration("timeout", 30*time.Second, "Timeout for Ollama requests")
	flag.Parse()

	config, err := NewProcessorConfig(useOllama, *ollamaURL, *ollamaModel, *timeout)
	if err != nil {
		log.Fatalf("Failed to initialize processor config: %v", err)
	}

	firstPath := ""
	if len(flag.Args()) > 0 {
		firstPath = flag.Args()[0]
	}
	runID := generateRunID(firstPath)

	output := func(r Record) error {
		r.RunID = runID
		return json.NewEncoder(os.Stdout).Encode(r)
	}

	for _, path := range flag.Args() {
		ctx := ProcessContext{
			RunID:        runID,
			CollectionID: generateCollectionID(path),
		}
		if err := processPath(path, *config, output, ctx); err != nil {
			log.Printf("Error processing %s: %v", path, err)
		}
	}
}


func processPath(path string, config ProcessorConfig, output OutputFunc, ctx ProcessContext) error {
    // Check if the path is a URL
    if strings.HasPrefix(path, "http://") || strings.HasPrefix(path, "https://") {
        // Create a new crawler for this URL
        crawlerConfig := CrawlConfig{
            MaxDepth:     1,      // Just get the initial page
            MaxPages:     1,      // Just get the initial page
            Delay:        1,      // 1 second delay
            AllowedHosts: []string{}, // Only crawl the same host
        }

        crawler := NewCrawler(crawlerConfig, config, func(result CrawlResult) {
            if result.Error != nil {
                log.Printf("Error crawling %s: %v", result.URL, result.Error)
                return
            }

            // Create record from crawl result
            record := Record{
                Source:    result.URL,
                Timestamp: time.Now().UTC().Format(time.RFC3339),
                GUID:      generateGUID(result.URL, []byte(result.Content)),
                SHA256:    calculateSHA256([]byte(result.Content)),
                Type:      "text/html",
                Data:      result.Metadata,
            }

            // Add any additional metadata
            if record.Data == nil {
                record.Data = make(llamaresponse)
            }
            record.Data["content"] = result.Content
            record.Data["url"] = result.URL

            output(record)
        })

        return crawler.Crawl(path, ctx.RunID)
    }

    // Original local file/directory processing logic
    if strings.HasSuffix(path, ".DS_Store") {
        return nil
    }
    
    info, err := os.Stat(path)
    if err != nil {
        return fmt.Errorf("failed to stat path %s: %v", path, err)
    }

    if info.IsDir() {
        dirCtx := ProcessContext{
            RunID:        ctx.RunID,
            CollectionID: generateCollectionID(path),
        }

        currentCtx := dirCtx

        return filepath.Walk(path, func(p string, info os.FileInfo, err error) error {
            if err != nil {
                log.Printf("Warning: error accessing path %s: %v", p, err)
                return nil
            }

            if shouldSkipPath(p, info) {
                if info.IsDir() {
                    return filepath.SkipDir
                }
                return nil
            }

            if info.IsDir() {
                if p != path {
                    currentCtx = ProcessContext{
                        RunID:        dirCtx.RunID,
                        CollectionID: generateHierarchicalCollectionID(dirCtx.CollectionID, p),
                    }
                }
                return nil
            }

            if err := processFile(p, config, output, currentCtx); err != nil {
                log.Printf("Warning: error processing file %s: %v", p, err)
                return nil
            }
            return nil
        })
    }
    
    return processFile(path, config, output, ctx)
}

// shouldSkipPath determines if a path should be skipped during processing
func shouldSkipPath(path string, info os.FileInfo) bool {
    // Skip hidden files and directories
    if strings.HasPrefix(filepath.Base(path), ".") {
        return true
    }

    // Skip specific directories
    if info.IsDir() {
        switch filepath.Base(path) {
        case "node_modules", "vendor", ".git", ".svn", ".hg":
            return true
        }
    }

    // Skip specific file types
    if !info.IsDir() {
        switch filepath.Ext(path) {
        case ".log", ".tmp", ".temp", ".swp":
            return true
        }
    }

    return false
}

// generateHierarchicalCollectionID creates a collection ID that maintains directory hierarchy
func generateHierarchicalCollectionID(parentID string, path string) string {
    // Extract the directory name
    dirName := filepath.Base(path)
    // Create a hierarchical ID by combining parent ID and current directory
    timestamp := time.Now().Format("20060102-150405")
    return fmt.Sprintf("%s/col-%s-%s", parentID, timestamp, dirName)
}

// Update processFile signature:
func processFile(path string, config ProcessorConfig, output1 OutputFunc, ctx ProcessContext) error {
	// If path ends in .DS_Store, ignore it
	if strings.HasSuffix(path, ".DS_Store") {
		return nil
	}
	output := func(r Record) error {
		r.Source = path
		r.RunID = ctx.RunID
		r.CollectionID = ctx.CollectionID
		return output1(r)
	}

	switch filepath.Ext(path) {
	case ".mbox":
		// Create a new collection ID for the mailbox
		mboxCtx := ProcessContext{
			RunID:        ctx.RunID,
			CollectionID: generateCollectionID(path),
		}
		return processMbox(path, func(data Record) error {
			data.RunID = mboxCtx.RunID
			data.CollectionID = mboxCtx.CollectionID
			content := []byte(data.Data["body"].(string))
			return output(Record{
				Source:       path,
				Timestamp:    time.Now().UTC().Format(time.RFC3339),
				GUID:         generateGUID(path, content),
				SHA256:       calculateSHA256(content),
				Type:         "email",
				Data:         data.Data,
				RunID:        mboxCtx.RunID,
				CollectionID: mboxCtx.CollectionID,
			})
		})
	default:
		return processSingleFile(path, config, output, ctx)
	}
}

// Store all successful detection results
type detectionResult struct {
	fileType string
	metadata llamaresponse
	confidence float64
}

func processSingleFile(path string, config ProcessorConfig, output OutputFunc, ctx ProcessContext) error {
	// If path ends in .DS_Store, ignore it
	if strings.HasSuffix(path, ".DS_Store") {
		return nil
	}
	data, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	// Get file size early since we already have the data
	fileSize := len(data)

	// Generate path vectors first
	pathVectors, err := PathVector(path)
	if err != nil {
		// Log error but continue processing
		log.Printf("Warning: path vector generation failed for %s: %v\n", path, err)
	}

	var results []detectionResult

	// Try file utility first
	mimeType, err := detectMimeType(data)
	if err == nil && mimeType != "" {
		// Initialize metadata with the content
		metadata := llamaresponse{
			"mime_type": mimeType,
			"size":      fileSize,
			"content":   string(data),  // Add the content to metadata
		}
		
		results = append(results, detectionResult{
			fileType:   mimeType,
			metadata:   metadata,
			confidence: 0.8, // High confidence for file utility
		})
	}

	// If no results yet, add a fallback detection
	if len(results) == 0 {
		metadata := llamaresponse{
			"mime_type": "application/octet-stream",
			"size":      fileSize,
			"content":   string(data),  // Add content even for unknown types
		}
		results = append(results, detectionResult{
			fileType:   "application/octet-stream",
			metadata:   metadata,
			confidence: 0.1,
		})
	}

	// Merge metadata from all detections, preferring metadata from higher confidence results
	mergedMetadata := make(llamaresponse)
	for _, result := range results {
		for k, v := range result.metadata {
			if _, exists := mergedMetadata[k]; !exists {
				mergedMetadata[k] = v
			}
		}
	}

	// Ensure size and content are included in merged metadata
	mergedMetadata["size"] = fileSize
	if _, hasContent := mergedMetadata["content"]; !hasContent {
		mergedMetadata["content"] = string(data)
	}

	// Create the record structure
	record := Record{
		Source:    path,
		Timestamp: time.Now().UTC().Format(time.RFC3339),
		GUID:      generateGUID(path, data),
		SHA256:    calculateSHA256(data),
		Type:      results[0].fileType,
		Data:      mergedMetadata,
	}

	// Generate AI vector first if Ollama is enabled
	if config.UseOllama {
		if aiVector, err := generateEmbedding(data, results[0].fileType, config); err == nil {
			record.AIVector = aiVector // Set AIVector properly
		} else {
			log.Printf("Warning: AI vector generation failed for %s: %v\n", path, err)
		}
	}

	// Add path vectors at top level
	if pathVectors != nil {
		if v, ok := pathVectors["path_mod26_bigram"].([]float32); ok {
			record.PathMod26Bigram = v
		}
		if v, ok := pathVectors["path_mod64_bigram"].([]float32); ok {
			record.PathMod64Bigram = v
		}
		if v, ok := pathVectors["path_mod26_trigram"].([]float32); ok {
			record.PathMod26Trigram = v
		}
	}

	// Generate content ngram vectors at top level
	if contentMod26Bigram, err := Mod26Vector(data); err == nil {
		record.ContentMod26Bigram = contentMod26Bigram
	}

	if contentMod64Bigram, err := ModNVector(data, 64); err == nil {
		record.ContentMod64Bigram = contentMod64Bigram
	}

	if contentMod26Trigram, err := trigramVector(data); err == nil {
		record.ContentMod26Trigram = contentMod26Trigram
	}

	// Add detection confidence information
	record.Data["detection_confidence"] = results[0].confidence
	record.Data["possible_types"] = getPossibleTypes(results)

	return output(record)
}

func detectMimeType(data []byte) (string, error) {
	// Create a temporary file
	tmpfile, err := createTempFile("detect-*", data)
	if err != nil {
		return "", err
	}
	defer cleanupTempFile(tmpfile)

	// Run file command with MIME type option
	cmd := exec.Command("file", "-b", "--mime-type", tmpfile.Name())
	output, err := cmd.Output()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(output)), nil
}

func getPossibleTypes(results []detectionResult) []map[string]interface{} {
	var types []map[string]interface{}
	for _, result := range results {
		types = append(types, map[string]interface{}{
			"type":       result.fileType,
			"confidence": result.confidence,
		})
	}
	return types
}

func generateGUID(path string, data []byte) string {
	hostname, _ := os.Hostname()
	pid := os.Getpid()
	now := time.Now().UnixNano()

	ns := uuid.NewSHA1(uuid.NameSpaceOID, []byte(fmt.Sprintf("%s-%d-%d-%x",
		hostname, pid, now, sha256.Sum256(data))))
	return ns.String()
}

func calculateSHA256(data []byte) string {
	hash := sha256.Sum256(data)
	return hex.EncodeToString(hash[:])
}
