package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"regexp"
	"strings"
	"unicode"
	"io"
	"net/http"
	"bytes"

)

type llamaresponse map[string]interface{}



type ollamaEmbeddingRequest struct {
	Model  string `json:"model"`
	Prompt string `json:"prompt"`
}

type ollamaEmbeddingResponse struct {
	Embedding []float64 `json:"embedding"`
}

type ollamaGenerateRequest struct {
	Model     string                 `json:"model"`
	Prompt    string                 `json:"prompt"`
	Stream    bool                   `json:"stream"`
	Options   map[string]interface{} `json:"options,omitempty"`
}

type ollamaGenerateResponse struct {
	Response string `json:"response"`
}

// generateEmbedding creates a vector embedding for the given content using Ollama
func generateEmbedding(data []byte, fileType string, config ProcessorConfig) ([]float64, error) {
    if !config.UseOllama {
        return []float64{}, fmt.Errorf("Ollama not configured for embedding generation")
    }

    // Extract and normalize text content based on file type
    normalizedText, err := extractText(data, fileType, config)
    if err != nil {
        return nil, fmt.Errorf("text extraction failed: %v", err)
    }

    // Truncate content if it's too long
    content := truncateContent([]byte(normalizedText), 8192)

    // Prepare request
    reqBody := ollamaEmbeddingRequest{
        Model:  config.OllamaModel,
        Prompt: string(content),
    }

    jsonData, err := json.Marshal(reqBody)
    if err != nil {
        return nil, fmt.Errorf("failed to marshal request: %v", err)
    }

    // Create HTTP request with context and timeout
    ctx, cancel := context.WithTimeout(context.Background(), config.Timeout)
    defer cancel()

    req, err := http.NewRequestWithContext(ctx, "POST", config.OllamaURL+"/api/embeddings", bytes.NewBuffer(jsonData))
    if err != nil {
        return nil, fmt.Errorf("failed to create request: %v", err)
    }
    req.Header.Set("Content-Type", "application/json")

    // Make request
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return nil, fmt.Errorf("embedding request failed: %v", err)
    }
    defer resp.Body.Close()

    // Read response
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return nil, fmt.Errorf("failed to read response: %v", err)
    }

    if resp.StatusCode != http.StatusOK {
        return nil, fmt.Errorf("embedding request failed with status %d: %s", resp.StatusCode, string(body))
    }

    // Parse response
    var embedResp ollamaEmbeddingResponse
    if err := json.Unmarshal(body, &embedResp); err != nil {
        return nil, fmt.Errorf("failed to parse response: %v", err)
    }

    return embedResp.Embedding, nil
}

func doOllama(prompt string, config ProcessorConfig) (string, error) {
    if !config.UseOllama {
        return "", fmt.Errorf("Ollama not configured")
    }

    // Prepare request
    reqBody := ollamaGenerateRequest{
        Model:  config.OllamaModel,
        Prompt: prompt,
        Stream: false,
        Options: map[string]interface{}{
            "temperature": 0.0,
            "top_p":      0.9,
            "top_k":      40,
        },
    }

    jsonData, err := json.Marshal(reqBody)
    if err != nil {
        return "", fmt.Errorf("failed to marshal request: %v", err)
    }

    // Create HTTP request with context and timeout
    ctx, cancel := context.WithTimeout(context.Background(), config.Timeout)
    defer cancel()

    req, err := http.NewRequestWithContext(ctx, "POST", config.OllamaURL+"/api/generate", bytes.NewBuffer(jsonData))
    if err != nil {
        return "", fmt.Errorf("failed to create request: %v", err)
    }
    req.Header.Set("Content-Type", "application/json")

    // Make request
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return "", fmt.Errorf("generate request failed: %v", err)
    }
    defer resp.Body.Close()

    // Read response
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return "", fmt.Errorf("failed to read response: %v", err)
    }

    if resp.StatusCode != http.StatusOK {
        return "", fmt.Errorf("generate request failed with status %d: %s", resp.StatusCode, string(body))
    }

    // Parse response
    var genResp ollamaGenerateResponse
    if err := json.Unmarshal(body, &genResp); err != nil {
        return "", fmt.Errorf("failed to parse response: %v", err)
    }

    return genResp.Response, nil
}

func promptForMapWithOllama(data string, config ProcessorConfig, keys []string) (llamaresponse, error) {
    prompt := createExtractionPrompt(data, keys)
    response, err := doOllama(prompt, config)

    if err != nil {
        return nil, fmt.Errorf("Ollama generation failed: %v", err)
    }

    // Clean up the response to extract just the JSON part
    response = strings.TrimSpace(response)
    startIdx := strings.Index(response, "{")
    endIdx := strings.LastIndex(response, "}")

    if startIdx == -1 || endIdx == -1 {
        return nil, fmt.Errorf("no valid JSON found in response")
    }

    jsonStr := response[startIdx:endIdx+1]
    var extracted llamaresponse
    if err := json.Unmarshal([]byte(jsonStr), &extracted); err != nil {
        return nil, fmt.Errorf("JSON parsing failed: %v for %v", err, jsonStr)
    }

    log.Printf("Extracted: %v\n", extracted)
    return extracted, nil
}

func promptForListWithOllama(data string, config ProcessorConfig) ([]llamaresponse, error) {
    response, err := doOllama(data, config)

    if err != nil {
        return nil, fmt.Errorf("Ollama generation failed: %v", err)
    }

    // Clean up the response to extract just the JSON part
    response = strings.TrimSpace(response)
    startIdx := strings.Index(response, "[")
    endIdx := strings.LastIndex(response, "]")

    if startIdx == -1 || endIdx == -1 {
        return nil, fmt.Errorf("no valid JSON found in response")
    }

    jsonStr := response[startIdx:endIdx+1]
    var extracted []llamaresponse
    if err := json.Unmarshal([]byte(jsonStr), &extracted); err != nil {
        return nil, fmt.Errorf("JSON parsing failed: %v", err)
    }

    return extracted, nil
}

func promptForStringWithOllama(data string, config ProcessorConfig) (string, error) {
    response, err := doOllama(data, config)

    if err != nil {
        return "", fmt.Errorf("Ollama generation failed: %v", err)
    }

    // Clean up the response to extract just the JSON part
    response = strings.TrimSpace(response)
    startIdx := strings.Index(response, "\"")
    endIdx := strings.LastIndex(response, "\"")

    if startIdx == -1 || endIdx == -1 {
        return "", fmt.Errorf("no valid JSON found in response")
    }

    return response[startIdx+1:endIdx], nil
}

// Rest of the file remains the same...

// createExtractionPrompt generates a prompt for information extraction
func createExtractionPrompt(content string, wantKeys []string) string {
	return fmt.Sprintf(`Extract key information from the following content as JSON key-value pairs.
Output at least these keys: %s

Content to analyze:
<content>%s</content>

Respond with valid JSON containing only the found information. Omit fields if no relevant information is found.  Example output:

{ "title": "a title",
 "author": "an author",
 "summary": "...",
 ...}

 Respond only with valid JSON. Do not write an introduction or summary.`, wantKeys, content)
}



// truncateContent ensures content doesn't exceed the maximum length
func truncateContent(data []byte, maxLength int) []byte {
	if len(data) <= maxLength {
		return data
	}
	return data[:maxLength]
}

func askOllamaYesNo(config ProcessorConfig, question string) bool {
	prompt := fmt.Sprintf(`Respond with
{ "answer": "yes"} or { "answer": "no" }

Answer the following question:

<question>%s</question>

Respond with { "answer": "yes"} or { "answer": "no" }
Respond only with valid JSON. Do not write an introduction or summary.
`, question)
	res, err := promptForStringWithOllama(prompt, config)
	if err != nil {
		panic(fmt.Errorf("failed to ask Ollama question: %v", err))
	}

	answer := res

	log.Printf("Is this source code?: %v\n", res)

	// Check if the response contains "Yes"
	if strings.Contains(fmt.Sprint(answer), "yes") {
		log.Println("Yes")
		return true
	}

	// Check if the response contains "No"
	if strings.Contains(fmt.Sprint(answer), "no") {
		log.Println("No")
		return false
	}

	 panic(fmt.Errorf("invalid Ollama response: %v", res))
}

// normalizeText performs various text normalizations to improve embedding quality
func normalizeText(text string) string {
    // Convert to lowercase
    text = strings.ToLower(text)
    
    // Replace all whitespace (including newlines and tabs) with single spaces
    space := regexp.MustCompile(`\s+`)
    text = space.ReplaceAllString(text, " ")
    
    // Remove non-printable characters
    text = strings.Map(func(r rune) rune {
        if unicode.IsPrint(r) || unicode.IsSpace(r) {
            return r
        }
        return -1
    }, text)
    
    // Remove redundant punctuation
    redundantPunct := regexp.MustCompile(`([.,!?])[.,!?]+`)
    text = redundantPunct.ReplaceAllString(text, "$1")
    
    // Normalize quotes
    //text = strings.ReplaceAll(text, """, "\"")
    //text = strings.ReplaceAll(text, """, "\"")
    text = strings.ReplaceAll(text, "'", "'")
    text = strings.ReplaceAll(text, "'", "'")
    
    // Remove URLs
    urlRegex := regexp.MustCompile(`https?://\S+`)
    text = urlRegex.ReplaceAllString(text, "")
    
    // Remove email addresses
    emailRegex := regexp.MustCompile(`\S+@\S+\.\S+`)
    text = emailRegex.ReplaceAllString(text, "")
    
    // Remove common file paths
    pathRegex := regexp.MustCompile(`[\/\\][\w\-. ]+[\/\\][\w\-. ]+`)
    text = pathRegex.ReplaceAllString(text, "")
    
    // Clean up any resulting multiple spaces
    text = space.ReplaceAllString(text, " ")
    
    // Trim spaces from start and end
    text = strings.TrimSpace(text)
    
    return text
}

// cleanSourceCode removes code-specific elements that might not be relevant for embeddings
func cleanSourceCode(text string) string {
    // Remove single-line comments
    singleLineComments := regexp.MustCompile(`//.*$`)
    text = singleLineComments.ReplaceAllString(text, "")
    
    // Remove multi-line comments
    multiLineComments := regexp.MustCompile(`/\*[\s\S]*?\*/`)
    text = multiLineComments.ReplaceAllString(text, "")
    
    // Remove common import/package declarations
    imports := regexp.MustCompile(`(?m)^import\s+\([\s\S]*?\)`)
    text = imports.ReplaceAllString(text, "")
    
    // Remove package declarations
    packages := regexp.MustCompile(`(?m)^package\s+\w+`)
    text = packages.ReplaceAllString(text, "")
    
    return normalizeText(text)
}
