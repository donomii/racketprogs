module yoinker

go 1.23.0

require (
	github.com/chromedp/cdproto v0.0.0-20250216233945-bd41ad9b04ce
	github.com/chromedp/chromedp v0.12.1
	github.com/dhowden/tag v0.0.0-20240417053706-3d75831295e8
	github.com/emersion/go-mbox v1.0.3
	github.com/google/uuid v1.6.0
	github.com/rwcarlsen/goexif v0.0.0-20190401172101-9e8deecbddbd
)

require (
	github.com/chromedp/sysutil v1.1.0 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.4.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.9.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
