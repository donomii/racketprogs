package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"time"

	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"strings"
)

// ...

func downloadAndSaveWithChrome(url, dir string) error {
	details := make(map[string]interface{})
	details["url"] = url
	details["mode"] = "chrome"
	details["timestamp"] = time.Now().Unix()
	details["dir"] = dir
	//If the url is youtube
	if strings.Contains(url, "youtube.com") {
		fmt.Println("Downloading YouTube video...")
		_, _, stats, err := downloadYouTubeVideo(url, dir)
		//Save the stats to a json file called details.json, in dir
		data ,err:= json.Marshal(stats)
		if err != nil {
			return fmt.Errorf("failed to marshal stats: %w", err)
		}
		err = ioutil.WriteFile(filepath.Join(dir, "details.json"), data, 0644)
		if err != nil {
			return fmt.Errorf("failed to save details.json: %w", err)
		}
		return nil
	}

	// Save as PDF using Chrome
	err := saveAsPDFWithChrome(url, filepath.Join(dir, "output_chrome.pdf"))
	if err != nil {
		details["PDFerror"] = err.Error()
		log.Println("Error saving as PDF with Chrome:", err)
	}

	// Save as PNG using Chrome
	err = saveAsPNGWithChrome(url, filepath.Join(dir, "output_chrome.png"))
	if err != nil {
		details["PNGerror"] = err.Error()
		log.Println("Error saving as PNG with Chrome:", err)
	}

	// Save as WAR and mirrored file can be done with the previous functions
	err = saveAsWARC(url, dir, filepath.Join(dir, "output"))
	if err != nil {
		details["WARCerror"] = err.Error()
		log.Println("Error saving as WARC:", err)
	}

	/*
	err = saveAsMirror(url, dir)
	if err != nil {
		details["Mirrorerror"] = err.Error()
		log.Println("Error saving as a mirrored file:", err)
	}
*/
	// Save the details to a json file called details.json, in dir
	data ,err:= json.Marshal(details)
	if err != nil {
		return fmt.Errorf("failed to marshal details: %w", err)
	}
	err = ioutil.WriteFile(filepath.Join(dir, "details.json"), data, 0644)
	if err != nil {
		return fmt.Errorf("failed to save details.json: %w", err)
	}

	return nil
}

func saveAsPDFWithChrome(url, path string) error {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// Set a timeout for the operation
	ctx, cancel = context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	var pdfBuffer []byte
	err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.ActionFunc(func(ctx context.Context) error {
			pdf, _, err := page.PrintToPDF().WithPrintBackground(true).Do(ctx)
			if err != nil {
				return err
			}
			pdfBuffer = pdf
			return nil
		}),
	)
	if err != nil {
		return fmt.Errorf("failed to generate PDF with Chrome: %w", err)
	}

	err = ioutil.WriteFile(path, pdfBuffer, 0644)
	if err != nil {
		return fmt.Errorf("failed to save PDF with Chrome: %w", err)
	}

	return nil
}

func saveAsPNGWithChrome(url, path string) error {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// Set a timeout for the operation
	ctx, cancel = context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	var pngBuffer []byte
	err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.ActionFunc(func(ctx context.Context) error {
			png, err := page.CaptureScreenshot().WithQuality(90).Do(ctx)
			if err != nil {
				return err
			}
			pngBuffer = png
			return nil
		}),
	)
	if err != nil {
		return fmt.Errorf("failed to capture PNG with Chrome: %w", err)
	}

	err = ioutil.WriteFile(path, pngBuffer, 0644)
	if err != nil {
		return fmt.Errorf("failed to save PNG with Chrome: %w", err)
	}

	return nil
}
