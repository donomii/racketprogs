package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"


	"github.com/donomii/goof"
)

var throttleChan chan bool

func main() {
	// Set up the throttle channel
	throttleChan = make(chan bool, 10)
	urlFile := ""
	viewMode :=false 
	// Check command line for an input file of urls
	flag.StringVar(&urlFile, "f", "", "File containing list of urls to archive")
	flag.BoolVar(&viewMode, "v", false, "View mode")
	flag.Parse()

	if viewMode {
		fmt.Println("View mode")
		serveArchive("8090")
		return
	}

	Config, err := readConfig(archiveDir + "/" + JsonConfigFileName)
	if err != nil {
		fmt.Println("Error reading config file:", err)
		Config = &ConfigFile{Mode: "chrome", Timeout: 60}
	}
	err = saveConfig(archiveDir+"/"+JsonConfigFileName, Config)
	if err != nil {
		fmt.Println("Error saving config file:", err)
	}

	urls, err := readInput(os.Args[1:])
	if err != nil {
		fmt.Println("Error reading input:", err)
		os.Exit(1)
	}

	if urlFile != "" {
		fileUrls, err := readURLsFromFile(urlFile)
		if err != nil {
			fmt.Println("Error reading input file:", err)
			os.Exit(1)
		}
		urls = append(urls, fileUrls...)
	}
	// Make the root archive directory
	err = os.Mkdir(archiveDir, 0755)
	if err != nil {
		if !os.IsExist(err) {
			fmt.Println("Error making the archives directory:", err)
			os.Exit(1)
		}
	}

	// Make the filestore directory
	filestore := filepath.Join(archiveDir, "filestore")
	err = os.Mkdir(filestore, 0755)
	if err != nil {
		if !os.IsExist(err) {
			fmt.Println("Error making the filestore directory:", err)
			os.Exit(1)
		}
	}

	for i, real_url := range urls {
		logStr := fmt.Sprintf("Archiving %s (%d/%d)...\n", real_url, i+1, len(urls))
		url := real_url
		//Sanitise the url
		url = strings.ReplaceAll(url, "/", "_")
		url = strings.ReplaceAll(url, ":", "_")
		url = strings.ReplaceAll(url, "?", "_")
		url = strings.ReplaceAll(url, "&", "_")
		url = strings.ReplaceAll(url, "=", "_")
		url = strings.ReplaceAll(url, "\\", "_")
		url = strings.ReplaceAll(url, "\n", "_")
		//Make a unique directory name in filestore combining the url, time, date, index and a random number
		dir := filepath.Join(filestore, time.Now().Format("2006-01-02_15-04-05")+"_"+strconv.Itoa(i)+"_"+strconv.Itoa(rand.Intn(1000000)) + "_" + url)
		dir = goof.ShortenStringWithEllipsis( 255, dir)

		logStr += " Creating " + dir + "...\n"
		err := os.Mkdir(dir, 0755)
		if err != nil {
			fmt.Println("Error making the directory:", err)
			os.Exit(1)
		}

		throttleChan <- true
		go func(real_url, dir string) {
			defer func() { <-throttleChan }()
			// Download and save the page
			logStr += " Downloading and saving " + real_url + " to " + dir + "...\n"
			err = downloadAndSaveWithChrome(real_url, dir)
			if err != nil {
				fmt.Println("Error downloading and saving:", err)
				os.Exit(0)
			}

			//Write the log
			logStr += " Writing log to " + dir + "...\n"
			err = writeLog(dir, logStr)
			if err != nil {
				fmt.Println("Error writing the log:", err)
				os.Exit(0)
			}
		}(real_url, dir)
	}
}

func writeLog(dir string, logStr string) error {
	logFile := filepath.Join(dir, "log.txt")
	err := ioutil.WriteFile(logFile, []byte(logStr), 0644)
	if err != nil {
		return fmt.Errorf("failed to write log file: %w", err)
	}
	return nil
}
