package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"path/filepath"

	
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)


func downloadAndSave(url, dir string) error {

	// Make an HTTP request
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Save as PDF
	err = saveAsPDF(url, filepath.Join(dir, "output.pdf"))
	if err != nil {
		log.Println("Error saving as PDF:", err)
	}

	// Save as PNG
	err = saveAsPNG(url, filepath.Join(dir, "output.png"))
	if err != nil {
		log.Println("Error saving as PNG:", err)
	}

	// Save as WARC
	err = saveAsWARC(url, dir,  filepath.Join(dir, "output"))
	if err != nil {
		log.Println("Error saving as WARC:", err)
	}

	// Save as a mirrored file
	err = saveAsMirror(url, dir)
	if err != nil {
		log.Println("Error saving as a mirrored file:", err)
	}

	return nil
}

func saveAsPDF(url, path string) error {
	wkhtmltopdf.SetPath("path/to/wkhtmltopdf") // Update with the correct path to the wkhtmltopdf binary
	generator, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return err
	}

	page := wkhtmltopdf.NewPage(url)
	generator.AddPage(page)

	err = generator.Create()
	if err != nil {
		return err
	}

	return generator.WriteFile(path)
}

func saveAsPNG(url, path string) error {
	
	return nil
}

func saveAsWARC(url, path, file string) error {
	//Create a wget command the webpage as a WARC file, in the specified path, as output.warc
	cmd := exec.Command("wget", "-e", "robots=off", "-U", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3", "-k", "-H", "-p", "-E", "-K", "-T", "30", "-w", "2", "-P", path, "--warc-file", file, url)
	//Print cmd
	fmt.Println(cmd)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to create WARC file: %w", err)
	}
	return nil
}

func saveAsMirror(url, path string) error {
	//Create a wget command the webpage as a locally mirrored directory, in the specified path, as mirror
	cmd := exec.Command("wget", "-e", "robots=off", "-U", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3", "-k", "-H", "-p", "-E", "-K", "-T", "30", "-w", "2", "-P", path, "--mirror", url)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to create mirrored directory: %w", err)
	}

	return nil
}

