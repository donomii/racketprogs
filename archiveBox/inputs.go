package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"io"
	"regexp"
)

var archiveDir = "archive"
const urlFile = "archive_config.json"
var JsonConfigFileName = "config.json"

type ConfigFile struct {
	Mode string `json:"mode"`  // "chrome" or "native"
	Timeout int `json:"timeout"` // in seconds
}

func readInput(args []string) ([]string, error) {
	if _, err := os.Stat(archiveDir); os.IsNotExist(err) {
		err = os.Mkdir(archiveDir, 0755)
		if err != nil {
			return nil, fmt.Errorf("failed to create archive directory: %w", err)
		}
	}

	configPath := filepath.Join(archiveDir, urlFile)
	urls := []string{}



	var err error
	if len(args) == 0 {
		fmt.Println("Enter URLs one per line, and press Ctrl-D (Unix) or Ctrl-Z (Windows) when done:")
		reader := bufio.NewReader(os.Stdin)

		for {
			input, err := reader.ReadString('\n')
			if err == io.EOF {
				break
			}
			if err != nil {
				return nil, fmt.Errorf("failed to read input: %w", err)
			}

			url := strings.TrimSpace(input)
			if url != "" {
				urls = append(urls, url)
			}
		}
	} else {
		input := args[0]
		if strings.HasPrefix(input, "http://") || strings.HasPrefix(input, "https://") {
			urls = args
		} else {
			urls, err = readURLsFromFile(input)
			if err != nil {
				return nil, err
			}
		}
	}

		// If config file exists, read URLs from the config file
		if _, err := os.Stat(configPath); !os.IsNotExist(err) {
			fileurls, err := readURLsFromConfig(configPath)
			if err != nil {
				return nil, err
			}
			urls = append(urls, fileurls...)
			
		}

	err = saveURLsToConfig(urls, configPath)
	if err != nil {
		return nil, err
	}

	return urls, nil
}


func readURLsFromFile(filePath string) ([]string, error) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
	return nil, fmt.Errorf("failed to read file: %w", err)
	}
	
	
	urls := make([]string, 0)
	
	// Create a regex to match URLs starting with 'http'
	urlPattern := regexp.MustCompile(`http[s]?://[^\s'"]+`)
	
	// Find all URL matches in the file content
	matches := urlPattern.FindAllString(string(content), -1)
	
	// Add matched URLs to the list
	for _, match := range matches {
		urls = append(urls, match)
	}
	
	return urls, nil
	}
func readURLsFromConfig(configPath string) ([]string, error) {
	content, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file: %w", err)
	}

	var urls []string
	err = json.Unmarshal(content, &urls)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal config JSON: %w", err)
	}

	return urls, nil
}

func saveURLsToConfig(urls []string, configPath string) error {
	data, err := json.Marshal(urls)
	if err != nil {
		return fmt.Errorf("failed to marshal URLs to JSON: %w", err)
	}

	err = ioutil.WriteFile(configPath, data, 0644)
	if err != nil {
		return fmt.Errorf("failed to save config file: %w", err)
	}

	return nil
}



func readConfig(path string) (*ConfigFile, error) {
	configPath := filepath.Join(path)

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("config file does not exist: %w", err)
	}

	content, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file: %w", err)
	}

	var config ConfigFile
	err = json.Unmarshal(content, &config)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal config JSON: %w", err)
	}

	return &config, nil
}

func saveConfig(path string, config *ConfigFile) error {
	data, err := json.Marshal(config)
	if err != nil {
		return fmt.Errorf("failed to marshal config to JSON: %w", err)
	}

	configPath := filepath.Join(path)
	err = ioutil.WriteFile(configPath, data, 0644)
	if err != nil {
		return fmt.Errorf("failed to save config file: %w", err)
	}

	return nil
}


