package main

import (
	
	"encoding/json"
	"fmt"
	"github.com/donomii/goof"
	"github.com/wolfgangmeyers/go-warc/warc"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"bytes"
)

func serveArchive(port string) {

	// Get a list of all the directories in the archive directory
	files, err := ioutil.ReadDir(archiveDir + "/filestore")
	if err != nil {
		log.Fatal(err)
	}

	var links []string
	// For each directory, find the output.warc.gz file
	for _, f := range files {
		warcPath := archiveDir + "/filestore/" + f.Name() + "/output.warc.gz"
		warcDir := archiveDir + "/filestore/" + f.Name()
		log.Println(warcPath)
		if goof.Exists(warcPath) {
			//Add a handler for the directory
			http.HandleFunc("/"+f.Name()+"/", makeWarcHandler(warcDir, warcPath, f.Name()))
			//Add a link to the directory
			links = append(links, "<a href=\"/"+f.Name()+"/\">"+f.Name()+"</a>")
		}
	}

	//Add a handler for the root directory
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `<html>
		<head>
		<title>WARC Archive</title>
		</head>
		<body>
		Archive:<br/>%s
		</body>
		</html>
		`, strings.Join(links, "<br/>"))
	})

	serveWarc(port)

}

func serveWarc(port string) {

	fmt.Println("Starting server on :" + port + "...")
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Printf("Server failed: %s\n", err.Error())
	}
}

func makeWarcHandler(FullWarcDir, warcFilePath, snapshotDir string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//Read the details.json file
		detailsData, err := ioutil.ReadFile(FullWarcDir + "/details.json")
		if err != nil {
			http.Error(w, fmt.Sprintf("Error reading details.json: %s", err.Error()), http.StatusInternalServerError)
			return
		}
		//Unmarshal the details.json file
		var details map[string]interface{}
		err = json.Unmarshal(detailsData, &details)
		if err != nil {
			http.Error(w, fmt.Sprintf("Error unmarshalling details.json: %s", err.Error()), http.StatusInternalServerError)
			return
		}

		warcFile, err := os.Open(warcFilePath)
		if err != nil {
			http.Error(w, fmt.Sprintf("Error opening WARC file: %s", err.Error()), http.StatusInternalServerError)
			return
		}
		defer warcFile.Close()

		reader, err := warc.NewWARCFile(warcFile)
		if err != nil {
			http.Error(w, fmt.Sprintf("Error creating WARC reader: %s", err.Error()), http.StatusInternalServerError)
			return
		}

		outputContentType := ""
		outputData := []byte{}
		for {
			record, err := reader.ReadRecord()
			if err == io.EOF {
				break
			}
			if err != nil {
				break
			}

			log.Printf("Headers: %v", record.GetHeader())
			targetURI, ok := record.Get("WARC-Target-URI")
			if !ok {
				log.Println(w, "WARC record missing WARC-Target-URI", http.StatusInternalServerError)
				continue
			}

			targetURI = strings.Trim(targetURI, "<>")

			path := strings.TrimPrefix(r.URL.Path, "/"+snapshotDir+"/")
			if path == "" {
				path = details["url"].(string)
			}

			//Trim the protocols fromm the target URI and the requested URI
			targetURI = strings.TrimPrefix(targetURI, "http://")
			targetURI = strings.TrimPrefix(targetURI, "https://")
			targetURI = strings.TrimPrefix(targetURI, "ftp://")

			path = strings.TrimPrefix(path, "http://")
			path = strings.TrimPrefix(path, "https://")
			path = strings.TrimPrefix(path, "ftp://")

			log.Println("internal URI:", path)
			log.Println("Requested URI:", targetURI)
			if strings.HasSuffix(targetURI, path) {
				contentType, ok := record.Get("Content-Type")
				if !ok {
					http.Error(w, "WARC record missing Content-Type", http.StatusInternalServerError)
					return
				}

				warc_type, ok := record.Get("WARC-Type")
				if !ok {
					continue
				}

				if warc_type != "response" {
					continue
				}

				w.Header().Set("Content-Type", contentType)
				outputData = record.GetPayload().GetData()
				// Split the output data into the header and the body
				bites := bytes.SplitN(outputData, []byte("\r\n\r\n"), 2)
				header, body := bites[0], bites[1]
				//Remove the first line of the header
				header = bytes.SplitN(header, []byte("\r\n"), 2)[1]
				log.Println("Header:", string(header))
				//Parse the http header
				httpHeader, err := parseHTTPHeader(string(header))
				if err != nil {
					http.Error(w, "Error parsing stored HTTP header"+err.Error(), http.StatusInternalServerError)
					return
				}
				log.Println("HTTP Header:", httpHeader)
				outputContentType = httpHeader["Content-Type"]
				log.Println("Content-Type:", outputContentType)
				outputData = body
				log.Println("Body:", string(outputData))
				log.Println("Found a match")
			}
		}

		if outputContentType != "" {
			w.Header().Set("Content-Type", outputContentType)
			w.Write(outputData)

			log.Println("Served something, probably")
			return
		}

		http.Error(w, "Resource not found", http.StatusNotFound)
	}
}


func parseHTTPHeader(header string) (map[string]string, error) {
//Split header on \r\n
	lines := strings.Split(header, "\r\n")
	headerMap := make(map[string]string)
	for _, line := range lines {
		
		line = strings.TrimSuffix(line, "\r\n")

		if line == "" {
			continue
		}

		parts := strings.SplitN(line, ":", 2)
		if len(parts) != 2 {
			return nil, fmt.Errorf("invalid header line: %s", line)
		}

		key := strings.TrimSpace(parts[0])
		value := strings.TrimSpace(parts[1])
		headerMap[key] = value
	}

	if len(headerMap) == 0 {
		return nil, fmt.Errorf("empty header")
	}

	return headerMap, nil
}
