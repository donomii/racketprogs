package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/kkdai/youtube"
)

func downloadYouTubeVideo(url, directory string) (string, string, map[string]string, error) {
	client := &http.Client{}
	yt := youtube.Client{HTTPClient: client}

	fail := map[string]string{"status": "fail"}

	video, err := yt.GetVideo(url)
	if err != nil {
		return "", "", map[string]string{"status": "fail", "error": err.Error()}, fmt.Errorf("error fetching video details: %v", err)
	}
	if len(video.Formats) == 0 {
		return "", "", map[string]string{"url": url, "status": "fail", "error": "No supported video format found"}, fmt.Errorf("no supported video format found")
	}
	format := video.Formats[0] // Automatically choose the best quality

	res, err := client.Get(format.URL)
	if err != nil {
		return "", "", map[string]string{"url": url, "status": "fail", "error": err.Error()}, fmt.Errorf("error downloading video: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", "", fail, fmt.Errorf("unexpected http status: %d", res.StatusCode)
	}

	filename := filepath.Join(directory, video.Title+".flv")
	out, err := os.Create(filename)
	if err != nil {
		return "", "", map[string]string{"url": url, "status": "fail", "error": err.Error()}, fmt.Errorf("error creating file: %v", err)
	}
	defer out.Close()

	_, err = io.Copy(out, res.Body)
	if err != nil {
		return "", "", map[string]string{"url": url, "status": "fail", "error": err.Error()}, fmt.Errorf("error writing file: %v", err)
	}

	stats := map[string]string{

		"Duration":        video.Duration.String(),
		"Author":          video.Author,
		"VideoWidth":      fmt.Sprintf("%v", format.Width),
		"VideoHeight":     fmt.Sprintf("%v", format.Height),
		"MimeType":        format.MimeType,
		"VideoQuality":    format.Quality,
		"AudioSampleRate": format.AudioSampleRate,
		"AudioQuality":    format.AudioQuality,
	}

	return filename, video.Title, stats, nil
}

type VideoStats struct {
	Duration        string
	Views           int
	Author          string
	VideoWidth      int
	VideoHeight     int
	MimeType        string
	VideoQuality    string
	AudioQuality    string
	VideoResolution string
	AudioSampleRate string
	VideoCodec      string
	AudioCodec      string
}
