package main

import (
	"flag"
	"os"
	"strings"
	"fmt"

	"github.com/donomii/goof"
)

func main() {
	startDir ,_:= os.Getwd()

	flag.Parse()
	dir := flag.Arg(0)
	if dir == "" {
		dir = "."
	}
	os.Chdir(dir)
	// List all files in the directory
	files := goof.Ls(".")

	subdirs := map[string]bool{}
	for _, file := range files {
		// Split the filename into parts
		parts := strings.Split(file, "__")
		if len(parts) < 2 {
			continue
		}
		subd := parts[0]
		subdirs[subd] = true
		os.Mkdir(subd, 0777)
		fmt.Printf("Moving %s to %s\n", file, subd+"/"+file)
		os.Rename(file, subd+"/"+file)
	}

	os.Chdir(startDir)

}
