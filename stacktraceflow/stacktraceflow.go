package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/alecthomas/chroma/formatters"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
	"github.com/fatih/color"
)

type StacktraceEntry struct {
	FilePath     string
	LineNumber   int
	FunctionName string
	Package      string
}

func parseStacktrace(stacktrace string) []StacktraceEntry {
	entries := []StacktraceEntry{}
	lines := strings.Split(stacktrace, "\n")

	/*
			testing.tRunner.func1.2({0x102c4a660, 0x102daccb8})
			/opt/homebrew/Cellar/go/1.20.2/libexec/src/testing/testing.go:1526 +0x1c8
		testing.tRunner.func1()
			/opt/homebrew/Cellar/go/1.20.2/libexec/src/testing/testing.go:1529 +0x364
		panic({0x102c4a660, 0x102daccb8})
			/opt/homebrew/Cellar/go/1.20.2/libexec/src/runtime/panic.go:884 +0x1f4
		github.com/weaviate/weaviate/adapters/repos/db/inverted.(*JsonPropertyLengthTracker).TrackProperty(0x0?, {0x0?, 0x0?}, 0x0?)
			/Users/jeremyprice/git/weaviate/adapters/repos/db/inverted/new_prop_length_tracker.go:150 +0x64
		github.com/weaviate/weaviate/adapters/repos/db.(*Shard).addPropLengths(0x140002d6380, {0x140002342a0?, 0x3, 0x103393ec0?})
			/Users/jeremyprice/git/weaviate/adapters/repos/db/shard_write_inverted_lsm.go:270 +0x88
	*/
	stacktraceRegex := regexp.MustCompile(`(.+):(\d+)`)
	for _, line := range lines {
		log.Println("line:", line)
		matches := stacktraceRegex.FindStringSubmatch(line)
		log.Println("matches:", matches)
		if len(matches) == 3 {
			entry := StacktraceEntry{
				FilePath:   strings.TrimSpace(matches[1]),
				LineNumber: atoi(matches[2]),
				//FunctionName: matches[1],
				//Package:      extractPackage(matches[1]),
			}
			entries = append(entries, entry)
		}
	}

	return entries
}

func atoi(s string) int {
	n, _ := strconv.Atoi(s)
	return n
}

func extractPackage(functionName string) string {
	split := strings.Split(functionName, ".")
	if len(split) > 1 {
		return split[0]
	}
	return ""
}

func main() {

	//Command line options
	//Print filenames and line numbers
	var printFilenames bool
	flag.BoolVar(&printFilenames, "f", false, "Print filenames and line numbers")
	//Don't print the function definition at the start of the snippet
	var noFunctionDefinition bool
	flag.BoolVar(&noFunctionDefinition, "n", false, "Don't print the function definition at the start of the snippet")
	flag.Parse()

	//Read stacktrace from stdin
	stacktrace := ""
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		stacktrace += scanner.Text() + "\n"
	}

	//log.Println("Received stacktrace:\n", stacktrace)
	parsed := parseStacktrace(stacktrace)
	//log.Println("Parsed stacktrace:\n", parsed)
	snippets, err := extractFunctionSnippets(parsed)
	//log.Println("Extracted snippets:\n", snippets)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// Set up the syntax highlighting
	lexer := lexers.Get("go")
	formatter := formatters.Get("terminal256")
	style := styles.Get("friendly")

	// Set up color printing
	//cyan := color.New(color.FgCyan).SprintFunc()
	blue := color.New(color.FgBlue).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()

	// Print snippets in reverse
	for i := len(snippets) - 1; i >= 0; i-- {
		snippet := snippets[i]
		if strings.Contains(snippet.FilePath, "/panic.go") || strings.Contains(snippet.FilePath, "/testing.go") {
			continue
		}

		if printFilenames {
			fmt.Println("\n", blue("=== "), snippet.FilePath, " ", snippet.StartLine, yellow("-->"), snippet.EndLine)
		}

		toPrint := snippet.Snippet
		if noFunctionDefinition {
			toPrint = strings.Join(strings.Split(toPrint, "\n")[1:], "\n")
		}

		// Add syntax highlighting to the snippet
		iterator, _ := lexer.Tokenise(nil, toPrint)
		formatter.Format(os.Stdout, style, iterator)
		fmt.Println()
	}
}

type FunctionSnippet struct {
	FilePath     string
	StartLine    int
	EndLine      int
	FunctionName string
	Snippet      string
}

func extractFunctionSnippets(stacktraceEntries []StacktraceEntry) ([]FunctionSnippet, error) {
	snippets := []FunctionSnippet{}

	for _, entry := range stacktraceEntries {
		content, err := ioutil.ReadFile(entry.FilePath)
		if err != nil {
			return nil, fmt.Errorf("failed to read file: %s, error: %v", entry.FilePath, err)
		}

		lines := strings.Split(string(content), "\n")

		snippet := FunctionSnippet{
			FilePath:     entry.FilePath,
			EndLine:      entry.LineNumber,
			FunctionName: entry.FunctionName,
		}

		for i := entry.LineNumber - 1; i >= 0; i-- {
			line := lines[i]

			if strings.HasPrefix(strings.TrimSpace(line), "func ") {
				snippet.StartLine = i + 1
				break
			}
		}

		if snippet.StartLine > 0 {
			snippet.Snippet = strings.Join(lines[snippet.StartLine-1:snippet.EndLine], "\n")
			snippets = append(snippets, snippet)
		}
	}

	return snippets, nil
}
