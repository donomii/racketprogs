package xsh

import (
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

// List all executable files in PATH
func ListPathExecutables() func(string) []string {
	return func(line string) []string {
		names := make([]string, 0)
		paths := strings.Split(os.Getenv("PATH"), ":")
		if runtime.GOOS == "windows" {
			paths = strings.Split(os.Getenv("PATH"), ";")
		}
		for _, p := range paths {
			files, _ := ioutil.ReadDir(p)
			for _, f := range files {
				if f.Mode().Perm()&0111 != 0 {
					names = append(names, f.Name())
				}
			}
		}
		return names
	}
}

func ListBuiltins() func(string) []string {
	return func(line string) []string {
		// FIXME we can now read this dynamically from the types map[]
		return []string{"id", "and", "or", "join", "split", "puts", "+", "-", "*", "/", "+.", "-.", "*.", "/.", "loadfile", "set", "run", "seq", "with", "cd"}
	}
}

// Function constructor - constructs new function for listing given directory
func ListFiles(path string) func(string) []string {
	return func(line string) []string {
		names := make([]string, 0)
		files, _ := ioutil.ReadDir(path)
		for _, f := range files {
			names = append(names, f.Name())
		}
		return names
	}
}
