package xsh

import (
	"fmt"
	"os"
	"runtime"
	"os/exec"
	"bytes"
	"strings"

	"github.com/donomii/goof"
)

// Search the current environment PATH for the named program.  Also searches for files ending in .exe, .bat, and .cmd.
// Returns the full path to the program, including filename, or an empty string if the program was not found.
func searchPath(execName string) string {
	drintf("Searching path for '%v'\n", execName)
	// Whoops, forgot the actual program name
	if execName == "" {
		return ""
	}
	// If the program name is a full path, just return it
	if execName[0] == '/' {
		return execName
	}

	// If the program name is a full path, just return it
	if execName[0] == '\\' {
		return execName
	}

	// If the program name is a full path(with windows drive), just return it
	if len(execName) > 1 && execName[1] == ':' {
		return execName
	}

	pathStr := os.Getenv("PATH")
	paths := []string{}

	if runtime.GOOS == "windows" {
		paths = strings.Split(pathStr, ";")
		drintf("Exec paths: %v\n", paths)
	} else {
		paths = strings.Split(pathStr, ":")
		drintf("Exec paths: %v\n", paths)
	}
	for _, extension := range []string{"", ".exe", ".bat", ".cmd"} {
		for _, path := range paths {
			fullPath := path + "/" + execName + extension
			//drintf("Searching for %v\n", fullPath)
			if fs, err := os.Stat(fullPath); err == nil {
				if fs.Mode().IsRegular() && fs.IsDir() == false {
					XshInform("Found %s\n", fullPath)
					return fullPath
				}
			}
		}
	}
	return ""
}

// Searches for the XshGuardian program, which is used to launch other programs
func FindGuardian() string {
	bindir := goof.ExecutablePath()
	var guardianPath string = "Guardian not found"
	if runtime.GOOS == "windows" {
		guardianPath = bindir + "/xshguardian.exe"
	} else {
		guardianPath = bindir + "/xshguardian"
	}

	// XshInform("Found guardian at %s\n", guardianPath)
	return guardianPath
}

// Launch another program using XshGuardian, in interactive mode.
func runWithGuardian(cmd []string, filename string, lineno int) error {
	guardianPath := FindGuardian()
	drintf("Launching guardian for %+v from %v\n", cmd, guardianPath)
	binfile := cmd[0]
	fullPath := searchPath(binfile)
	if fullPath == "" {

		XshErr("Could not find %s in PATH at file %s, line %d\n", binfile, filename, lineno)
		if runtime.GOOS == "windows" && (goof.Exists(binfile) || goof.Exists(binfile+".exe") || goof.Exists(binfile+".bat")) {
			XshHelpful("Your file is in the current directory, but not in PATH.  Xsh does not automatically run programs in the current directory.  To do so, add '.' to PATH with 'set PATH [join [list $PATH \".\"] \";\"]'.\n")
		}
		return fmt.Errorf("Could not find application %v in search path: %v\nTo see full path: puts $PATH\n", binfile, clampString(os.Getenv("PATH"), 100))
	}

	cmd[0] = fullPath
	cmd = append([]string{guardianPath, "interactive"}, cmd...)
	XshInform("Running %+v interactively with guardian %v\n", cmd[1:], guardianPath)
	return goof.QCI(cmd)
}

// Launch another program using XshGuardian, and wait for it to finish.  Returns everything printed to
// STDOUT as a string.
func runWithGuardianCapture(cmd []string) (string, error) {
	guardianPath := FindGuardian()
	drintf("Launching guardian for capturing %+v from %v\n", cmd, guardianPath)
	binfile := cmd[0]
	fullPath := searchPath(binfile)
	if fullPath == "" && !goof.Exists(binfile) {
		XshErr("Could not find %s in PATH\n", binfile)
		if runtime.GOOS == "windows" && (goof.Exists(binfile) || goof.Exists(binfile+".exe") || goof.Exists(binfile+".bat")) {
			XshHelpful("Your file is in the current directory, but not in PATH.  Xsh does not automatically run programs in the current directory.  To do so, add '.' to PATH with 'set PATH [join [list $PATH \".\"] \";\"]'.\n")
		}
		return "", fmt.Errorf("Could not find application %v in search path: %v\nTo see full path: puts $PATH\n", binfile, clampString(os.Getenv("PATH"), 100))
	}

	cmd[0] = fullPath
	cmd = append([]string{guardianPath, "noninteractive"}, cmd...)
	XshTrace("Command: %+v\n", cmd)
	XshInform("Running %+v for capture with guardian %v\n", cmd, guardianPath)
	return goof.QC(cmd)
}

func runWithoutGuardian(cmd []string) (int, string, string, error) {
	binfile := cmd[0]
	fullPath := searchPath(binfile)
	if fullPath == "" && !goof.Exists(binfile) {
		XshErr("Could not find %s in PATH\n", binfile)
		if runtime.GOOS == "windows" && (goof.Exists(binfile) || goof.Exists(binfile+".exe") || goof.Exists(binfile+".bat")) {
			XshHelpful("Your file is in the current directory, but not in PATH.  Xsh does not automatically run programs in the current directory.  To do so, add '.' to PATH with 'set PATH [join [list $PATH \".\"] \";\"]'.\n")
		}
		return 0,"","",fmt.Errorf("Could not find application %v in search path: %v\nTo see full path: puts $PATH\n", binfile, clampString(os.Getenv("PATH"), 100))
	}

	cmd[0] = fullPath
	XshTrace("Command: %+v\n", cmd)
	return CaptureCommand(cmd[0], cmd[1:])
}

// Command runs the specified command with the given arguments.
// It returns the exit code, stdout, and stderr.
func CaptureCommand(cmd string, args []string) (int, string, string, error) {
	// Prepare the command.
	command := exec.Command(cmd, args...)

	// Create buffers to capture stdout and stderr.
	var stdoutBuf, stderrBuf bytes.Buffer
	command.Stdout = &stdoutBuf
	command.Stderr = &stderrBuf

	// Run the command.
	err := command.Run()

	// Default exit code.
	exitCode := 0

	// If an error occurred, try to get the exit code.
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			// Command executed but exited with a non-zero exit code.
			exitCode = exitError.ExitCode()
		} else {
			// An error occurred before the command could be executed.
			// You might choose to handle this differently.
			exitCode = -1
		}
	} else if command.ProcessState != nil {
		// On successful execution, retrieve the exit code.
		exitCode = command.ProcessState.ExitCode()
	}

	return exitCode, stdoutBuf.String(), stderrBuf.String(), nil
}
