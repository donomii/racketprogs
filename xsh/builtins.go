// Builtins.go
package xsh

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/donomii/goof"
	autoparser "gitlab.com/donomii/racketprogs/autoparser"
)

// Set the type for a function.  The type format is []string{return type, arg0, arg1, arg2 ....}
func SetType(s State, fname string, t []string) {
	s.TypeSigs[fname] = t
}

func addBuiltinTypes(s State) {
	s.TypeSigs["puts"] = []string{"void", "any", "..."}
	s.TypeSigs["put"] = []string{"void", "any", "..."}
	s.TypeSigs["format"] = []string{"void", "any", "..."}
	s.TypeSigs["list"] = []string{"list", "any", "..."}
	s.TypeSigs["background"] = []string{"void", "string", "..."}
	s.TypeSigs["seq"] = []string{"any", "any", "..."}
	s.TypeSigs["set"] = []string{"void", "symbol", "string"}
	s.TypeSigs["run"] = []string{"string", "string", "..."}
	s.TypeSigs["shell"] = []string{"string", "string"}
	s.TypeSigs["map"] = []string{"list", "lambda", "list"}
	s.TypeSigs["fold"] = []string{"any", "lambda", "any", "list"}
	s.TypeSigs["cd"] = []string{"void", "string"}
	s.TypeSigs["\n"] = []string{"void"}
	s.TypeSigs["+"] = []string{"symbol", "symbol", "symbol"}
	s.TypeSigs["-"] = s.TypeSigs["+"]
	s.TypeSigs["*"] = s.TypeSigs["+"]
	s.TypeSigs["/"] = s.TypeSigs["+"]
	s.TypeSigs["gt"] = s.TypeSigs["+"]
	s.TypeSigs["lt"] = s.TypeSigs["+"]
	s.TypeSigs["+."] = s.TypeSigs["+"]
	s.TypeSigs["-."] = s.TypeSigs["+"]
	s.TypeSigs["*."] = s.TypeSigs["+"]
	s.TypeSigs["/."] = s.TypeSigs["+"]
	s.TypeSigs["gt."] = s.TypeSigs["+"]
	s.TypeSigs["lt."] = s.TypeSigs["+"]
	s.TypeSigs["eq"] = []string{"any", "any", "any"}
	s.TypeSigs["loadfile"] = []string{"string", "string"}
	s.TypeSigs["proc"] = []string{"void", "string", "list", "list"}
	s.TypeSigs["func"] = []string{"void", "symbol", "lambda"}
	s.TypeSigs["exit"] = []string{"void", "symbol"}
	s.TypeSigs["cons"] = []string{"list", "any", "list"}
	s.TypeSigs["empty?"] = []string{"string", "list"}
	s.TypeSigs["length"] = []string{"string", "list"}
	s.TypeSigs["lindex"] = []string{"any", "list", "symbol"}
	s.TypeSigs["lset"] = []string{"string", "list", "string", "any"}
	s.TypeSigs["lrange"] = []string{"list", "list", "symbol", "symbol"}
	s.TypeSigs["split"] = []string{"list", "string", "string"}
	s.TypeSigs["join"] = []string{"string", "list", "string"}
	s.TypeSigs["chr"] = []string{"string", "string"}
	s.TypeSigs["saveInterpreter"] = []string{"void string"}
	s.TypeSigs["return"] = []string{"any", "any"}
	s.TypeSigs["id"] = []string{"any", "any"}
	s.TypeSigs["tron"] = []string{"void"}
	s.TypeSigs["troff"] = s.TypeSigs["tron"]
	s.TypeSigs["dron"] = s.TypeSigs["tron"]
	s.TypeSigs["droff"] = s.TypeSigs["tron"]
	s.TypeSigs["debug"] = []string{"void", "string"}
	s.TypeSigs["trace"] = s.TypeSigs["debug"]
	s.TypeSigs["warn"] = s.TypeSigs["debug"]
	s.TypeSigs["errors"] = s.TypeSigs["debug"]
	s.TypeSigs["inform"] = s.TypeSigs["debug"]
}

func NotifyEvent(cmd []string) {
	if WantEvents {
		Events = append(Events, cmd)
	}
}

func Help(topic string) string {
	XshInform("Help for: %s", topic)
	switch topic {
	case "builtins":
		return `
XSHELL builtins

Basic xsh commands

  puts   [any] [...]    : Prints the arguments to the console.
  put    [any] [...]    : Prints the arguments to the console.
  format [any] [...]    : Formats a string, printf style.
  list   [any] [...]    : Creates a list from the arguments.
  seq    [any] [...]    : Calculates the arguments, returns the last.  See help syntax.
  set    [string] [any] : Sets an environment variable to the value.
  run    [string] [...] : Runs a command with the arguments.
  shell  [string]       : Runs a command with the arguments.
  map    [lambda] [list]: Maps a function over a list.
  fold   [lambda] [any] [list]: Folds a function over a list.
  cd     [string]       : Changes the current working directory.

Xsh does not have automatic type promotion, so you must use different functions for integers and floats.

  Integers
  +     [integer] [integer]: Adds two integers.
  -     [integer] [integer]: Subtracts two integers.
  *     [integer] [integer]: Multiplies two integers.
  /     [integer] [integer]: Divides two integers.
  gt     a b: True if a < b.
  lt     a b: True if a < b.

  Floats
  +.      [integer] [integer]: Adds two floats.
  -.      [integer] [integer]: Subtracts two floats.
  *.      [integer] [integer]: Multiplies two floats.
  /.      [integer] [integer]: Divides two floats.
  gt.     a b: True if a < b.
  lt.     a b: True if a < b.

  eq       [string] [string]: Returns true if the strings are equal.
  loadfile [string]         : Loads a file.
  proc     [string] [list] [list]: Creates a procedure.  See help syntax.
  func     [string] [lambda]: Creates a function.  See help syntax.
  exit     [string]         : Exits the interpreter.
  cons     [string] [list]  : Creates a list from the arguments.
  empty?   [list]           : Returns true if the list is empty.
  length   [list]           : Returns the length of the list.
  lindex   [list] [string]  : Returns the value at the index.
  lset     [list] index [any]value: Sets the value at the index.
  lrange   [list] [string] [string]: Returns a sublist.
  split    [string] [string]separator: Splits a string into a list.
  join     [list] [string]  : Joins a list into a string.
  chr      [integer]        : Converts a number to a (Unicode) character.
  saveInterpreter [string]  : Saves the interpreter state.
  return   [any]            : Returns the argument.
  id       [any]            : Returns the argument.

  Controlling output:
  tron/troff    : Enables/disables trace messages.
  dron/droff    : Enables/disables debug messages.
  debug on/off  : Enables/disables debug messages.
  trace on/off  : Enables/disables trace messages.
  warn on/off   : Enables/disables warning messages.
  errors on/off : Enables/disables error messages.
  inform on/off : Enables/disables informational messages.

`
	case "scripting":
		return `
XSHELL scripting

  The xsh scripting language is simple, but with features that make it easier to check for errors, and harder to make common mistakes.

Xsh works like shell commands.  You can call functions the samw way as programs.
  
    ls $HOME      # A program
    puts $HOME    # A function
    
Evaluate expressions with [].

    puts "2 + 2 = " [+ 2 2]
    puts "Files in the current directory: " [ls]

Create your own functions with the func command.

    func add2 [x| + x 2]  #Add 2 to the input
	puts "2 + 3 = " [add2 3]

For more features, see "help advanced"
`
	case "advanced":
		return `
Advanced scripting

Lambda functions are created with the [| ] syntax.  e.g. a lambda function that adds 2 to the input:

    [x| + x 2]

Xsh supports variable length argument lists for functions.  Adding ... to the end of a type definition gathers all the extra arguments into a list, and puts that list in the last paramter.

    func joinArgs { separator pieces ... | join  pieces separator }
    puts [joinArgs , 1 2 3 4 5 6]        
    1,2,3,4,5,6

Xsh supports optional types for functions.  e.g.

    type joinArgs string string ... string 

If you pass the wrong types to joinArgs, xsh will print a warning.
`

	case "flags":
		return `
XSHELL

XSHELL is a interactive shell written in xsh and golang.

Use: xsh [options] [command] [arguments]

Options:
  -h, --help: Show this help message.
  -v, --version: Show the version number.
  -e, --events: Show events.
  -s, --silent: Show nothing.
  -d, --debug: Show debug messages.
  -t, --trace: Show trace messages.
  -w, --warn: Show warn messages.
  -i, --info: Show info messages.
  -e, --errors: Show errors messages.
  -l, --load: Load a file.
  -p, --proc: Create a procedure.
  -f, --func: Create a function.
  -x, --exit:


`
	case "shell":
		return `
        XSHELL command line
        
The xshell command line is shell that is not POSIX compatible, but tries to look like it.  The basic operations are the same as sh.  You move around the disk, work on files and launch programs.

Like most shells, xsh calls on other programs.  When you type a command, xsh first checks to see if it is an xsh command (function or procedure).  If that doesn't work, xsh checks the current $PATH to find a program with that name.

Configure the shell with the following commands:

    debug on/off: Enables/disables debug messages.
    trace on/off: Enables/disables trace messages.
    warn on/off : Enables/disables warning messages.
    errors on/off: Enables/disables error messages.
    inform on/off: Enables/disables informational messages.
    helpful on/off: Enables/disables extra helpful messages.

    tron,troff     : Turns tracing on/off.

The xsh scripting language is very different to sh.  see 'help scripting' for more details.

`
	default:
		return `
XSHELL HELP

Xshell is a command shell and scripting language.  It is not compatible with POSIX shells, but is still familiar.

Basic commands and environment variables work as usual:

ls -lh
set HOMEDIR /home
ls -lh $HOMEDIR
exit 1


Key shortcuts function roughly as usual:

TAB - rotate through completion suggestions
F5 - auto-complete
F1 - help
Up/Down - history
Ctrl-D - exit

More help is available by typing "help" or "?" followed by one of these topics:

online, scripting, builtins, variables, history, shell, environment, flags
`
	}
}

func builtin(s State, command []autoparser.Node, parent *autoparser.Node, f string, args []autoparser.Node, level int, argsNode autoparser.Node) autoparser.Node {
	cmd, _ := ListToStrings(command)

	switch f {
	case "help":
		if len(args) == 0 {
			XshResponse(Help(""))
			return N_loc(Help(""), command[0])
		} else {
			XshResponse(Help(S(args[0])))
			return N_loc(Help(S(args[0])), command[0])
		}
	case "typeof":
		if len(args) == 0 {
			return Error("Missing arg", *parent)
		} else {
			return N_loc(typeOf(args[0]), command[0])
		}
	case "start-eval-server":
		if len(args) == 0 {
			XshErr("start-eval-server requires a port number")
			return Error("start-eval-server requires a port number", *parent)
		} else {
			port, err := strconv.Atoi(S(args[0]))
			if err != nil {
				XshErr("start-eval-server requires a port number")
				return Error("start-eval-server requires a port number", *parent)
			}
			go StartEvalServer(New(), port)
		}
	case "trimRight":
		if len(args) == 0 {
			XshErr("trimRight requires a string")
			return Error("trimRight requires a string", *parent)
		} else {
			// Trim all whitespace from the right
			return N_loc(strings.TrimRight(S(args[0]), " \t\r\n"), command[0])
		}
	case "type":
		if len(args) == 0 {
			XshResponse(Help("type"))
			return N_loc(Help("type"), command[0])
		} else {
			types, _ := ListToStrings(args)
			// Move the last element of types to the front
			types[0], types[len(types)-1] = types[len(types)-1], types[0]
			SetType(s, f, types)
		}
	case "sleep":
		if len(args) == 1 {
			t := atoi(S(args[0]))
			time.Sleep(time.Duration(t) * time.Millisecond)
		}
	case "nand":
		if S(args[0]) == "1" && S(args[1]) == "1" {
			return Bool(false)
		} else {
			return Bool(true)
		}
	case "seq":
		// log.Printf("seq %v\n", TreeToTcl(args))
		if len(args) == 0 {
			return Void(command[0], "seq requires at least one argument")
		}
		return args[len(args)-1]
	case "with":
		letparams := args[0].List
		if NodeToString(args[1]) != "=" {
			return Error("with requires a = as the second argument", *parent)
		}
		letbod := CopyTree(args[3])
		// fmt.Printf("Replacing %v with %v\n", TreeToXsh(letparams), TreeToXsh(evalledArgs))
		//Make a node containing list
		listNode := autoparser.Node{"list", "", nil, "", args[2].Line, args[2].Column, args[2].ChrPos, args[2].File, false, "PARSED", nil}
		l := append([]autoparser.Node{listNode}, args[2].List...)
		evalledArgs := treeReduce(s, l, parent, 0, args[2])
		nbod := ReplaceArgs(evalledArgs.List, letparams, letbod)
		drintf("Calling function %+v\n", TreeToXsh(nbod))
		return blockReduce(s, nbod.List, parent, 0)
	case "..":
		NotifyEvent(cmd)
		os.Chdir("..")
		XshResponse("Current working directory: %v", goof.Cwd())
	case "cd":
		NotifyEvent(cmd)
		switch {
		case len(args) == 0:
			os.Setenv("OLDPWD", os.Getenv("PWD"))
			os.Chdir(os.Getenv("HOME"))
		case S(args[0]) == "-":
			oldpwd := os.Getenv("OLDPWD")
			os.Setenv("OLDPWD", os.Getenv("PWD"))
			os.Chdir(oldpwd)
		default:
			os.Setenv("OLDPWD", os.Getenv("PWD"))
			os.Chdir(S(args[0]))
		}
		XshResponse("Current working directory: %v", goof.Cwd())
	case "\n":
		// Fuck
		return Void(command[0], "A newline as a function name(bug in parser)")
	case "+":
		return Sym(fmt.Sprintf("%v", atoi(S(args[0]))+atoi(S(args[1]))), command[0])
	case "-":
		return Sym(fmt.Sprintf("%v", atoi(S(args[0]))-atoi(S(args[1]))), command[0])
	case "*":
		return Sym(fmt.Sprintf("%v", atoi(S(args[0]))*atoi(S(args[1]))), command[0])
	case "/":
		return Sym(fmt.Sprintf("%v", atoi(S(args[0]))/atoi(S(args[1]))), command[0])
	case "gt":
		if atoi(S(args[0])) > atoi(S(args[1])) {
			return Sym("1", command[0])
		} else {
			return Sym("0", command[0])
		}
	case "lt":
		if atoi(S(args[0])) < atoi(S(args[1])) {
			return Sym("1", command[0])
		} else {
			return Sym("0", command[0])
		}
	case "+.":
		return N_loc(fmt.Sprintf("%v", atof(S(args[0]))+atof(S(args[1]))), command[0])
	case "-.":
		return N_loc(fmt.Sprintf("%v", atof(S(args[0]))-atof(S(args[1]))), command[0])
	case "*.":
		return N_loc(fmt.Sprintf("%v", atof(S(args[0]))*atof(S(args[1]))), command[0])
	case "/.":
		return N_loc(fmt.Sprintf("%v", atof(S(args[0]))/atof(S(args[1]))), command[0])
	case "gt.":
		if atof(S(args[0])) > atof(S(args[1])) {
			return N_loc("1", command[0])
		} else {
			return N_loc("0", command[0])
		}
	case "lt.":
		if atof(S(args[0])) < atof(S(args[1])) {
			return N_loc("1", command[0])
		} else {
			return N_loc("0", command[0])
		}
	case "dump":
		fmt.Println("Raw Dump:", N_loc(fmt.Sprintf("%+v", args), command[0]))
		return N_loc(fmt.Sprintf("%v", TreeToXsh(args[0])), command[0])

	case "eq":
		if S(args[0]) == S(args[1]) {
			return N_loc("1", command[0])
		} else {
			return N_loc("0", command[0])
		}
		// FIXME dedupe
	case "tron":
		WantTrace = true
		return N_loc("Trace on", command[0])
	case "troff":
		WantTrace = false
		return N_loc("Trace off", command[0])
	case "dron":
		WantDebug = true
		return N_loc("Debug on", command[0])
	case "droff":
		WantDebug = false
		return N_loc("Debug off", command[0])
		// FIXME replace with an options hash later on
	case "debug":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantDebug = true
		} else {
			WantDebug = false
		}
	case "inform":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantInform = true
		} else {
			WantInform = false
		}
	case "helpful":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantHelp = true
		} else {
			WantHelp = false
		}
	case "trace":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantTrace = true
		} else {
			WantTrace = false
		}
	case "errors":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantErrors = true
		} else {
			WantErrors = false
		}
	case "warn":
		if S(args[0]) == "1" || S(args[0]) == "on" {
			WantWarn = true
		} else {
			WantWarn = false
		}
	case "put":
		for i, v := range args {
			if i != 0 {
				fmt.Print(" ")
			}
			if v.List != nil {
				fmt.Printf("%v", ListToStr(v.List))
			} else {
				fmt.Print(S(v))
			}
		}
		return N_loc(TreeToXsh(argsNode), command[0])
	case "puts":
		for i, v := range args {
			if i != 0 {
				fmt.Print(" ")
			}
			fmt.Printf(TreeToXsh(v))
		}
		fmt.Println()
		if len(args) > 0 {
			return N_loc(TreeToXsh(argsNode), command[0])
		} else {
			return N_loc("", command[0])
		}
	case "format":
		return N_loc(TreeToXsh(argsNode), command[0])
	case "loadfile":
		NotifyEvent(cmd)
		b, _ := ioutil.ReadFile(S(args[0]))
		return N_loc(string(b), command[0])
	case "set":
		if len(args) == 2 {
			if isList(args[1]) {
				XshErr("set: value must be a string")
				XshHelpful("\"set\" sets an external environment variable (i.e. the process environment).  The value must be a string, because the OS only supports strings.  You have provided a list.  If you really want to store a list, you can use the \"join\" command to print it to a string.  Reading it back is a larger problem.")
				return N_loc("Error: value must be a string", command[0])
			}
			data := S(args[1])
			name := S(args[0])

			s.Globals[name] = data
			os.Setenv(name, data) // FIXME add "use environment" toggle so we can have private globals, or exported, like bash?
		} else {
			// return N_loc(globals[S(args[0])], command[0])
			// Apparently we return the value of the variable if there is no value to set it to, like TCL?
			return N_loc(os.Getenv(S(args[0])), command[0])
		}
	case "eval":
		res := RunTree(s, args[0])
		return res
	case "run":
		NotifyEvent(cmd)
		stringCommand, err := ListToStrings(command[1:])
		if err != nil {
			return N_loc(err.Error(), command[0])
		} else {
			var res string
			var err error

			err = runWithGuardian(stringCommand, command[0].File, command[0].Line)

			if err == nil {
				if res == "" {
					return N_loc("", command[0])
				} else {
					return N_loc(fmt.Sprintf("%v", res), command[0])
				}
			} else {
				// FIXME revisit this after adding proper error handling
				fmt.Printf("Error: %+v\n", err)
				fmt.Println("Exiting after critical error")
				//FIXME: quit here or halt execution of the current script?
				return N_loc(err.Error(), command[0])
			}
		}
	case "shell":
		NotifyEvent(cmd)
		shellcmd := S(args[0])
		return N_loc(goof.Shell(shellcmd), command[0])
	case "func":
		/*
			func procedureName {arguments|
			   body
			}
		*/
		if len(args) != 2 {
			a := TreeToXsh(argsNode)
			msg := fmt.Sprintf("Error %v,%v: func requires 2 arguments: func name {arguments| body}\n[%v %v]\n", command[0].Line, command[0].Column, f, a)
			XshErr(msg)
			panic(msg)
		}
		// fmt.Printf("%+v\n", args)
		body := args[1].List

		if !(len(body) > 0) {
			a := TreeToXsh(argsNode)
			msg := fmt.Sprintf("Error %v,%v: func requires 2 arguments: func name {arguments| body}\n[%v %v]\n", command[0].Line, command[0].Column, f, a)
			XshErr(msg)
			log.Panic(msg)
		}
		s.Functions[S(args[0])] = Function{
			Name:       S(args[0]),
			Parameters: body[0],
			Body:       autoparser.Node{argsNode.Raw, argsNode.Str, body[1:], argsNode.Note, argsNode.Line, argsNode.Column, argsNode.ChrPos, argsNode.File, argsNode.ScopeBarrier, "FUNCTION", nil},
		}

		// fmt.Printf("%+v\n", s.Functions[S(args[0])])

	case "proc":
		/*
			proc procedureName {arguments} {
			   body
			}
		*/
		if len(args) != 3 {
			a := TreeToXsh(argsNode)
			msg := fmt.Sprintf("Error %v,%v: proc requires 3 arguments: proc name {arguments} {body}\n[%v %v]\n", command[0].Line, command[0].Column, f, a)

			log.Panicf(msg)
		}
		s.Functions[S(args[0])] = Function{
			Name:       S(args[0]),
			Parameters: args[1],
			Body:       args[2],
		}
	case "exit":
		if len(args) == 0 {
			os.Exit(0)
		} else {
			os.Exit(atoi(S(args[0])))
		}
	case "cons":
		// log.Printf("Cons %+v\n", args)
		thing := args[0]
		list := args[1].List
		list = append([]autoparser.Node{thing}, list...)

		return ListToNode(list, command[0])
	case "empty?":
		if args[0].List == nil || len(args[0].List) == 0 {
			return N_loc("1", command[0])
		} else {
			return N_loc("0", command[0])
		}
	case "length":
		return StringToNode(fmt.Sprintf("%v", len(args[0].List)), command[0])
	case "lindex":
		return args[0].List[atoi(S(args[1]))]
	case "lset":
		array := args[0]
		pos := atoi(S(args[1]))
		data := args[2]
		array.List[pos] = data
		return array
	case "lrange":
		var start int
		if S(args[1]) == "start" {
			start = 0
		} else {
			start = atoi(S(args[1]))
		}
		var end int
		if S(args[2]) == "end" {
			end = len(args[0].List)
		} else {
			end = atoi(S(args[2]))
		}

		return ListToNode(args[0].List[start:end], command[0])
	case "range":
		start := atoi(S(args[0]))
		end := atoi(S(args[1]))
		var list []autoparser.Node
		for i := start; i < end; i++ {
			list = append(list, N_loc(fmt.Sprintf("%v", i), command[0]))
		}
		return ListToNode(list, command[0])
	case "split":
		text := S(args[0])
		delim := S(args[1])
		bits := strings.Split(text, delim)
		var list []autoparser.Node
		for _, v := range bits {
			list = append(list, N_loc(v, command[0]))
		}

		return ListToNode(list, command[0])
	case "join":
		// FIXME each element needs a unique id
		list, _ := ListToStrings(args[0].List)
		delim := S(args[1])
		out := N_loc(strings.Join(list, delim), command[0])
		out.File = parent.File
		out.Line = parent.Line
		out.Column = parent.Column
		out.ChrPos = parent.ChrPos
		return out
	case "chr":
		return N_loc(string(atoi(S(args[0]))), command[0])
	case "if":
		if len(args) == 3 && S(args[2]) != "else" {
			panic("If missing else")
		}
		if atoi(S(args[0])) != 0 {
			ret := blockReduce(s, args[1].List, parent, level)
			drintln("Returning from if true branch:", TreeToXsh(ret))
			return ret
		} else if len(args) == 4 {
			ret := blockReduce(s, args[3].List, parent, level)
			drintln("Returning from if false branch:", TreeToXsh(ret))
			return ret
		} else {
			msg := fmt.Sprintf("No else for if at %v:%v\n", command[0].Line, command[0].Column)
			XshWarn(msg)
			return N_loc(msg, command[0])
		}
		panic("Inconceivable")

	case "saveInterpreter":
		NotifyEvent(cmd)
		if len(args) < 1 {
			XshErr("saveInterpreter: no filename specified")
			return N_loc("No filename given", command[0])
		}
		*parent = Void(command[0], "return value from saveinterpreter")
		rest := TreeToXsh(*s.Tree)
		funcs := FunctionsToXsh(s.Functions)
		code := funcs + "\n\n" + rest
		fmt.Printf("Function defs: %v\n\nRemaining code: %v\n", funcs, rest)

		ioutil.WriteFile(S(args[0]), []byte(code), 0644)

	case "return":
		return args[0]
	case "id":
		return args[0]
	case "list":
		// FIXME need a unique id for each element in tree
		return ListToNode(args, command[0])
	case "background":
		stringCommand, _ := ListToStrings(args)
		//Launch a command in the background
		drintln("Running", stringCommand, "in background.  Output will be lost.")
		go func() {
			runWithoutGuardian(stringCommand)
		}()
		return N_loc("Backgrounded", args[0])
	default:
		// It is an external call, prepare the shell command then run it
		// fixme warn user on verbose?
		// fmt.Printf("Unknown command: '%s', attempting shell\n", f)
		if command[0].Note == "\"" {
			return command[0]
		}
		stringCommand, err := ListToStrings(command)
		if err != nil {
			msg := fmt.Sprintf("Error %v,%v,%v: converting command to string: %v\n", command[0].File, command[0].Line, command[0].Column, err)
			log.Printf(msg)
			return Error(msg, command[0])
		} else {
			var res string
			var err error
			if level == 1 {
				drintln("Running", stringCommand, "interactively")
				err = runWithGuardian(stringCommand, command[0].File, command[0].Line)
			} else {
				// This is the (only) point where we try to run the command as a subprocess
				drintln("Running", stringCommand, "to capture output")
				resCode, stdout, stderr, err := runWithoutGuardian(stringCommand)
				if err != nil {
					err = fmt.Errorf("Error %v,%v: %v\n", command[0].Line, command[0].Column, err)
				} else {
					resultList := []string{fmt.Sprintf("%v", resCode), stdout, stderr}
					return StringsToList(resultList, command[0])
				}
			}
			var out autoparser.Node
			if err == nil {
				if res == "" {
					out = N_loc("", command[0])
				} else {
					out = N_loc(fmt.Sprintf("%v", res), command[0])
				}
			} else {
				// FIXME revisit this after adding proper error handling
				fmt.Printf("Error: %+v\n", err)
				fmt.Println("Exiting after critical error")
				//FIXME: quit here or stop execution of current script?
				out = N_loc(err.Error(), command[0])
			}
			out.File = command[0].File
			out.Line = command[0].Line
			out.Column = command[0].Column
			out.ChrPos = command[0].ChrPos
			return out
		}
	}
	return Error(fmt.Sprintf("Unknown function: %v", S(command[0])), command[0])
}
