package xsh

import (
	_ "embed"
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"

	//"github.com/lmorg/readline"
	"github.com/donomii/goof"
	autoparser "gitlab.com/donomii/racketprogs/autoparser"
)

//go:embed stdlib.xsh
var Stdlib_str string

type Function struct {
	Name       string
	Parameters autoparser.Node
	Body       autoparser.Node
}

var (
	UsePterm             = true
	WantDebug       bool = false
	WantTrace       bool = false
	WantInform      bool = false
	WantHelp        bool = true
	WantWarn        bool = true
	WantErrors      bool = true
	WantInteractive bool = true
)

var (
	WantEvents bool = true
	Events          = [][]string{}
)

type State struct {
	Functions     map[string]Function
	Globals       map[string]string
	Tree          *autoparser.Node
	ExtraBuiltins func(s State, command autoparser.Node, parent *autoparser.Node, level int, timeout int) (autoparser.Node, bool) //FIXME change timeout to userdata
	TypeSigs      map[string][]string
	UserData      interface{}
}

// Create a new XSH engine
func New() State {
	s := State{map[string]Function{}, map[string]string{}, nil, nil, map[string][]string{}, nil}
	addBuiltinTypes(s)
	return s
}

// Given a program in string form, parse it into a tree.
func ParseXSH(code, filename string) autoparser.Node {
	l := autoparser.NewTree(code, filename)
	// FIXME we need to run both of these in parallel

	r, _ := autoparser.MultiStringify(l, []autoparser.StringDelimiter{
		{"/*", "*/", "\\", ""},
		{"\"", "\"", "\\", ""},
		{"#", "\n", "\\", "\n"},
	})

	// fmt.Printf("Stringified: ")
	// PrintTree(r, 0, false)
	r, _, _ = autoparser.Groupify(r)
	r = autoparser.KeywordBreak(r, []string{"|", "|\n", "\n"}, false)
	// r = KeywordBreak(r, []string{"|"})
	r = autoparser.MergeNonWhiteSpace(r)
	r = autoparser.StripNL(r)

	r = autoparser.StripWhiteSpace(r)
	r = autoparser.StripEmptyLists(r)
	// PrintTree(r, 0, false)
	n := ListToNode(r, r[0])
	return n
	// fmt.Printf("%+v\n", r)
}

// Eval, but with options that make sense for interactive sessions.  i.e. run sub programs connected
// to the terminal stdio so that text editors etc can work properly
func ShellEval(s State, code, fname string) autoparser.Node {
	// Trim spaces from both ends of the code
	code = strings.TrimSpace(code)
	if code == "" {
		XshTrace("ShellEval: Empty code\n")
		return Error("ShellEval: Empty code\n", autoparser.Node{Line: 1, Column: 0, ChrPos: 1, File: fname})
	}
	XshTrace("Evaluating shell command: %v\n", code)
	tree := ParseXSH(code, fname)
	drintf("Tree: %+v\n", tree)
	s.Tree = &tree
	res := treeReduce(s, tree.List, &tree, 1, tree)
	drintf("Res: %+v\n", res)
	return res
}

// Eval, in "batch" mode.  STDIN is nil and STDOUT is collected into a string as the return value
func Eval(s State, code, fname string) autoparser.Node {
	// XshTrace("Evalling: %v\n", code)
	tree := ParseXSH(code, fname)
	// drintf("Tree: %+v\n", tree)
	// drintf("[Eval] Tree: %+v\n", TreeToXsh(tree))
	// fmt.Println("[Eval] PrintTree")
	// PrintTree(tree, New().Tree)

	s.Tree = &tree
	res := treeReduce(s, tree.List, &tree, -1, tree)
	drintf("Res: %+v\n", res)
	return res
}

// Parse some xsh code into a tree
func Parse(code, fname string) autoparser.Node {
	return ParseXSH(code, fname)
}

// Run a tree (a program)
func RunTree(s State, tree autoparser.Node) autoparser.Node {
	s.Tree = &tree
	res := treeReduce(s, tree.List, &tree, 20, tree)
	return res
}

// Load a file and evaluate it
func LoadEval(s State, fname string) autoparser.Node {
	if !goof.Exists(fname) {
		XshErr("File not found: %v\n", fname)
		os.Exit(1)
	}
	// Pretty print the tree
	data := autoparser.LoadFile(fname)

	tree := ParseXSH(data, fname)

	tree.Kind = "LIST"
	tree.List = append([]autoparser.Node{{Raw: "list", Kind: "PARSED"}}, tree.List...)
	return RunTree(s, tree)
}

// Duplicate the tree.  This only duplicates the structure, not the contents (the data in the nodes).
func CopyTree(t autoparser.Node) autoparser.Node {
	out := []autoparser.Node{}
	for _, v := range t.List {
		if v.List != nil {
			out = append(out, autoparser.Node{v.Raw, v.Str, CopyTree(v).List, v.Note, v.Line, v.Column, v.ChrPos, v.File, v.ScopeBarrier, v.Kind, nil})
		} else {
			out = append(out, autoparser.Node{v.Raw, v.Str, v.List, v.Note, v.Line, v.Column, v.ChrPos, v.File, v.ScopeBarrier, v.Kind, nil})
		}
	}
	r := autoparser.Node{t.Raw, t.Str, out, t.Note, t.Line, t.Column, t.ChrPos, t.File, t.ScopeBarrier, t.Kind, nil}
	return r
}

// Is this still relevent?
func checkArgs(args []autoparser.Node, params []autoparser.Node) error {
	for _, v := range args {
		for _, p := range params {
			if p.Raw == v.Raw {
				XshErr("%v,%v,%v: Cannot shadow args in lambda: %+v, %+v\n", v.File, v.Line, v.Column, v, p)
				os.Exit(1)
			}
		}
	}
	return nil
}

// Recurse through the code tree, replacing any function calls with the function bodies, and any variables with the values.
// We replace multiple variables at the same time, for efficiency
func ReplaceArg(args, params []autoparser.Node, t autoparser.Node) autoparser.Node {
	if len(args) != len(params) {
		XshErr("Invalid number of arguments in function call: %v, %v\n", args, params)
		panic(fmt.Sprintf("ReplaceArg: Args and params must be the same length: %+v, %+v\n", args, params))
		// os.Exit(1)
	}
	// t must be a list
	out := []autoparser.Node{}
	for _, v := range t.List {
		// The scope barrier exists to prevent variable names being incorrectly replaced in substituted code
		// If there are multiple LET statements, or with some combinations of lambdas and recursion, it is possible
		// that a lambda will be copied into the function tree, and then another variable replace pass runs, incorrectly replacing
		// variables inside the lambda when they are really in a different scope and should not be replaced.  (i.e. modifying the wrong lexical scope)
		if v.ScopeBarrier {
			// v was copied from another scope, so we don't replace anything in it
			out = append(out, CopyNode(v))
		} else {
			// Recurse through this tree and replace variables with their values
			switch typeOf(v) {
			case "list":
				// Recurse through this list
				out = append(out, autoparser.Node{v.Raw, v.Str, ReplaceArg(args, params, v).List, v.Note, v.Line, v.Column, v.ChrPos, v.File, v.ScopeBarrier, v.Kind, nil})
			case "lambda":

				replaced := 0
				for param_index, param := range params {
					// drintf("Comparing function arg: %+v with node %+v\n", farg, v)
					// drintf("Comparing function arg: %+v with node %+v\n", args[parami].Raw, v.Raw)
					// We found a variable to replace
					if param.Raw == v.Raw {
						drintf("Replacing param %+v with %+v\n", param, args[param_index])
						new := CopyNode(args[param_index])
						// New node is assumed to be from a different scope, so we need to set the scope barrier
						// This will protect the subtree from further replacements
						new.ScopeBarrier = true
						out = append(out, new)
						// log.Printf("Inserting: %v\n", S(args[parami]))
						replaced = 1
					}
				}
				if replaced == 0 {
					out = append(out, CopyNode(v))
				}
			case "string":
				//We don't change strings
				out = append(out, CopyNode(v))
			case "comment":
				// We don't change comments
				out = append(out, CopyNode(v))
			case "symbol":
				//Parsed are symbols - variable names, function calls, etc
				replaced := 0
				for parami, param := range params {
					// drintf("Comparing function arg: %+v with node %+v\n", farg, v)
					// drintf("Comparing function arg: %+v with node %+v\n", args[parami].Raw, v.Raw)
					// We found a variable to replace
					if param.Raw == v.Raw {
						drintf("Replacing param %+v with %+v\n", param, args[parami])
						new := CopyNode(args[parami])
						// New node is assumed to be from a different scope, so we need to set the scope barrier
						// This will protect the subtree from further replacements
						new.ScopeBarrier = true
						out = append(out, new)
						// log.Printf("Inserting: %v\n", S(args[parami]))
						replaced = 1
					}
				}
				if replaced == 0 {
					out = append(out, CopyNode(v))
				}
			default:
				log.Panicf("replace arg on unknown type:%v", typeOf(v))
			}

		}
	}
	r := autoparser.Node{t.Raw, t.Str, out, t.Note, t.Line, t.Column, t.ChrPos, t.File, t.ScopeBarrier, t.Kind, nil}
	return r
}

// Recurse through the code tree, replacing any function calls with the function bodies, and any variables with the values.
func ReplaceArgs(args, params []autoparser.Node, t autoparser.Node) autoparser.Node {
	a, _ := ListToStrings(params)
	b, _ := ListToStrings(args)
	drintf("Replacing function params %+v with %+v\n", a, b)
	// log.Printf("Replacing function params %+v with %+v\n", TreeToTcl(params), TreeToTcl(args))

	t = ReplaceArg(args, params, t)

	return t
}

// Print the global function definitions into a string, to be parsed later
func FunctionsToXsh(functions map[string]Function) string {
	out := ""
	for _, v := range functions {
		out += fmt.Sprintf("proc %s {%v} {\n", v.Name, TreeToXsh(v.Parameters))
		out += TreeToXsh(v.Body)
		out += "}\n"
	}
	return out
}

// Create a Void node.  You must provide an existing node to copy the source location from.  The source location is used by a lot of debugging and display code, returning 0,0,0 will result in a lot of glitches.
func Void(command autoparser.Node, reason string) autoparser.Node {
	drintf("Creating void at %+v\n", command.Line)
	if command.ChrPos == 0 {
		XshWarn("Warning: Create void with invalid position.  This is probably an error.  Code: %+v (%v)\n", command, reason)
		//Dump stack trace
		trace := make([]byte, 1024*1024)
		count := runtime.Stack(trace, true)
		fmt.Printf("Stack trace: %s\n", trace[:count])

	}
	return autoparser.Node{"", "", nil, reason, command.Line, command.Column, command.ChrPos, command.File, command.ScopeBarrier, "VOID", nil}
}

// Is the Node a List?
func isList(n autoparser.Node) bool {
	return n.List != nil
}

// Creates a new copy of the node
func CopyNode(n autoparser.Node) autoparser.Node {
	return autoparser.Node{n.Raw, n.Str, n.List, n.Note, n.Line, n.Column, n.ChrPos, n.File, n.ScopeBarrier, n.Kind, nil}
}

// Returns the node type
func typeOf(e autoparser.Node) string {

	if e.List != nil {
		if len(e.List) > 0 {
			if e.List[0].List != nil {
				drintf("aaaaaaaChecking for lambda: %+v\n", e)
				if e.List[0].Raw == "||" || e.List[0].Raw == "|" {
					//It's actually a lambda definition
					drintf("aaaaaaaFound lambda: %+v\n", e)
					return "lambda"
				}
			}
		}
		return "list"
	}

	if e.Kind == "List" {
		if len(e.List) > 0 {
			if e.List[0].Kind == "List" {
				drintf("aaaaaaaChecking for lambda: %+v\n", e)
				if e.List[0].Raw == "||" || e.List[0].Raw == "|" {
					//It's actually a lambda definition
					drintf("aaaaaaaFound lambda: %+v\n", e)
					return "lambda"
				}
			}
		}
		return "list"
	}
	if e.Kind == "PARSED" {
		return "symbol"
	}
	if e.Kind == "STRING" {
		if e.Note == "#" || e.Note == "/*" {
			return "comment"
		}
		if e.Note == "\"" {
			return "string"
		}
	}
	if e.Kind == "" {
		switch e.Note {
		case "#":
			return "comment"
		case "/*":
			return "comment"
		case "\"":
			return "string"
		case "{":
			return "list"
		default:
			return e.Note
		}
	}

	if e.List != nil && e.Note == "{" {
		return "list"
	}
	if e.Str != "" {
		return "string"
	}
	if e.Raw != "" {
		return "string"
	}
	switch e.Kind {
	case "VOID":
		return "void"
	}
	return e.Kind
	return "void"
}

// Create a new empty list.  You must provide a source node with valid location information.
func EmptyList(sourceNode autoparser.Node) autoparser.Node {
	return ListToNode([]autoparser.Node{}, sourceNode)
}

// Is the node a lambda?
func isLambda(e autoparser.Node) bool {
	return typeOf(e) == "lambda"

	if e.List == nil {
		return false
	}
	command := e.List
	if len(command) == 0 {
		return false
	}
	if isList(command[0]) {
		theLambdaFunction := command[0]
		if theLambdaFunction.Kind == "LAMBDA" {
			return true
		}
		if theLambdaFunction.Kind == "lambda" {
			return true
		}
		if theLambdaFunction.Note == "|" {
			return true
		}
		if theLambdaFunction.Raw == "|" || theLambdaFunction.Raw == "||" {
			return true
		}
		if theLambdaFunction.List == nil {
			return false
		}
		if len(theLambdaFunction.List) < 2 {
			return false
		}
		if theLambdaFunction.List[1].Raw == "|" {
			return true
		}
		if theLambdaFunction.List[1].Str == "|" {
			return true
		}
	}
	return false
}

// Evaluate a single function call.
func eval(s State, command autoparser.Node, parent *autoparser.Node, level int) autoparser.Node {
	if len(command.List) == 0 {
		//Replace this empty list with a seq function call, by copying the command and updating the type and raw fields
		seq := CopyNode(command)
		seq.Kind = "PARSED"
		seq.Raw = "seq"
		seq.List = nil
		seq.Note = "Replaced empty list with seq"
		return seq
		// Empty list is created by the parser when it sees an empty line (especially the first line in multi-line functions).  Return a void so it gets filtered out later
		v := Void(command, "Replaced empty list with void in eval")
		return v
	}
	drintf("Evaluating: %v\n", TreeToXsh(command))
	// If list, assume it is a lambda function and evaluate it
	if isList(command.List[0]) {

		theLambdaFunction := command.List[0]
		drintf("Evaluating lambda: %v\n", theLambdaFunction)
		args := command.List[1:]

		bod := CopyTree(theLambdaFunction)
		bod.List = bod.List[1:]
		params := theLambdaFunction.List[0].List
		if isLambda(theLambdaFunction) {

			if len(params) != len(args) {
				XshErr("Error %v,%v,%v: Mismatched function args in ->|%v|<-  expected %v, given %v\n[%v %v]\n", command.List[0].File, command.List[0].Line, command.List[0].Column, TreeToXsh(command), TreeToXsh(ListToNode(params, command)), TreeToXsh(ListToNode(args, command)), S(theLambdaFunction), TreeToXsh(ListToNode(args, command)))
				os.Exit(1)
			}
			nbod := ReplaceArgs(args, params, bod)
			drintf("Calling lambda %+v\n", nbod)
			return blockReduce(s, nbod.List, parent, 0)
		} else {
			// Programmer is trying to return a list, it's not a lambda function
			l := CopyNode(command)
			return l
		}
	}

	// If it is a string or symbol, look it up in the global
	// functions map and return the result
	f := S(command.List[0])
	preargs := command.List[1:]
	args := []autoparser.Node{}
	for _, v := range preargs {
		// if typeOf(v) != "void" {  //FIXME causes map etc. to fail.  Filtering out void alters the number of args to some calls.  Need to improve type system so void can't be returned in those situations.
		args = append(args, v)
		//}
	}

	//Named function
	//do some checks
	// Do we have a type for this function?
	ftype, ok := s.TypeSigs[f]
	if ok {
		lastArgType := ftype[len(ftype)-1]
		fixedArgs := []string{}
		if len(ftype)-2 > 0 {
			fixedArgs = ftype[1 : len(ftype)-2]
		}
		if len(ftype) != len(args)+1 {
			if lastArgType != "..." {
				XshErr(`Error %v,%v,%v: Mismatched function args to %v

Expected:
	%v %v (%v args)

Given:
	%v %v (%v args)
	`, command.List[0].File, command.List[0].Line, command.List[0].Column, f, f, strings.Join(ftype[1:], " "), len(ftype)-1, f, TreeToXsh(ListToNode(args, command)), len(args))

				os.Exit(1)
			}
		}
		// Different checks for variadic functions
		// FIXME refactor duplicate code
		if lastArgType == "..." {
			varArgsType := ftype[len(ftype)-2]
			varArgs := []autoparser.Node{}
			if len(ftype)-2 < len(args) {
				varArgs = args[len(ftype)-2:]
			}
			// First, check that the specified types match
			// This is the fixed args array
			for i, v := range fixedArgs {
				drintf("Checking type %v against arg %v\n", v, typeOf(args[i]))
				if typeOf(args[i]) != v && v != "any" {
					XshWarn("Type error in named args at file %v line %v (command %v).  At argument %v, expected %v, got %v\n", command.List[0].File, command.List[0].Line, TreeToXsh(command), i+1, v, typeOf(args[i]))
				}
			}
			// Now check that the remaining args are of the varargs type
			drintf("Checking remaining args %v against varargs type %v\n", TreeToXsh(ListToNode(varArgs, command)), varArgsType)
			for i, v := range varArgs {
				drintf("Checking type %v against arg %v\n", typeOf(v), varArgsType)
				if typeOf(v) != varArgsType && varArgsType != "any" {
					XshWarn("Type error in vararg at file %v line %v (command %v).  At argument %v, expected %v (in ...), got %v\n", command.List[0].File, command.List[0].Line, TreeToXsh(command), i+len(ftype)-1, varArgsType, typeOf(v))
				}
			}
		} else {
			// There are no varags, simply check that the types match, and there are the correct number of args
			for i, v := range ftype[1:] {
				if (typeOf(args[i]) != v) && (v != "any") && (typeOf(args[i]) != "symbol") { /*symbols can resolve to any type, so I guess either we resolved them early, or ignore them here*/
					XshWarn("Type warning in fixed args at file %v line %v ( %v).  At argument %v(%v), expected %v, got %v\n", command.List[0].File, command.List[0].Line, TreeToXsh(command), i+1, TreeToXsh(args[i]), v, typeOf(args[i]))
					XshInform("Node: %+v\n", args[i])
				}
			}
		}

	}

	//call named function
	fu, ok := s.Functions[f]
	if ok {
		XshTrace("Calling named function %v with args %v\n", f, TreeToXsh(ListToNode(args, command)))
		// It's a user-defined function
		bod := CopyTree(fu.Body)
		fparams := fu.Parameters
		// FIXME: Need to handle user varargs here
		// Is the last element a ...?
		if len(fparams.List) > 0 && S(fparams.List[len(fparams.List)-1]) == "..." {
			// Yes, it's a varargs function
			// Remove the ... from fparams
			fparams.List = fparams.List[:len(fparams.List)-1]
			// Collect all the extra arguments into a list
			varArgs := []autoparser.Node{}
			if len(fparams.List)-1 < len(args) {
				varArgs = append(varArgs, args[len(fparams.List)-1:]...)

				// trim the same extra arguments from args
				args = args[:len(fparams.List)-1]
			}
			n := command
			// Add the extra arguments list to the end of args
			varargList := ListToNode(varArgs, n)
			args = append(args, varargList)
		}
		// Check that the number of args is correct
		if len(fparams.List) != len(args) {
			XshErr(`Error %v,%v,%v: Mismatched function args in call to ->|%v|<-, expected %v, got %v
		
	Expected: %v
	Given:    %v
`, command.List[0].File, command.List[0].Line, command.List[0].Column, TreeToXsh(command), len(fparams.List), len(args), TreeToXsh(fparams), TreeToXsh(ListToNode(args, command)))
			os.Exit(1)
		}

		nbod := ReplaceArgs(args, fparams.List, bod)
		drintf("Calling named function %+v\n", TreeToXsh(nbod))
		return blockReduce(s, nbod.List, parent, 0)

	} else {
		// It is a builtin function or an external call
		if f == "" {
			// We shouldn't get an empty string here, but if we do, we ignore it and keep going
			return Void(command, fmt.Sprintf("attempt to lookup a function with an empty string for a name, %v:%v:%v.  %+v", command.List[0].File, command.List[0].Line, command.List[0].Column, command))
		}

		// This is for the embedded xsh, it allows the embeddor to add their own functions,
		// e.g. to connect to data sources or do IO
		if s.ExtraBuiltins != nil {
			ret, handled := s.ExtraBuiltins(s, command, parent, level, 1000) //FIXME change 1000 to userdata
			if handled {
				return ret
			} else {
				drintf("No extra builtin for %v\n", TreeToXsh(command))
			}
		}
		drintf("No extra builtins while evaluating %v\n", TreeToXsh(command))
		argsNode := CopyNode(command)
		argsNode.List = args
		return builtin(s, command.List, parent, f, args, level, argsNode)

	}
	// Maybe we should return a warning here?  Or even exit?  Need a strict mode.
}

// BlockReduce operates a little bit oddly.  It evals a list of statements, and replaces each statement with
// the result of the statement.  It then returns the last result.
// The allows the debugger to show each statement as it is evaluated.
func blockReduce(s State, t []autoparser.Node, parent *autoparser.Node, toplevel int) autoparser.Node {
	drintf("BlockReduce: starting %+v\n", TreeToXsh(ListToNode(t, *parent)))
	// log.Printf("BlockReduce: starting %+v\n", t)

	// Empty block
	if len(t) == 0 {
		return Void(*parent, "an empty block of code")
	}

	// The scopebarrier prevents variable substitution happening to a code branch that has been copied in
	// from another function or scope.
	if (parent != nil) && parent.ScopeBarrier {
		return *parent
	}

	var out autoparser.Node
	if t[0].Note == "\n" && t[0].Raw == "\n" {
		// It is a multi line block, eval as statements, return the last

		for i, v := range t {
			out = treeReduce(s, v.List, parent, toplevel, v)
			t[i] = out
		}
	} else {
		// It is a single line lambda, eval it as an expression
		out = treeReduce(s, t, parent, toplevel, *parent)
	}

	drintf("Returning from BlockReduce: %v\n", TreeToXsh(out))
	return out
}

// Build a node location string from file, line, and column
func Loc(n autoparser.Node) string {
	return fmt.Sprintf("%v:%v:%v", n.File, n.Line, n.Column)
}

// Recursively evaluate a program tree down to the leaves, then "fold up" the results into a single value
// This is the heart of the interpreter.
func treeReduce(s State, t []autoparser.Node, parent *autoparser.Node, toplevel int, orig autoparser.Node) autoparser.Node {
	drintf("Reducing: %+v\n", TreeListToXsh(t))

	// Because XSH replaces functions with their definitions, and then evals them, there are bits of "other scopes" that get pasted into the running code.  These must not be evaluated in the current function scope, but in their own scope.  So we stop processing when we discover that we are in a different scope.
	if (parent != nil) && parent.ScopeBarrier {
		return *parent
	}
	/*  FIXME need some way to display each line as we run it, but this is not it
	if toplevel == 1 {
		XshInform(TreeToXsh(t))
	}
	*/
	// TreeReduce calls the first argument of a list on the rest of the args.  So first we must resolve every arg, which will either be a literal or an expression that returns a single item (an atom)

	out := []autoparser.Node{}

	// Here we resolve the args (and the function, since it will be a varible or a lambda literal)
	for i, v := range t {
		if WantTrace && v.File != "" { // FIXME this is just masking a problem.  Every element should have a file and line number
			fmt.Printf("Trace: In %v: ", Loc(v))
			PrintTree(orig, v)
			fmt.Print("\n")
		}
		// Switch on type
		switch typeOf(v) {
		case "comment":
			// Comment node, discard
			out = append(out, Void(v, "comment"))
		case "lambda":
			// a lambda apply like { x | + x 1 } {1 2 3}
			// log.Println("Command:", TreeToTcl(v.List))
			out = append(out, v)
			t[i] = v

		case "list":
			// A list can be a function call (i.e. a sub-expression) or a normal list
			if v.Raw == "{" { //It's  a list literal
				drintf("storing list: %+v\n", v)
				drintln("treeReduce: found list ", TreeToXsh(v))
				out = append(out, v)
				t[i] = v
			} else { //It's a function call

				atom := treeReduce(s, v.List, &t[i], toplevel-1, orig)
				out = append(out, atom)
				t[i] = atom
			}
		default:
			// A literal or a variable
			atom := autoparser.Node{ChrPos: -1, Note: "Blank placeholder", Str: "Blank placeholder", Raw: "Blank placeholder", Line: -1, Column: -1, File: "Blank placeholder", ScopeBarrier: false, Kind: "Blank placeholder", List: nil}
			if strings.HasPrefix(S(v), "$") {
				// Environment variable or global variable
				drintf("Environment variable lookup: %+v\n", S(v))
				vname := S(v)[1:] // Remove the $, this is the variable name
				drintf("Fetching %v from Globals: %+v\n", vname, s.Globals)
				if vname == "" {
					XshErr("$ found without variable name.  $ defines a variable, and cannot be used on its own.")
					os.Exit(1)
				} else {
					// Valid variable name
					if _, ok := s.Globals[vname]; ok {
						// Global variable (like an environment variable, but inside xsh)
						// FIXME: globals should go away?
						atom = N_loc(s.Globals[vname], v)
						// FIXME
						atom.File = v.File
						atom.Line = v.Line
						atom.Column = v.Column
						atom.ChrPos = v.ChrPos
					} else {
						// Load value from environment
						if val := os.Getenv(vname); val != "" {
							atom = N_loc(val, v)
							atom.File = v.File
							atom.Line = v.Line
							atom.Column = v.Column
							atom.ChrPos = v.ChrPos
						} else {
							// FIXME detect if environment variable is not set, or just empty.  Only throw error on not set.
							XshErr("Global variable $%v not found.  You must define a variable before you use it.", vname)
							os.Exit(1)
						}
					}
				}

			} else {
				atom = v
			}
			out = append(out, atom)
			t[i] = atom
		}
	}

	// Run command
	if len(out) > 0 {
		if WantTrace {
			fmt.Printf("Trace: Out %v: ", Loc(out[0]))
			PrintTree(orig, out[0])
			fmt.Print("\n")
		}
	}

	drintf("Evaluating: %v\n", TreeListToXsh(out))
	ret := eval(s, ListToNode(out, orig), parent, toplevel)
	drintf("Returning from treeReduce: %v\n", TreeToXsh(ret))
	drintf("Returning from treeReduce: %+v\n", ret)
	return ret
}

// Turns a segment of a program tree into a json string
func TreeToJson(t autoparser.Node) string {
	out := ""
	for _, v := range t.List {
		switch {
		case v.Kind == "VOID":
		case v.List != nil:
			out = out + "[" + TreeToJson(v) + "]"
		case v.Raw != "":
			out = out + "\"" + v.Raw + "\"" + " " // FIXME escape string properly for JSON

		default:
			out = out + "\"\\\"" + v.Str + "\\\"\"" + " " // FIXME escape string properly for JSON

		}
	}
	return out
}

// Convert a list of nodes into a string.
func TreeListToXsh(t []autoparser.Node) string {
	out := ""
	for _, v := range t {
		out = out + TreeToXsh(v)
	}
	return out
}

// Convert a single tree into a string
func TreeToXsh(t autoparser.Node) string {
	var out string
	//fmt.Printf("Type: %v\n", typeOf(t))
	switch typeOf(t) {
	case "list":
		out = "["
		for i, v := range t.List {
			if i != 0 {
				out = out + " "
			}
			out = out + TreeToXsh(v)

		}
		out = out + "]"
	case "lambda":
		out = "{"
		for i, v := range t.List {
			if i != 0 {
				out = out + " "
			}
			out = out + TreeToXsh(v)

		}
		out = out + "}"
	case "void":
		out = "void"
	default:
		out = S(t)
	}

	return out
}

// Recursively apply a function to every node in a tree, and build a new tree from the results
func TreeMap(f func(autoparser.Node) autoparser.Node, t autoparser.Node) autoparser.Node {
	if t.List == nil {
		return f(t)
	}
	out := []autoparser.Node{}
	for _, v := range t.List {
		if v.List != nil {
			out = append(out, TreeMap(f, v))
		} else {
			new := f(v)
			if isList(new) {
				out = append(out, new.List...)
			} else {
				out = append(out, new)
			}
		}
	}
	r := autoparser.Node{t.Raw, t.Str, out, t.Note, t.Line, t.Column, t.ChrPos, t.File, t.ScopeBarrier, t.Kind, nil}
	return r
}
