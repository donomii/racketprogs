package xsh

import (
	"fmt"
	"net/http"
)

func wrapEvalXsh(state State, code string) (result string) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
			result =  fmt.Sprintf("%v", r)
		}
	}()

	resultTree := Eval(state, code, "web request")
	fmt.Printf("resultTree %+v", resultTree)
	result = TreeToXsh(resultTree)
	return result
}

func StartEvalServer(state State, port int) {
	    http.HandleFunc("/eval", func(w http.ResponseWriter, r *http.Request) {
        method := r.Method
		switch method {
		case "GET":
			codeString := r.URL.Query().Get("code")
			fmt.Println("codeString", codeString)
			result := wrapEvalXsh(state, codeString)
			fmt.Println("result", result)
			w.Write([]byte(result))
			case "POST":
			codeString := r.FormValue("code")
			fmt.Println("codeString", codeString)
			result := wrapEvalXsh(state, codeString)
			fmt.Println("result", result)
			w.Write([]byte(result))
			default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			w.Write([]byte("405 - Method not allowed - "+method))
		}
    })
    http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	

}