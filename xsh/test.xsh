put "2+3="
set x [+ 2 3]
puts $x
set y [+ 1 $x]
puts "6=" $y
func return1 {| return 1}
puts "1=" [return1]
test "Call a function, return a constant" 1 [return1]

func return2 {|
	+ 2 2
	return 2
}

puts "2=" [return2]
test "Call a function, run some code" 2 [return2]

func emptyList {|
	return [list]
}

puts "Empty list:" [emptyList]
test "Empty list" [list] [emptyList]

func sayHello {name|
	puts "Hello" name
}
func returnOne {| "1" }
puts "Testing return"
puts "Test 1 (should equal 1):" [returnOne]
test return 1 [return1]
/*#puts [+ [+ 1 2] [saveInterpreter "savefile.cont"] [+ 3 4] ]
#puts "Editing main.go"
#run vim main.go
#puts [loadfile "main.go"]
*/
puts [LS .]
sayHello "Jeremy"
func map {f alist|
	if [eq [length alist] 0] {
			return [list]
        } else {
			with { car cdr } = { [f [lindex alist 0]] [map f [lrange alist 1 end]] } {
				cons car cdr
			}
	}
}

puts Add 1 to list: [map {a| + 1 a} { 1 2 3 4 5}]
test "map" {2 3 4 5 6}  [map {a| + 1 a} { 1 2 3 4 5}]


func fold { func accum alist|
	/*#puts "fold list: " alist "accum:" accum*/
	if [eq [length alist] 0] {
		accum
        } else {
		if [eq [length alist] 1] {
			func accum [lindex alist 0]
		} else {
			fold func [func accum [lindex alist 0]] [lrange alist 1 end]
		}
	}
}


func CR {| chr 13 }
func LF {| chr 10 }


test "fold" 15 [fold + 0 {1 2 3 4 5}]


func reverse {alist|
       fold {a b| seq [cons b a]} {} alist 
}


puts "Reversed: " [reverse { a b c d } ]
test "Reverse list" { d c b a } [reverse { a b c d } ]

func countdown {n|
	puts n
	if [gt n 0] {
	       	return [countdown [- n 1]]
	} else {
		return 0
	}
}

countdown 5

func thr {count threes|
	if [eq count threes] {
		puts "fizz"
		+ 3 threes
	} else {
		return threes 
	}
}

func fiv { count fives|
	if [eq count fives] {
		puts "buzz"
		+ 5 fives
	} else {
		return fives
	}
}

func do_fizzbuzz {num count threes fives|
	if [gt num count] {
		if [and [eq count threes] [eq count fives]] {
			puts "fizzbuzz"
			do_fizzbuzz num [+ 1 count ] [+ 3 threes] [+ 5 fives]
		} else {
			if [eq count threes] {
				puts "fizz"
				do_fizzbuzz num  [+ 1 count] [+ 3 threes] fives
			} else {
				if [eq count fives] {
					puts "buzz"
					do_fizzbuzz num [+ 1 count] threes [+ 5 fives]
				} else {
					puts count
					do_fizzbuzz num [+ 1 count] threes fives
				}
			}
		}
	} else {
	}
}

func fizzbuzz {| do_fizzbuzz 10 1 3 5}

fizzbuzz

{a b c|
        puts c
        puts [+ a b]
     } 1 2 "Result:"

{a b c|
        puts c
        puts [+ a b]
     } 1 2 "Result:"

/*#Test evaluation in assignment block*/
with {sum} = {[+ 2 3]} {
	puts "Sum:" sum
}


/*Test that "dontreplace" contains the correct values after scope substitution*/
with {testscope} = { {dontreplace| puts dontreplace} } {
	with {x y z dontreplace lambdacopy} = {1 2 3 fail testscope} {
		 puts "x:"  x  " y:"  y  " z:"  z
		 puts "Scope working" = [testscope "pass"]
		 puts  "copy function" = lambdacopy
		 puts "test" = [lambdacopy "pass"]
	}
}


/*Test that "dontreplace" contains the correct values after scope substitution*/
with {testwith} = { {dontreplace1| puts dontreplace1} } {
	puts [testwith "testwith pass"]
}


with { testwitheval } = { [+ 2 5]} {
	puts testwitheval
}

#test comment
puts "Comment parser works"


puts [range 0 10 1]


set NUM_SERVERS 10
set NODEIDS [range 0 10 1]
set BASE_RAFT_PORT 8000
set RAFT_HTTP_PORTS [map { p | join "weaviate-" p ":" [+ $BASE_RAFT_PORT p] ""} $NODEIDS]
puts $RAFT_HTTP_PORTS

with { numServers basePort } = { 10  8000 } {
	with { nodeIds } = { [range 0 numServers 1] } {
		 puts NodeIds: nodeIds 
	}
}

set ABCD { 0 1 2 3 }
