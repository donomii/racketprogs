package xsh

import (
	"fmt"
	"log"
	"strings"

	autoparser "gitlab.com/donomii/racketprogs/autoparser"
	"github.com/pterm/pterm"
)

func drintf(formatStr string, args ...interface{}) {
	out := fmt.Sprintf(formatStr, args...)
	if WantDebug {
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgLightMagenta))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			log.Printf(formatStr, args...)
		}
	}
}

func drintln(args ...interface{}) {
	out := fmt.Sprintln(args...)
	if WantDebug {
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgLightMagenta))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			log.Println(args...)
		}
	}
}

func XshErr(formatStr string, args ...interface{}) {
	if WantErrors {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgRed))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			fmt.Println("Err:", out)
		}
	}
}

func XshWarn(formatStr string, args ...interface{}) {
	if WantWarn {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgYellow))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			fmt.Println("Warn:", out)
		}
	}
}

func XshInform(formatStr string, args ...interface{}) {
	if WantInform {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgBlue))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			fmt.Println("Info:", out)
		}
	}
}

func XshHelpful(formatStr string, args ...interface{}) {
	if WantHelp {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgBlue))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			fmt.Println("Help:", out)
		}
	}
}

func XshTrace(formatStr string, args ...interface{}) {
	if WantTrace {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgGreen))
			pterm.DefaultCenter.Println(header.Sprint(out))
		} else {
			fmt.Println("Trace:", out)
		}
	}
}

func XshResponse(formatStr string, args ...interface{}) {
	if WantInteractive {
		out := fmt.Sprintf(formatStr, args...)
		if UsePterm {
			header := pterm.DefaultHeader.WithBackgroundStyle(pterm.NewStyle(pterm.BgBlue))
			pterm.Println(header.Sprint(out))
		} else {
			fmt.Println("xsh:", out)
		}
	}
}

// Pretty print code.  The subTree will be highlighted if it is found in the tree.
// This is used by the debugger to show the current statement being evaluated.
func PrintTree(t autoparser.Node, subTree autoparser.Node) {
	PrintTreeRec(t, subTree, true, 1)
}

func indent(str string, level int) string {
	return strings.Repeat(str, level)
}

func printIndent(str string, level int) {
	fmt.Print(indent(str, level))
}

func NLindent(str string, level int) string {
	return "\n" + indent(str, level)
}

func PrintNLindent(str string, level int) {
	fmt.Print(NLindent(str, level))
}

// Recurse through program tree, printing each node
func PrintTreeRec(t autoparser.Node, subTree autoparser.Node, inBlock bool, level int) {
	highlight := pterm.NewStyle(pterm.FgRed, pterm.Bold)
	// function := pterm.NewStyle(pterm.FgMagenta, pterm.Bold)

	/*if Loc(t[0]) == Loc(subTree) {
		highlight.Print("(")
		function.Printf("%v", S(t[0]))
	} else {
		fmt.Printf("(%v", S(t[0]))
	}*/

	for i, v := range t.List {

		if i == 1 && isList(v) && len(v.List) == 1 && v.List[0].Raw == "|" {
			inBlock = true
			// fmt.Printf("%+v\n", v)
			continue
		}
		if i == 0 && isList(v) && len(v.List) == 0 {
			inBlock = true
			// fmt.Printf("%+v\n", v)
			continue
		}
		if i != 0 {
			fmt.Print(" ")
		}

		fmt.Printf(":%v,%v,%v:", typeOf(v), v.Note, v.Raw)
		switch {
		case v.Note == "VOID":
		case v.List != nil:

			switch v.Note {

			case "{":
				if Loc(v) == Loc(subTree) {
					highlight.Print("{")
					PrintTreeRec(v, subTree, false, level+1)

					highlight.Print("}")
				} else {
					fmt.Print("{")

					PrintTreeRec(v, subTree, false, level+1)

					if inBlock {
						fmt.Print(NLindent(" ", level))
					}
					fmt.Print("}")
				}
			case "|":
				PrintTreeRec(v, subTree, false, level)
				fmt.Print("|")
			default:
				if Loc(v) == Loc(subTree) {
					highlight.Print("(")
					PrintTreeRec(v, subTree, false, level)
					highlight.Print(")")
				} else {
					if inBlock {
						PrintNLindent(" ", level)
						PrintTreeRec(v, subTree, false, level)
					} else {
						fmt.Print("(")
						PrintTreeRec(v, subTree, false, level)
						fmt.Print(")")
					}
				}

			}
		case v.Raw != "":
			if Loc(v) == Loc(subTree) {
				highlight.Print(v.Raw) // FIXME escape string properly to include in JSON
			} else {
				fmt.Print(v.Raw)
			}
		default:
			if v.Note == "#" {
				fmt.Print("#")
				if Loc(v) == Loc(subTree) {
					highlight.Print(v.Str)
				} else {
					fmt.Print(v.Str)
				}
			} else {
				if Loc(v) == Loc(subTree) {
					// fmt.Printf("%+v", v)
					highlight.Print("\"" + v.Str + "\"") // FIXME escape string properly for JSON
				} else {
					fmt.Print("\"" + v.Str + "\"")
				}
			}
		}
	}
	if inBlock {
		PrintNLindent(" ", level-1)
	}
	/*
		if Loc(t[0]) == Loc(subTree) {
			highlight.Print(")")
		} else {
			fmt.Print(")")
		}*/
}

// Trim a string to the requested length, with an ellipsis if the string is longer than the requested length
func clampString(s string, max int) string {
	if len(s) > max {
		if max > 3 {
			return s[:max-3] + "..."
		} else {
			return s[:max]
		}
	}
	return s
}
