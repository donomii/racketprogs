# Demonstrate environment variable scoping

# Set an environment variable
set TEST "hello"
puts $TEST

# What happens when it is part of a lambda?
func printTest {| puts $TEST }


# Output should be goodbye, environment variables are resolved at the last possible moment
set TEST "goodbye"
printTest

trace "on"
