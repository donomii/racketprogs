package main

import (
	"fmt"
	"image"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	_ "image/jpeg"
    _ "image/png"
	_ "image/gif"
	_ "golang.org/x/image/webp"
	_ "golang.org/x/image/tiff"
	_ "golang.org/x/image/bmp"
	_ "golang.org/x/image/vp8"
	_ "golang.org/x/image/vp8l"
	"github.com/h2non/filetype"
	"github.com/h2non/filetype/types"
)

var imageTypes = map[string]bool{
    "jpg":  true,
    "jpeg": true,
    "png":  true,
    "gif":  true,
    "bmp":  true,
    "webp": true,
    "tiff": true,
}

func imageCheck(path string) {
	dirPtr := &path


    err := filepath.Walk(*dirPtr, func(path string, info fs.FileInfo, err error) error {
        if err != nil {
            fmt.Printf("Error accessing path %q: %v\n", path, err)
            return err
        }

        if !info.IsDir() && isImageFile(path) {
            if err := verifyImage(path); err != nil {
                fmt.Printf("Corrupted or unsupported image: %s, error: %v\n", path, err)
            } else {
                //fmt.Printf("Verified image: %s\n", path)
            }
        }
        return nil
    })

    if err != nil {
        fmt.Printf("Error walking the path %q: %v\n", *dirPtr, err)
    }
}

func isImageFile(path string) bool {
	if len(path) < 5 {
		return false
	}
	extwithdot :=filepath.Ext(path)
	if len(extwithdot) < 2 {
		return false
	}
    ext := strings.ToLower(extwithdot[1:])
    if _, ok := imageTypes[ext]; !ok {
        return false
    }

    file, err := os.Open(path)
    if err != nil {
        return false
    }
    defer file.Close()

    buf := make([]byte, 261) // 261 bytes is enough to hold any magic number
    if _, err := file.Read(buf); err != nil && err != io.EOF {
        return false
    }

    kind, _ := filetype.Match(buf)
    return kind != types.Unknown && imageTypes[kind.Extension]
}

func verifyImage(path string) error {
    file, err := os.Open(path)
    if err != nil {
        return err
    }
    defer file.Close()

    _, _, err = image.Decode(file)
    return err
}
