// Find programs listening on ports
package main

import (
	"fmt"
	"os/exec"
	"runtime"
	"strings"
	"strconv"
	"regexp"
)

func getProcessPath(pid int) string {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("wmic", "process", "where", fmt.Sprintf("ProcessId=%d", pid), "get", "ExecutablePath")
	} else if runtime.GOOS == "darwin" {
		cmd = exec.Command("ps", "-p", fmt.Sprintf("%d", pid), "-o", "comm=")
	} else {
		cmd = exec.Command("readlink", fmt.Sprintf("/proc/%d/exe", pid))
	}
	output, _ := cmd.Output()
	return strings.TrimSpace(string(output))
}

func FindListeningProcess(port int) {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("netstat", "-ano")
	} else {
		cmd = exec.Command("lsof","-Pi",   fmt.Sprintf(":%d", port),"-n")
	}
	output, _ := cmd.Output()
	lines := strings.Split(string(output), "\n")

	//COMMAND      PID USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
	//weaviate- 158082 user   15u  IPv4 47343146      0t0  TCP localhost:http-alt (LISTEN)
	for _, line := range lines {
		if strings.Contains(line, "LISTEN") {
			words := strings.Fields(line)
			pid, _ := strconv.Atoi(words[1])
			//Get the path for pid
			path := getProcessPath(pid)

			fmt.Printf("PID %d is listening on port %d, path: %s\n", pid, port, path)
	}
}
}

func FindAllListeningPorts() {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("netstat", "-ano")
	} else {
		cmd = exec.Command("lsof","-Pi",  "-n")
	}
	output, _ := cmd.Output()
	lines := strings.Split(string(output), "\n")

	//COMMAND      PID USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
	//weaviate- 158082 user   15u  IPv4 47343146      0t0  TCP localhost:http-alt (LISTEN)
	for _, line := range lines {
		if strings.Contains(line, "LISTEN") {
			words := strings.Fields(line)
			pid, _ := strconv.Atoi(words[1])
			//Get the path for pid
			path := getProcessPath(pid)

			fmt.Println(line)
			//Extract PORT with a regex that matches "TCP *:57394 (LISTEN)"
			regex := regexp.MustCompile(`:\d+ .LISTEN.`)
			port := regex.FindString(line)
			fmt.Println(port)
			if len(port) <9 {
				continue
			}
			//Remove the first character (the colon)
			port = port[1:]
			//Remove the last 8 characters (the space and "(LISTEN)")
			port = port[:len(port)-8]
			

			fmt.Printf("PID %d is listening on port %v, path: %s\n", pid, port, path)
	}
}
}

func KillProcessListeningOnPort(port int) {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("netstat", "-ano")
	} else {
		cmd = exec.Command("lsof", "-i", fmt.Sprintf(":%d", port))
	}
	output, _ := cmd.Output()
	lines := strings.Split(string(output), "\n")

	//COMMAND      PID USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
	//weaviate- 158082 user   15u  IPv4 47343146      0t0  TCP localhost:http-alt (LISTEN)
	for _, line := range lines {
		if strings.Contains(line, "LISTEN") {
			words := strings.Fields(line)
			pid, _ := strconv.Atoi(words[1])
			//Get the path for pid
			path := getProcessPath(pid)

			fmt.Printf("PID %d is listening on port %d, path: %s\n", pid, port, path)
			//Kill the process
			if runtime.GOOS == "windows" {
				cmd = exec.Command("taskkill", "/F", "/PID", fmt.Sprintf("%d", pid))
			} else {
				cmd = exec.Command("kill", fmt.Sprintf("%d", pid))
			}
			cmd.Run()
	}
}
}