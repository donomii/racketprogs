package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)


func findFiles(root string, nameGlob string, fileType string, minSize int64, maxSize int64) {
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.Name() == "." || info.Name() == ".." {
			return nil
		}
		if nameGlob != "" {
			matched, err := filepath.Match(nameGlob, info.Name())
			if err != nil {
				return err
			}
			if !matched {
				return nil
			}
		}
		if fileType != "" && fileType != info.Mode().String()[:1] {
			return nil
		}
		if info.Size() < minSize || (maxSize > 0 && info.Size() > maxSize) {
			return nil
		}
		fmt.Println(path)
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}