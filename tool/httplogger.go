package main

 // Simply prints any parameters send to it in a http request.  Handy for debugging programs that mess up the stdout/stderr

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

// getLocalIP finds the most likely local IP address to be used for communication.
func getLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Fatalf("Error getting network interfaces: %v", err)
	}
	for _, addr := range addrs {
		if ipNet, ok := addr.(*net.IPNet); ok && !ipNet.IP.IsLoopback() && ipNet.IP.To4() != nil {
			return ipNet.IP.String()
		}
	}
	return ""
}

// listenAndLogHTTP starts an HTTP server that listens on the provided port and logs all incoming requests.
func listenAndLogHTTP(port int) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Read the body of the request
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Log the request details
		log.Printf("Received request: Method=%s, URL=%s, Headers=%v, Body=%s", r.Method, r.URL.String(), r.Header, string(body))

		// Respond with a success status
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("Success"))
	})

	localIP := getLocalIP()
	if localIP != "" {
		log.Printf("Most likely IP address to contact: %s", localIP)
	}

	listenAddr := fmt.Sprintf(":%d", port)
	log.Printf("HTTP Logger listening on %s...", listenAddr)
	if err := http.ListenAndServe(listenAddr, nil); err != nil {
		log.Fatalf("Error starting HTTP logger: %v", err)
	}
}
