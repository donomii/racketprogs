# The Tool

The tool is a collection of simple programs that I frequently need on all platforms.  It fills in the gaps on bad Operating Systems, and provides useful functions for all Operating Systems.

So far:

* a simple find tool
* find programs listening on ports
* kill programs listening on ports
* ipv6 to ipv4 port relay
* watch a program and diff the output

## Examples

    tool --image-check c:\photos

Recursively searches c:\photos for images and checks them for corruption.  Has a limited set of image formats it can check, but it's better than nothing.


    tool --find.root c:\files --find.name config.* 

Searches c:\files for any file starting with config.


    tool --find.root c:\files --find.name cache --find.type d

Finds directories called cache.  It has limit options compared to dedicated find programs, but it's helpful on a system that doesn't have one.


    tool --ports

Lists all listening ports, and the programs that are listening on them


    tool --port 1234

Finds the program listening on port 1234


    tool --killport 1234

Find and kill the program running on port 1234


    tool --ipv6relay.start-port 2000 --ipv6relay.end-port 2001 --ipv6relay.ipv4-target 12.34.56

Listens on IPv6 ports and relays to the corresponding ipv4 ports.  Handy for old programs that don't know about ipv6.


    tool --watch ls -la

Runs ls every 2 seconds and diffs the output


    tool --watch --watch.interval 5 ls -la

Runs ls every 5 seconds and diffs the output

