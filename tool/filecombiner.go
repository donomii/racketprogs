package main

// Recurses through directories and concatenates all the text files it finds.  Adds the path before each file.

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func xmlEscape(s string) string {
	s = strings.ReplaceAll(s, "&", "&amp;")
	s = strings.ReplaceAll(s, "<", "&lt;")
	s = strings.ReplaceAll(s, ">", "&gt;")
	return s
}

func combineFiles(rootDir string, outputPath string) error {
	var out io.Writer
	if outputPath != "" {
		f, err := os.Create(outputPath)
		if err != nil {
			return fmt.Errorf("error creating output file: %v", err)
		}
		defer f.Close()
		out = f
	} else {
		out = os.Stdout
	}

	fmt.Fprintln(out, "<documents>")
	index := 1

	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() {
			return err
		}

		content, err := os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("error reading file %s: %v", path, err)
		}

		if isBinary(content) {
			return nil
		}

		relPath, err := filepath.Rel(rootDir, path)
		if err != nil {
			return fmt.Errorf("error getting relative path for %s: %v", path, err)
		}

		fmt.Fprintf(out, "  <document index=\"%d\">\n", index)
		fmt.Fprintf(out, "    <source>%s</source>\n", xmlEscape(relPath))
		fmt.Fprintf(out, "    <document_content>%s</document_content>\n", xmlEscape(string(content)))
		fmt.Fprintln(out, "  </document>")
		index++

		return nil
	})

	if err != nil {
		return fmt.Errorf("error walking directory: %v", err)
	}

	fmt.Fprintln(out, "</documents>")
	return nil
}

func isBinary(content []byte) bool {
	contentType := http.DetectContentType(content)
	if !strings.HasPrefix(contentType, "text/") {
		return true
	}

	nonPrintable := 0
	for i := 0; i < len(content) && i < 512; i++ {
		if content[i] == 0 {
			return true
		}
		if content[i] < 32 && content[i] != 9 && content[i] != 10 && content[i] != 13 {
			nonPrintable++
		}
	}

	return nonPrintable > 1
}
