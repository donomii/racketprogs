package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"sync"
	"time"
)

type Node struct {
	IP       string    `json:"ip"`
	Port     int       `json:"port"`
	LastSeen time.Time `json:"last_seen"`
	Metadata string    `json:"metadata,omitempty"`
}

var (
	nodes      = make(map[string]*Node)
	nodesMutex sync.RWMutex
)

func autoNodeLocator(networkToScan string, scanPorts []int, listenPort int) {
	addOwnIPToDatabase(listenPort)
	go listenForNodes(listenPort)
	if networkToScan != "" {
		go scanSpecificNetwork(networkToScan, scanPorts, listenPort)
	} else {
		go scanNetworks(scanPorts, listenPort)
	}

	select {} // Keep the function running
}

func getNetworks() []string {
	var networks []string
	interfaces, err := net.Interfaces()
	if err != nil {
		logVerbose("Error getting network interfaces: %v\n", err)
		return networks
	}

	for _, iface := range interfaces {
		addrs, err := iface.Addrs()
		if err != nil {
			logVerbose("Error getting addresses for interface %s: %v\n", iface.Name, err)
			continue
		}

		for _, addr := range addrs {
			if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
				if ipnet.IP.To4() != nil {
					networks = append(networks, ipnet.String())
				}
			}
		}
	}

	return networks
}

func addOwnIP(listenPort int) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		logVerbose("Error getting own IP addresses: %v\n", err)
		return
	}

	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				node := &Node{IP: ipnet.IP.String(), Port: listenPort, LastSeen: time.Now(), Metadata: "self"}
				updateNode(node)
			}
		}
	}
}

func addOwnIPToDatabase(listenPort int) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		logVerbose("Error getting own IP addresses: %v\n", err)
		return
	}

	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil || ipnet.IP.To16() != nil {
				node := &Node{IP: ipnet.IP.String(), Port: listenPort, LastSeen: time.Now(), Metadata: "self"}
				updateNode(node)
			}
		}
	}
}

func scanNetwork(network string, scanPorts []int, listenPort int) {
	if network == "" {
		logVerbose("No network specified for scanning\n")
		return
	}

	ip, ipnet, err := net.ParseCIDR(network)
	if err != nil {
		logVerbose("Error parsing network %s: %v\n", network, err)
		return
	}

	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); incrementIP(ip) {
		go tryConnect(ip.String(), scanPorts, listenPort)
		time.Sleep(500 * time.Millisecond)
	}
}

func incrementIP(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func listenForNodes(listenPort int) {
    listener, err := net.Listen("tcp", fmt.Sprintf(":%d", listenPort))
    if err != nil {
        fmt.Printf("Error listening on port %d: %v\n", listenPort, err)
        return
    }
    defer listener.Close()

    for {
        conn, err := listener.Accept()
        if err != nil {
            logVerbose("Error accepting connection: %v\n", err)
            continue
        }
        go autoNode_handleConnection(conn)
    }
}

func autoNode_handleConnection(conn net.Conn) {
    defer conn.Close()

    // Send our nodes
    nodesMutex.RLock()
    for _, node := range nodes {
        json.NewEncoder(conn).Encode(node)
    }
    nodesMutex.RUnlock()

    // Receive their nodes
    scanner := bufio.NewScanner(conn)
    for scanner.Scan() {
        var node Node
        if err := json.Unmarshal(scanner.Bytes(), &node); err != nil {
            logVerbose("Error decoding node: %v\n", err)
            continue
        }
        if isNewNode := updateNode(&node); isNewNode {
            fmt.Printf("%s %d\n", node.IP, node.Port)
        }
    }
}


func logVerbose(format string, v ...interface{}) {
	if *verboseFlag {
		fmt.Fprintf(os.Stderr, format, v...)
	}
}

func updateNode(node *Node) bool {
	nodesMutex.Lock()
	defer nodesMutex.Unlock()

	key := fmt.Sprintf("%s %d", node.IP, node.Port)
	if existingNode, ok := nodes[key]; ok {
		if node.LastSeen.After(existingNode.LastSeen) {
			nodes[key] = node
			logVerbose("Updated node: %s\n", key)
		}
		return false
	} else {
		nodes[key] = node
		fmt.Printf("%s %d\n", node.IP, node.Port)
		return true
	}
}

func getLocalIPs() []string {
	var ips []string
	interfaces, err := net.Interfaces()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting network interfaces: %v\n", err)
		return ips
	}

	for _, iface := range interfaces {
		addrs, err := iface.Addrs()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error getting addresses for interface %s: %v\n", iface.Name, err)
			continue
		}

		for _, addr := range addrs {
			switch v := addr.(type) {
			case *net.IPNet:
				if !v.IP.IsLoopback() {
					if v.IP.To4() != nil || v.IP.To16() != nil {
						ips = append(ips, v.IP.String())
					}
				}
			case *net.IPAddr:
				if !v.IP.IsLoopback() {
					if v.IP.To4() != nil || v.IP.To16() != nil {
						ips = append(ips, v.IP.String())
					}
				}
			}
		}
	}

	return ips
}

func tryConnect(ip string, ports []int, listenPort int) {
    for _, port := range ports {
        address := fmt.Sprintf("%s:%d", ip, port)
        logVerbose("Attempting to connect to %s\n", address)
        conn, err := net.DialTimeout("tcp", address, time.Second)
        if err != nil {
            logVerbose("Failed to connect to %s: %v\n", address, err)
            continue
        }
        defer conn.Close()

        node := &Node{IP: ip, Port: port, LastSeen: time.Now()}
        if isNewNode := updateNode(node); isNewNode {
            fmt.Printf("%s %d\n", ip, port)
        }

        if port == listenPort {
            autoNode_handleConnection(conn)
        } else {
            // For other ports, we might want to send a request and check the response
            // This is just a simple example
            fmt.Fprintf(conn, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", ip)
            node.Metadata = "HTTP server"
            updateNode(node)
        }
        break // If we successfully connect, no need to try the other ports
    }
}

func scanSpecificNetwork(network string, scanPorts []int, listenPort int) {
	for {
		addOwnIPToDatabase(listenPort)
		scanNetwork(network, scanPorts, listenPort)
		time.Sleep(30 * time.Second)
	}
}

func scanNetworks(scanPorts []int, listenPort int) {
	for {
		addOwnIPToDatabase(listenPort)
		networks := getNetworks()
		for _, network := range networks {
			scanNetwork(network, scanPorts, listenPort)
		}
		time.Sleep(30 * time.Second)
	}
}
