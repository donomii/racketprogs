package main

import (
	"io"
	"log"
	"net"
)

func portrelay(listenPort, targetIP, targetPort string) {
	listen, err := net.Listen("tcp", ":"+listenPort)
	if err != nil {
		log.Fatal(err)
	}
	defer listen.Close()

	log.Printf("Listening on port %s...", listenPort)
	for {
		clientConn, err := listen.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go handlePortRelayConnection(clientConn, targetIP, targetPort)
	}
}

func handlePortRelayConnection(clientConn net.Conn, targetIP string, targetPort string) {
	defer clientConn.Close()

	targetConn, err := net.Dial("tcp", net.JoinHostPort(targetIP, targetPort))
	if err != nil {
		log.Print(err)
		return
	}
	defer targetConn.Close()

	go io.Copy(targetConn, clientConn)
	io.Copy(clientConn, targetConn)
}
