package main

import (
	"crypto/rand"
	"embed"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//go:embed README.md
var readmeFS embed.FS

var verboseFlag = flag.Bool("verbose", false, "Enable verbose output")

func generateRandomPassword(length int) (string, error) {
	bytes := make([]byte, length)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(bytes), nil
}

func basicAuth(next http.Handler, username, password string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok || user != username || pass != password {
			w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
			http.Error(w, "Unauthorized.", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func serveHTTP(dir string, listenIP string, port int, username, password string) {
	fs := http.FileServer(http.Dir(dir))
	if username != "" && password != "" {
		fs = basicAuth(fs, username, password)
	}
	http.Handle("/", fs)

	listenAddr := net.JoinHostPort(listenIP, strconv.Itoa(port))
	log.Printf("Listening on %s...\n", listenAddr)
	log.Fatal(http.ListenAndServe(listenAddr, nil))
}

func printHelp() {
	readmeFile, err := readmeFS.Open("README.md")
	if err != nil {
		log.Fatalf("Error opening README.md: %v", err)
	}
	defer readmeFile.Close()

	stat, err := readmeFile.Stat()
	if err != nil {
		log.Fatalf("Error getting README.md file info: %v", err)
	}

	data := make([]byte, stat.Size())
	_, err = readmeFile.Read(data)
	if err != nil {
		log.Fatalf("Error reading README.md: %v", err)
	}

	print(string(data))

	println("\nCommand-line options:")
	flag.PrintDefaults()
}

func main() {
	help := flag.Bool("help", false, "display help information")


	var (
		autoNodeLocatorFlag = flag.Bool("auto-node-locator", false, "run automatic network node locator and relay")
		scanNetworkFlag     = flag.String("scan-network", "", "Network to scan in CIDR notation (e.g., 192.168.1.0/24)")
		scanPortsFlag       = flag.String("scan-ports", "11111", "Comma-separated list of ports to scan")
		listenPort = flag.Int("listen-port", 11111, "Port to listen on for incoming connections")

	)

	networkInfoFlag := flag.Bool("network-info", false, "Get basic network details")

	httpDir := flag.String("http.dir", "", "directory to serve over HTTP")
	httpListenIP := flag.String("http.listen", "", "IP address to listen on for HTTP")
	httpPort := flag.Int("http.port", 8080, "port to listen on for HTTP")
	httpUsername := flag.String("http.username", "", "username for HTTP basic auth")
	httpPassword := flag.String("http.password", "", "password for HTTP basic auth")

	findRoot := flag.String("find.root", "", "root directory to start find")
	findName := flag.String("find.name", "", "file name pattern to match")
	findType := flag.String("find.type", "", "file type to match (d=directory, f=file, l=symlink)")
	findMinSize := flag.Int64("find.min-size", 0, "minimum file size to match in bytes")
	findMaxSize := flag.Int64("find.max-size", 0, "maximum file size to match in bytes")
	relaylistenPort := flag.String("relay.listen", "", "Port to listen on")
	relaytargetIP := flag.String("relay.target-ip", "127.0.0.1", "Target IP address")
	relaytargetPort := flag.String("relay.target-port", "9000", "Target port")
	metadatadiff_flag := flag.Bool("metadatadiff", false, "compare two directories and print the differences in their file metadata")
	metadatadiff_skiptimecheck := flag.Bool("metadatadiff.skip-time-check", false, "skip checking the file modification time")

	portSearch := flag.Int("port", 0, "port to find the listening process")
	ports := flag.Bool("ports", false, "find all listening ports")
	killport := flag.Int("killport", 0, "kill the process listening on a port")

	ipv6minPort := flag.Int("ipv6relay.start-port", 0, "minimum port to listen on for IPv6")
	ipv6maxPort := flag.Int("ipv6relay.end-port", 0, "maximum port to listen on for IPv6")
	ipv4TargetAddress := flag.String("ipv6relay.ipv4-target", "", "IPv4 address to relay to")

	imageCheckDir := flag.String("image-check", "", "Directory to scan for images")

	watch := flag.Bool("watch", false, "run a command every interval seconds")
	watchInterval := flag.Int("watch.interval", 2, "interval in seconds to run the command")

	httpLoggerPort := flag.Int("http-logger", 0, "Start HTTP logger on specified port")


	combineSourceDir := flag.String( "combine-files", "", "source directory to combine files from")
	combineOutput := flag.String( "combine.output", "", "output file path for combined files")




	flag.Parse()

	if *help {
		printHelp()
		return
	}
	if *networkInfoFlag {
		networkInfo()
		return
	}
	if *httpDir != "" {
		password := *httpPassword
		if *httpPassword == "" {
			var err error
			password, err = generateRandomPassword(12)
			if err != nil {
				log.Fatalf("Error generating random password: %v", err)
			}
			fmt.Printf("Generated password: %s\n", password)
		}
		serveHTTP(*httpDir, *httpListenIP, *httpPort, *httpUsername, password)
		return
	}

	systemsActioned := 0
	systems := ""
	if *autoNodeLocatorFlag {
		systemsActioned++
		systems += "auto-node-locator "
	}
	if *networkInfoFlag {
		systemsActioned++
		systems += "network-info "
	}
	if *httpDir != "" {
		systemsActioned++
		systems += "http "
	}
	if *findRoot != "" {
		systemsActioned++
		systems += "find "
	}
	if *portSearch != 0 {
		systemsActioned++
		systems += "port "
	}
	if *ports {
		systemsActioned++
		systems += "ports "
	}
	if *ipv6minPort != 0 {
		systemsActioned++
		systems += "ipv6relay "
	}
	if *killport != 0 {
		systemsActioned++
		systems += "killport "
	}
	if *watch {
		systemsActioned++
		systems += "watch "
	}
	if *relaylistenPort != "" {
		systemsActioned++
		fmt.Printf("relaylistenPort: '%s'\n", *relaylistenPort)
		systems += "relay "
	}
	if *imageCheckDir != "" {
		systemsActioned++
		systems += "image-check "
	}
	if *metadatadiff_flag {
		systemsActioned++
		systems += "metadatadiff "
	}

	if *httpLoggerPort != 0 {
		systemsActioned++
		systems += "http-logger "
	}



	if systemsActioned > 1 {
		log.Fatalf("Only one subsystem can be requested at a time. You requested: %s", systems)
	}

	if *httpLoggerPort != 0 {
		listenAndLogHTTP(*httpLoggerPort)
	} else if *findRoot != "" {
		findFiles(*findRoot, *findName, *findType, *findMinSize, *findMaxSize)
	} else if *portSearch != 0 {
		FindListeningProcess(*portSearch)
	} else if *ports {
		FindAllListeningPorts()
	} else if *ipv6minPort != 0 {
		ipv6relay(*ipv6minPort, *ipv6maxPort, *ipv4TargetAddress)
	} else if *killport != 0 {
		KillProcessListeningOnPort(*killport)
	} else if *watch {
		watchCommand(time.Duration(*watchInterval)*time.Second, flag.Args())
	} else if *relaylistenPort != "" {
		portrelay(*relaylistenPort, *relaytargetIP, *relaytargetPort)
	} else if *autoNodeLocatorFlag {
		scanPorts := []int{*listenPort}
		if *scanPortsFlag != "" {
			for _, portStr := range strings.Split(*scanPortsFlag, ",") {
				if port, err := strconv.Atoi(portStr); err == nil {
					if port != *listenPort {
						scanPorts = append(scanPorts, port)
					}
				}
			}
		}
		autoNodeLocator(*scanNetworkFlag, scanPorts, *listenPort)
	} else if *imageCheckDir != "" {
		imageCheck(*imageCheckDir)
	} else if *metadatadiff_flag {
		if flag.NArg() != 2 {
			flag.Usage()
			return
		}
		dir1 := flag.Arg(0)
		dir2 := flag.Arg(1)
		metadatadiff(dir1, dir2, *metadatadiff_skiptimecheck)
	} else if *combineSourceDir != "" {
		if err := combineFiles(*combineSourceDir, *combineOutput); err != nil {
			log.Fatalf("Error combining files: %v", err)
		}
		return
	} else {
		flag.Usage()
	}
}
