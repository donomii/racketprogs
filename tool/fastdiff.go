package main

import (

	"fmt"
	"os"
	"path/filepath"

)

// readDir reads the directory and returns a map of file names and their metadata.
func readDir(dir string, skipTime bool) (map[string]os.FileInfo, error) {
	files := make(map[string]os.FileInfo)
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		relPath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}
		files[relPath] = info
		return nil
	})
	return files, err
}

// metadatadiff compares two directories and prints the differences in their file metadata.
func metadatadiff(dir1, dir2 string, skipTime bool) {
	files1, err := readDir(dir1, skipTime)
	if err != nil {
		fmt.Println("Error reading directory 1:", err)
		return
	}

	files2, err := readDir(dir2, skipTime)
	if err != nil {
		fmt.Println("Error reading directory 2:", err)
		return
	}

	for file, info1 := range files1 {
		info2, exists := files2[file]
		if !exists {
			fmt.Println(file, "exists in", dir1, "but not in", dir2)
			continue
		}
		if info1.Size() != info2.Size() {
			fmt.Println("Size difference in file:", file, "(", info1.Size(), "vs", info2.Size(), ")")
		}
		if !skipTime && !info1.ModTime().Equal(info2.ModTime()) {
			fmt.Println("Time difference in file:", file, "(", info1.ModTime(), "vs", info2.ModTime(), ")")
		}
		delete(files2, file)
	}

	for file := range files2 {
		fmt.Println(file, "exists in", dir2, "but not in", dir1)
	}
}