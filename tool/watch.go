package main

import (
	"os/exec"
	"fmt"
	"time"

	"github.com/sergi/go-diff/diffmatchpatch"
	"github.com/fatih/color"
)

func watchCommand(interval time.Duration, cmdArgs []string) {
	var prevOut string
	dmp := diffmatchpatch.New()

	for {
		cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			color.New(color.FgRed).Println(err)
			continue
		}

		if prevOut != "" {
			diffs := dmp.DiffMain(prevOut, string(out), true)

			for _, diff := range diffs {
				switch diff.Type {
				case diffmatchpatch.DiffInsert:
					color.New(color.FgGreen).Printf("%v", diff.Text)
				case diffmatchpatch.DiffEqual:
					fmt.Printf("%v",diff.Text)
				}
			}
			fmt.Println()
		} else {
			fmt.Println(string(out))
		}

		prevOut = string(out)
		time.Sleep(interval)
	}
}
