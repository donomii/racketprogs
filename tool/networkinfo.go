package main

import (
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

// networkInfo retrieves the local subnet devices, performs IPv4 and IPv6 scans, and prints detailed results.
func networkInfo() {
	fmt.Println("Getting network information...")

	// List network interfaces and perform IPv4 and IPv6 scanning
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatalf("Error getting network interfaces: %v", err)
	}

	for _, iface := range ifaces {
		fmt.Printf("\nInterface Name: %s\n", iface.Name)
		addrs, err := iface.Addrs()
		if err != nil {
			log.Printf("Error getting addresses for interface %s: %v", iface.Name, err)
			continue
		}

		for _, addr := range addrs {
			switch v := addr.(type) {
			case *net.IPNet:
				if v.IP.To4() != nil {
					fmt.Printf("  IPv4 Address: %s\n", v.IP.String())
					fmt.Printf("  Subnet Mask: %s\n", net.IP(v.Mask).String())
					network := v.IP.Mask(v.Mask)
					fmt.Printf("  Network Address: %s\n", network.String())
					scanIPv4Subnet(v.IP, v.Mask)
				} else if v.IP.To16() != nil {
					fmt.Printf("  IPv6 Address: %s\n", v.IP.String())
					if strings.HasPrefix(v.IP.String(), "fe80::") {
						// IPv6 link-local address, use it for neighbor discovery
						fmt.Println("  Performing IPv6 Neighbor Discovery...")
						performIPv6NeighborDiscovery(iface.Name)
					}
				}
			}
		}
	}

	fmt.Println("\nPerforming IPv6 Router Discovery...")
	performIPv6RouterDiscovery()
}

// scanIPv4Subnet scans the given IPv4 subnet for active devices.
func scanIPv4Subnet(ip net.IP, mask net.IPMask) {
	network := ip.Mask(mask)
	fmt.Printf("\nScanning IPv4 subnet: %s\n", network.String())
	for i := 1; i <= 254; i++ {
		go func(i int) {
			addr := fmt.Sprintf("%s.%d", network.String()[:strings.LastIndex(network.String(), ".")], i)
			conn, err := net.DialTimeout("tcp", addr+":80", time.Second)
			if err == nil {
				fmt.Printf("  Active device found at: %s\n", addr)
				conn.Close()
			}
		}(i)
	}
	time.Sleep(5 * time.Second) // Wait for goroutines to finish
}

// performIPv6NeighborDiscovery performs a simple neighbor discovery by attempting to connect to common addresses.
func performIPv6NeighborDiscovery(interfaceName string) {
	// Define a list of common link-local multicast addresses for neighbor discovery
	neighborAddrs := []string{
		"ff02::1", // All nodes multicast address
		"ff02::2", // All routers multicast address
	}

	for _, addr := range neighborAddrs {
		dialer := &net.Dialer{
			Timeout: time.Second,
			// Set the interface for link-local addresses
			LocalAddr: &net.UDPAddr{
				IP:   net.ParseIP("::"), // Use unspecified address "::" to allow the OS to select the interface
				Zone: interfaceName,     // Specify the network interface
			},
		}

		conn, err := dialer.Dial("tcp", fmt.Sprintf("[%s]:80", addr))
		if err == nil {
			fmt.Printf("  Neighbor reachable at: %s\n", addr)
			conn.Close()
		} else {
			fmt.Printf("  No response from: %s, Error: %v\n", addr, err)
		}
	}
}

// performIPv6RouterDiscovery performs a basic check for IPv6 routers on the network.
func performIPv6RouterDiscovery() {
	routerAddrs := []string{
		"ff02::2", // All routers multicast address
	}

	for _, addr := range routerAddrs {
		dialer := &net.Dialer{
			Timeout: time.Second,
			// Use the unspecified address "::" without a specific interface for global router discovery
			LocalAddr: &net.UDPAddr{
				IP: net.ParseIP("::"),
			},
		}

		conn, err := dialer.Dial("tcp", fmt.Sprintf("[%s]:80", addr))
		if err == nil {
			fmt.Printf("  Router reachable at: %s\n", addr)
			conn.Close()
		} else {
			fmt.Printf("  No response from: %s, Error: %v\n", addr, err)
		}
	}
}
