//Accept connections on IPv6 and relay them to IPv4
package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"sync"
)


func ipv6relay(minPort, maxPort int, ipv4TargetAddress string) {
	wg := sync.WaitGroup{}

	for port := minPort; port <= maxPort; port++ {
		wg.Add(1)
		go func(port int) {
			defer wg.Done()
			ipv6ListenAddress := fmt.Sprintf("[::]:%d", port)
			listener, err := net.Listen("tcp6", ipv6ListenAddress)
			if err != nil {
				log.Printf("Error listening on %s: %v", ipv6ListenAddress, err)
				return
			}
			defer listener.Close()
			log.Printf("Listening on %s", ipv6ListenAddress)

			for {
				conn, err := listener.Accept()
				if err != nil {
					log.Printf("Error accepting connection: %v", err)
					continue
				}

				go handleConnection(conn, ipv4TargetAddress, port)
			}
		}(port)
	}

	wg.Wait()
}

func handleConnection(src net.Conn, ipv4TargetAddress string, port int) {
	defer src.Close()

	dst, err := net.Dial("tcp4", fmt.Sprintf("%s:%d", ipv4TargetAddress, port))
	if err != nil {
		log.Printf("Error dialing IPv4 target: %v", err)
		return
	}
	defer dst.Close()

	errChan := make(chan error, 1)

	go func() {
		_, err := io.Copy(src, dst)
		errChan <- err
	}()

	go func() {
		_, err := io.Copy(dst, src)
		errChan <- err
	}()

	<-errChan
}
