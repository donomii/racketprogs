#!/bin/xsh

puts Building assorted programs...

set GO111MODULE auto
set BUILDDIR [joinArgs "-" "build/funzone" [trimRight [uname]] [trimRight [uname -m]] "/"]
puts mkdir -p $BUILDDIR
mkdir -p $BUILDDIR
go get "github.com/donomii/glim"  "github.com/donomii/sceneCamera" "github.com/go-gl/gl/v3.2-core/gl"  "github.com/go-gl/glfw/v3.3/glfw"  "github.com/go-gl/mathgl/mgl32" "github.com/atotto/clipboard"

puts Builddir is $BUILDDIR

mkdir build

say Building racket progs


say Building d.c.b.h.
cd dcbh
go build .
cp dcbh [joinArgs "/" ".." $BUILDDIR ""]
cp -r assets [joinArgs "/" ".." $BUILDDIR ""]
cd ..



say Building try grammer
cd trigrammr
go get github.com/donomii/trigrammr github.com/chzyer/readline
go build cmd/trigrammr-import-csv/trigrammr-import-csv.go
go build cmd/trigrammr-client/trigrammr-client.go
go build cmd/trigrammr-import-book/trigrammr-import-book.go 
cp trigrammr-import-book trigrammr-client trigrammr-import-csv [joinArgs "/" ".." $BUILDDIR ""]
cd ..

say Building boxes
cd boxes
go build -o boxes-sdl -tags sdl .
go build -o boxes .
cp boxes [joinArgs "/" ".." $BUILDDIR ""]
cp boxes-sdl [joinArgs "/" ".." $BUILDDIR ""]
cp -r assets [joinArgs "/" ".." $BUILDDIR ""]
cd ..

say Building watcher
cd watcher
go build . 
cp watcher [joinArgs "/" ".." $BUILDDIR ""]
cd ..

cd mdnscan
go build .
cp mdnscan [joinArgs "/" ".." $BUILDDIR ""]
cd ..

cd myvox
go build . 
cp myvox [joinArgs "/" ".." $BUILDDIR ""]
cd ..

cd xsh
go get -v .
go get -v github.com/donomii/termbox-go github.com/mitchellh/go-homedir github.com/radovskyb/watcher github.com/chzyer/readline github.com/creack/pty github.com/nsf/termbox-go
go build ./cmd/xsh
go build ./cmd/xshguardian
go build -o xshwatch ../watcher
go build -buildmode c-shared -o libxsh.so ./cmd/libxsh/*.go
gcc -L. -lxsh -o testxshCbridge ./cmd/libxshtest/test.c
cp xsh xshwatch  xshguardian [joinArgs "/" ".." $BUILDDIR ""]
cd ..

rm -r funzone
mv build funzone

mkdir dist
cp -r funzone dist/

puts Your files are in dist/funzone
exit 0
