
package main

import (
	"fmt"
	"gitlab.com/donomii/racketprogs/autoparser"
	"gitlab.com/donomii/racketprogs/xsh"
	"strconv"
	"time"

	. "gitlab.com/donomii/racketprogs/pmoolib"
)

func xshBuiltins(s xsh.State, commandNodes []autoparser.Node, parent *autoparser.Node, level int, timeout int) (autoparser.Node, bool) {
	player := s.UserData.(string)
	if len(commandNodes) > 0 {
		command, err := xsh.ListToStrings(commandNodes)
		if err != nil {
			// log.Println("Error converting command to string:", err)
		} else {
			// fmt.Printf("Running custom handler for command %v\n", c)
			switch command[0] {
			case "help":
				if len(command) == 1 {
					command = []string{"help", "all"}
				}
				fmt.Println("Running help command:", command)
				switch command[1] {
				case "cluster":
					return xsh.N(`
PMOO  - Personal MUD Object Oriented

*** WARNING Cluster mode has no security! ***

Running in cluster mode allows any user to potentially delete your computer!  Do not run cluster mode
unless you are sure you have complete network security.

PMOO has a cluster mode that allows multiple computers to run the same PMOO.  This mostly works, except
that there is no protection against concurrent updates to an object overwriting each other.  This may
be solved by only allowing an object to update itself, allowing it to enforce consistency for itself.

Setting up the cluster is covered elsewhere.  The important thing to know inside the MOO is 'affinity'.

Affinity is a string that identifies the node that an object is limited to.  This is used to determine which nodes
will actually run the code for an object.  For example, the player object has an affinity set to the player's
computer.  When someone sends a 'tell player that ...' message, the tell verb will only run on the player's
computer, displaying a message on the player's screen.  If there was no affinity, the tell verb would run
on any node that picked it off the queue.  The message would appear on a random screen in the cluster.

The affinity is set by the 'become' verb for players, otherwise the normal 'setprop' command works.

setprop shell affinity "Node A"

The affinity of a node is set on the command line (or in the config file) when the node is started.  There is 
currently no way to list all the nodes in the cluster.
					`), true
				case "scripting":
					return xsh.N(`
PMOO - Personal MUD Object Oriented 

PMOO is a 'live object' system.  You create objects and verbs, and then use them.  Objects have inheritence through their 'parent'
property.  All commands are executed in parallel.  You cannot rely on commands running in the same order that you give them, as one
might be held up, allowing the next one to run first.

Pmoo is a relatively complex system to program in.  There are multiple levels to program in:

- The user level, which uses pseudo-english to give commands
- The object level, where objects receive direct messages
- The verb level, where verbs are executed in one of many scripting languages

The following example follows a "snap photo" command, which uses the computer's camera to take a photo. 

When the user types 'snap photo', the system will break the sentence up to find the verb and direct object.  The verb
is 'snap', and the direct object is 'photo'.  The parser then searches for an object that can handle the 'snap' verb, 
generates a message, and sends it to that object.  If running in cluster mode, the message might be sent to another computer
before it is executed.

Messages are exchanged as JSON data.  They are added to a queue, and later on are removed and processed by a random node (or the 
affinity node, if set).  They might be delayed - even by a couple of seconds.  There is currently no way to tell if a message
succeeded or failed.  There is also no way to get the result value of a message.  To do that, the receiving object must create a new
message and send that back to the sender.  There is also no way to tell if this message succeeds.

The object receives the message, and then runs the verb.  The code might look something like:

    raspistill -o /tmp/snap.jpg
	setprop this data [contents /tmp/snap.jpg]

the first line is a system command that uses the raspistill program to take a photo.  The second line is a PMOO command that
sets the 'data' property of the current object (the camera) to the contents of the photo.  A better version would perhaps create
a new object called 'picture', and set the 'data' property of that picture object to the contents of the photo.

There are currently 3 options for writing code in PMOO:  

yaegi, a scripted version of go that allows most access to the underlying
PMOO engine.  Yaegi is unfortunately a bit unstable and incomplete.

sh, the original system shell.  sh has almost no access to PMOO.  It is handy for controlling the system, not so much for extracting data for processing it.

Xsh, a shell-like scripting language.  It is much more regular and safe than traditional POSIX shells.  It has access to most of PMOO.

PMOO is mostly scripted in the xsh language.  You can learn about xsh at: xxxx add url here.  Xsh has been extended with 
several commands to work with PMOO.  These commands are:

sleep 1000 - Sleep for 1000 milliseconds
become UUID - Become the player with the given UUID
setprop UUID PROP VALUE - Set the property PROP to VALUE on the object with the given UUID
getprop UUID PROP - Get the property PROP from the object with the given UUID
inroomobject NAME - Find the object with the given name, in the current room
clone UUID NAME - Clone the object with the given UUID and give it the given NAME and new UUID.
formatobject UUID - Format the object with the given UUID as a string
move UUID TO_UUID- Move the object with the given UUID into the object with the given TO_UUID
setverb UUID PROP "xsh" PROGRAM - Set the verb PROP to the given PROGRAM on the object with the given UUID.
msg FROM_UUID TO_UUID VERB OBJ PREP IOBJ - Send a message from FROM_UUID to TO_UUID.
o NAME - Get the object with the given name

All verb scripts are started with some standard variables available:
	here - The location that the caller is located
	playerid - The player id of the player who sent the message
	this - The object id of the object that the verb is being called on
	verb - The verb that is being called
	dobj - The direct object of the verb
	dpropstr - The direct property of the dobj
	prepstr - The preposition of the verb
	iobj - The indirect object of the verb
	ipropstr - The indirect property of the iobj
	dobjstr - The direct object of the verb as a string
	iobjstr - The indirect object of the verb as a string
	trace - The trace of the message
	args - The arguments of the message, parsed as a posix shell command
	ticks - The number of ticks that have passed since the message was sent


You can run scripts directly by typing 'x command', e.g.

x puts hello world

or create your own verb with

x setverb 4 help xsh "msg playerid playerid \"tell\" [help dobj] 0 0"

Note that you do not write %4 for the object UUID in xsh, just use the number directly.
					`), true
				case "moo":
					return xsh.N(`
PMOO - Personal MUD Object Oriented Programming

MOOs are an online, multiuser version of old text adventure games like Zork or Adventure.  MOOs were briefly
popular in the 90s, but have since been replaced by more modern games that are actually fun.  You can still 
find the original MOOs online, the wikipedia page is a good place to start: https://en.wikipedia.org/wiki/LambdaMOO

PMOO is an experiment to use MOOs beyond their original purpose, and has ended up being a kind of virtual world
command shell.  It is still possible to use it as a multiplayer environment, but it lacks any of the security
features that would make that possible.  In particular, it does not prevent players from formatting the 
computer if they want to!

Players can move around the MOO using the traditional commands:
> go east
> take box
> open door
> say hello to Bob
> say "What do you want to do?" to Bob

There are some shortcut words for common objects:

> examine me
> examine here

If you want to use the UUID of an object directly, put a % in front of it:

> examine %1

Writing your own commands is encouraged.  The scripting language is XSH, you can learn more with 'help scripting'.
					`), true
				case "commands":
					return xsh.N(`
PMOO - Personal MUD Object Oriented

The following commands are available in PMOO:
help - Get help on a topic
sleep 1000 - Sleep for 1000 milliseconds
become UUID - Become the player with the given UUID
setprop UUID PROP VALUE - Set the property PROP to VALUE on the object with the given UUID
getprop UUID PROP - Get the property PROP from the object with the given UUID
inroomobject NAME - Find the object with the given name, in the current room
clone UUID NAME - Clone the object with the given UUID and give it the given NAME and new UUID.
formatobject UUID - Format the object with the given UUID as a string
move UUID TO_UUID- Move the object with the given UUID into the object with the given TO_UUID
setverb UUID PROP "xsh" PROGRAM - Set the verb PROP to the given PROGRAM on the object with the given UUID.
msg FROM_UUID TO_UUID VERB OBJ PREP IOBJ - Send a message from FROM_UUID to TO_UUID.
o NAME - Get the object with the given name
visibleobjects - Get a list of all visible objects
searchprop PROP - Get a list of all objects with the given property
search PROP VALUE - Get a list of all objects with the given property set to the given value
listverbs UUID - Get a list of all verbs that the player can use
allverbs UUID - Get a list of all verbs that the player can use
`), true
case "quickstart":
					return xsh.N(`
PMOO - Personal MUD Object Oriented

PMOO tries to work like old text mode games and MOOs.  You can move around, take objects, and act on them.

The following commands are available:

look - look around the room
examine something - examine an object
examine %1 - examine an object by ID.  The first 10 objects are predefined and get a number from 0-10.  Later objects get a UUID.
go direction - move in a direction.  Directions, or exits, are created with dig.
take object - take an object
drop object - drop an object
move object to object - move an object into another object
clone object - clone an object

All user commands are in the form of 'verb direct-object preposition indirect-object'.  The parser will try to find an object that can handle the verb, and send a message to it.  The object will then run the verb, and return a result.

PMOO does not try, at all, to enforce rules that make sense.  You can pick up a room and put it in your pocket, or put a table in a book.  It is up to the object to decide what to do with the message it receives.
`), true

				default:
					return xsh.N(`
PMOO - Personal MUD Object Oriented

PMOO is a Personal MOO.  Rather than duplicating the original MOOs, it builds on their ideas and combines it with 
modern concepts, like clusters, or running on your laptop.  It is not sandboxed off from your computer, so you can 
create MOO objects that manipulate your computer.

A quick feature list:
- MOO objects are saved to files and be can checked into git.  Your entire MOO can be branched and reverted.
- No sandbox.  You can create MOO objects that manipulate your computer, using the built in scripting language
- Cluster operation.  Multiple computers can run the MOO at the same time.
- Disconnected operation.  MOO objects now have UUIDs, so disconnected operation is possible.
- Multiple scripting languages.  Adding more is possible, if not easy.

Additional help is available for:

quickstart - a quick start guide
moo - what is a MOO?  how to use it?
scripting - writing commands to control the MOO
cluster - cluster mode
commands - what you can do
					`), true
				}

			case "sleep":
				// sleep for x milliseconds
				duration, _ := strconv.ParseInt(command[1], 10, 64)
				time.Sleep(time.Duration(duration) * time.Millisecond)
				return xsh.Bool(true), true
			case "become":
				// become playerid affinity - become the given playerid (objectid) on the given affinity.  Set affinity = "" for no affinity
				return xsh.Bool(become(command[1], command[2])), become(command[1], command[2])
			case "setprop":
				// setprop object_uuid prop value - sets the property of an object
				SetProp(command[1], command[2], command[3])
				return commandNodes[3], true
			case "inroomobject":
				// inroomobject room_name - shows all objects in room
				num := GetObjectByName(player, command[1], timeout)
				fmt.Println("Searched for object", command[1], "found", num, "objects")
				if num != "" {
					return xsh.N(num), true
				}
				return xsh.Void(commandNodes[0], "failed to find object"), true
			case "search":
				// search property_name property_value - SearchObjects returns a list of objects that have a property set to the given value
				objs := SearchObjects(command[1], command[2])
				return xsh.StringsToList(objs, commandNodes[0]), true
			case "searchprop":
				// searchprop property_name - SearchProp returns a list of objects that have the given property defined
				objs := SearchProp(command[1])
				return xsh.StringsToList(objs, commandNodes[0]), true
			case "getprop":
				// getprop object_uuid prop - gets the property of an object
				return xsh.N(GetProp(command[1], command[2], timeout)), true
			case "delprop":
				// delprop object_uuid prop - deletes the property of an object
				DelProp(command[1], command[2])
				return xsh.N(command[2]), true
			case "clone":
				// clone object_uuid name parent_uuid - clones an object
				return xsh.N(Clone(command[1], command[2])), true
			case "formatobject":
				// formatobject object_uuid - formats an object as a string
				fmt.Printf("Formatting object from args: %v\n", command)
				return xsh.N(FormatObject(command[1], timeout)), true
			case "move":
				// move object_uuid to_uuid - moves an object into another object
				return xsh.Bool(MoveObj(command[1], command[2], timeout)), true
			case "setverb":
				// setverb object_uuid prop interpreter program - sets a property of an object, and marks it as a verb
				obj := command[1]
				prop := command[2]
				interpreter := command[3]
				value := command[4]
				SetVerb(obj, prop, value, interpreter)
				return xsh.Void(commandNodes[0], "set verb"), true
			case "listverbs":
				// listverbs player_object_uuid - lists all verbs visible to player
				obj := command[1]
				verbs := VerbList(obj)
				fmt.Println("Found verbs:", verbs)
				return xsh.StringsToList(verbs, commandNodes[0]), true
			case "allverbs":
				// allverbs - lists all verbs that the player can use
				obj := command[1]
				verbs := AllVerbsList(obj)
				fmt.Println("Found verbs:", verbs)
				return xsh.StringsToList(verbs, commandNodes[0]), true
			case "msg":
				// msg from to verb dobj prep iobj - sends a message to an object.  This is a async method call.  It expects parts of a parsed sentence.
				from := command[1]
				target := command[2]
				verb := command[3]
				dobj := command[4]
				prep := command[5]
				iobj := command[6]

				thisObj := LoadObject(target)
				if thisObj == nil {
					fmt.Println("Failed to send message, target ", target, " not found")
					return xsh.Void(commandNodes[0], "can't find message target"), true
				}
				affProp, ok := thisObj.Properties["affinity"]
				affin := ""
				if ok {
					affin = affProp.Value
				}
				// log.Printf("From: %v, Target: %v, Verb: %v, Dobj: %v, Prep: %v, Iobj: %v\n", from.GetString(), target.GetString(), verb.GetString(), dobj.GetString(), prep.GetString(), iobj.GetString())

				if Cluster {
					// SendNetMessage(Message{From: from.GetString(), Player: player, This: target.GetString(), Verb: verb.GetString(), Dobj: dobj.GetString(), Prepstr: prep.GetString(), Iobj: iobj.GetString(), Trace: traceId})
					ClusterConn.MyQMessage(Message{From: from, Player: player, This: target, Verb: verb, Dobj: dobj, Prepstr: prep, Iobj: iobj, Trace: "FIXME", Affinity: affin, Ticks: DefaultTicks})
				} else {
					RawMsg(Message{From: player, Player: player, This: target, Verb: verb, Dobj: dobj, Prepstr: prep, Iobj: iobj, Trace: "FIXME", Ticks: DefaultTicks})
				}
				return xsh.Void(commandNodes[0], "send message"), true
			case "o":
				// o id - returns the object with the given name (not uuid)
				num := GetObjectByName(player, command[1], timeout)
				fmt.Println("Searched for object", command[1], "found", num)
				if num != "" {
					return xsh.N(num), true
				}
				return xsh.Void(commandNodes[0], "couldn't find object"), true
			case "visibleobjects":
				// visibleobjects - returns a list of all visible objects (i.e. in the same room)
				objs := VisibleObjects(LoadObject(player))
				return xsh.StringsToList(objs, commandNodes[0]), true

			}
		}
	}
	return autoparser.Node{}, false
}
