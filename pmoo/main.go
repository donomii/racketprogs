package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/donomii/racketprogs/autoparser"
	"gitlab.com/donomii/racketprogs/xsh"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"runtime/debug"
	"strings"
	"time"

	"github.com/chzyer/readline"
	"github.com/donomii/goof"

	//. "github.com/donomii/pmoo"

	"github.com/donomii/throfflib"
	"github.com/traefik/yaegi/interp"
	. "gitlab.com/donomii/racketprogs/pmoolib"
	pmoo "gitlab.com/donomii/racketprogs/pmoolib"
)

var (
	DefaultPlayerId   = "2"
	DefaultSystemCore = "7"
	ClusterConn       pmoo.RemoteStorage
	pmooVerbose       bool
)

func verbosef(format string, v ...interface{}) {
	if pmooVerbose {
		fmt.Printf(format, v...)
	}
}
func ConsoleInputHandler(queue chan *Message) {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("Enter command: ")
		text, err := reader.ReadString('\n')
		if err != nil {

			if batch {
				log.Println("Waiting for commands to finish...")
				time.Sleep(time.Millisecond * 5000)
			}
			if err == io.EOF {
				log.Println("Input complete")
				os.Exit(0)
			} else {
				log.Println("Readline error:", err)
				os.Exit(1)
			}
		}
		text = strings.TrimSuffix(text, "\r\n")
		text = strings.TrimSuffix(text, "\n")
		if text != "" {
			// fmt.Printf("Examining input: %s\n", text[:2])
			if text[:2] == "x " {
				verbosef("xsh program: %v\n", text[2:])
				fmt.Println(xshRun(text[2:], DefaultPlayerId))
			} else {
				verbosef("Sentence: %s\n", text)
				// Console is always the wizard, at least for now
				InputMsg(DefaultPlayerId, DefaultSystemCore, "input", text)
			}
		}
		time.Sleep(time.Millisecond * 2000)

	}
}

func listVerbs(player string) func(string) []string {
	return func(s string) []string {
		vs := VerbList(player)
		// fmt.Println("Found verbs:", vs)
		return vs
	}
}

func xshRun(text, player string) string {
	// fmt.Printf("Evalling xsh %v\n", text)
	xsh.WantDebug = false
	state := xsh.New()
	addPmooTypes(state)
	xsh.UsePterm = false
	state.UserData = player
	state.ExtraBuiltins = xshBuiltins
	// xsh.Eval(state, xsh.Stdlib_str, "stdlib")
	// res := xsh.Eval(state, text, "pmoo")
	// fmt.Printf("Result: %+v\n", res)
	// l := []autoparser.Node{res}
	// resstr := xsh.TreeToXsh(l)
	// fmt.Printf("Result: %+v\n", resstr)
	std := xsh.Parse(xsh.Stdlib_str, "stdlib")
	xsh.RunTree(state, std)
	xsh.WantDebug = pmooDebug
	xsh.WantTrace = pmooDebug
	xsh.WantErrors = pmooDebug
	xsh.WantWarn = pmooDebug

	tr := xsh.Parse(text, "pmoo")
	// fmt.Printf("Substituting pmoo vars\n")
	tr = subsitutePmooVars(tr, 1000)
	res := xsh.RunTree(state, tr)
	resstr := xsh.TreeToXsh(res)

	return resstr
}

func keys(m map[string][]string) []string {
	out := []string{}
	for k := range m {
		out = append(out, k)
	}
	return out
}

func listFuncs(xshEngine xsh.State) func(string) []string {
	return func(string) []string {
		return keys(xshEngine.TypeSigs)
	}
}

// The loop that executes user input
func ReadLineInputHandler(queue chan *Message) {
	xsh.WantDebug = false
	xshEngine := xsh.New()
	addPmooTypes(xshEngine)
	xshEngine.ExtraBuiltins = xshBuiltins
	xsh.WantDebug = pmooDebug

	completer := readline.NewPrefixCompleter(
		readline.PcItemDynamic(listVerbs(DefaultPlayerId)),
		readline.PcItem("x", readline.PcItemDynamic(listFuncs(xshEngine))))

	l, err := readline.NewEx(&readline.Config{
		Prompt:          "",
		HistoryFile:     "/tmp/readline.tmp",
		AutoComplete:    completer,
		InterruptPrompt: "^C",
		EOFPrompt:       "exit",

		HistorySearchFold: true,
	})
	if err != nil {
		panic(err)
	}
	for {
		completer = readline.NewPrefixCompleter(
			readline.PcItemDynamic(listVerbs(DefaultPlayerId)),
			readline.PcItem("x", readline.PcItemDynamic(listFuncs(xshEngine))))
		fmt.Print("\033[31m»\033[0m ")
		text, err := l.Readline()
		if batch {
			fmt.Print(text)
		}
		if batch {
			for len(queue) > 0 {
				log.Println("Waiting on queue", len(queue), "/", cap(queue))
				time.Sleep(18 * time.Millisecond)
			}
		}

		if err != nil {
			log.Println("Readline error:", err, "Exiting...")

			os.Exit(1)
		}
		text = strings.TrimSuffix(text, "\r\n")
		text = strings.TrimSuffix(text, "\n")
		if text == "exit" {
			fmt.Println("Exiting...")
			os.Exit(0)
		}
		if text != "" {
			if len(text) < 3 {
				fmt.Println("Too short")
				continue
			}
			verbosef("Examining input: '%s'\n", text[:2])
			if text[:2] == "x " {

				state := xsh.New()
				addPmooTypes(state)
				state.ExtraBuiltins = xshBuiltins
				state.UserData = DefaultPlayerId
				std := xsh.Parse(xsh.Stdlib_str, "stdlib")
				xsh.WantDebug = false
				xsh.WantInform = false
				xsh.WantWarn = false
				xsh.WantHelp = false
				xsh.WantTrace = false
				xsh.WantErrors = false
				xsh.WantInteractive = false
				xsh.RunTree(state, std)
				xsh.WantDebug = pmooDebug
				code := text[2:]
				code = BuildXshCode(code, DefaultPlayerId, DefaultPlayerId, "", "", "", "", "", "", "", "", 1000)
				log.Println("1 xsh program: ", code)

				tree := xsh.ParseXSH(code, "user input")
				tree = subsitutePmooVars(tree, 1000)

				log.Printf("2 Running xsh program: %v\n", tree)
				verbosef("3 Running code: %v\n", xsh.TreeToXsh(tree))
				tree.Kind = "LIST"
				tree.List = append([]autoparser.Node{{Raw: "list", Kind: "PARSED"}}, tree.List...)
				verbosef("runtree: %v\n", xsh.RunTree(state, tree))

			} else {
				verbosef("Sending sentence to input queue: %s\n", text)
				// Console is always the wizard, at least for now
				InputMsg(DefaultPlayerId, DefaultSystemCore, "input", text)
			}
		}
	}
	l.Close()
}

var (
	Affinity  string
	batch     bool
	pmooDebug bool
	cmdProg   = path.Base(os.Args[0])
)

func main() {
	xsh.WantDebug = false
	xsh.WantInform = false
	xsh.WantWarn = false
	xsh.WantHelp = false
	xsh.WantTrace = false
	xsh.WantErrors = false
	xsh.WantInteractive = false
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var init bool
	var RawTerm bool
	var inQ chan *Message = make(chan *Message, 100)
	cmdProg = path.Base(os.Args[0])
	pmooDebug = false
	pmooVerbose = false

	flag.BoolVar(&pmooDebug, "debug", false, "Print log messages")
	flag.BoolVar(&init, "init", false, "Create basic objects.  Overwrites existing")
	flag.BoolVar(&batch, "batch", false, "Batch mode.  Wait for each command to finish before starting next.")
	flag.BoolVar(&Cluster, "cluster", false, "Run in cluster mode.  See instructions in README.")
	flag.StringVar(&QueueServer, "queue", "http://127.0.0.1:8080", "Location of queue server.")
	flag.StringVar(&Affinity, "affinity", DefaultSystemCore, "Will exclusively process all messages with this affinity id.")
	flag.BoolVar(&RawTerm, "raw", false, "Batch mode.  No shell enhancements, read directly from STDIN.  Works with rlwrap.")
	flag.BoolVar(&pmooVerbose, "verbose", false, "Print information on what is happening.")
	pmoo.Flags()

	flag.Parse()
	if pmooDebug {
		xsh.WantDebug = true
		pmoo.WantDebug = true
	} else {
		log.SetOutput(ioutil.Discard)
	}

	if pmooVerbose {
		pmoo.WantVerbose = true
	}

	SetQ(inQ)

	if cmdProg == "p" {
		// fmt.Println("Single command mode")
		dataDir := goof.HomeDirectory() + "/.pmoo/objects"
		os.MkdirAll(dataDir, 0600)
		SetDataDir(dataDir)

		// FIXME duplicate code
		text := strings.Join(flag.Args(), " ")
		text = strings.TrimSuffix(text, "\r\n")
		text = strings.TrimSuffix(text, "\n")
		if text != "" {
			// fmt.Printf("Examining input: %s\n", text[:2])
			if text[:2] == "x " {
				verbosef("xsh program: %v\n", text[2:])
				fmt.Println(xshRun(text[2:], DefaultPlayerId))
			} else {
				verbosef("p input handler: %s\n", text)
				// Console is always the wizard, at least for now
				InputMsg(DefaultPlayerId, DefaultSystemCore, "input", text)
			}
		}
		log.Println("Using MOO objects from", pmoo.DataDir)
	} else {
		if Cluster {
			// okdb.Connect("192.168.178.22:7778")

			// go StartClient(QueueServer)
			fmt.Println("Starting cluster receiver")
			SetQueueServer(QueueServer)
			ClusterConn = pmoo.StartP2P("", "")
			go ClusterConn.Receiver("main", func(b []byte) {
				var m Message
				err := json.Unmarshal(b, &m)
				if err == nil {
					inQ <- &m
				}
			})
			go ClusterConn.Receiver(DefaultSystemCore, func(b []byte) {
				var m Message
				err := json.Unmarshal(b, &m)
				if err == nil {
					inQ <- &m
				}
			})
			log.Println("Using MOO objects from cluster", QueueServer)
		} else {

			if !goof.Exists(pmoo.DataDir) {
				dataDir := goof.HomeDirectory() + "/.pmoo/objects"
				os.MkdirAll(dataDir, 0600)
				SetDataDir(dataDir)
				fmt.Println("Setting data dir to", dataDir)
			}

		}

		verbosef("Using MOO objects from %v\n", pmoo.DataDir)
		if init {
			if Cluster {
				fmt.Println("Waiting for cluster, wait 5 seconds...")

				time.Sleep(5 * time.Second)
			}
			fmt.Println("Initialising database")

			initDB()
			fmt.Println("Initialised database, exiting\n")
			os.Exit(0)
		}

		if RawTerm {
			go ConsoleInputHandler(inQ)
		} else {
			go ReadLineInputHandler(inQ)
		}
	}

	MOOloop(inQ)
}

func MOOloop(inQ chan *Message) {
	for {
		log.Println("Waiting on queue", len(inQ), "/", cap(inQ))
		if cmdProg == "p" && len(inQ) == 0 {
			os.Exit(0)
		}
		m := <-inQ
		timeout := m.Ticks
		log.Println("Q:", m)
		verbosef("Q: %+v\n", m)
		if m.Ticks < 1 {
			log.Println("Audit: Dropped message because it timed out:", m)
		}
		if m.Affinity != "" && m.Affinity != Affinity && Cluster && Affinity != "" {
			// Put this message back in the queue so the right server can get it
			// FIXME add queues for each server so we can send it directly to the right machine
			ClusterConn.MyQMessage(m)
			continue
		}
		if m.This != DefaultSystemCore {
			log.Println("Handling direct message")

			if m.This != "" && m.Player != "" && m.Verb != "" { // Skip broken messages
				verbosef("Invoking direct message for verb %v\n", m.Verb)
				invoke(m.Player, m.This, m.Verb, m.Dobj, m.Dpropstr, m.Prepstr, m.Iobj, m.Ipropstr, m.Dobjstr, m.Iobjstr, *m, timeout)
			} else {
				log.Println("Dropped message because it was malformed:", m)
			}
			continue
		}

		text := m.Data
		var args []string
		var verb, dobjstr, prepstr, iobjstr string
		if m.Verb == "%commandline" {
			args = m.Args
			verb, dobjstr, prepstr, iobjstr = m.Args[0], m.Args[1], m.Args[2], m.Args[3]
		} else {
			log.Println("Handling input - Breaking sentence")
			args, _ = LexLine(text)
			if len(args) > 0 {
				args = args[1:]
			}
			verb, dobjstr, prepstr, iobjstr = BreakSentence(text)
		}

		log.Println(strings.Join([]string{verb, dobjstr, prepstr, iobjstr}, ":"))
		dobj, dpropstr, dobjerr := ParseDirectObject(dobjstr, DefaultPlayerId, timeout)
		if dobjerr != nil {
			RawMsg(Message{From: DefaultSystemCore, Player: DefaultPlayerId, Verb: "notify", Dobjstr: dobjerr.Error(), Ticks: m.Ticks - 100})

		}
		iobj, ipropstr, iobjerr := ParseDirectObject(iobjstr, DefaultPlayerId, timeout)
		if iobjerr != nil {
			RawMsg(Message{From: DefaultSystemCore, Player: DefaultPlayerId, Verb: "notify", Dobjstr: iobjerr.Error(), Ticks: m.Ticks - 100})

		}

		thisObj, _ := VerbSearch(DefaultPlayerId, verb, timeout)

		if thisObj == nil {
			msg := fmt.Sprintf("Verb '%v' not found!\n", verb)
			RawMsg(Message{From: DefaultSystemCore, Player: DefaultPlayerId, Verb: "notify", Dobjstr: msg, Ticks: m.Ticks - 100})

		} else {
			this := ToStr(thisObj.Id)
			affin := thisObj.Properties["affinity"].Value
			log.Println("Props", thisObj.Properties)
			log.Println("Found affinity:", affin)

			msg := Message{Player: DefaultPlayerId, This: this, Verb: verb, Dobj: dobj, Dpropstr: dpropstr, Prepstr: prepstr, Iobj: iobj, Ipropstr: ipropstr, Dobjstr: dobjstr, Iobjstr: iobjstr, Trace: m.Trace, Affinity: affin, Args: args, Ticks: m.Ticks - 1}
			if Cluster && affin != Affinity && Affinity != "" && affin != "" {
				log.Println("Handling input - Queueing direct message:", msg)
				ClusterConn.MyQMessage(msg)
			} else {
				log.Println("Handling input - Invoking direct message:", msg)
				RawMsg(msg)
			}

		}

	}
}

func addPmooTypes(s xsh.State) {
	s.TypeSigs["setprop"] = []string{"void", "string", "string", "string"}
	s.TypeSigs["allobjects"] = []string{"list"}
	s.TypeSigs["inroomobject"] = []string{"string", "string"}
	s.TypeSigs["search"] = []string{"string", "string", "search"}
	s.TypeSigs["clone"] = []string{"string", "string", "string"}
	s.TypeSigs["formatobject"] = []string{"string", "string"}
	s.TypeSigs["move"] = []string{"void", "string", "string"} // Should be bool?
	s.TypeSigs["getprop"] = []string{"string", "string", "string"}
	s.TypeSigs["setverb"] = []string{"void", "string", "string", "string", "string"}
	s.TypeSigs["msg"] = []string{"void", "string", "string", "string", "string", "string", "string"}
	s.TypeSigs["become"] = []string{"bool", "string", "string"}
	s.TypeSigs["sleep"] = []string{"void", "string"}
	s.TypeSigs["o"] = []string{"string", "string"}
}

func become(player, affinity string) bool {
	DefaultPlayerId = player
	pmoo.SetProp(player, "affinity", affinity)
	fmt.Printf("Became player id: %v on node: %v\n", DefaultPlayerId, affinity)
	return true
}

// Actually evaluate the verb
func invoke(player, this, verb, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr string, m Message, timeout int) {
	verbosef("Invoking verb %v, player %v, this %v, dobj %v, dpropstr %v, prepstr %v, iobj %v, ipropstr %v, dobjstr %v, iobjstr %v\n", verb, player, this, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr)
	code := ""
	defer func() {
		if r := recover(); r != nil {
			log.Println("Paniced:", r)
			log.Println("Failed to eval:", player, this, verb, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr, "code:", code, r)
			log.Println("stacktrace from panic in eval: \n" + string(debug.Stack()))
			if verb != "notify" {
				Msg(this, player, "notify", fmt.Sprintf("Failed to %v %v, because %v %v", verb, dobjstr, r, string(debug.Stack())), "", "")
			}
		}
	}()
	if verb != "tell" && verb != "notify" {
		objstr := dobjstr
		if objstr == "me" {
			objstr = "yourself"
		}
		Msg(this, player, "notify", fmt.Sprintf("You %v %v %v %v", verb, dobjstr, prepstr, iobjstr), "", "")
	}

	vobj := RecSearchProp(LoadObject(this), verb, 10)
	if vobj == nil {
		log.Println("Failed to find verb", verb, "in object", this)
		return
	}
	verbStruct := GetVerbStruct(vobj, verb, 10)
	if verbStruct == nil {
		fmt.Printf("Failed to lookup '%v' on %v\n", verb, this)
	} else {
		log.Printf("Starting %+v\n", verbStruct)

		switch verbStruct.Interpreter {
		case "":
			fallthrough
		case "throff":
			t := throfflib.MakeEngine()
			AddEngineFuncs(t, player, this, "0")
			t = t.RunString(throfflib.BootStrapString(), "Internal Bootstrap")
			args := throfflib.StringsToArray(m.Args)
			code = code + verbStruct.Value
			log.Println("Throff program: ", code)
			// code = code + "  PRINTLN A[ ^player: player ]A PRINTLN A[ ^this: this ]A PRINTLN  A[ ^verb: verb ]A PRINTLN   A[ ^dobjstr: dobjstr ]A PRINTLN A[ ^dpropstr: dpropstr ]A PRINTLN  A[ ^prepstr: prepstr ]A  PRINTLN A[ ^iobjstr: iobjstr ]A   PRINTLN A[ ^ipropstr: ipropstr ]A PRINTLN [ ]  "
			code = code + " ARG args TOK ARG player TOK   ARG  this TOK   ARG verb TOK   ARG  dobj TOK   ARG dpropstr TOK   ARG prepstr TOK  ARG iobj TOK   ARG ipropstr TOK   ARG dobjstr TOK   ARG iobjstr TOK SAFETYON "
			t = throfflib.PushData(t, args)
			t.CallArgs(code, player, this, verb, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr)
		case "yaegi":
			log.Println("Goscript program: ", code)
			var goScript *interp.Interpreter
			goScript = NewInterpreter()
			goScript.Eval(`
			import . "github.com/donomii/pmoo"
			import "os"
			import . "fmt"`)
			code = BuildDefinitions(player, this, verb, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr)
			code = code + verbStruct.Value
			Eval(goScript, code)
		case "xsh":

			state := xsh.New()
			addPmooTypes(state)
			state.ExtraBuiltins = xshBuiltins
			state.UserData = player
			code = BuildXshCode(verbStruct.Value, player, this, verb, dobj, dpropstr, prepstr, iobj, ipropstr, dobjstr, iobjstr, timeout)
			log.Println("4 xsh program: ", code)
			std := xsh.Parse(xsh.Stdlib_str, "stdlib")
			xsh.RunTree(state, std)
			xsh.WantDebug = pmooDebug
			tr := xsh.Parse(code, "pmoo")
			// fmt.Printf("Substituting pmoo vars\n")
			tr = subsitutePmooVars(tr, timeout)
			log.Printf("5 Running xsh program: %v\n", tr)

			xsh.RunTree(state, tr)

		default:
			log.Println("Unknown interpreter: ", verbStruct.Interpreter)
		}
	}
}

func subsitutePmooVars(code autoparser.Node, timeout int) autoparser.Node {
	Debugf("Substituting vars in %v\n", xsh.TreeToXsh(code))
	out := xsh.TreeMap(func(n autoparser.Node) autoparser.Node {
		str := xsh.S(n)
		//Debugf("Examining %v for %% format\n", str)
		if strings.HasPrefix(str, "%") {
			objStr := string(str[1:])
			//fmt.Printf("Looking up object '%v'\n", objStr)
			id := GetObjectByName(DefaultPlayerId, objStr, timeout)
			//fmt.Printf("Found object %v\n", id)
			return xsh.N(fmt.Sprintf("%v", id))
		}

		return n
	}, code)
	return out
}
