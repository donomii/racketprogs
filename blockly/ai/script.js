$(document).ready(function () {
    // Make blocks in the palette draggable
    $('.block').draggable({
        helper: 'clone',
        revert: 'invalid'
    });

    // Function to make an element droppable and handle block nesting
    function makeDroppable(element) {
        element.droppable({
            accept: '.block',
            greedy: true, // Prevent event bubbling to parent elements
            drop: function (event, ui) {
                // If the element being dragged is from the palette, create a new block
                if (!ui.helper.hasClass('dropped')) {
                    var blockType = ui.helper.data('type');
                    var newBlock = $('<div></div>')
                        .addClass('block nested dropped')
                        .text(ui.helper.text())
                        .attr('data-type', blockType);

                    // Append the new block to the current element (or arena)
                    $(this).append(newBlock);

                    // Make the new block droppable and draggable
                    makeDroppable(newBlock);
                    makeDraggable(newBlock);
                } else {
                    // If the element is already in the arena, move it
                    var originalElement = ui.helper.data('originalElement');
                    $(this).append(originalElement);
                }
            }
        });
    }

    // Function to make an element draggable
    function makeDraggable(element) {
        element.draggable({
            helper: 'clone',
            revert: 'invalid',
            start: function (event, ui) {
                // Store a reference to the original element to move it during drop
                ui.helper.data('originalElement', $(this));
            }
        });
    }

    // Initialize the arena as a droppable area
    makeDroppable($('#arena'));
});
