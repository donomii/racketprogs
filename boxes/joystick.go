package boxes

import (
	"fmt"

	"github.com/0xcafed00d/joystick"
)

var GlobalJoystik joystick.Joystick

func readJoystick(js joystick.Joystick) ([]int, []bool) {
	if GlobalJoystik == nil {
		return nil, nil
	}
	jinfo, err := js.Read()

	if err != nil {
		fmt.Print("Error: " + err.Error())
		return nil, nil
	}

	buttons := []bool{}

	for button := 0; button < js.ButtonCount(); button++ {
		if jinfo.Buttons&(1<<uint32(button)) != 0 {
			buttons = append(buttons, true)
		} else {
			buttons = append(buttons, false)
		}
	}

	axes := jinfo.AxisData

	return axes, buttons
}

func initjoystick() joystick.Joystick {
	jsid := 0
	var jserr error
	var joystik joystick.Joystick
	joystik, jserr = joystick.Open(jsid)

	if jserr != nil {
		fmt.Println(jserr)
		return nil
	}
	return joystik
}
