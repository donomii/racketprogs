package boxes

import (
	"encoding/json"
	"fmt"
	"image"
	fuckwits "image/draw"
	"io/ioutil"
	"os"
	"sync"
	"time"

	"github.com/donomii/goof"
	"github.com/nfnt/resize"
)

//Golang cannot serialise functions, so we need to use a string to represent the function.  Each box keeps a pointer to a function, and the function name
//  The function name is used during loading, to lookup the function pointer and reassign it to the box

// Define your functions here, and then add them to the callbacks map in callbacks.go
// The key is the name of the function, and the value is the function itself

// The same callback function is used for all events, so most callbacks will need a switch statement to check
// for the event type.

//The return value indicates that the event has been handled, and should not be passed to other callbacks.
// It does not indicate the success of the callback.  On returning false, the default builtin callback handler
// will also be called.

//Originally I expected that this system would be more heavily used, but it turns out to be clumsy
// and annoying.  It's useful for developing the base system, since it provides some level of customisation
// without trading off a lot of speed, but in general the messaging system is a much better place for adding
// custom behaviour.

var EventCallbacks map[string]func(*World_s, *Box, *Box, string, float64, float64) bool
var RenderCallbacks map[string]func(*World_s, *Box, *Box, float64, float64) bool

func initCallbacks() {
	EventCallbacks = map[string]func(*World_s, *Box, *Box, string, float64, float64) bool{

		"LaunchExternalViewer": LaunchExternalViewer,
		"SaveWorld":            SaveWorld,
		"LoadWorld":            LoadWorld,
		"DefaultCallback":      DefaultCallback,
		"RefreshDirBoxes":      RefreshDirBoxes,
		"Halo_resize_callback": Halo_resize_callback,
		"QuitCallback":         QuitCallback,
		"Halo_close_callback":  Halo_close_callback,
		"Halo_pin_callback":    Halo_pin_callback,
		"SelectNextCallback":   SelectNextCallback,
	}

	RenderCallbacks = map[string]func(*World_s, *Box, *Box, float64, float64) bool{
		"RenderPicture":            RenderPicture,
		"Halo_resize_drawcallback": Halo_resize_drawcallback,
		"Halo_locate_drawcallback": Halo_locate_drawcallback,
		"Halo_close_drawcallback":  Halo_close_drawcallback,
		"Halo_pin_drawcallback":    Halo_pin_drawcallback,
	}
}

func LaunchExternalViewer(w *World_s, b, parent *Box, action string, x, y float64) bool {
	if action == "click" {
		fmt.Println("Clicked on", b.Text)
		command := "open \"" + b.Text + "\""
		fmt.Print(command)
		goof.Shell(command)
		return true
	}
	return false
}

func SaveWorld(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	if action == "click" {
		fmt.Println("Save")
		//Write the World variable to a file in JSON format

		WalkBoxTree(CurrentWorld.Top, func(b *Box) {
			if b.DynamicBox {
				b.Children = []*Box{}
			}
		})
		//Marshal the World variable into a JSON string
		jsonString, err := json.Marshal(CurrentWorld)
		if err != nil {

			fmt.Println("Can't marshal world", err)
			return true
		}
		//Write the JSON string to a file
		err = ioutil.WriteFile("world.json", jsonString, 0644)
		if err != nil {
			fmt.Println("Can't write world to disk", err)
			return true
		}
		WalkBoxTree(CurrentWorld.Top, func(b *Box) {
			if b.DynamicBox {
				if b.Callback != nil {
					b.Callback(w, b, parent, "regenerate", 0, 0)
				}
			}
		})
		return true
	}
	return false

}

func LoadWorld(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	if action != "click" {
		return false
	}
	//Load json from "world.json" into a string
	jsonString, err := ioutil.ReadFile("world.json")
	if err != nil {
		fmt.Println("Can't read world from disk", err)
		return true
	}

	CurrentWorld = &World_s{}
	//Unmarshal the JSON string into a World_s struct
	err = json.Unmarshal(jsonString, CurrentWorld)
	if err != nil {
		fmt.Println("Can't unmarshal world", err)
		return true
	}
	//Walk through World.Top and register every box with the World.Boxes map
	CurrentWorld.Boxes = sync.Map{}
	WalkBoxTree(CurrentWorld.Top, func(b *Box) {
		fmt.Printf("Registering %v\n", b.Id)
		CurrentWorld.Boxes.Store(b.Id, b)

		//Register the callbacks
		if b.CallbackName != "" {
			fmt.Printf("Reconnecting callback %v for %v\n", b.CallbackName, b.Id)
			callback, ok := EventCallbacks[b.CallbackName]
			if ok {
				b.Callback = callback
			} else {
				fmt.Printf("Callback %v not registered!\n", b.CallbackName)
			}
		}
		if b.DrawCallbackName != "" {
			fmt.Printf("Reconnecting draw callback %v for %v\n", b.DrawCallbackName, b.Id)
			callback, ok := RenderCallbacks[b.DrawCallbackName]
			if ok {
				b.DrawCallback = callback
			} else {
				fmt.Printf("Draw Callback %v not registered!\n", b.DrawCallbackName)
			}
		}

		if b.DynamicBox {
			if b.Callback != nil {
				b.Callback(w, b, parent, "regenerate", 0, 0)
			}
		}

	})

	//Recreate the halo, in case the old one was loaded with stale configuration
	CreateHalo(CurrentWorld)
	return true

}

func imageToRGBA(src image.Image) *image.RGBA {
	// No conversion needed if image is an *image.RGBA.
	if dst, ok := src.(*image.RGBA); ok {
		return dst
	}

	// Use the image/draw package to convert to *image.RGBA.
	b := src.Bounds()
	dst := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	fuckwits.Draw(dst, dst.Bounds(), src, b.Min, fuckwits.Src)
	return dst
}

func RenderPicture(w *World_s, box, parent *Box, localX, localY float64) bool {

	scaledName := fmt.Sprintf("%v:%v", w.Scale, box.Id)
	origPic := pictureCacheOf(box.Id)
	if origPic == nil {

		filename := FilePathOf(box.Id)
		if filename == "" {
			return false //No picture to render
		}
		img := LoadImage(filename)
		if img == nil {
			//fmt.Printf("Failed to load %v\n", filename)
			return false //No picture to render
		}
		imga := imageToRGBA(img)
		setPictureCache(box.Id, imga)
		setScaledPicture(scaledName, nil)
		box.Data = nil
	}

	if scaledPictureOf(scaledName) == nil {
		origPic := pictureCacheOf(box.Id)
		im := resize.Resize(uint(box.ScreenW), uint(box.ScreenH), origPic, resize.Lanczos3)
		//box.W = float64(PictureData.Bounds().Dx())
		//box.H = float64(PictureData.Bounds().Dy())

		p := imageToRGBA(im)

		setScaledPicture(scaledName, p)

		drawImage(localX, localY, p)
		return true

	} else {
		drawImage(localX, localY, scaledPictureOf(scaledName))
		return true
	}
	return false
}

func FpsRenderPassthrough(w *World_s, box, parent *Box, localX, localY float64) bool {
	if box.Data != nil {

		lapsed := time.Since(box.Data.(time.Time))
		box.Text = fmt.Sprintf("lapsed: %v", lapsed.Milliseconds())
	}
	box.Data = time.Now()
	return false
}

func FindBox(w *World_s, id string) *Box {
	if id == "" {
		return nil
	}
	if w == nil {
		fmt.Println("FindBox called with nil world")
		return nil
	}

	box, ok := w.Boxes.Load(id)
	if !ok {
		//fmt.Printf("FindBox failed to find %v in %v\n", id, w.Id)
		return nil
	}
	return box.(*Box)
}

func RemoveBoxRecurs(p, b *Box) {
	if p == nil {
		return
	}
	if b == nil {
		return
	}
	for _, c := range p.Children {
		RemoveBoxRecurs(c, b)
	}
	for i, c := range p.Children {
		if c.Id == b.Id {
			p.Children = append(p.Children[:i], p.Children[i+1:]...)
			return
		}
	}

}

func RemoveBox(world *World_s, box *Box) {
	RemoveBoxRecurs(world.Top, box)
	world.Boxes.Delete(box.Id)
}

func ToggleChildren(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	if action != "click" {
		return false
	}
	if box.Children == nil {
		return true
	}
	if len(box.Children) == 0 {
		return true
	}
	for _, child := range box.Children {
		child.Invisible = !child.Invisible
		child.Intangible = !child.Intangible
	}
	return true
}

func SelectNextCallback(w *World_s, box, otherBox *Box, action string, x, y float64) bool {
	switch action {
	case "click":
		SendMessage(w.Id, "Select Next", nil)
		return true
	default:
		return false
	}
	return false
}

func DumpTreeCallback(w *World_s, box, otherBox *Box, action string, x, y float64) bool {
	switch action {
	case "click":
		PrintBoxTree(w.Top, 0, true)
		return true
	default:
		return false
	}
	return false
}

func DefaultCallback(w *World_s, box, otherBox *Box, action string, x, y float64) bool {
	switch action {
	case "click":
		FindBox(w, "Halo").Data = box
		if box.DefaultMessage != "" {
			SendMessage(w.Id, box.DefaultMessage, box)
		} else {
			SendMessage(w.Id, "select", box) //Allow override here?
		}

	case "drop":
		from := box
		to := otherBox
		if from != nil && to != nil {
			fmt.Printf("%v at %v,%v from %v, to %v\n", action, x, y, from.Id, to.Id)
		}

	default:
		return false
	}
	return true
}

func QuitCallback(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	if action == "click" {
		os.Exit(0)
	}
	return false
}
