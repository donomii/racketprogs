//go:build !sdl
// +build !sdl

package boxes

// Routines to handle mouse movement, keyboard, and window events

import (
	"fmt"
	"log"

	"github.com/go-gl/glfw/v3.3/glfw"
)

var selected int

func handleKeys(window *glfw.Window) {
	window.SetKeyCallback(func(w *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
		log.Printf("Got key %c,%v,%v,%v", key, key, mods, action)

		//2022/09/18 02:23:37 Got key ŕ,341,2,1
		//2022/09/18 02:23:39 Got key ŕ,341,0,0
		if key == 261 || key == 259 {
			dispatch("DELETE-LEFT", CurrentWorld.CurrentEditor)
			CurrentWorld.CurrentEditBox.Text = ActiveBufferText(CurrentWorld.CurrentEditor)
			return
		}

		if CurrentWorld.CurrentEditBox != nil {
			if mods == 0 {
				ActiveBufferInsert(CurrentWorld.CurrentEditor, fmt.Sprintf("%c", key))

				CurrentWorld.CurrentEditBox.Text = ActiveBufferText(CurrentWorld.CurrentEditor)
				if CurrentWorld.CurrentEditBox.AstNode != nil {
					CurrentWorld.CurrentEditBox.AstNode.Str = ActiveBufferText(CurrentWorld.CurrentEditor)
				}
			}

		}

		if action == glfw.Press && key == 341 {
			EditMode = true
			log.Println("Edit mode on")
		}

		if action == glfw.Release && key == 341 {
			EditMode = false
			log.Println("Edit mode off")
		}

		if action == glfw.Press && key == 80 {
			// P
			SendMessage(CurrentWorld.Id, "Previous World", nil)
		}

		//Move select box

		if action == glfw.Press && key == 265 {
			// Up
			SendMessage(CurrentWorld.Id, "Up", nil)
			SendMessage(CurrentWorld.Id, "Select Next", nil)
		}

		if action == glfw.Press && key == 264 {
			//Down
			SendMessage(CurrentWorld.Id, "Down", nil)
			SendMessage(CurrentWorld.Id, "Select Previous", nil)
		}

		if action == glfw.Press && key == 263 {
			//Left
			SendMessage(CurrentWorld.Id, "Left", nil)
			SendMessage(CurrentWorld.Id, "Select Previous", nil)
		}

		if action == glfw.Press && key == 262 {
			//Right
			SendMessage(CurrentWorld.Id, "Right", nil)
			SendMessage(CurrentWorld.Id, "Select Next", nil)
		}

		if CurrentWorld.Id == "DebugWorld" {
			if action == glfw.Press && key == 265 {
				b := FindBox(CurrentWorld, "joybox1")
				b.Y -= 10
			}

			if action == glfw.Press && key == 264 {
				b := FindBox(CurrentWorld, "joybox1")
				b.Y += 10
			}

			if action == glfw.Press && key == 263 {
				b := FindBox(CurrentWorld, "joybox1")
				b.X -= 10
			}

			if action == glfw.Press && key == 262 {
				b := FindBox(CurrentWorld, "joybox1")
				b.X += 10
			}
		}

		/*if key == 301 {
			hideWindow()
			return
		}
		*/
		/*
				EscapeKeyCode := 256
			MacEscapeKeyCode := 53
			MacF12KeyCode := 301

					if action > 0 {
						if key == glfw.Key(MacF12KeyCode) || key == 109 {
							doKeyPress("HideWindow")
						}
						// ESC
						if key == glfw.Key(EscapeKeyCode) || key == glfw.Key(MacEscapeKeyCode) {
							// os.Exit(0)
							log.Println("Escape pressed")
							wantExit = true
							doKeyPress("HideWindow")
							return
						}

						if key == 265 {
							doKeyPress("SelectPrevious")
						}

						if key == 264 {
							doKeyPress("SelectNext")
						}

						if key == 257 {
							doKeyPress("Activate")
						}

						if key == 259 {
							doKeyPress("Backspace")
						}

						UpdateBuffer(ed, input)
						update = true
					}
		*/
	})

	window.SetCharModsCallback(func(w *glfw.Window, char rune, mods glfw.ModifierKey) {
		/*
			text := fmt.Sprintf("%c", char)
			input = input + text
			UpdateBuffer(ed, input)
			update = true
		*/
	})
}

func handleFocus(window *glfw.Window) {
	/*
		window.SetFocusCallback(func(w *glfw.Window, focused bool) {
			log.Printf("Focus changed to %v\n", focused)
			if !focused {
				invalidCoords = true
				log.Println("Marked coords as invalid")
			}
		})
	*/
}

func handleMouse(window *glfw.Window) {
	window.SetMouseButtonCallback(func(w *glfw.Window, button glfw.MouseButton, action glfw.Action, mods glfw.ModifierKey) {
		fX, fY := window.GetCursorPos()
		mouseX, mouseY = fX, fY
		if button == glfw.MouseButtonLeft {
			if action == glfw.Press {
				log.Println("Mouse button pressed")

				SendMessage(CurrentWorld.Id, "Mouse Press", "left")
				draw_action = "press"
				need_redraw = true
			}
		}

		if button == glfw.MouseButtonLeft {
			if action == glfw.Release {
				draw_action = "release"
				SendMessage(CurrentWorld.Id, "Mouse Release", "left")
				need_redraw = true
			}
		}
	})
}

func handleScroll(window *glfw.Window) {
	window.SetScrollCallback(func(w *glfw.Window, xoff float64, yoff float64) {
		if yoff > 0 {
			log.Println("Scroll up")
			draw_action = "wheel up"
		} else {
			log.Println("Scroll down")
			draw_action = "wheel down"
		}

		mouseY = yoff
		// draw_action = "wheel"
		need_redraw = true
	})
}

func handleMouseMove(window *glfw.Window) {
	window.SetCursorPosCallback(func(w *glfw.Window, xpos float64, ypos float64) {
		//Do we redraw every mouse move, or just significant ones?
		NeedRedraw()
		mouseX, mouseY = xpos, ypos
		/*
			// fmt.Printf("Mouse moved to %v,%v\n", xpos, ypos)
			if invalidCoords {
				lastMouseX = xpos
				lastMouseY = ypos
			} else {
				lastMouseX = mouseX
				lastMouseY = mouseY
			}
			mouseX = xpos
			mouseY = ypos
			X, Y := window.GetPos()
			globalMouseX = xpos - float64(X)
			globalMouseY = ypos - float64(Y)

			log.Printf("Mouse moved to %v,%v, last X,Y: %v,%v\n", mouseX, mouseY, lastMouseX, lastMouseY)
			if mouseDrag && !invalidCoords {

				deltaX := globalMouseX - dragStartX
				deltaY := globalMouseY - dragStartY
				log.Printf("deltaX: %v, deltaY: %v, mouseX: %v, mouseY: %v, dragStartX: %v, dragStartY: %v, dragOffSetX: %v, dragOffSetY: %v\n", deltaX, deltaY, mouseX, mouseY, dragStartX, dragStartY, dragOffSetX, dragOffSetY)
				log.Printf("globalMouseX: %v, globalMouseY: %v\n", globalMouseX, globalMouseY)
				if (deltaX > 10) || (deltaX < -10) || (deltaY > 10) || (deltaY < -10) {
					windowPosX += int(mouseX - dragOffSetX)
					windowPosY += int(mouseY - dragOffSetY)
					log.Printf("Dragged to %v,%v\n", windowPosX, windowPosY)
					window.SetPos(int(windowPosX), int(windowPosY))
				}
			}

			invalidCoords = false
			log.Println("Marked coords as valid")
		*/
	})
}
