package boxes

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func Uniquify(s *[]string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range *s {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func make_pic_grid(prompt string) *World_s {
	PicGridWorld := NewWorld()
	PicGridWorld.Scale = 1.5
	PicGridWorld.Id = "PicGridWorld"
	CurrentWorld = PicGridWorld
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: DefaultCallback}
	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}

	register_global_message_handlers(PicGridWorld)
	Add(PicGridWorld, PicGridWorld.Top, &debug_message_display_box)

	Add(PicGridWorld, PicGridWorld.Top,  &NextWorld)
	CreateHalo(PicGridWorld) //FIXME

	//Find all files in the current directory starting with prompt
	//Start by getting a list of all files in the current directory
	//Then filter the list to only include files that start with prompt
	//Then create a grid of boxes with the filtered list of files
	//Then add the grid to the world
	//Then add the world to the world stack

	//Get a list of all files in the current directory
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}
	//Filter the list of files to only include files that start with prompt
	var filtered_files []os.FileInfo
	for _, f := range files {
		if strings.HasPrefix(f.Name(), prompt) {
			filtered_files = append(filtered_files, f)
		}
	}

	seeds := []string{}
	gs := []string{}
	iters := []string{}
	base_str := ""
	//Split all the filenames at "__"
	for _, f := range filtered_files {
		name := strings.TrimSuffix(f.Name(), ".png")
		bits := strings.Split(name, "__")
		if len(bits) > 1 && bits[0] != "" {
			base_str = bits[0]
			settings_str := bits[1]
			settings := strings.Split(settings_str, "_")
			seed := strings.TrimPrefix(settings[1], "seed")
			g := strings.TrimPrefix(settings[2], "g")
			iter := strings.TrimPrefix(settings[3], "iter")
			seeds = append(seeds, seed)
			gs = append(gs, g)
			iters = append(iters, iter)
		}
	}

	seeds = Uniquify(&seeds)
	gs = Uniquify(&gs)
	iters = Uniquify(&iters)
	log.Println("seeds", seeds)
	log.Println("gs", gs)
	log.Println("iters", iters)

	side := float64(128)
	grid := Box{Text: "Picture Grid", Id: "picture_grid", X: 0, Y: 0, W: 400, H: 400, Scale: 1.0, PinToScreen: false, SplitLayout: "free"}
	for _, gg := range gs {
		for i, iter := range iters {
			g, _ := strconv.ParseFloat(gg, 64)
			x := float64(g * side)
			y := float64(i) * side
			//Fixme handle mps, cuda and cpu
			fname := fmt.Sprintf("%v__devicecuda_seed%v_g%v.0_iter%v.png", base_str, seeds[0], g, iter)
			b := Box{Text: "", Id: "picture_box_" + fname, X: x, Y: y, W: side, H: side, Scale: 1.0, PinToScreen: false, Colour: [4]uint8{64, 64, 64, 255}}
			b.DrawCallback = RenderPicture
			b.DrawCallbackName = "RenderPicture"

			Add(PicGridWorld, &grid, &b)
			SetFilePath(b.Id, fname)

		}
	}

	Add(PicGridWorld, PicGridWorld.Top, &grid)
	return PicGridWorld
}
