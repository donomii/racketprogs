//go:build !sdl
// +build !sdl

package boxes

import (
	"fmt"
	"image"
	"image/color"
	fuckyou "image/draw"
	"log"
	"os"
	"time"

	"github.com/donomii/glim"
	"github.com/go-gl/glfw/v3.3/glfw"
)

var (
	win   *glfw.Window
	state *State

	ed               *GlobalConfig // The text editor
	transfer_texture []uint8       // The texture buffer to transfer to the GPU

	form      *glim.FormatParams
	WinWidth  = 1280
	WinHeight = 800

	displayPicPath string
)

func DrawWidth() float64 {
	return float64(WinWidth)
}

func DrawHeight() float64 {
	return float64(WinHeight)
}

func InitGraphics() {
	WinWidth = 1280
	WinHeight = 800
	win, state = gfxStart(1280, 800, "test", false)
	transfer_texture = make([]uint8, 8000*8000*4)

	ed = NewEditor()
	// Create a text formatter.  This controls the appearance of the text, e.g. colour, size, layout
	form = glim.NewFormatter()
	ed.ActiveBuffer.Formatter = form
	SetFont(ed.ActiveBuffer, DefaultFontSize)

	handleKeys(win)
	handleMouse(win)
	handleMouseMove(win)
	handleFocus(win)
	handleScroll(win)

	win.SetSizeCallback(func(w *glfw.Window, width int, height int) {
		WinWidth = width
		WinHeight = height
	})

	glfw.SetJoystickCallback(func(joy glfw.Joystick, event glfw.PeripheralEvent) {

		fmt.Println("Joystick ", joy, " event ", event)
		//fmt.Printf("Axes: %+v, hats: %+v, buttons: %+v\n", joy.GetAxes(), joy.GetButtons(), joy.GetHats())
	})

	Register("Global", "Toggle Fullscreen", "ToggleFullscreen", func(name, id string, args interface{}) {
		if win.GetMonitor() == nil {
			win.SetMonitor(glfw.GetPrimaryMonitor(), 0, 0, 1280, 800, 30)
		} else {
			win.SetMonitor(nil, 0, 0, 1280, 800, 30)
		}

	})

}

func equalArray(arr []float64) bool {
	for i := 1; i < len(arr); i++ {
		if arr[i] != arr[0] {
			return false
		}
	}
	return true
}

var latches = make(map[int]bool)

func StartMain() {
	for !win.ShouldClose() {

		deadZone := 0.2
		joy := glfw.Joystick(0)
		//if !joy.Present() {
		//	joy = glfw.Joystick(1)
		//}

		//Check for joystick
		if joy.Present() {

			//fmt.Println("Joystick 1 present.  Axes: ", joy.GetAxes(), " Buttons: ", joy.GetButtons(), " Hats: ", joy.GetHats(), "Latches: ", latches)

			//Fetch button states
			buttons := joy.GetButtons()

			//Check for button presses
			for i := 0; i < len(buttons); i++ {

				b := FindBox(CurrentWorld, fmt.Sprintf("button_%v", i))
				butt := buttons[i]

				//If the button is available

				//If the button is pressed, and it wasn't pressed last time
				if butt == glfw.Press {

					if !latches[i] {
						latches[i] = true
						SendMessage(CurrentWorld.Id, "Button", i)
						if b != nil {
							b.Colour = [4]uint8{255, 0, 0, 255}
							b.Text = fmt.Sprintf("%v", i) + " pressed"
						}

						//Left shoulder trigger moves backwards
						if i == 4 {
							SendMessage(CurrentWorld.Id, "Previous World", nil)
						}

						//Right shoulder trigger moves forwards
						if i == 5 {
							SendMessage(CurrentWorld.Id, "Next World", nil)
						}

					}
				} else {
					//Button is released, so reset the latch
					latches[i] = false
					if b != nil {
						b.Colour = [4]uint8{0, 0, 255, 255}
						b.Text = fmt.Sprintf("%v", i) + " released"
					}
				}

			}
			if joystick_type == "steamdeck" {
				for i := 0; i < len(joy.GetAxes()); i = i + 3 {
					var ydeflect float64 = 0
					var trigger float64 = 0
					xdeflect := float64(joy.GetAxes()[i])
					if i+1 < len(joy.GetAxes()) {
						ydeflect = float64(joy.GetAxes()[i+1])
					}
					if i+2 < len(joy.GetAxes()) {
						trigger = float64(joy.GetAxes()[i+2])
					}
					var b *Box
					b = FindBox(CurrentWorld, "joybox1")

					if i > 2 {
						b = FindBox(CurrentWorld, "joybox2")
					}
					if b == nil {
						continue
					}
					if xdeflect > deadZone || xdeflect < -deadZone || ydeflect > deadZone || ydeflect < -deadZone {
						b.X = b.X + (xdeflect)*10.0
						b.Y = b.Y + (ydeflect)*10.0

					}

					normval := trigger / 2.0
					b.Colour = [4]uint8{0, 0, uint8(normval * 255.0), 255}

					ax1 := FindBox(CurrentWorld, fmt.Sprintf("axis_%v", i))
					ax1.Text = fmt.Sprintf("%v", xdeflect)

					ax2 := FindBox(CurrentWorld, fmt.Sprintf("axis_%v", i+1))
					ax2.Text = fmt.Sprintf("%v", ydeflect)

					ax3 := FindBox(CurrentWorld, fmt.Sprintf("axis_%v", i+2))
					ax3.Text = fmt.Sprintf("%v", trigger)

				}

				//fmt.Printf("Axes: %+v, hats: %+v, buttons: %+v\n", joy.GetAxes(), joy.GetButtons(), joy.GetHats())
			}

			if joystick_type == "F310" {
				for i := 0; i < len(joy.GetAxes()); i = i + 2 {
					var ydeflect float64 = 0
					var trigger float64 = 0
					xdeflect := float64(joy.GetAxes()[i])
					if i+1 < len(joy.GetAxes()) {
						ydeflect = float64(joy.GetAxes()[i+1])
					}
					if i+2 < len(joy.GetAxes()) {
						trigger = float64(joy.GetAxes()[i+2])
					}
					var b *Box
					b = FindBox(CurrentWorld, "joybox1")

					if i > 2 {
						b = FindBox(CurrentWorld, "joybox2")
					}
					if b == nil {
						continue
					}
					if xdeflect > deadZone || xdeflect < -deadZone || ydeflect > deadZone || ydeflect < -deadZone {
						b.X = b.X + (xdeflect)*10.0
						b.Y = b.Y + (ydeflect)*10.0

					}

					normval := trigger / 2.0
					b.Colour = [4]uint8{0, 0, uint8(normval * 255.0), 255}

					ax1 := FindBox(CurrentWorld, fmt.Sprintf("axis_%v", i))
					if ax1 != nil {
						ax1.Text = fmt.Sprintf("%v", xdeflect)
					}

					ax2 := FindBox(CurrentWorld, fmt.Sprintf("axis_%v", i+1))
					if ax2 != nil {
						ax2.Text = fmt.Sprintf("%v", ydeflect)
					}

				}
			}
		}

		// Make a random number between 0 and 255
		// r := uint8(rand.Intn(255))

		//Clear the screen
		drawBox(0, 0, float64(WinWidth), float64(WinHeight), CurrentTheme.WorldBackground[0], CurrentTheme.WorldBackground[1], CurrentTheme.WorldBackground[2], CurrentTheme.WorldBackground[3])
		if use_threads {
			ready_for_render = false
			for !ready_for_render {
				if batterySaver {
					time.Sleep(100 * time.Millisecond)
					NeedRedraw()
				} else {
					time.Sleep(1 * time.Millisecond)
				}
			}
			fmt.Println("render")
		}

		// Update routines must set need_redraw after changing part of the screen
		if need_redraw {
			log.Printf("Redraw needed, drawing frame now")
			if !use_threads {
				draw(mouseX, mouseY, draw_action)
				draw_action = ""
			}

			gfxMain(win, state, transfer_texture, WinWidth, WinHeight)
			need_redraw = false
		} else {
			//log.Printf("Redraw not needed, skipping frame")
			if batterySaver {
				time.Sleep(100 * time.Millisecond)
				NeedRedraw()
			}
			time.Sleep(10 * time.Millisecond)
		}
		glfw.PollEvents()

		//time.Sleep(time.Millisecond * 30)

	}
	os.Exit(0)
}

func drawBox(x, y, w, h float64, r, g, b, a uint8) {
	// fmt.Println("Box at ", x, y, w, h)
	// fmt.Println("Colour ", r, g, b, a)
	glim.DrawBox(int(x), int(y), int(w), int(h), WinWidth, WinHeight, transfer_texture, color.RGBA{r, g, b, a})
}

func drawText(x, y float64, str string, size float64, r, g, b, a uint8) {
	f := glim.NewFormatter()
	f.FontSize = size
	f.Colour = &glim.RGBA{r, g, b, a}
	glim.RenderPara(f, int(x), int(y), 0, 0, WinWidth, WinHeight, WinWidth, WinHeight, 0, 0, transfer_texture, str, false, true, false)
}

func drawImage(x, y float64, src image.Image) {

	b := src.Bounds()
	m := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	fuckyou.Draw(m, m.Bounds(), src, b.Min, fuckyou.Src)
	//glim.DumpBuff(m.Pix, uint(m.Bounds().Dx()), uint(m.Bounds().Dy()))
	glim.PasteBytes(int(m.Bounds().Dx()), int(m.Bounds().Dy()), m.Pix, int(x), int(y), WinWidth, WinHeight, transfer_texture, true, false, true)
}
