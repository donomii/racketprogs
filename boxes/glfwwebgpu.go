//go:build webgpu
// +build webgpu

package boxes

import (
	"fmt"
	"image"
	"image/color"
	fuckyou "image/draw"
	"log"
	"os"
	"time"

	"github.com/donomii/glim"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/rajveermalviya/go-webgpu-examples/internal/glm"
	"github.com/rajveermalviya/go-webgpu/wgpu"
	wgpuext_glfw "github.com/rajveermalviya/go-webgpu/wgpuext/glfw"
)




var forceFallbackAdapter = os.Getenv("WGPU_FORCE_FALLBACK_ADAPTER") == "1"

func init() {
	runtime.LockOSThread()

	switch os.Getenv("WGPU_LOG_LEVEL") {
	case "OFF":
		wgpu.SetLogLevel(wgpu.LogLevel_Off)
	case "ERROR":
		wgpu.SetLogLevel(wgpu.LogLevel_Error)
	case "WARN":
		wgpu.SetLogLevel(wgpu.LogLevel_Warn)
	case "INFO":
		wgpu.SetLogLevel(wgpu.LogLevel_Info)
	case "DEBUG":
		wgpu.SetLogLevel(wgpu.LogLevel_Debug)
	case "TRACE":
		wgpu.SetLogLevel(wgpu.LogLevel_Trace)
	}
}

type Vertex struct {
	pos      [4]float32
	texCoord [2]float32
}

var VertexBufferLayout = wgpu.VertexBufferLayout{
	ArrayStride: uint64(unsafe.Sizeof(Vertex{})),
	StepMode:    wgpu.VertexStepMode_Vertex,
	Attributes: []wgpu.VertexAttribute{
		{
			Format:         wgpu.VertexFormat_Float32x4,
			Offset:         0,
			ShaderLocation: 0,
		},
		{
			Format:         wgpu.VertexFormat_Float32x2,
			Offset:         4 * 4,
			ShaderLocation: 1,
		},
	},
}

func vertex(pos1, pos2, pos3, tc1, tc2 float32) Vertex {
	return Vertex{
		pos:      [4]float32{pos1, pos2, pos3, 1},
		texCoord: [2]float32{tc1, tc2},
	}
}

var vertexData = [...]Vertex{
	// top (0, 0, 1)
	vertex(-1, -1, 1, 0, 0),
	vertex(1, -1, 1, 1, 0),
	vertex(1, 1, 1, 1, 1),
	vertex(-1, 1, 1, 0, 1),
	// bottom (0, 0, -1)
	vertex(-1, 1, -1, 1, 0),
	vertex(1, 1, -1, 0, 0),
	vertex(1, -1, -1, 0, 1),
	vertex(-1, -1, -1, 1, 1),
	// right (1, 0, 0)
	vertex(1, -1, -1, 0, 0),
	vertex(1, 1, -1, 1, 0),
	vertex(1, 1, 1, 1, 1),
	vertex(1, -1, 1, 0, 1),
	// left (-1, 0, 0)
	vertex(-1, -1, 1, 1, 0),
	vertex(-1, 1, 1, 0, 0),
	vertex(-1, 1, -1, 0, 1),
	vertex(-1, -1, -1, 1, 1),
	// front (0, 1, 0)
	vertex(1, 1, -1, 1, 0),
	vertex(-1, 1, -1, 0, 0),
	vertex(-1, 1, 1, 0, 1),
	vertex(1, 1, 1, 1, 1),
	// back (0, -1, 0)
	vertex(1, -1, 1, 0, 0),
	vertex(-1, -1, 1, 1, 0),
	vertex(-1, -1, -1, 1, 1),
	vertex(1, -1, -1, 0, 1),
}

var indexData = [...]uint16{
	0, 1, 2, 2, 3, 0, // top
	4, 5, 6, 6, 7, 4, // bottom
	8, 9, 10, 10, 11, 8, // right
	12, 13, 14, 14, 15, 12, // left
	16, 17, 18, 18, 19, 16, // front
	20, 21, 22, 22, 23, 20, // back
}

const texelsSize = 256

func createTexels() (texels [texelsSize * texelsSize]uint8) {
	for id := 0; id < (texelsSize * texelsSize); id++ {
		cx := 3.0*float32(id%texelsSize)/float32(texelsSize-1) - 2.0
		cy := 2.0*float32(id/texelsSize)/float32(texelsSize-1) - 1.0
		x, y, count := float32(cx), float32(cy), uint8(0)
		for count < 0xFF && x*x+y*y < 4.0 {
			oldX := x
			x = x*x - y*y + cx
			y = 2.0*oldX*y + cy
			count += 1
		}
		texels[id] = count
	}

	return texels
}

func generateMatrix(aspectRatio float32) glm.Mat4[float32] {
	projection := glm.PerspectiveRH(math.Pi/4, aspectRatio, 1, 10)
	view := glm.LookAtRH(
		glm.Vec3[float32]{1.5, -5, 3},
		glm.Vec3[float32]{0, 0, 0},
		glm.Vec3[float32]{0, 0, 1},
	)

	return projection.Mul4(view)
}

//go:embed shader.wgsl
var shader string

type State struct {
	surface    *wgpu.Surface
	swapChain  *wgpu.SwapChain
	device     *wgpu.Device
	queue      *wgpu.Queue
	config     *wgpu.SwapChainDescriptor
	vertexBuf  *wgpu.Buffer
	indexBuf   *wgpu.Buffer
	uniformBuf *wgpu.Buffer
	pipeline   *wgpu.RenderPipeline
	bindGroup  *wgpu.BindGroup
}

func InitState(window *glfw.Window) (s *State, err error) {
	defer func() {
		if err != nil {
			s.Destroy()
			s = nil
		}
	}()
	s = &State{}

	instance := wgpu.CreateInstance(nil)
	defer instance.Release()

	s.surface = instance.CreateSurface(wgpuext_glfw.GetSurfaceDescriptor(window))

	adapter, err := instance.RequestAdapter(&wgpu.RequestAdapterOptions{
		ForceFallbackAdapter: forceFallbackAdapter,
		CompatibleSurface:    s.surface,
	})
	if err != nil {
		return s, err
	}
	defer adapter.Release()

	s.device, err = adapter.RequestDevice(nil)
	if err != nil {
		return s, err
	}
	s.queue = s.device.GetQueue()

	caps := s.surface.GetCapabilities(adapter)

	width, height := window.GetSize()
	s.config = &wgpu.SwapChainDescriptor{
		Usage:       wgpu.TextureUsage_RenderAttachment,
		Format:      caps.Formats[0],
		Width:       uint32(width),
		Height:      uint32(height),
		PresentMode: wgpu.PresentMode_Fifo,
		AlphaMode:   caps.AlphaModes[0],
	}

	s.swapChain, err = s.device.CreateSwapChain(s.surface, s.config)
	if err != nil {
		return s, err
	}

	s.vertexBuf, err = s.device.CreateBufferInit(&wgpu.BufferInitDescriptor{
		Label:    "Vertex Buffer",
		Contents: wgpu.ToBytes(vertexData[:]),
		Usage:    wgpu.BufferUsage_Vertex,
	})
	if err != nil {
		return s, err
	}

	s.indexBuf, err = s.device.CreateBufferInit(&wgpu.BufferInitDescriptor{
		Label:    "Index Buffer",
		Contents: wgpu.ToBytes(indexData[:]),
		Usage:    wgpu.BufferUsage_Index,
	})
	if err != nil {
		return s, err
	}

	texels := createTexels()
	textureExtent := wgpu.Extent3D{
		Width:              texelsSize,
		Height:             texelsSize,
		DepthOrArrayLayers: 1,
	}
	texture, err := s.device.CreateTexture(&wgpu.TextureDescriptor{
		Size:          textureExtent,
		MipLevelCount: 1,
		SampleCount:   1,
		Dimension:     wgpu.TextureDimension_2D,
		Format:        wgpu.TextureFormat_R8Uint,
		Usage:         wgpu.TextureUsage_TextureBinding | wgpu.TextureUsage_CopyDst,
	})
	if err != nil {
		return s, err
	}
	defer texture.Release()

	textureView, err := texture.CreateView(nil)
	if err != nil {
		return s, err
	}
	defer textureView.Release()

	s.queue.WriteTexture(
		texture.AsImageCopy(),
		wgpu.ToBytes(texels[:]),
		&wgpu.TextureDataLayout{
			Offset:       0,
			BytesPerRow:  texelsSize,
			RowsPerImage: wgpu.CopyStrideUndefined,
		},
		&textureExtent,
	)

	mxTotal := generateMatrix(float32(s.config.Width) / float32(s.config.Height))
	s.uniformBuf, err = s.device.CreateBufferInit(&wgpu.BufferInitDescriptor{
		Label:    "Uniform Buffer",
		Contents: wgpu.ToBytes(mxTotal[:]),
		Usage:    wgpu.BufferUsage_Uniform | wgpu.BufferUsage_CopyDst,
	})
	if err != nil {
		return s, err
	}

	shader, err := s.device.CreateShaderModule(&wgpu.ShaderModuleDescriptor{
		Label:          "shader.wgsl",
		WGSLDescriptor: &wgpu.ShaderModuleWGSLDescriptor{Code: shader},
	})
	if err != nil {
		return s, err
	}
	defer shader.Release()

	s.pipeline, err = s.device.CreateRenderPipeline(&wgpu.RenderPipelineDescriptor{
		Vertex: wgpu.VertexState{
			Module:     shader,
			EntryPoint: "vs_main",
			Buffers:    []wgpu.VertexBufferLayout{VertexBufferLayout},
		},
		Fragment: &wgpu.FragmentState{
			Module:     shader,
			EntryPoint: "fs_main",
			Targets: []wgpu.ColorTargetState{
				{
					Format:    s.config.Format,
					Blend:     nil,
					WriteMask: wgpu.ColorWriteMask_All,
				},
			},
		},
		Primitive: wgpu.PrimitiveState{
			Topology:  wgpu.PrimitiveTopology_TriangleList,
			FrontFace: wgpu.FrontFace_CCW,
			CullMode:  wgpu.CullMode_Back,
		},
		DepthStencil: nil,
		Multisample: wgpu.MultisampleState{
			Count:                  1,
			Mask:                   0xFFFFFFFF,
			AlphaToCoverageEnabled: false,
		},
	})
	if err != nil {
		return s, err
	}

	bindGroupLayout := s.pipeline.GetBindGroupLayout(0)
	defer bindGroupLayout.Release()

	s.bindGroup, err = s.device.CreateBindGroup(&wgpu.BindGroupDescriptor{
		Layout: bindGroupLayout,
		Entries: []wgpu.BindGroupEntry{
			{
				Binding: 0,
				Buffer:  s.uniformBuf,
				Size:    wgpu.WholeSize,
			},
			{
				Binding:     1,
				TextureView: textureView,
				Size:        wgpu.WholeSize,
			},
		},
	})
	if err != nil {
		return s, err
	}

	return s, nil
}

func (s *State) Resize(width, height int) {
	if width > 0 && height > 0 {
		s.config.Width = uint32(width)
		s.config.Height = uint32(height)

		mxTotal := generateMatrix(float32(width) / float32(height))
		s.queue.WriteBuffer(s.uniformBuf, 0, wgpu.ToBytes(mxTotal[:]))

		if s.swapChain != nil {
			s.swapChain.Release()
		}
		var err error
		s.swapChain, err = s.device.CreateSwapChain(s.surface, s.config)
		if err != nil {
			panic(err)
		}
	}
}

func (s *State) Render() error {
	nextTexture, err := s.swapChain.GetCurrentTextureView()
	if err != nil {
		return err
	}
	defer nextTexture.Release()

	encoder, err := s.device.CreateCommandEncoder(nil)
	if err != nil {
		return err
	}
	defer encoder.Release()

	renderPass := encoder.BeginRenderPass(&wgpu.RenderPassDescriptor{
		ColorAttachments: []wgpu.RenderPassColorAttachment{
			{
				View:       nextTexture,
				LoadOp:     wgpu.LoadOp_Clear,
				StoreOp:    wgpu.StoreOp_Store,
				ClearValue: wgpu.Color{R: 0.1, G: 0.2, B: 0.3, A: 1.0},
			},
		},
	})
	defer renderPass.Release()

	renderPass.SetPipeline(s.pipeline)
	renderPass.SetBindGroup(0, s.bindGroup, nil)
	renderPass.SetIndexBuffer(s.indexBuf, wgpu.IndexFormat_Uint16, 0, wgpu.WholeSize)
	renderPass.SetVertexBuffer(0, s.vertexBuf, 0, wgpu.WholeSize)
	renderPass.DrawIndexed(uint32(len(indexData)), 1, 0, 0, 0)
	renderPass.End()

	cmdBuffer, err := encoder.Finish(nil)
	if err != nil {
		return err
	}
	defer cmdBuffer.Release()

	s.queue.Submit(cmdBuffer)
	s.swapChain.Present()

	return nil
}

func (s *State) Destroy() {
	if s.bindGroup != nil {
		s.bindGroup.Release()
		s.bindGroup = nil
	}
	if s.pipeline != nil {
		s.pipeline.Release()
		s.pipeline = nil
	}
	if s.uniformBuf != nil {
		s.uniformBuf.Release()
		s.uniformBuf = nil
	}
	if s.indexBuf != nil {
		s.indexBuf.Release()
		s.indexBuf = nil
	}
	if s.vertexBuf != nil {
		s.vertexBuf.Release()
		s.vertexBuf = nil
	}
	if s.swapChain != nil {
		s.swapChain.Release()
		s.swapChain = nil
	}
	if s.config != nil {
		s.config = nil
	}
	if s.queue != nil {
		s.queue.Release()
		s.queue = nil
	}
	if s.device != nil {
		s.device.Release()
		s.device = nil
	}
	if s.surface != nil {
		s.surface.Release()
		s.surface = nil
	}
}


var (
	win   *glfw.Window
	state *State

	ed               *GlobalConfig // The text editor
	transfer_texture []uint8       // The texture buffer to transfer to the GPU

	form      *glim.FormatParams
	WinWidth  = 1280
	WinHeight = 800

	displayPicPath string
)

func DrawWidth() float64 {
	return float64(WinWidth)
}

func DrawHeight() float64 {
	return float64(WinHeight)
}

func InitGraphics() {
	WinWidth = 1280
	WinHeight = 800
	if err := glfw.Init(); err != nil {
		panic(err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.ClientAPI, glfw.NoAPI)
	window, err := glfw.CreateWindow(WinWidth, Win, "go-webgpu with glfw", nil, nil)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	s, err := InitState(window)
	if err != nil {
		panic(err)
	}
	defer s.Destroy()

	window.SetSizeCallback(func(w *glfw.Window, width, height int) {
		s.Resize(width, height)
	})



	win, state = gfxStart(1280, 800, "test", false)
	transfer_texture = make([]uint8, 8000*8000*4)

	ed = NewEditor()
	// Create a text formatter.  This controls the appearance of the text, e.g. colour, size, layout
	form = glim.NewFormatter()
	ed.ActiveBuffer.Formatter = form
	SetFont(ed.ActiveBuffer, DefaultFontSize)

	handleKeys(win)
	handleMouse(win)
	handleMouseMove(win)
	handleFocus(win)
	handleScroll(win)

	win.SetSizeCallback(func(w *glfw.Window, width int, height int) {
		WinWidth = width
		WinHeight = height
	})

	glfw.SetJoystickCallback(func(joy glfw.Joystick, event glfw.PeripheralEvent) {

		fmt.Println("Joystick ", joy, " event ", event)
		//fmt.Printf("Axes: %+v, hats: %+v, buttons: %+v\n", joy.GetAxes(), joy.GetButtons(), joy.GetHats())
	})

	Register("Global", "Toggle Fullscreen", "ToggleFullscreen", func(name, id string, args interface{}) {
		if win.GetMonitor() == nil {
			win.SetMonitor(glfw.GetPrimaryMonitor(), 0, 0, 1280, 800, 30)
		} else {
			win.SetMonitor(nil, 0, 0, 1280, 800, 30)
		}

	})

}

func equalArray(arr []float64) bool {
	for i := 1; i < len(arr); i++ {
		if arr[i] != arr[0] {
			return false
		}
	}
	return true
}

var latches = make(map[int]bool)

func StartMain() {
	for !win.ShouldClose() {



		//Clear the screen
		drawBox(0, 0, float64(WinWidth), float64(WinHeight), CurrentTheme.WorldBackground[0], CurrentTheme.WorldBackground[1], CurrentTheme.WorldBackground[2], CurrentTheme.WorldBackground[3])
		if use_threads {
			ready_for_render = false
			for !ready_for_render {
				if batterySaver {
					time.Sleep(100 * time.Millisecond)
					NeedRedraw()
				} else {
					time.Sleep(1 * time.Millisecond)
				}
			}
			fmt.Println("render")
		}

		// Update routines must set need_redraw after changing part of the screen
		if need_redraw {
			log.Printf("Redraw needed, drawing frame now")
			if !use_threads {
				draw(mouseX, mouseY, draw_action)
				draw_action = ""
			}

			gfxMain(win, state, transfer_texture, WinWidth, WinHeight)
			need_redraw = false
		} else {
			//log.Printf("Redraw not needed, skipping frame")
			if batterySaver {
				time.Sleep(100 * time.Millisecond)
				NeedRedraw()
			}
			time.Sleep(10 * time.Millisecond)
		}
		glfw.PollEvents()

		//time.Sleep(time.Millisecond * 30)

	}
	os.Exit(0)
}

func drawBox(x, y, w, h float64, r, g, b, a uint8) {
	// fmt.Println("Box at ", x, y, w, h)
	// fmt.Println("Colour ", r, g, b, a)
	glim.DrawBox(int(x), int(y), int(w), int(h), WinWidth, WinHeight, transfer_texture, color.RGBA{r, g, b, a})
}

func drawText(x, y float64, str string, size float64, r, g, b, a uint8) {
	f := glim.NewFormatter()
	f.FontSize = size
	f.Colour = &glim.RGBA{r, g, b, a}
	glim.RenderPara(f, int(x), int(y), 0, 0, WinWidth, WinHeight, WinWidth, WinHeight, 0, 0, transfer_texture, str, false, true, false)
}

func drawImage(x, y float64, src image.Image) {

	b := src.Bounds()
	m := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	fuckyou.Draw(m, m.Bounds(), src, b.Min, fuckyou.Src)
	//glim.DumpBuff(m.Pix, uint(m.Bounds().Dx()), uint(m.Bounds().Dy()))
	glim.PasteBytes(int(m.Bounds().Dx()), int(m.Bounds().Dy()), m.Pix, int(x), int(y), WinWidth, WinHeight, transfer_texture, true, false, true)
}
