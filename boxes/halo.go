package boxes

import "fmt"

//The halo is a ring of boxes around the selected box.  They provide capabilities to edit
// and configure the selected box, like resize, edit contents, ...

//To limit the number of callback types, there is only one callback type, for handling all user interactions (messages).

//The callback function is called with the action, and the local coordinates of the mouse pointer.  So almost every
// callback function will have a switch statement to handle the different actions.
// The callback function returns a boolean, indicating if the event was handled or not.  If the event was not handled (false), the then default
// handler is also called.  Callbacks are welcome to do some work and also return false, to let the default handler add its own work later.

// There is also a draw callback, separate to the action callback, because it will be called more often.  This is usually nil
// unless you want to do all the work of drawing the box yourself.  The draw callback is called before the default drawing
// callback, and again you can do some drawing then hand over to the default drawing callback by returning false.

// Creates the boxes for the halo, and sets up the callbacks.
func CreateHalo(w *World_s) {

	halo := Box{Text: "Halo", Id: "Halo", X: 350, Y: 350, W: 10, H: 10, Scale: 1.0, PinToScreen: true, SplitLayout: "free", SplitRatio: 0.5, Draggable: false, Intangible: true, Invisible: true, Data: nil}

	halo.DrawCallback = Halo_locate_drawcallback
	halo.DrawCallbackName = "Halo_locate_drawcallback"

	resize := Box{Text: "Resize", Id: "Halo_resize", X: 450, Y: 350, W: 10, H: 10, Scale: 1.0, PinToScreen: true, Draggable: true}
	resize.Callback = Halo_resize_callback
	resize.CallbackName = "Halo_resize_callback"
	resize.DrawCallback = Halo_resize_drawcallback
	resize.DrawCallbackName = "Halo_resize_drawcallback"

	Add(w, &halo,  &resize)

	halo_close := Box{Text: "halo_close", Id: "Halo_close", X: 450, Y: 350, W: 10, H: 10, Scale: 1.0, PinToScreen: true, Draggable: false}
	halo_close.Callback = Halo_close_callback
	halo_close.CallbackName = "Halo_close_callback"
	halo_close.DrawCallback = Halo_close_drawcallback
	halo_close.DrawCallbackName = "Halo_close_drawcallback"

	Add(w, &halo,  &halo_close)

	pinbox := CreateBoxWithCallbacks(w, "Pin", "Halo_pin", 450, 350, 10, 10, "Halo_pin_callback", "Halo_pin_drawcallback")
	Add(w, &halo,  pinbox)

	w.Boxes.Store(halo.Id, &halo)

}

func CreateBoxWithCallbacks(w *World_s, txt, id string, x, y, width, h float64, cb_name, draw_cb_name string) *Box {
	box := Box{Text: txt, Id: id, X: x, Y: y, W: width, H: h, Scale: 1.0, PinToScreen: true, Draggable: false}
	box.DrawCallback = RenderCallbacks[draw_cb_name]
	box.DrawCallbackName = draw_cb_name
	box.Callback = EventCallbacks[cb_name]
	box.CallbackName = cb_name
	w.Boxes.Store(box.Id, &box)
	fmt.Printf("CreateBoxWithCallbacks %+v\n", box)
	return &box
}

func Halo_locate_drawcallback(w *World_s, box, parent *Box, localX, localY float64) bool {
	if box.Data == nil {
		return false
	}
	target := box.Data.(*Box)
	box.X = target.ScreenX - box.W
	box.Y = target.ScreenY - box.H
	return false
}

func Halo_resize_drawcallback(w *World_s, box, parent *Box, localX, localY float64) bool {
	if parent == nil {
		return false
	}
	if parent.Data == nil {
		return false
	}
	target := parent.Data.(*Box)

	box.X = target.ScreenX + target.ScreenW
	box.Y = target.ScreenY + target.ScreenH

	return false
}

func Halo_resize_callback(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	if action == "dragging" {
		fmt.Println("Halo_resize_callback", action)
		if FindBox(w, "Halo").Data == nil {
			return false
		}

		target := FindBox(w, "Halo").Data.(*Box)
		if target.PinToScreen {
			target.W = ( box.ScreenX - target.ScreenX)
			target.H = (box.ScreenY - target.ScreenY)
		} else {
			target.W = (box.ScreenX - target.ScreenX) / w.Scale
			target.H = (box.ScreenY - target.ScreenY) / w.Scale
		}
		clearScaledPictureCache()
		return true
	}
	return false
}

func Halo_close_drawcallback(w *World_s, box, parent *Box, localX, localY float64) bool {
	if parent.Data == nil {
		return false
	}
	target := parent.Data.(*Box)

	box.X = target.ScreenX + target.ScreenW
	box.Y = target.ScreenY - box.ScreenH

	return false
}

func Halo_close_callback(w *World_s, box, parent *Box, action string, localX, localY float64) bool {
	fmt.Println("Halo_close_callback", action)
	if action == "click" {
		HaloMode = false
		return true
	}
	return false
}

func Halo_pin_drawcallback(w *World_s, box, parent *Box, localX, localY float64) bool {
	if parent.Data == nil {
		return false
	}
	target := parent.Data.(*Box)

	box.X = target.ScreenX + target.ScreenW
	box.Y = target.ScreenY + target.ScreenH/2.0

	return false
}

func Halo_pin_callback(w *World_s, box, parent *Box, action string, localX, localY float64) bool {

	parent = ParentOf(box.Id)
	if action == "click" {
		if parent.Data == nil {
			return false
		}
		target := parent.Data.(*Box)
		if !target.PinToScreen {
			target.PinToScreen = true
			target.X = target.ScreenX
			target.Y = target.ScreenY
			target.W = target.ScreenW
			target.H = target.ScreenH
			//target.LocalScale = 1.0
		} else {
			//FIXME translate back to parent coordinates
			//For now, drop on top of parent
			target.X = parent.X + 10
			target.Y = parent.Y + 10
			target.W = target.ScreenW / w.Scale
			target.H = target.ScreenH / w.Scale
			target.PinToScreen = !target.PinToScreen
		}
		return true
	}
	return false
}
