package boxes


type Theme_s struct {
	WorldBackground []uint8
	//WorldBackgroundImage string
	ButtonBackground []uint8
	TrimColour	[]uint8
	TextColour []uint8
	SelectColour []uint8
	SelectTrimColour []uint8
}

var CurrentTheme Theme_s = Theme_s{
	WorldBackground: []uint8{117, 67, 228, 255},
	//WorldBackgroundImage: "data/images/wood.jpg",
	ButtonBackground: []uint8{179, 255, 227, 255},
	TrimColour: []uint8{255, 255, 255, 255},
	TextColour: []uint8{1, 1, 1, 255},
	SelectColour: []uint8{255, 237, 127, 255},
	SelectTrimColour: []uint8{255, 237, 127, 255},
}