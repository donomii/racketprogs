package main

import (
	boxes "gitlab.com/donomii/racketprogs/boxes"

)


func setupWorlds() {
	TextWorld := make_text_world()
	boxes.RegisterWorld(TextWorld)

	PicGrid := make_pic_grid("")
	boxes.RegisterWorld(PicGrid)


	boxes.CurrentWorld = PicGrid
}

func main() {



	boxes.StartBoxes(func () {
		setupWorlds()
	})
}
