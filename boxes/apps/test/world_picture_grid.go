package main

import (
	"io/ioutil"
	"log"
	"math"
	"os"
	"strings"

	. "gitlab.com/donomii/racketprogs/boxes"
	boxes "gitlab.com/donomii/racketprogs/boxes"
)

func make_pic_grid(prompt string) *World_s {
	PicGridWorld := NewWorld()
	PicGridWorld.Scale = 1.5
	PicGridWorld.Id = "PicGridWorld"
	boxes.CurrentWorld = PicGridWorld
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: boxes.DefaultCallback}
	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}

	Add(PicGridWorld, PicGridWorld.Top, &debug_message_display_box)

	Add(PicGridWorld, PicGridWorld.Top, &NextWorld)
	CreateHalo(PicGridWorld) //FIXME

	//Find all files in the current directory starting with prompt
	//Start by getting a list of all files in the current directory
	//Then filter the list to only include files that start with prompt
	//Then create a grid of boxes with the filtered list of files
	//Then add the grid to the world
	//Then add the world to the world stack

	//Get a list of all files in the current directory
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}
	//Filter the list of files to only include files that start with prompt
	var filtered_files []os.FileInfo
	for _, f := range files {
		if strings.HasSuffix(f.Name(), "png") || strings.HasSuffix(f.Name(), "jpg") || strings.HasSuffix(f.Name(), "jpeg") {
			filtered_files = append(filtered_files, f)
		}
	}

	psize := float64(128)

	grid := Box{Text: "Picture Grid", Id: "picture_grid", X: 0, Y: 0, W: 400, H: 400, Scale: 1.0, PinToScreen: false, SplitLayout: "free"}

	// sides of grid are sqrt(len(files))

	side := math.Sqrt(float64(len(files)))
	for i, file := range files {
		fname := file.Name()
		x := math.Mod(float64(i), side) * psize
		y := float64(int(float64(i)/side)) * psize

		b := Box{Text: "", Id: "picture_box_" + fname, X: x, Y: y, W: psize, H: psize, Scale: 1.0, PinToScreen: false, Colour: [4]uint8{64, 64, 64, 255}}
		b.DrawCallback = RenderPicture
		b.DrawCallbackName = "RenderPicture"

		Add(PicGridWorld, &grid, &b)
		SetFilePath(b.Id, fname)

	}

	/*
	Add(PicGridWorld, PicGridWorld.Top, &grid)
	Register(PicGridWorld.Id, "Down", "World previous box", func(name, id string, args interface{}) {
		selected_box := FindBox(boxes.CurrentWorld, boxes.CurrentWorld.Selected)
		center_on(PicGridWorld, selected_box)
	})
	Add(PicGridWorld, PicGridWorld.Top, &grid)
	Register(PicGridWorld.Id, "Up", "World next box", func(name, id string, args interface{}) {
		selected_box := FindBox(boxes.CurrentWorld, boxes.CurrentWorld.Selected)
		center_on(PicGridWorld, selected_box)
	})
	*/

	Add(PicGridWorld, PicGridWorld.Top, &grid)
	Register(PicGridWorld.Id, "Select", "Center when a picture is selected", func(name, id string, args interface{}) {
		selected_box := FindBox(boxes.CurrentWorld, boxes.CurrentWorld.Selected)
		center_on(PicGridWorld, selected_box)
	})

	return PicGridWorld
}
