package main

import (
	"fmt"

	"golang.org/x/image/font/basicfont"
	b  "gitlab.com/donomii/racketprogs/boxes"

)

func center_on(w *b.World_s, selected_box *b.Box) {
	//Move the world.top so that this box is in the center of the screen
	//Find the distance to the center of the screen
	ydiff := selected_box.ScreenY - float64(b.WinHeight/2)
	//Shift world.top by that distance, scaled by the zoom factor
	b.WorldOf(w.Id).Top.Y -= ydiff / b.WorldOf(w.Id).Scale

	//Find the distance to the center of the screen
	xdiff := selected_box.ScreenX - float64(b.WinWidth/2)
	//Shift world.top by that distance, scaled by the zoom factor
	b.WorldOf(w.Id).Top.X -= xdiff / b.WorldOf(w.Id).Scale

	b.WorldOf(w.Id).Top.Y -= selected_box.H / 2
	b.WorldOf(w.Id).Top.X -= selected_box.W / 2

	b.NeedRedraw()
}

func make_text_world() *b.World_s {
	debug_message_display_box := b.Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: b.DefaultCallback}

	NextWorld := b.Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}

	dw := b.NewWorld()
	w := dw
	dw.Id = "TextWorld"
	TextWorld := dw
	b.CurrentWorld = TextWorld
	cw := b.CurrentWorld
	TextWorld.Scale = 1.0

	//register_global_message_handlers(TextWorld)
	b.Register(w.Id, "Down", "World next box", func(name, id string, args interface{}) {
		b.AttemptSelectNextSibling(cw, cw.Selected)
		b.SendMessage(cw.Id, "select", b.FindBox(cw, cw.Selected))
		b.SendMessage(cw.Id, cw.Selected, "select")
		fmt.Printf("Selected: %v\n", cw.Selected)
		b.SetDebug("Selected: " + cw.Selected)

		selected_box := b.FindBox(cw, cw.Selected)
		center_on(w, selected_box)
	})

	b.Register(w.Id, "Up", "World previous box", func(name, id string, args interface{}) {
		curr := w.Selected
		b.AttemptSelectPreviousSibling(cw, cw.Selected)
		b.SendMessage(cw.Id, "select", b.FindBox(cw, cw.Selected))
		b.SendMessage(cw.Id, cw.Selected, "select")
		fmt.Printf("Selected: %v\n", cw.Selected)
		b.SetDebug("Selected: " + cw.Selected)
		fmt.Printf("Selected parent: %v for %v\n", cw.Selected, curr)
		center_on(w, b.FindBox(cw, cw.Selected))
		b.NeedRedraw()
	})

	b.Register(w.Id, "Right", "World first child", func(name, id string, args interface{}) {
		curr := w.Selected
		//Printf all the children of the selected box
		sel := b.FindBox(w, w.Selected)
		if sel == nil {
			fmt.Printf("No selection\n")
			return
		}

		//Iterate through the children and print their ids
		for _, child := range sel.Children {
			fmt.Printf("Child: %v\n", child.Id)
		}
		if w.Selected == "" {
			//There is no selection, so select the first box
			w.Selected = w.Top.Id
			fmt.Printf("No current selection, selecting %s\n", w.Selected)
		} else {
			sel := b.FindBox(w, w.Selected)
			if sel == nil {
				fmt.Printf("Error: box %v not found\n", w.Selected)
			}
			if len(sel.Children) > 0 {
				w.Selected = sel.Children[0].Id
			}
		}
		fmt.Printf("Selected child: %v for %v\n", cw.Selected, curr)

		b.SendMessage(cw.Id, "select", b.FindBox(cw, cw.Selected))
		b.SendMessage(cw.Id, cw.Selected, "select")
		fmt.Printf("Selected: %v\n", cw.Selected)
		b.SetDebug("Selected: " + cw.Selected)
		center_on(w, b.FindBox(cw, cw.Selected))
		b.NeedRedraw()
	})

	b.Add(TextWorld, TextWorld.Top, &debug_message_display_box)
	b.CreateHalo(TextWorld) //FIXME
	f := basicfont.Face7x13
	menu := b.Box{Text: "menu", Id: "menu", X: 0, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&menu, f, b.DefaultFontSize)
	menu.Callback = b.ToggleChildren
	menu.CallbackName = "ToggleChildren"
	menu.SplitLayout = "free"

	b.Add(TextWorld, TextWorld.Top, &menu)

	b.Add(TextWorld, TextWorld.Top, &NextWorld)

	save := b.Box{Text: "Save", Id: "save", X: 100, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&save, f, b.DefaultFontSize)
	save.Callback = b.SaveWorld
	save.CallbackName = "SaveWorld"

	b.Add(TextWorld, &menu, &save)

	load := b.Box{Text: "Load", Id: "Load", X: 200, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&load, f, b.DefaultFontSize)
	load.Callback = b.LoadWorld
	load.CallbackName = "LoadWorld"

	b.Add(TextWorld, &menu, &load)

	fps := b.Box{Text: "FPS", Id: "fps_box", X: 400, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&fps, f, b.DefaultFontSize)
	fps.DrawCallback = b.FpsRenderPassthrough
	fps.DrawCallbackName = "FpsRenderPassthrough"

	b.Add(TextWorld, &menu, &fps)

	quit := b.Box{Text: "Quit", Id: "quit_button", X: 300, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&quit, f, b.DefaultFontSize)
	quit.Callback = b.QuitCallback
	quit.CallbackName = "QuitCallback"

	b.Add(TextWorld, &menu, &quit)

	version := b.Box{Text: fmt.Sprintf("Version %v", b.Version), Id: "version_button", X: 450, Y: 50, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&version, f, b.DefaultFontSize)
	b.Add(TextWorld, &menu, &version)

	selecyBox := b.Box{Text: "Select Next", Id: "select_next_button", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&selecyBox, f, b.DefaultFontSize)
	selecyBox.Callback = b.SelectNextCallback
	selecyBox.CallbackName = "SelectNextCallback"
	b.Add(TextWorld, TextWorld.Top, &selecyBox)

	dumpTreeBox := b.Box{Text: "Dump Tree", Id: "dumpTreeBox", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	b.EncloseText(&dumpTreeBox, f, b.DefaultFontSize)
	dumpTreeBox.Callback = b.DumpTreeCallback
	dumpTreeBox.CallbackName = "DumpTreeCallback"
	b.Add(TextWorld, TextWorld.Top, &dumpTreeBox)

	codeNode := b.ParseAndDisplayFile(TextWorld, "boxes.go")

	b.Register(w.Id, "Left", "World parent box", func(name, id string, args interface{}) {
		curr := w.Selected
		if w.Selected == "" {
			//There is no selection, so select the first box
			w.Selected = codeNode.Id
			fmt.Printf("No current selection, selecting %s\n", w.Selected)
		} else {
			s := b.ParentOf(w.Selected)
			if s != nil && s.Id != codeNode.Id {
				w.Selected = s.Id
			}
		}
		fmt.Printf("Selected parent: %v for %v\n", cw.Selected, curr)
		b.SendMessage(cw.Id, "select", b.FindBox(cw, cw.Selected))
		b.SendMessage(cw.Id, cw.Selected, "select")
		fmt.Printf("Selected: %v\n", cw.Selected)
		b.SetDebug("Selected: " + cw.Selected)
		center_on(w, b.FindBox(cw, cw.Selected))
		b.NeedRedraw()
	})

	b.SetZoomIndex(TextWorld, 0)

	return TextWorld
}
