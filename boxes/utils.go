package boxes

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
)

// Handle a click on the refresh button
func RefreshDirBoxes(w *World_s, dirbox *Box, p *Box, action string, x float64, y float64) bool {
	x = 100
	y = 200

	NextBoxId = NextBoxId + 1000
	fmt.Printf("RefreshDirBoxes %+v %+v %+v %v %v %v\n", w, dirbox, p, action, x, y)
	if action == "click" || action == "regenerate" {
		dir := ""
		if dirbox.Data != nil {
			dir = dirbox.Data.(string)
		}
		if dir == "" {
			dir = "."
		}
		//Delete all the boxes in the directory box
		dirbox.Children = []*Box{}
		dirbox.DrawOrderList = []*Box{}

		fmt.Printf("Refreshing directory %s\n", dir)
		//Get the directory listing
		files, err := ioutil.ReadDir(string(dir))
		if err != nil {
			return false
		}

		fmt.Println("Files in directory", dir)

		//Sort the children by time added
		sort.Slice(files, func(i, j int) bool {

			return files[j].ModTime().Before(files[i].ModTime())
		})

		for _, file := range files {
			if file.IsDir() {
				Add(w, dirbox,  DirBoxes(x, y, dir+"/"+file.Name()))
			} else {
				AddBox(w, dirbox, x, y, file.Name(), nil)
				NextBoxId++

				
			}
			y = y + dirbox.H*3
		}
		return true
	}
	return false

}

// recurse through the given directory and return a box tree
// of the files and directories
func DirBoxes(x, y float64, dir string) *Box {

	dirbox := &Box{
		Text:         "Refresh directory",
		Id:           "dir_" + strconv.Itoa(NextBoxId),
		X:            x,
		Y:            y,
		W:            300,
		H:            200,
		Scale:        1.0,
		SplitLayout:  "free",
		SplitRatio:   0.5,
		Callback:     RefreshDirBoxes,
		CallbackName: "RefreshDirBoxes",
		Data:         dir,
		DynamicBox:   true,
	}

	NextBoxId++
	x = 100
	y = 200
	CurrentWorld.Boxes.Store(dirbox.Id, dirbox)

	RefreshDirBoxes(CurrentWorld, dirbox, nil, "click", x, y)

	return dirbox
}

// Load an image and try to convert it to RGBA
func LoadImage(filename string) image.Image {
	//Read image from filename and decode it
	imgFile, err := os.Open(filename)
	if err != nil {
		return nil
	}
	defer imgFile.Close()
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return nil
	}
	if img == nil {
		//fmt.Println("Failed to load image", filename)
	}
	return convertImageToRgb(img)
}

// Sets the default debug widget
func SetDebug(message string) {
	b := FindBox(CurrentWorld, "debug_message_display")
	if b != nil {
		b.Text = "Debug: " + message
		b.Text = "" //FIXME remove this
	}
}

// Attempt to select the next element in the world
func AttemptSelectNext(w *World_s, sel string) {
	// 1. If this box is selected, and there are children, select the first child
	// 2. If there are no children (leaf node), select the next sibling - find parent, move to next child
	// 4. If there are no remaining siblings, move to the parent, select the parent's next sibling

	//Dump parentComponents print id
	/*
		for k, v := range parentComponents {
			if v != nil {
				fmt.Printf("Parent of %v: %v\n", k, v.Id)
			}
		}
	*/

	if sel == "" {
		//There is no selection, so select the first box
		w.Selected = w.Top.Id
		fmt.Printf("No current selection, selecting %s\n", w.Selected)
	} else {
		thisBox := FindBox(w, sel)
		if thisBox == nil {
			// Box has been moved or deleted, so select the top box
			w.Selected = w.Top.Id
			fmt.Printf("Selected box %s has been deleted, selecting %s\n", sel, w.Selected)
		} else {

			//There is a selection, check for children
			if len(FindBox(CurrentWorld, sel).Children) > 0 {
				//There are children, so select the first child
				w.Selected = thisBox.Children[0].Id
				fmt.Printf("Selecting first child of %s, %s\n", sel, w.Selected)
			} else {
				//There are no children, so select the next sibling
				//Find the parent
				parent := ParentOf(sel)
				if parent == nil {
					//There is no parent, so select the top box
					w.Selected = w.Top.Id
					fmt.Printf("No parent of %s, selecting %s\n", sel, w.Selected)
				} else {
					//There is a parent, so select the next sibling
					//Find the index of the current box in the parent's children

					fmt.Printf("Searching parent %v for child %v\n", parent.Id, sel)
					index := -1
					for i, child := range parent.Children {
						if child.Id == sel {
							index = i
						}
					}
					fmt.Printf("Found child %v at index %v\n", sel, index)
					if index == -1 {
						//The current box is not in the parent's children, so select the top box
						w.Selected = w.Top.Id
						fmt.Printf("Current box %s is not in parent %s, selecting %s\n", sel, parent.Id, w.Selected)
					} else {
						//The current box is in the parent's children, so select the next sibling
						if index == len(parent.Children)-1 {
							//There is no next sibling, so recurse to the parent
							w.Selected = parent.Id
							fmt.Printf("No next sibling of %s, recursing to %s\n", sel, parent.Id)
							AttemptSelectNext(w, parent.Id)
						} else {
							//There is a next sibling, so select it
							w.Selected = parent.Children[index+1].Id
							fmt.Printf("Selecting next sibling of %s, %s\n", sel, w.Selected)
						}
					}
				}
			}
		}
	}

	SendMessage(w.Id, "Select", w.Selected)
}


// Attempt to select the next element in the world
func AttemptSelectNextSibling(w *World_s, sel string) {
	// 1. If this box is selected, and there are children, select the first child
	// 2. If there are no children (leaf node), select the next sibling - find parent, move to next child
	// 4. If there are no remaining siblings, move to the parent, select the parent's next sibling

	//Dump parentComponents print id
	/*
		for k, v := range parentComponents {
			if v != nil {
				fmt.Printf("Parent of %v: %v\n", k, v.Id)
			}
		}
	*/

	if sel == "" {
		//There is no selection, so select the first box
		w.Selected = w.Top.Id
		fmt.Printf("No current selection, selecting %s\n", w.Selected)
	} else {
		thisBox := FindBox(w, sel)
		if thisBox == nil {
			// Box has been moved or deleted, so select the top box
			w.Selected = w.Top.Id
			fmt.Printf("Selected box %s has been deleted, selecting %s\n", sel, w.Selected)
		} else {

			
				//There are no children, so select the next sibling
				//Find the parent
				parent := ParentOf(sel)
				if parent == nil {
					//There is no parent, so select the top box
					w.Selected = w.Top.Id
					fmt.Printf("No parent of %s, selecting %s\n", sel, w.Selected)
				} else {
					//There is a parent, so select the next sibling
					//Find the index of the current box in the parent's children

					fmt.Printf("Searching parent %v for child %v\n", parent.Id, sel)
					index := -1
					for i, child := range parent.Children {
						if child.Id == sel {
							index = i
						}
					}
					fmt.Printf("Found child %v at index %v\n", sel, index)
					if index == -1 {
						//The current box is not in the parent's children, so select the top box
						w.Selected = w.Top.Id
						fmt.Printf("Current box %s is not in parent %s, selecting %s\n", sel, parent.Id, w.Selected)
					} else {
						//The current box is in the parent's children, so select the next sibling
						if index == len(parent.Children)-1 {
							//There is no next sibling, so recurse to the parent
							w.Selected = parent.Id
							fmt.Printf("No next sibling of %s, recursing to %s\n", sel, parent.Id)
							AttemptSelectNext(w, parent.Id)
						} else {
							//There is a next sibling, so select it
							w.Selected = parent.Children[index+1].Id
							fmt.Printf("Selecting next sibling of %s, %s(%v)\n", sel, w.Selected, parent.Children[index+1].Text)
						}
					}
				}
			
		}
	}

	SendMessage(w.Id, "Select", w.Selected)
}


// Attempt to select the previous element on the page
func AttemptSelectPrevious(w *World_s, sel string) {
	// 1. If this box is selected, and there are children, select the first child
	// 2. If there are no children (leaf node), select the next sibling - find parent, move to next child
	// 4. If there are no remaining siblings, move to the parent, select the parent's next sibling

	//Dump parentComponents print id
	/*
		for k, v := range parentComponents {
			if v != nil {
				fmt.Printf("Parent of %v: %v\n", k, v.Id)
			}
		}
	*/

	if sel == "" {
		//There is no selection, so select the first box
		w.Selected = w.Top.Id
		fmt.Printf("No current selection, selecting %s\n", w.Selected)
	} else {
		thisBox := FindBox(w, sel)
		if thisBox == nil {
			// Box has been moved or deleted, so select the top box
			w.Selected = w.Top.Id
			fmt.Printf("Selected box %s has been deleted, selecting %s\n", sel, w.Selected)
		} else {

			//There is a selection, check for children
			if len(FindBox(CurrentWorld, sel).Children) > 0 {
				//There are children, so select the last child
				w.Selected = thisBox.Children[len(thisBox.Children)-1].Id
				fmt.Printf("Selecting first child of %s, %s\n", sel, w.Selected)
			} else {
				//There are no children, so select the next sibling
				//Find the parent
				parent := ParentOf(sel)
				if parent == nil {
					//There is no parent, so select the top box
					w.Selected = w.Top.Id
					fmt.Printf("No parent of %s, selecting %s\n", sel, w.Selected)
				} else {
					//There is a parent, so select the previous sibling
					//Find the index of the current box in the parent's children

					fmt.Printf("Searching parent %v for child %v\n", parent.Id, sel)
					index := -1
					for i, child := range parent.Children {
						if child.Id == sel {
							index = i
						}
					}
					fmt.Printf("Found child %v at index %v\n", sel, index)
					if index == -1 {
						//The current box is not in the parent's children, so select the top box
						w.Selected = w.Top.Id
						fmt.Printf("Current box %s is not in parent %s, selecting %s\n", sel, parent.Id, w.Selected)
					} else {
						//The current box is in the parent's children, so select the previous sibling
						if index == 0 {
							//There is no previous sibling, so recurse to the parent
							w.Selected = parent.Id
							fmt.Printf("No next sibling of %s, recursing to %s\n", sel, parent.Id)
							AttemptSelectPrevious(w, parent.Id)
						} else {
							//There is a previous sibling, so select it
							w.Selected = parent.Children[index-1].Id
							fmt.Printf("Selecting previous sibling of %s, %s\n", sel, w.Selected)
						}
					}
				}
			}
		}
	}

	SendMessage(w.Id, "Select", w.Selected)
}


// Attempt to select the previous element on the page
func AttemptSelectPreviousSibling(w *World_s, sel string) {
	// 1. If this box is selected, and there are children, select the first child
	// 2. If there are no children (leaf node), select the next sibling - find parent, move to next child
	// 4. If there are no remaining siblings, move to the parent, select the parent's next sibling

	//Dump parentComponents print id
	/*
		for k, v := range parentComponents {
			if v != nil {
				fmt.Printf("Parent of %v: %v\n", k, v.Id)
			}
		}
	*/

	if sel == "" {
		//There is no selection, so select the first box
		w.Selected = w.Top.Id
		fmt.Printf("No current selection, selecting %s\n", w.Selected)
	} else {
		thisBox := FindBox(w, sel)
		if thisBox == nil {
			// Box has been moved or deleted, so select the top box
			w.Selected = w.Top.Id
			fmt.Printf("Selected box %s has been deleted, selecting %s\n", sel, w.Selected)
		} else {

			 
				//There are no children, so select the next sibling
				//Find the parent
				parent := ParentOf(sel)
				if parent == nil {
					//There is no parent, so select the top box
					w.Selected = w.Top.Id
					fmt.Printf("No parent of %s, selecting %s\n", sel, w.Selected)
				} else {
					//There is a parent, so select the previous sibling
					//Find the index of the current box in the parent's children

					fmt.Printf("Searching parent %v for child %v\n", parent.Id, sel)
					index := -1
					for i, child := range parent.Children {
						if child.Id == sel {
							index = i
						}
					}
					fmt.Printf("Found child %v at index %v\n", sel, index)
					if index == -1 {
						//The current box is not in the parent's children, so select the top box
						w.Selected = w.Top.Id
						fmt.Printf("Current box %s is not in parent %s, selecting %s\n", sel, parent.Id, w.Selected)
					} else {
						//The current box is in the parent's children, so select the previous sibling
						if index == 0 {
							//There is no previous sibling, so recurse to the parent
							w.Selected = parent.Id
							fmt.Printf("No next sibling of %s, recursing to %s\n", sel, parent.Id)
							AttemptSelectPrevious(w, parent.Id)
						} else {
							//There is a previous sibling, so select it
							w.Selected = parent.Children[index-1].Id
							fmt.Printf("Selecting previous sibling of %s, %s\n", sel, w.Selected)
						}
					}
				}
			}
		
	}

	SendMessage(w.Id, "Select", w.Selected)
}

