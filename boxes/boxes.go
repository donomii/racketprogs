package boxes

import (
	"fmt"

	. "gitlab.com/donomii/racketprogs/autoparser"
	"github.com/donomii/goof"
)

var joystickHistory [][]float64
var joystickHistoryIndex int

//var joystickOffset []float64

var (
	NextBoxId int //Boxes are not required to use this, but all the automatic creation routins do
)

var zoomLevels []float64 = []float64{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.3, 3.6, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0}

type Box struct {
	// The text to display in the box
	Text string "json:\"text\""
	// A unique id for the box
	Id string "json:\"id\""
	// The position and size of the box, relative to the parent
	X           float64 "json:\"x\""
	Y           float64 "json:\"y\""
	W           float64 "json:\"w\""
	H           float64 "json:\"h\""
	ScreenX     float64 "json:\"screenx\""
	ScreenY     float64 "json:\"screeny\""
	ScreenW     float64 "json:\"screenw\""
	ScreenH     float64 "json:\"screenh\""
	SplitLayout string  "json:\"splitlayout\""
	SplitRatio  float64 "json:\"splitratio\""
	// The child boxes
	Children     []*Box                                                    "json:\"children\""
	CallbackName string                                                    "json:\"callbackname\""
	Callback     func(*World_s, *Box, *Box, string, float64, float64) bool `json:"-"`
	//Holds a pointer to the AST node that created this box
	AstNode *Node "json: \"astnode\""
	// The order in which to draw the children.
	DrawOrderList []*Box "json: \"draworderlist\""
	// The scale of the box, relative to the parent
	Scale float64 "json:\"scale\""
	// The transform needed to bring this into window coordinates
	LocalScale       float64                                           "json:\"localscale\""
	DrawCallbackName string                                            "json:\"drawcallbackname\""
	DrawCallback     func(*World_s, *Box, *Box, float64, float64) bool `json:"-"`
	//User defined data
	Data interface{} "json:\"-\""
	// Floats the box.  Coordinates then become window coordinates, and aren't transformed
	PinToScreen    bool     "json:\"pintoscreen\""
	DynamicBox     bool     "json:\"dynamicbox\""
	Draggable      bool     "json:\"draggable\""
	Invisible      bool     "json:\"visible\""
	Intangible     bool     "json:\"tangible\""
	Colour         [4]uint8 "json:\"colour\""
	DefaultMessage string   "json:\"defaultmessage\""
}

// Add a child to a parent box.  The child box  will be drawn after the parent box is drawn i.e. on top of the parent.
func Add(w *World_s, parent *Box, child *Box) {
	parent.Children = append(parent.Children, child)
	parent.DrawOrderList = append(parent.DrawOrderList, child)
	w.Boxes.Store(child.Id, child) //  For quick lookups by id
}

// Creates a box and adds it to the given parent.  The box uses default settings.
// Creates a box and adds it to the given parent. The box uses default settings.
func AddBox(w *World_s, parent *Box, x, y float64, text string, n *Node) *Box {

	b := &Box{
		Id:           fmt.Sprintf("testbox_%v", getNextBoxId()),
		Text:         text,
		X:            x,
		Y:            y,
		// Start with small default size - EncloseText will adjust
		W:            10,
		H:            10,
		Scale:        1.0,
		CallbackName: "DefaultCallback",
		Callback:     DefaultCallback,
		AstNode:      n,
		SplitLayout:  "free",
	}


	// Add minimum margins
	b.W += 10
	b.H += 10
	
	Add(w, parent, b)
	return b
}

// Finds id in an array of boxes, and moves it to the end of the array
// NB there is now a separate draw order list, the original children list is never altered
func MoveToEnd(id string, boxes []*Box) {
	for i, b := range boxes {
		if b.Id == id {
			boxes = append(boxes[:i], boxes[i+1:]...)
			boxes = append(boxes, b)
			return
		}
	}
}

// Recursively searches through box and all children and returns the first box with the given id
func SearchChildTrees(b *Box, id string) *Box {
	if b.Id == id {
		return b
	}
	for _, child := range b.Children {
		res := SearchChildTrees(child, id)
		if res != nil {
			return res
		}
	}
	return nil
}

// Searches the child list for a box with the given id and removes it.  Not recursive.
func RemoveChild(b *Box, id string) {
	for i, child := range b.Children {
		if child.Id == id {
			b.Children = append(b.Children[:i], b.Children[i+1:]...)
			return
		}
	}
}

// Recurse through the box tree and apply the callback to each box
func WalkBoxTree(t *Box, callback func(box *Box)) {
	callback(t)
	for _, z := range t.Children {
		WalkBoxTree(z, callback)
	}
}

// Dump the box tree in a vaguely readable way
func PrintBoxTree(t *Box, indent int, newlines bool) {
	for _, z := range t.Children {
		PrintIndent(indent, "tree:  ")
		fmt.Printf("%v\n", z.Id)

		PrintBoxTree(z, indent+2, true)

	}

}

func SetZoomIndex(w *World_s, i int) {
	w.ZoomIndex = i
	if w.ZoomIndex < 0 {
		w.ZoomIndex = 0
	}
	if w.ZoomIndex > len(zoomLevels)-1 {
		w.ZoomIndex = len(zoomLevels) - 1
	}
	w.Scale = zoomLevels[w.ZoomIndex]
}

func zoomStepOut(CurrentWorld *World_s) {
	SetZoomIndex(CurrentWorld, CurrentWorld.ZoomIndex+1)
	fmt.Printf("Zoomlevel %v, scale %v\n", CurrentWorld.ZoomIndex, CurrentWorld.Scale)
}

func zoomStepIn(CurrentWorld *World_s) {
	// clearScaledPictureCache()
	SetZoomIndex(CurrentWorld, CurrentWorld.ZoomIndex-1)
	fmt.Printf("Zoomlevel %v, scale %v\n", CurrentWorld.ZoomIndex, CurrentWorld.Scale)
}

//Rendering

// The boxes renderer is a simple recursive function that draws the boxes,
// with text and pictures.  Child boxes are draw on top of parent boxes, there
// is no z-ordering.

//It requires three functions to be defined:
// 1. DrawBox(x,y,w,h,box) - draw the box
// 2. DrawText(box) - draw the text in the box
// 3. DrawImage(box) - draw the image in the box

var (
	dragItem                     string
	clickTarget                  string
	dragStartX, dragStartY       float64
	ItemPosStartX, ItemPosStartY float64
)

func isBoxOnScreen(w *World_s, b *Box) bool {
	return b.ScreenX > -b.ScreenW && b.ScreenX < float64(WinWidth) && b.ScreenY > -b.ScreenH && b.ScreenY < float64(WinHeight)
}

func NeedRedraw() {
	need_redraw = true
}


// The main box draw function.  Takes the mouse position in window coordinates, and an extra
// action, such as    "press" or "release", "wheel up", "wheel down", etc
func draw(mouseX, mouseY float64, action string) {
	//fmt.Printf("mscrx mscry %v,%v %v\n", mouseX, mouseY, action)
	/*if scrollY > 1.0 {
		scrollY = 1.0
	}
	if scrollY < -1.0 {
		scrollY = -1.0
	}*/

	//Try to read a joystick
	/*
		//axes, _ := readJoystick(GlobalJoystik)
		//fmt.Printf("axes %+v buttons %+v\n", axes, buttons)

		if len(axes) > 1 {
			b := FindBox(CurrentWorld, "joybox2")
			b.X = b.X + float64(axes[0]*10)
			b.Y = b.Y + float64(axes[1]*10)
		}
	*/

	hoverTarget := RenderAll(mouseX, mouseY, 1.0)
	if len(hoverTarget) > 0 {
		//fmt.Printf("hover %v\n", hoverTarget[0])
	}

	fmt.Printf("action %v\n", action)
	switch action {
	case "press":
		defer NeedRedraw()

		fmt.Printf("Pressed at %v,%v\n", mouseX, mouseY)
		dragStartX = mouseX
		dragStartY = mouseY
		if len(hoverTarget) > 0 {
			clickTarget = hoverTarget[len(hoverTarget)-1]
		}
		dragItem = CurrentWorld.Top.Id

		if len(hoverTarget) > 0 {

			activeBox := FindBox(CurrentWorld, hoverTarget[len(hoverTarget)-1])

			if activeBox != nil {
				if EditMode || activeBox.Draggable {
					dragItem = hoverTarget[len(hoverTarget)-1]
					// FIXME? MoveToEnd(dragItem, World.Top.DrawOrderList)

				}
			}
		}
		dragItemBox := FindBox(CurrentWorld, dragItem)
		ItemPosStartX = dragItemBox.X
		ItemPosStartY = dragItemBox.Y
		need_redraw = true
	case "release":
		defer NeedRedraw()
		fmt.Printf("Released at %v,%v: %v\n", mouseX, mouseY, hoverTarget)
		// If the mouse has moved the minimum distance to not be a click
		if goof.AbsFloat64(mouseX-dragStartX) > 5 || goof.AbsFloat64(mouseY-dragStartY) > 5 {
			fmt.Printf("Dragged from %v,%v to %v,%v\n", dragStartX, dragStartY, mouseX, mouseY)
			// if it released over a box
			if len(hoverTarget) > 0 {
				ht := hoverTarget[len(hoverTarget)-1]
				fmt.Println("Dropping over:", ht)

				// Not released over the background
				if dragItem != CurrentWorld.Top.Id {
					hoverTargetBox := FindBox(CurrentWorld, ht)
					dragItemBox := FindBox(CurrentWorld, dragItem)
					// If we were able to find the box we are dropping onto
					if hoverTargetBox != nil {
						box := SearchChildTrees(hoverTargetBox, ht)
						fmt.Printf("Found box: %+v\n", box)
						if box != nil && box.Callback != nil {
							box.Callback(CurrentWorld, dragItemBox, hoverTargetBox, "drop", mouseX, mouseY)
						}

					}
				}
			}
			dragItem = ""

		} else {
			// It's a click
			fmt.Printf("\nClicked at %v,%v %v.  Existing clickTarget %v\n", mouseX, mouseY, hoverTarget, clickTarget)
			if dragItem != "" {
				dragItemBox := FindBox(CurrentWorld, dragItem)
				if dragItemBox != nil {
					fmt.Printf("Dragging: %+v\n", dragItemBox.Id)

					if EditMode {
						HaloMode = true
						FindBox(CurrentWorld, "Halo").Data = dragItemBox
						// Start editing ht
						CurrentWorld.CurrentEditor = NewEditor()
						CurrentWorld.CurrentEditBox = dragItemBox
						ActiveBufferInsert(CurrentWorld.CurrentEditor, CurrentWorld.CurrentEditBox.Text)
						fmt.Printf("Now editing %v\n", dragItem)
					}
					SendMessage(CurrentWorld.Id, "click", dragItemBox)
					SendMessage(CurrentWorld.Id, dragItemBox.Id, "click")
					SendMessage(CurrentWorld.Id, dragItemBox.DefaultMessage, dragItemBox)

				}
			}
			if len(hoverTarget) > 0 {
				ht := hoverTarget[len(hoverTarget)-1]
				//fmt.Println("Hovering over:", ht)

				activeBox := FindBox(CurrentWorld, ht)
				if activeBox != nil {

					fmt.Printf("Found box: %+v\n", activeBox.Id)
					if ht == clickTarget {
						SendMessage(CurrentWorld.Id, "click", activeBox)
						SendMessage(CurrentWorld.Id, activeBox.Id, "click")
						SendMessage(CurrentWorld.Id, activeBox.DefaultMessage, activeBox)
						if activeBox.Callback != nil {

							activeBox.Callback(CurrentWorld, activeBox, nil, "click", mouseX, mouseY)
						}
					}
				}
			}
		}
		dragItem = ""
		clickTarget = ""
		defer NeedRedraw()
	case "wheel":
		//clearScaledPictureCache()
		defer NeedRedraw()
	case "wheel down":
		zoomStepIn(CurrentWorld)
		defer NeedRedraw()
	case "wheel up":
		zoomStepOut(CurrentWorld)
		defer NeedRedraw()
	default:
		defer NeedRedraw()
		if dragItem != "" {
			//fmt.Printf("Dragging %v to %v,%v\n", dragItem, mouseX, mouseY)
			if len(hoverTarget) > 0 {
				// fmt.Println("Hovering over:", hoverTarget[0])
			}
			dragBox := FindBox(CurrentWorld, dragItem)
			if dragBox != nil {
				// box := Get(dragBox, dragItem)
				// if box != nil {
				if dragBox.PinToScreen {
					dragBox.X = ItemPosStartX + float64(mouseX-dragStartX)
					dragBox.Y = ItemPosStartY + float64(mouseY-dragStartY)

				} else {
					dragBox.X = ItemPosStartX + float64(mouseX-dragStartX)/CurrentWorld.Scale/dragBox.LocalScale
					dragBox.Y = ItemPosStartY + float64(mouseY-dragStartY)/CurrentWorld.Scale/dragBox.LocalScale
					//}
				}
				if dragBox.Callback != nil {
					dragBox.Callback(CurrentWorld, dragBox, nil, "dragging", mouseX, mouseY)
				}
			}

		}
		defer NeedRedraw()
	}
}

var deferredBoxes []func() []string

func applyDefaultColour(col, override []uint8) []uint8 {
	if col[0] == 0 && col[1] == 0 && col[2] == 0 && col[3] == 0 {
		return override
	}
	return col
}

// Start rendering at the top level of the box tree
func RenderAll(x, y float64, localScale float64) []string {
	deferredBoxes = []func() []string{}
	CurrentWorld.Top.LocalScale = localScale
	hoverTarget := []string{}

	drawText(CurrentWorld.Top.X*CurrentWorld.Scale, CurrentWorld.Top.Y*CurrentWorld.Scale, CurrentWorld.Top.Text, localScale*18*CurrentWorld.Scale, CurrentTheme.TextColour[0], CurrentTheme.TextColour[1], CurrentTheme.TextColour[2], CurrentTheme.TextColour[3])

	//Recurse down the draw tree, rendering each box, and calculating the hover target.
	for _, b := range CurrentWorld.Top.Children {
		// x = x - World.Top.X
		// y = y - World.Top.Y

		//If a mouse cursor is in a box, it adds itself to the list of hover targets.
		// The first box in the list is the topmost box, which gets the event.
		targets := Render(b, CurrentWorld.Top, x, y, 0, 0, localScale, ".", DrawWidth()/2, DrawHeight()/2)

		if len(targets) > 0 && targets[0] != dragItem {
			hoverTarget = targets
		}
	}

	//Halo gets drawn on top of the other boxes
	if HaloMode {
		//fmt.Printf("Rendering halo %+v\n", halo)
		halo_targets := Render(FindBox(CurrentWorld, "Halo"), nil, x, y, 0, 0, localScale, ".", DrawWidth()/2, DrawHeight()/2)
		if len(halo_targets) > 0 && halo_targets[0] != dragItem {
			hoverTarget = halo_targets
		}
	}

	//The box being dragged in drawn on top of everything else except the pinned boxes
	dragbox := FindBox(CurrentWorld, dragItem)
	if dragbox != nil {
		if dragbox.DrawCallback != nil {
			res := dragbox.DrawCallback(CurrentWorld, dragbox, nil, dragbox.ScreenX, dragbox.ScreenY)
			if !res {

				display := applyDefaultColour(dragbox.Colour[:], CurrentTheme.ButtonBackground)
				drawBox(dragbox.ScreenX, dragbox.ScreenY, dragbox.ScreenW, dragbox.ScreenH, display[0], display[1], display[2], display[3]/4)
				//FIXME drawText(dragbox.ScreenX, dragbox.ScreenY, dragbox.Text, font_scale)
			}
		} else {
			display := applyDefaultColour(dragbox.Colour[:], CurrentTheme.ButtonBackground)
			drawBox(dragbox.ScreenX, dragbox.ScreenY, dragbox.ScreenW, dragbox.ScreenH, display[0], display[1], display[2], display[3]/4)
		}
	}
	//Pinning a box to the screen requires that we draw it last, so that it's on top of everything else.  While parsing the draw tree,
	// we collect all the boxes that need to be pinned to the screen, and then draw them last.
	//fmt.Printf("Rendering %v deferred boxes\n", len(deferredBoxes))
	for _, b := range deferredBoxes {
		//fmt.Printf("Drawing deferred box: %v", i)
		res := b()
		if len(res) > 0 && res[0] != dragItem {
			hoverTarget = append(hoverTarget, res...)
		}
	}

	drawBox(x, y, 10*localScale, 10*localScale, 0, 128, 0, 255)
	return hoverTarget
}

// Recursively render the box tree
//
// b - the current box
// parent - the parent box in the tree
// hoverX, hoverY - the mouse position, in screenspace
// offsetX, offsetY - the offset of the current box from the parent, in the world coordinate system
// parentScale - the scale of the parent box
// path - the path to the current box in the tree (should make this a linked list)
func Render(b *Box, parent *Box, hoverX, hoverY, offsetX, offsetY float64, parentScale float64, path string, halfScreenX, halfScreenY float64) []string {
	setParent(b.Id, parent)
	if parent != nil {

		//fmt.Printf("Setting parent of %v to %v\n", b.Id, parent.Id)
	}
	hoverTarget := []string{}
	localScale := parentScale * b.Scale
	b.LocalScale = localScale
	ch := b.Children

	// fmt.Printf("localScale: %v\n", localScale)

	// worldXhover := hoverX*b.Scale + b.X
	// worldYhover := hoverY*b.Scale + b.Y

	screenX := offsetX + b.X*localScale
	screenX = (screenX+CurrentWorld.Top.X-halfScreenX)*CurrentWorld.Scale + halfScreenX
	screenY := offsetY + b.Y*localScale
	screenY = (screenY+CurrentWorld.Top.Y-halfScreenY)*CurrentWorld.Scale + halfScreenY
	screenW := b.W * localScale * CurrentWorld.Scale
	screenH := b.H * localScale * CurrentWorld.Scale

	if b.PinToScreen {
		screenX = b.X
		screenY = b.Y
		screenW = b.W
		screenH = b.H
	}

	b.ScreenX = screenX
	b.ScreenY = screenY
	b.ScreenW = screenW
	b.ScreenH = screenH

	deferHoverTarget := []string{}

	if b.Id != "" &&
		!b.Intangible &&
		hoverX >= screenX &&
		hoverX <= screenX+screenW &&
		hoverY >= screenY &&
		hoverY <= screenY+screenH {
		if b.Id != dragItem {

			hoverTarget = []string{b.Id}
			deferHoverTarget = []string{b.Id}

			if b.CallbackName != "" && b.Callback != nil {
				handled := b.Callback(CurrentWorld, b, parent, "hover", hoverX, hoverY)
				if !handled {
					DefaultCallback(CurrentWorld, b, parent, "hover", hoverX, hoverY)
					SendMessage(CurrentWorld.Id, b.Id, "hover")
					SendMessage(CurrentWorld.Id, "hover", b)

				}
			} else {
				DefaultCallback(CurrentWorld, b, parent, "hover", hoverX, hoverY)
				SendMessage(CurrentWorld.Id, b.Id, "hover")
				SendMessage(CurrentWorld.Id, "hover", b)
			}

		}
	}

	if ch != nil && len(ch) > 0 {
		switch b.SplitLayout {
		case "horizontal":
			ch[0].X = b.X + offsetX
			ch[0].Y = b.Y + offsetY
			ch[0].W = float64(b.W) * b.SplitRatio
			ch[0].H = b.H
			ch[1].X = ch[0].X + offsetX + ch[0].W
			ch[1].Y = b.Y + offsetY
			ch[1].W = b.W - ch[0].W
			ch[1].H = b.H
		case "vertical":
			ch[0].X = b.X + offsetX
			ch[0].Y = b.Y + offsetY
			ch[0].W = b.W
			ch[0].H = float64(b.H) * b.SplitRatio
			ch[1].X = b.X + offsetX
			ch[1].Y = ch[0].Y + offsetY + ch[0].H
			ch[1].W = b.W
			ch[1].H = b.H - ch[0].H
		case "free":
			// Arbitrary layout
		default:
			panic("invalid split layout" + b.SplitLayout)

		}
	}
	font_scale := localScale * 72 * CurrentWorld.Scale
	if b.PinToScreen {
		font_scale = 12
	}
	if CurrentWorld.Selected == b.Id {

		drawBox(screenX-2, screenY-2, screenW+4, screenH+4, CurrentTheme.SelectTrimColour[0], CurrentTheme.SelectTrimColour[1], CurrentTheme.SelectTrimColour[2], CurrentTheme.SelectTrimColour[3])
	}
	if b.PinToScreen {
		deferredBoxes = append(deferredBoxes, func() []string {
			//fmt.Printf("Deferred render: %v\n", b.Id)
			displayColour := applyDefaultColour(b.Colour[:], CurrentTheme.ButtonBackground)
			drawBox(screenX, screenY, screenW, screenH, displayColour[0], displayColour[1], displayColour[2], displayColour[3])
			displayColour = CurrentTheme.TextColour
			drawText(screenX, screenY, b.Text, font_scale, displayColour[0], displayColour[1], displayColour[2], displayColour[3])
			var res bool
			if b.DrawCallback != nil {
				res = b.DrawCallback(CurrentWorld, b, parent, screenX, screenY)
			}
			if !res {
				if !b.Invisible {
					display := applyDefaultColour(b.Colour[:], CurrentTheme.ButtonBackground)
					if b.Id == CurrentWorld.Selected {
						display = applyDefaultColour(b.Colour[:], CurrentTheme.SelectColour)
					}
					drawBox(screenX, screenY, screenW, screenH, display[0], display[1], display[2], display[3])
					display = CurrentTheme.TextColour
					drawText(screenX, screenY, b.Text, font_scale, display[0], display[1], display[2], display[3])
				}
			}
			return deferHoverTarget
		})
	} else {
		if isBoxOnScreen(CurrentWorld, b) {
			var res bool
			if b.DrawCallback != nil {
				res = b.DrawCallback(CurrentWorld, b, parent, screenX, screenY)
			}
			if !res {
				if !b.Invisible {
					display := applyDefaultColour(b.Colour[:], CurrentTheme.ButtonBackground)
					if b.Id == CurrentWorld.Selected {
						display = applyDefaultColour(b.Colour[:], CurrentTheme.SelectColour)
					}
					drawBox(screenX, screenY, screenW, screenH, display[0], display[1], display[2], display[3])
					display = CurrentTheme.TextColour
					drawText(screenX, screenY, b.Text, font_scale, display[0], display[1], display[2], display[3])
				}
			}

		}
	}

	for _, child := range b.Children {
		res := Render(child, b, hoverX, hoverY, b.X*localScale+offsetX, b.Y*localScale+offsetY, localScale, path+"/"+b.Text, halfScreenX, halfScreenY)

		hoverTarget = append(hoverTarget, res...)

	}
	return hoverTarget
}

// Search a Box tree for a box with a given id and delete it
func DeleteBox(b *Box, id string) *Box {
	if b.Id == id {
		b = nil
		return nil
	}

	new_child_list := []*Box{}

	for _, child := range b.Children {
		p := DeleteBox(child, id)
		if p != nil {
			new_child_list = append(new_child_list, child)
		}
	}
	b.Children = new_child_list
	return b
}
