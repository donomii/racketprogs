package boxes

import (
	"image"
	"log"	

	ff "golang.org/x/image/font"
)



// textBounds returns the pixel dimensions of text at a given size
func textBounds(text string, fontFace ff.Face, size float64) (x1, y1, x2, y2 float64) {
    bounds, _ := ff.BoundString(fontFace, text)
    return float64(bounds.Min.X), float64(bounds.Min.Y), 
           float64(bounds.Max.X), float64(bounds.Max.Y)
}

// getTextDimensions calculates width and height of text at a given size
func getTextDimensions(text string, fontFace ff.Face, size float64) (width, height float64) {
    x1, y1, x2, y2 := textBounds(text, fontFace, size)
    width = (x2 - x1) * size / 10.0  // Scale factor to convert from font units
    height = (y2 - y1) * size / 10.0
    return width, height
}

// EncloseText adjusts box dimensions to fit text, or shrinks text to fit box
func EncloseText(b *Box, fontFace ff.Face, initialSize float64) {
    if b.Text == "" {
        return
    }

    // Use default font if none provided
    if fontFace == nil {
        return
    }

    // Start with initial size
    currentSize := initialSize
    
    // Get text dimensions at current size
    textWidth, textHeight := getTextDimensions(b.Text, fontFace, currentSize)



    // If text is too big, iteratively reduce size until it fits
    maxIterations := 10
    iteration := 0
    
    for (textWidth > b.W || textHeight > b.H) && iteration < maxIterations {



        currentSize = currentSize * 0.75
        textWidth, textHeight = getTextDimensions(b.Text, fontFace, currentSize)

        iteration++
    }

    // Log if we hit max iterations without fitting
    if iteration >= maxIterations {
        log.Printf("Warning: Max iterations reached trying to fit text '%s' in box %s", b.Text, b.Id)
    }
}

func getCorners(r image.Rectangle) (x1, y1, x2, y2 int) {
    x1 = r.Min.X
    y1 = r.Min.Y
    x2 = r.Max.X
    y2 = r.Max.Y
    return
}



