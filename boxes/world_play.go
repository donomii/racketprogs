package boxes

import (
	"fmt"
	"math/rand"
	"time"
)

func make_play_world() *World_s {
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: DefaultCallback}

	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World",X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	

	dw := NewWorld()
	dw.Id = "PlayWorld"
	PlayWorld := dw
	CurrentWorld = PlayWorld
	PlayWorld.Scale = 1.0

	register_global_message_handlers(PlayWorld)
	Add(PlayWorld, PlayWorld.Top,  &debug_message_display_box)
	CreateHalo(PlayWorld) //FIXME

	menu := Box{Text: "menu", Id: "menu", X: 0, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	menu.Callback = ToggleChildren
	menu.CallbackName = "ToggleChildren"
	menu.SplitLayout = "free"

	Add(PlayWorld, PlayWorld.Top, &menu)

	Add(PlayWorld, PlayWorld.Top,  &NextWorld)

	save := Box{Text: "Save", Id: "save", X: 100, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	save.Callback = SaveWorld
	save.CallbackName = "SaveWorld"

	Add(PlayWorld, &menu,  &save)

	load := Box{Text: "Load", Id: "Load", X: 200, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	load.Callback = LoadWorld
	load.CallbackName = "LoadWorld"

	Add(PlayWorld, &menu,  &load)

	fps := Box{Text: "FPS", Id: "fps_box", X: 400, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	fps.DrawCallback = FpsRenderPassthrough
	fps.DrawCallbackName = "FpsRenderPassthrough"

	Add(PlayWorld, &menu,  &fps)

	quit := Box{Text: "Quit", Id: "quit_button", X: 300, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	quit.Callback = QuitCallback
	quit.CallbackName = "QuitCallback"

	Add(PlayWorld, &menu,  &quit)

	version := Box{Text: fmt.Sprintf("Version %v", Version), Id: "version_button", X: 450, Y: 50, W: 50, H: 50, Scale: 1.0, PinToScreen: true}

	Add(PlayWorld, &menu,  &version)

	joybox1 := Box{Text: "joybox1", Id: "joybox1_box", X: 250, Y: 150, W: 64, H: 64, Scale: 1.0}
	PlayWorld.Boxes.Store(joybox1.Id, &joybox1)
	PlayWorld.Boxes.Store("joybox1", &joybox1)
	joybox1.DrawCallback = RenderPicture
	joybox1.DrawCallbackName = "RenderPicture"
	SetFilePath(joybox1.Id, "assets/deep_elf_annihilator.png")
	Add(PlayWorld, PlayWorld.Top, &joybox1)

	joybox2 := Box{Text: "joybox2", Id: "joybox2_box", X: 350, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	PlayWorld.Boxes.Store(joybox2.Id, &joybox2)
	PlayWorld.Boxes.Store("joybox2", &joybox2)
	Add(PlayWorld, &menu,  &joybox2)

	tiles := []string{"assets/Tiny sakura iso.png","assets/Tiny banana tree iso.png","assets/tiny lighthouse iso.png", "assets/tiny temple iso.png", "assets/tiny temple iso 1.png", "assets/tiny temple iso 2.png"}
	
	for j := 0; j < 5; j++ {
		for i := 0; i < 5; i++ {
			//Pick a random tile
			rand.Seed(time.Now().UnixNano())
			tile := tiles[rand.Intn(len(tiles))]


			x := float64(i * 512)
			y := float64(j * 128)
		
			if j%2 == 1 {
				x = x + 256
			}
			Add(PlayWorld, PlayWorld.Top,  make_picture_box(PlayWorld, x, y, 512, 512, tile))

		}
	}

	/*
		for i := 0; i < 5; i++ {
			for j := 0; j < 3; j++ {
				n := i*3 + j
				boxname := fmt.Sprintf("button_%v", n)
				Button := Box{Text: fmt.Sprintf("Button %v", n), Id: boxname, X: float64(i*150 + 50), Y: float64(j*50 + 250), W: 140, H: 50, Scale: 1.0, PinToScreen: true, Colour: [4]uint8{0, 255, 255, 255}}

				Add(PlayWorld, &menu, 50, 50, &Button)

			}
		}


			go func() {
				files := DirBoxes(100, 0, ".")
				files.Scale = 0.2
				Add(DebugWorld.Top, 100, 0, files)
			}()
	*/

	selecyBox := Box{Text: "Select Next", Id: "select_next_button", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	selecyBox.Callback = SelectNextCallback
	selecyBox.CallbackName = "SelectNextCallback"
	Add(PlayWorld, PlayWorld.Top,  &selecyBox)

	dumpTreeBox := Box{Text: "Dump Tree", Id: "dumpTreeBox", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	dumpTreeBox.Callback = DumpTreeCallback
	dumpTreeBox.CallbackName = "DumpTreeCallback"
	Add(PlayWorld, PlayWorld.Top, &dumpTreeBox)

	return PlayWorld
}

func getNextBoxId() string {
	id := fmt.Sprintf("%v", NextBoxId)
	NextBoxId = NextBoxId + 1
	return id
}

func make_picture_box(w *World_s, x, y, width, h float64, filepath string) *Box {

	id := "picture_" + getNextBoxId()
	Monster := Box{Text: "", Id: id, X: x, Y: y, W: width, H: h, Scale: 1.0}
	w.Boxes.Store(Monster.Id, &Monster)

	Monster.DrawCallback = RenderPicture
	Monster.DrawCallbackName = "RenderPicture"
	SetFilePath(Monster.Id, filepath)    
	return &Monster
}

func StartPlaySystem() {

	go func() {
		// Get PlayWorld from global worlds var
		PlayWorld := WorldOf("PlayWorld")
		for {
			time.Sleep(100 * time.Millisecond)
			//Get player position
			player := FindBox(PlayWorld, "joybox1_box")
			if player == nil {
				panic("Player not found")
				return
			}

			//Loop over all monsters
			//Move them towards the player
			//If they reach the player, remove them from the world
			monsterBaseCache.ForEach(func(monster_id string, monster *Monster_s) bool {

				//Get the monster box
				monsterBox := FindBox(PlayWorld, monster_id)
				if monsterBox == nil {
					//fmt.Printf("World boxes: %+v\n", PlayWorld.Boxes)
					//fmt.Printf("Monster %v not found\n", monster_id)
					return true
				}
				//Move it towards the player
				monsterBox.X = monsterBox.X + (player.X-monsterBox.X)*0.01
				monster.X = monsterBox.X
				monsterBox.Y = monsterBox.Y + (player.Y-monsterBox.Y)*0.01
				monster.Y = monsterBox.Y
				//If it reaches the player, remove it from the world
				if monsterBox.X > player.X-10 && monsterBox.X < player.X+10 && monsterBox.Y > player.Y-10 && monsterBox.Y < player.Y+10 {
					RemoveBox(PlayWorld, monsterBox)
					monsterBaseCache.Del(monster.Id)
				}
				return true //to coninue iteration
			})
		}
	}()
	go func() {
		//Get PlayWorld from global worlds var
		PlayWorld := WorldOf("PlayWorld")
		for {
			//Create a new box holding a picture of a tentacled_starspawn.png
			//Add it to the world
			//Move it towards the player
			//When it reaches the player, remove it from the world

			//Make a random float64 position
			x := rand.Float64() * 800
			y := rand.Float64() * 600
			id := "monster_" + getNextBoxId()
			Monster := Box{Text: "", Id: id, X: x, Y: y, W: 100, H: 50, Scale: 1.0}
			PlayWorld.Boxes.Store(Monster.Id, &Monster)

			Monster.DrawCallback = RenderPicture
			Monster.DrawCallbackName = "RenderPicture"
			SetFilePath(Monster.Id, "assets/tentacled_starspawn.png")
			Add(PlayWorld, PlayWorld.Top,  &Monster)
			setMonster(Monster.Id, &Monster_s{Monster.Id, x, y})
			
			time.Sleep(10 * time.Second)
		}
	}()
}
