package boxes

import "fmt"

func make_menu(DebugWorld *World_s) {
	menu := Box{Text: "menu", Id: "menu", X: 0, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	menu.Callback = ToggleChildren
	menu.CallbackName = "ToggleChildren"
	menu.SplitLayout = "free"

	Add(DebugWorld, DebugWorld.Top, &menu)

	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(DebugWorld, DebugWorld.Top, &NextWorld)

	save := Box{Text: "Save", Id: "save", X: 100, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	save.Callback = SaveWorld
	save.CallbackName = "SaveWorld"

	Add(DebugWorld, &menu, &save)

	load := Box{Text: "Load", Id: "Load", X: 200, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	load.Callback = LoadWorld
	load.CallbackName = "LoadWorld"

	Add(DebugWorld, &menu, &load)

	fps := Box{Text: "FPS", Id: "fps_box", X: 400, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	fps.DrawCallback = FpsRenderPassthrough
	fps.DrawCallbackName = "FpsRenderPassthrough"

	Add(DebugWorld, &menu, &fps)

	quit := Box{Text: "Quit", Id: "quit_button", X: 300, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	quit.Callback = QuitCallback
	quit.CallbackName = "QuitCallback"
	Add(DebugWorld, &menu, &quit)

	fullscreen := Box{Text: "Fullscreen", Id: "fullscreen_button", DefaultMessage: "Toggle Fullscreen", X: 300, Y: 50, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	Add(DebugWorld, &menu, &fullscreen)

	editbox := Box{Text: "Edit Me", Id: "editbox", X: 300, Y: 100, W: 200, H: 50, Scale: 1.0, PinToScreen: true}
	Add(DebugWorld, &menu, &editbox)
}

func make_debug_world() *World_s {
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: DefaultCallback}

	dw := NewWorld()
	dw.Id = "DebugWorld"
	DebugWorld := dw
	CurrentWorld = DebugWorld

	register_global_message_handlers(DebugWorld)
	Add(DebugWorld, DebugWorld.Top, &debug_message_display_box)
	CreateHalo(DebugWorld) //FIXME

	make_menu(DebugWorld)

	version := Box{Text: fmt.Sprintf("Version %v", Version), Id: "version_button", X: 450, Y: 50, W: 50, H: 50, Scale: 1.0, PinToScreen: true}

	Add(DebugWorld, DebugWorld.Top, &version)

	joybox1 := Box{Text: "joybox1", Id: "joybox1_box", X: 250, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	DebugWorld.Boxes.Store(joybox1.Id, &joybox1)
	DebugWorld.Boxes.Store("joybox1", &joybox1)
	Add(DebugWorld, DebugWorld.Top, &joybox1)

	joybox2 := Box{Text: "joybox2", Id: "joybox2_box", X: 350, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	DebugWorld.Boxes.Store(joybox2.Id, &joybox2)
	DebugWorld.Boxes.Store("joybox2", &joybox2)
	Add(DebugWorld, DebugWorld.Top, &joybox2)

	for i := 0; i < 6; i++ {
		boxname := fmt.Sprintf("axis_%v", i)
		axis := Box{Text: fmt.Sprintf("Axis %v", i), Id: boxname, X: float64(i*100 + 50), Y: 200, W: 100, H: 50, Scale: 1.0, PinToScreen: true, Colour: [4]uint8{0, 255, 255, 255}}

		Add(DebugWorld, DebugWorld.Top, &axis)

	}

	for i := 0; i < 5; i++ {
		for j := 0; j < 3; j++ {
			n := i*3 + j
			boxname := fmt.Sprintf("button_%v", n)
			Button := Box{Text: fmt.Sprintf("Button %v", n), Id: boxname, X: float64(i*150 + 50), Y: float64(j*50 + 250), W: 140, H: 50, Scale: 1.0, PinToScreen: true, Colour: [4]uint8{0, 255, 255, 255}}

			Add(DebugWorld, DebugWorld.Top, &Button)

		}
	}

	/*
		go func() {
			files := DirBoxes(100, 0, ".")
			files.Scale = 0.2
			Add(DebugWorld.Top, 100, 0, files)
		}()
	*/

	selecyBox := Box{Text: "Select Next", Id: "select_next_button", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	selecyBox.Callback = SelectNextCallback
	selecyBox.CallbackName = "SelectNextCallback"
	Add(DebugWorld, DebugWorld.Top, &selecyBox)

	dumpTreeBox := Box{Text: "Dump Tree", Id: "dumpTreeBox", X: 400, Y: 0, W: 50, H: 50, Scale: 1.0, PinToScreen: true}
	dumpTreeBox.Callback = DumpTreeCallback
	dumpTreeBox.CallbackName = "DumpTreeCallback"
	Add(DebugWorld, DebugWorld.Top, &dumpTreeBox)
	return DebugWorld
}
