// The dumping ground for all the messages that don't have a home

package boxes

//Note:  All messages run in the callers thread.  This means that any data they affect has to be locked or multi-threaded safe


func init_default_messages() {

	Register("Global", "Next World", "Next World", func(name , id string, args interface{}) {
	
		current_world++
		if current_world >= len(Worlds) {
			current_world = 0
		}
		CurrentWorld = Worlds[current_world]
	
	
})


Register("Global", "Previous World", "World action handler", func(name , id string, args interface{}) {
	current_world = current_world - 1
	if current_world < 0 {
		current_world = len(Worlds) - 1
	}
	CurrentWorld = Worlds[current_world]
})



}