// #cgo pkg-config: sdl2

package boxes

// Make a gui layout module

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"log"
	"os"
	"strings"

	"reflect"
	"runtime"

	"github.com/donomii/goof"
	//"math/rand"

	"net/http"
	"net/http/pprof"
)

// Global flag to switch on steam deck features, such as correct button mapping
var steamDeck = false

// Default to a games controller
var joystick_type = "F310"

// Boxes version
var Version = 10

// Editmode is a separate mode for changing the widgets on the screen
var EditMode = false

// Display a "halo", floating buttons around a widget
var HaloMode = false

type Conf struct {
	WantTransparent bool
	RetinaMode      bool
	AutoRetina      bool
}

var conf Conf
var (
	//Battery saver option.  Attempt lower frame rate
	batterySaver bool
	//Skip drawing the frame unless this is set
	need_redraw bool

	//Current mouse position (may be a bit stale depending on frame rate)
	mouseX, mouseY   float64
	draw_action      string
	ready_for_render bool
	use_threads      bool
	profile          bool // Enable profiling through http://localhost:7777/profile

	//Worlds are our way of making different "screens", or modes.  The global World variabel points to the currently active world.

	Worlds        []*World_s
	current_world int
)

const (
	fontPath = "test.ttf"
	DefaultFontSize = 12
)

type txtcall struct {
	X, Y float64
	Str  string
	Size float64
}

var TextList []txtcall

// Switch off the garbage collector, lock the thread to a single core
func init() {
	os.Setenv("SDL_MOUSE_TOUCH_EVENTS", "1") // Force SDL to send us touch events instead of turning them into mouse events
	batteryCheckStr, _ := goof.QC([]string{"pmset", "-g", "batt"})
	if strings.Contains(batteryCheckStr, "Battery Power") {
		fmt.Println("Detected battery power, switching to battery saver mode")
		batterySaver = true
	} else {
		fmt.Println("No battery found, switching off battery saver mode")
		batterySaver = false
	}
	need_redraw = true
	runtime.LockOSThread()
	fmt.Println("Locked to main thread")
	//debug.SetGCPercent(-1)

}

var promptArg string

func StartBoxes(program func()) {
	initComponents()

	fmt.Println("Starting boxes, version", Version)
	//Setup command line flags
	//wantInit := flag.Bool("init", false, "Initialize a new world")
	flag.StringVar(&promptArg, "prompt", "", "Prompt for a command")
	flag.BoolVar(&use_threads, "threads", false, "Use threads")
	flag.BoolVar(&profile, "profile", false, "Enable profiling")
	flag.BoolVar(&batterySaver, "battery-saver", batterySaver, "Battery Saver mode")
	flag.Parse()

	//Attempt to detect steam deck
	if goof.Exists("/sys/devices/virtual/dmi/id/board_vendor") {

		if goof.FileContains("/sys/devices/virtual/dmi/id/board_vendor", "Valve") ||
			goof.FileContains("/sys/devices/virtual/dmi/id/board_vendor", "valve") {
			steamDeck = true
			joystick_type = "steamdeck"
		}
	}

	joystickHistory = make([][]float64, 6)
	for j := 0; j < 6; j++ {
		joystickHistory[j] = make([]float64, 50)
	}

	//joystickOffset = make([]float64, 100)

	fmt.Println("Joystick history initialised")
	fmt.Printf("History is %+v\n", joystickHistory)

	initCallbacks()

	
	conf.AutoRetina = true
	InitGraphics()

	initjoystick()
	init_worlds()
	init_default_messages()

	fmt.Println("Starting main loop with:")
	fmt.Println("\tuse_threads:", use_threads)
	fmt.Println("\tprofile:", profile)
	fmt.Println("\tbatterySaver:", batterySaver)

	if use_threads {
		//Need double buffering for this to work
		go func() {

			for {
				//for ready_for_render {
				//	time.Sleep(1 * time.Millisecond)
				//}
				fmt.Println("drawing")
				draw(mouseX, mouseY, draw_action)
				draw_action = ""
				ready_for_render = true
			}
		}()
	}

	if profile {
		fmt.Printf("Profiling information available at http://localhost:7777/profile\n")
		go func() {
			mux := http.NewServeMux()
			mux.HandleFunc("/profile", pprof.Profile)
			log.Fatal(http.ListenAndServe(":7777", mux))

		}()
	}

	init_worlds()
	go program()
	StartMain()
}

func convertImageToRgb(original image.Image) *image.RGBA {
	if original == nil {
		return nil
	}
	if reflect.TypeOf(original) == reflect.TypeOf(&image.RGBA{}) {
		return original.(*image.RGBA)
	}
	bounds := original.Bounds()
	converted := image.NewRGBA(bounds)

	for row := 0; row < bounds.Max.Y; row++ {
		for col := 0; col < bounds.Max.X; col++ {
			r, g, b, a := original.At(col, row).RGBA()
			//y, cb, cr := color.RGBToYCbCr(uint8(r), uint8(g), uint8(b))
			converted.Set(col, row, color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b), A: uint8(a)})
		}
	}
	return converted
}
