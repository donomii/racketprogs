package boxes

//This start as the component section of an Entity Component System, but ended up being being a cache instead.  In theory the
//variables ending in component should be saved and loaded, but that's not implemented yet.

//Effectively, the routines here are a kind of database that allows the programmer to store auxillary data for entities.  These
//components could be written to an SQL data base or stored in key/value buckets or whatever.  All caches and components are expected
//to share the same entity ID space.  i.e. if you have an entity with ID 1, then any cache that contains a "1" key holds data
//for entity #1.

//It is not required that all entities have a component.  If an entity does not have a component, then the component cache will
//not have an entry for that ID number.

import (
	"image"

	"github.com/alphadose/haxmap"
)

var parentComponents *haxmap.Map[string, *Box]
var filePathComponents *haxmap.Map[string, string]
var pictureCache *haxmap.Map[string, *image.RGBA]
var scaledPictureCache *haxmap.Map[string, *image.RGBA]

var worldCache *haxmap.Map[string, *World_s]
var monsterBaseCache *haxmap.Map[string, *Monster_s]

type Monster_s struct {
	Id string
	X  float64
	Y  float64
}

func initComponents() {
	parentComponents = haxmap.New[string, *Box]()
	filePathComponents = haxmap.New[string, string]()
	pictureCache = haxmap.New[string, *image.RGBA]()
	scaledPictureCache = haxmap.New[string, *image.RGBA]()
	worldCache = haxmap.New[string, *World_s]()
	monsterBaseCache = haxmap.New[string, *Monster_s]()
}

func setComponent[K int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | uintptr | float32 | float64 | string | complex64 | complex128, V any](m *haxmap.Map[K, V], key K, val V) {
	m.Set(key, val)
}

func getComponent[K int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | uintptr | float32 | float64 | string | complex64 | complex128, V any](m *haxmap.Map[K, V], key K) V {
	val, _ := m.Get(key)
	return val
}

// Data for monsters
func monsterOf(id string) *Monster_s     { return getComponent(monsterBaseCache, id) }
func setMonster(id string, m *Monster_s) { setComponent(monsterBaseCache, id, m) }

// Caching the parent of each box, this is updated each frame
func ParentOf(id string) *Box          { return getComponent(parentComponents, id) }
func setParent(id string, parent *Box) { setComponent(parentComponents, id, parent) }

// Location of data file for this entity (should be a list?)
func FilePathOf(id string) string            { return getComponent(filePathComponents, id) }
func SetFilePath(id string, filePath string) { setComponent(filePathComponents, id, filePath) }

// Original image picture cache
func pictureCacheOf(id string) *image.RGBA           { return getComponent(pictureCache, id) }
func setPictureCache(id string, picture *image.RGBA) { setComponent(pictureCache, id, picture) }

//Fuck you gofmt stop moving this up here

// Scaled picture cache
func scaledPictureOf(id string) *image.RGBA           { return getComponent(scaledPictureCache, id) }
func setScaledPicture(id string, picture *image.RGBA) { setComponent(scaledPictureCache, id, picture) }
func clearScaledPictureCache()                        { scaledPictureCache = haxmap.New[string, *image.RGBA]() }

// Cached pointers to the worlds.  Each world is a separate "screen" or "tab" in the UI.
func RegisterWorld(w *World_s) {
	setComponent(worldCache, w.Id, w)
	Worlds = append(Worlds, w)
}
func WorldOf(id string) *World_s { return getComponent(worldCache, id) }