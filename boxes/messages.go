// Implements a messaging system that allows other routines to register handlers for messages
package boxes


import "log"

// Messages are scoped, by default, to one world.  If you want to send a message to all worlds, use the worldId "Global".

// Messages are sent twice, once to the global message handler, and once to the world specific message handler.  A message to the global handler 
// is only sent once, even if the world has a handler for the same message.

var MessageRegistry map[string]map[string]func(name , id string, args interface{}) = make(map[string]map[string]func(name , id string, args interface{}))


//Register your message handler with the message system.  Name is the lookup name for the message, id is a free text field
//that you can use to identify your handler, and handler is the function that will be called when the message is sent.

//There can be as many handlers as you want for a message.  The id is used to identify the handler, so you can remove it later.
//Registering a handler with the same id will replace the old handler.
func Register(worldId string, name string, id string, handler func(name , id string, args interface{})) {
	key_name := worldId + ":" + name
	if MessageRegistry[key_name][id] == nil {
		MessageRegistry[key_name] = make(map[string]func(name , id string, args interface{}))
	}
	MessageRegistry[key_name][id] = handler
}

//Delete a handler.  The name and id must be the same as the ones used to register the handler.
func Unregister(world_id string, name string, id string) {
	key_name := world_id + ":" + name
	delete(MessageRegistry[key_name], id)
}

//Activates all the handlers for a message.  The name is the lookup name for the message, and args is a uncontrolled field that can be used to pass
//arguments.  You will have to check the sender code to see which args are passed. (TODO:  Make this more disciplined)
func SendMessage(world_Id string, name string, args interface{}) {
	if world_Id !="Global" {
	key_name := "Global:" + name
	SetDebug("Sending global message: " + key_name)
	log.Printf("SendMessage: %s", key_name)
	for id, handler := range MessageRegistry[key_name] {
		handler(key_name, id, args)
	}
}
	key_name := world_Id + ":" + name
	SetDebug("Sending message: " + key_name)
	log.Printf("SendMessage: %s", key_name)
	for id, handler := range MessageRegistry[key_name] {
		handler(key_name, id, args)
	}	
}
