package boxes

import (
	"errors"
	"image"
	"image/jpeg"
	"os/signal"
	"syscall"

	"context"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	vnc "github.com/amitbet/vnc2video"
	"github.com/amitbet/vnc2video/logger"
	"github.com/donomii/glim"
)

var VncImage *image.RGBA
var frameBufferRequestDelay = 3000 * time.Millisecond

func make_vnc_world() *World_s {
	VncViewWorld := NewWorld()
	VncViewWorld.Id = "VncWorld"
	CurrentWorld = VncViewWorld
	CreateHalo(VncViewWorld) //FIXME

	register_global_message_handlers(VncViewWorld)
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 140, Y: 450, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: DefaultCallback}

	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}

	Add(VncViewWorld, VncViewWorld.Top,  &debug_message_display_box)

	Add(VncViewWorld, VncViewWorld.Top,  &NextWorld)

	serverIP :=Box{Text: "192.168.0.11", Id: "server_ip",  X: 600, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(VncViewWorld, VncViewWorld.Top,  &serverIP)
	port :=Box{Text: "5900", Id: "server_port",  X: 600, Y: 150, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(VncViewWorld, VncViewWorld.Top,  &port)
	password :=Box{Text: "aaaaaa", Id: "server_password",  X: 600, Y: 250, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(VncViewWorld, VncViewWorld.Top,  &password)
	connect :=Box{Text: "Connect", Id: "connect",  DefaultMessage: "Start VNC from GUI", X: 600, Y: 350, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(VncViewWorld, VncViewWorld.Top,  &connect)

	b := Box{Text: "VncPicture box", Id: "vnc_picture_box", X: 0, Y: 0, W: 512, H: 512, Scale: 1.0, PinToScreen: true}
	b.DrawCallback = RenderPicture
	b.DrawCallbackName = "RenderPicture"
	Add(VncViewWorld, VncViewWorld.Top, &b)
	setPictureCache(b.Id, VncImage)

	Register(VncViewWorld.Id, "Start VNC", "World vnc handler", func(name , id string, args interface{}) {
		login := args.(string)
		go func() {
			for {
				fmt.Println("Connecting to VNC server")
				doVnc(login, "aaaaaa", &b,  VncViewWorld.Id)
			}
		}()
	})

	Register(VncViewWorld.Id,  "Start VNC from GUI", "World vnc handler", func(name , id string, args interface{}) {
		go func() {
			for {
				fmt.Println("Connecting to VNC server")
				ip:= FindBox(VncViewWorld, "server_ip").Text
				port:= FindBox(VncViewWorld, "server_port").Text
				password:= FindBox(VncViewWorld, "server_password").Text
				doVnc(ip+":"+port, password, &b, VncViewWorld.Id)
			}
		}()
	})

	//SendMessage(VncViewWorld.Id, "Start VNC", "192.168.0.11:5900")

	return VncViewWorld
}

func connectVnc(ctx context.Context, c net.Conn, cfg *vnc.ClientConfig) (conn *vnc.ClientConn, err error) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
			conn = nil
			err = errors.New("vnc connection failed")
		}
	}()
	conn, err = vnc.Connect(context.Background(), c, cfg)
	return conn, err
}

func queueWork(p chan func(), f func()) {
	go func() {
		p <- f
	}()
}

var mjpeg = false

func doVnc(server, password string, b *Box, worldId string) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in doVnc", r)
		}
	}()

	// Establish TCP connection to VNC server.
	nc, err := net.DialTimeout("tcp", server, 5*time.Second)
	if err != nil {
		logger.Fatalf("Error connecting to VNC host. %v", err)
	}

	logger.Tracef("starting up the client, connecting to: %s", server)
	// Negotiate connection with the server.
	cchServer := make(chan vnc.ServerMessage)
	cchClient := make(chan vnc.ClientMessage)
	errorCh := make(chan error)

	ccfg := &vnc.ClientConfig{
		SecurityHandlers: []vnc.SecurityHandler{
			//&vnc.ClientAuthATEN{Username: []byte(os.Args[2]), Password: []byte(os.Args[3])}
			&vnc.ClientAuthVNC{Password: []byte(password)},
			&vnc.ClientAuthNone{},
		},
		DrawCursor:      true,
		PixelFormat:     vnc.PixelFormat32bit,
		ClientMessageCh: cchClient,
		ServerMessageCh: cchServer,
		Messages:        vnc.DefaultServerMessages,
		Encodings: []vnc.Encoding{
			&vnc.RawEncoding{},
			&vnc.TightEncoding{},
			&vnc.HextileEncoding{},
			&vnc.ZRLEEncoding{},
			&vnc.CopyRectEncoding{},
			&vnc.CursorPseudoEncoding{},
			&vnc.CursorPosPseudoEncoding{},
			&vnc.ZLibEncoding{},
			&vnc.RREEncoding{},
		},
		ErrorCh: errorCh,
	}
	var cc *vnc.ClientConn
	//var err error
	for cc, err = connectVnc(context.Background(), nc, ccfg); err != nil; cc, err = connectVnc(context.Background(), nc, ccfg) {
	}
	screenImage := cc.Canvas
	if err != nil {
		logger.Fatalf("Error negotiating connection to VNC host. %v", err)
	}

	//MJPEG server
	if mjpeg {
		go func() {
			for {
				servAddr := "192.168.178.39:1001"
				tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
				if err != nil {
					println("ResolveTCPAddr failed:", err.Error())

				} else {

					conn, err := net.DialTCP("tcp", nil, tcpAddr)
					if err != nil {
						println("Dial failed:", err.Error())

					} else {

						for err == nil {
							time.Sleep(50 * time.Millisecond)
							var o image.Image
							o, err = jpeg.Decode(conn)

							if err == nil {
								println("read image")
								VncImage = convertImageToRgb(o)
								setPictureCache("vnc_picture_box", VncImage)
								log.Println("Updated image")
							} else {
								log.Printf("Received corrupted jpeg: %v", err)
							}
						}
						conn.Close()
					}
				}
			}
		}()
	}

	counter := 0

	//screenImage := vnc.NewVncCanvas(int(cc.Width()), int(cc.Height()))
	//rect := image.Rect(0, 0, int(cc.Width()), int(cc.Height()))
	//screenImage := image.NewRGBA64(rect)

	log.Printf("Using screen image %+v\n", screenImage.Bounds())
	for _, enc := range ccfg.Encodings {
		myRenderer, ok := enc.(vnc.Renderer)

		if ok {
			log.Printf("Supported encoding: %v", enc.Type().String())
			myRenderer.SetTargetImage(screenImage)
		}
	}

	logger.Tracef("connected to: %s", server)
	defer cc.Close()

	cc.SetEncodings([]vnc.EncodingType{
		//vnc.EncCursorPseudo,
		//vnc.EncPointerPosPseudo,
		//vnc.EncCopyRect,
		vnc.EncTight,
		vnc.EncZRLE,
		vnc.EncHextile,
		vnc.EncZlib,
		vnc.EncRRE,
	})
	//rect := image.Rect(0, 0, int(cc.Width()), int(cc.Height()))
	//screenImage := image.NewRGBA64(rect)
	// Process messages coming in on the ServerMessage channel.

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	frameBufferReq := 0

	serverOutPipe := make(chan func(), 20)
	go func() {
		for {
			f := <-serverOutPipe
			fmt.Println("Running queued function")
			f()
		}
	}()

	/*
	Register(worldId, "vnc_picture_box", "Vnc Event Handler", func(name string, id, args interface{}) {
		//
		fmt.Printf("vnc_picture_box Received event %v %v %v\n", name, id, args)
		b := FindBox(WorldOf("VncWorld"), "vnc_picture_box")
		x := mouseX
		y := mouseY

		VncImage := pictureCacheOf("vnc_picture_box")

		if VncImage == nil {
			fmt.Println("VncImage is nil")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenW == 0 {
			fmt.Println("ScreenW is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenH == 0 {
			fmt.Println("ScreenH is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		imw := VncImage.Rect.Size().X
		imh := VncImage.Rect.Size().Y
		if imw == 0 {
			fmt.Println("imw is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		rX := int(x-b.ScreenX) * int(imw) / int(b.ScreenW)
		rY := int(y-b.ScreenY) * int(imh) / int(b.ScreenH)
		aspect := float64(imw) / float64(imh)
		b.H = float64(b.W) / aspect
		fmt.Println("FUCK", args.(string))
		switch args.(string) {
		case "click":

			fmt.Printf("Vnc Click Handler %v %v\n", x, y)

			b := uint8(1)

			//fmt.Printf("Image %+v\n", img)

			queueWork(serverOutPipe, func() {
				reqMsg := vnc.PointerEvent{Mask: b, X: uint16(rX), Y: uint16(rY)}
				fmt.Println("click press at local", x, ",", y, "remote screen pos ", rX, ",", rY, "button:", b, "remote screen size", cc.Width(), ",", cc.Height())
				reqMsg.Write(cc)
				fmt.Println("Message sent")
			})

			queueWork(serverOutPipe, func() {
				reqMsg := vnc.PointerEvent{Mask: b, X: uint16(rX), Y: uint16(rY)}
				fmt.Println("click release at local", x, ",", y, "remote screen pos ", rX, ",", rY, "button:", 0, "remote screen size", cc.Width(), ",", cc.Height())
				reqMsg.Write(cc)
				fmt.Println("Message sent")
			})

			fmt.Printf("Sending mouse click at %v %v\n", rX, rY)

			reqMsg1 := vnc.FramebufferUpdateRequest{Inc: 1, X: 0, Y: 0, Width: cc.Width(), Height: cc.Height()}
			queueWork(serverOutPipe, func() {
				fmt.Println("Sending framebuffer request after event")
				//cc.ResetAllEncodings()
				reqMsg1.Write(cc)

			})
			fmt.Printf("Sending frame buffer request\n")

		case "hover":

			//fmt.Println("Remote scree ", cc.Width(), ",", cc.Height())
			fmt.Println("Callback motion at ", rX, ",", rY)
			reqMsg := vnc.PointerEvent{Mask: uint8(1), X: uint16(rX), Y: uint16(rY)}
			queueWork(serverOutPipe, func() {
				fmt.Printf("Sending mouse move at %v %v\n", rX, rY)
				reqMsg.Write(cc)
			})
		default:
			fmt.Println("Unknown event", args.(string))
		}
	})
	*/

	Register(worldId, "Mouse Press", "Vnc Event Handler", func(name , id string, args interface{}) {
		b := FindBox(WorldOf("VncWorld"), "vnc_picture_box")
		x := mouseX
		y := mouseY

		VncImage := pictureCacheOf("vnc_picture_box")

		if VncImage == nil {
			fmt.Println("VncImage is nil")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenW == 0 {
			fmt.Println("ScreenW is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenH == 0 {
			fmt.Println("ScreenH is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		imw := VncImage.Rect.Size().X
		imh := VncImage.Rect.Size().Y
		if imw == 0 {
			fmt.Println("imw is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		rX := int(x-b.ScreenX) * int(imw) / int(b.ScreenW)
		rY := int(y-b.ScreenY) * int(imh) / int(b.ScreenH)

		//fmt.Println("Remote scree ", cc.Width(), ",", cc.Height())
		fmt.Println("Callback motion at ", rX, ",", rY)
		reqMsg := vnc.PointerEvent{Mask: uint8(1), X: uint16(rX), Y: uint16(rY)}
		queueWork(serverOutPipe, func() {
			fmt.Printf("Sending mouse move at %v %v\n", rX, rY)
			reqMsg.Write(cc)
		})

		fmt.Println("HAndled mouse press")
	})

	Register(worldId, "Mouse Release", "Vnc Event Handler", func(name , id string, args interface{}) {
		b := FindBox(WorldOf("VncWorld"), "vnc_picture_box")
		x := mouseX
		y := mouseY

		VncImage := pictureCacheOf("vnc_picture_box")

		if VncImage == nil {
			fmt.Println("VncImage is nil")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenW == 0 {
			fmt.Println("ScreenW is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}
		if b.ScreenH == 0 {
			fmt.Println("ScreenH is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		imw := VncImage.Rect.Size().X
		imh := VncImage.Rect.Size().Y
		if imw == 0 {
			fmt.Println("imw is 0")
			fmt.Printf("vnc box: %+v\n", b)
			return
		}

		rX := int(x-b.ScreenX) * int(imw) / int(b.ScreenW)
		rY := int(y-b.ScreenY) * int(imh) / int(b.ScreenH)

		//fmt.Println("Remote scree ", cc.Width(), ",", cc.Height())
		fmt.Println("Callback motion at ", rX, ",", rY)
		reqMsg := vnc.PointerEvent{Mask: uint8(0), X: uint16(rX), Y: uint16(rY)}
		queueWork(serverOutPipe, func() {
			fmt.Printf("Sending mouse move at %v %v\n", rX, rY)
			reqMsg.Write(cc)
		})

		fmt.Println("HAndled mouse release")

	})

	if !mjpeg {
		go func() {
			for {

				reqMsg1 := vnc.FramebufferUpdateRequest{Inc: 1, X: 0, Y: 0, Width: cc.Width(), Height: cc.Height()}
				queueWork(serverOutPipe, func() {
					//cc.ResetAllEncodings()
					fmt.Println("Sending framebuffer request")
					reqMsg1.Write(cc)
				})
				time.Sleep(frameBufferRequestDelay)

			}
		}()
	}

	reqMsg := vnc.FramebufferUpdateRequest{Inc: 0, X: 0, Y: 0, Width: cc.Width(), Height: cc.Height()}
	//cc.ResetAllEncodings()
	queueWork(serverOutPipe, func() {
		fmt.Println("Sending framebuffer request")
		reqMsg.Write(cc)
	})

	for {
		fmt.Println("Waiting for message")
		select {
		case err := <-errorCh:
			panic(err)
		case msg := <-cchClient:
			fmt.Printf("Received client message type:%v msg:%v\n", msg.Type(), msg)
		case msg := <-cchServer:
			fmt.Printf("Received server message type:%v msg:%v\n", msg.Type(), msg)
			//fmt.Printf("Received server message type:%v msg:%v\n", msg.Type(), msg)
			if msg.Type() == vnc.FramebufferUpdateMsgType {
				fmt.Printf("Received server frame buffer update type:%v msg:%v\n", msg.Type(), msg)
				frameBufferReq++
				counter++

				p, w, h := glim.GFormatToImage(screenImage.Image, nil, 0, 0)
				p = glim.ForceAlpha(p, 255)
				p = glim.FlipUp(w, h, p)
				o := glim.ImageToGFormat(w, h, p)

				VncImage = convertImageToRgb(o)
				setPictureCache("vnc_picture_box", VncImage)
				clearScaledPictureCache()
			}
		}
		log.Println("Finished select, looping")
	}
}
