//Routines for setting up and managing worlds

package boxes

import (
	"fmt"
	"sync"
)

type World_s struct {
	Boxes     sync.Map `json:"-"`
	Top       *Box
	Scale     float64
	Selected  string
	Id        string
	ZoomIndex int
	//Current text editor, used for updating widget text
	CurrentEditor *GlobalConfig
	//Current box having its text updated
	CurrentEditBox *Box
}

var CurrentWorld *World_s

func NewWorld() *World_s {
	newWorld := World_s{}
	newWorld.Boxes = sync.Map{}

	newWorld.Top = &Box{
		Text: "TestWindow",

		Id:           fmt.Sprintf("top_test_%v", NextBoxId),
		X:            100,
		Y:            100,
		W:            400,
		H:            400,
		Scale:        1.0,
		SplitLayout:  "",
		SplitRatio:   0.5,
		Callback:     DefaultCallback,
		CallbackName: "DefaultCallback",
	}

	NextBoxId = NextBoxId + 1
	//if *wantInit {
	//top.AstNode = &Node{List: ast}
	newWorld.Boxes.Store(newWorld.Top.Id, newWorld.Top)
	newWorld.Scale = 0.1
	newWorld.CurrentEditor = NewEditor() //FIXME add this to the world create function
	newWorld.CurrentEditBox = &Box{Text: "hiddeneditbox", Id: "hiddeneditbox", X: 0, Y: 0, W: 100, H: 50, Scale: 1.0, PinToScreen: false, Invisible: true, Intangible: true}

	return &newWorld
}

func register_global_message_handlers(w *World_s) {

	Register(w.Id, "Select Next", "World select handler", func(name, id string, args interface{}) {
		AttemptSelectNext(CurrentWorld, CurrentWorld.Selected)
		SendMessage(CurrentWorld.Id, "select", FindBox(CurrentWorld, CurrentWorld.Selected))
		SendMessage(CurrentWorld.Id, CurrentWorld.Selected, "select")
		fmt.Printf("Selected: %v\n", CurrentWorld.Selected)
		SetDebug("Selected: " + CurrentWorld.Selected)
		need_redraw = true
	})

	Register(w.Id, "Select Previous", "World select handler", func(name, id string, args interface{}) {
		AttemptSelectPrevious(CurrentWorld, CurrentWorld.Selected)
		SendMessage(CurrentWorld.Id, "select", FindBox(CurrentWorld, CurrentWorld.Selected))
		SendMessage(CurrentWorld.Id, CurrentWorld.Selected, "select")
		fmt.Printf("Selected: %v\n", CurrentWorld.Selected)
		SetDebug("Selected: " + CurrentWorld.Selected)
		need_redraw = true
	})

	Register(w.Id, "Select", "World select handler", func(name, id string, args interface{}) {
		selected := args.(string)
		box := FindBox(w, selected)
		if box != nil {
			if box.Callback != nil {
				box.Callback(w, box, nil, "select", -1, -1)
			} else {
				DefaultCallback(w, box, nil, "select", -1, -1)
			}
		}
		need_redraw = true
	})
	Register(w.Id, "click", "World click handler", func(name, id string, args interface{}) {
		fmt.Printf("Handling click message %v\n", args)
		b := args.(*Box)
		if b != nil {
			CurrentWorld.Selected = b.Id
		}
		need_redraw = true
	})
	Register(w.Id, "p pressed", "SDL keybinding", func(name, id string, args interface{}) {
		SendMessage(w.Id, "Previous World", nil)
		need_redraw = true
	})

	Register(w.Id, "Button", "Button translator", func(name, id string, args interface{}) {

		w := CurrentWorld.Id
		button := args.(int)
		fmt.Printf("Handling button message %v\n", button)
		if joystick_type == "F310" {
			switch button {
			case 0:
				SendMessage(w, "The awkward button that nobody really knows what to do with", nil)
			case 1:
				SendMessage(w, "Choose OK", nil)
			case 2:
				SendMessage(w, "Choose Cancel", nil)
			case 3:
				SendMessage(w, "Choose Special", nil)
			case 6:
				SendMessage(w, "Left Trigger", nil) //For cheap controllers that have binary triggers
			case 7:
				SendMessage(w, "Right Trigger", nil) //For cheap controllers that have binary triggers
			case 10:
				SendMessage(w, "Left Stick", nil)
			case 11:
				SendMessage(w, "Right Stick", nil)
			case 12:
				SendMessage(w, "Dpad Up", nil)
			case 13:
				SendMessage(w, "Dpad Right", nil)
			case 14:
				SendMessage(w, "Dpad Down", nil)
			case 15:
				SendMessage(w, "Dpad Left", nil)
			}
		}

		if joystick_type == "steamdeck" {
			switch button {
			case 0:
				SendMessage(w, "Choose OK", nil)
			case 1:
				SendMessage(w, "Choose Cancel", nil)
			case 2:
				SendMessage(w, "Choose Special", nil)
			case 3:
				SendMessage(w, "The awkward button that nobody really knows what to do with", nil)
			case 6:
				SendMessage(w, "Choose Start", nil)
			case 7:
				SendMessage(w, "Choose Menu", nil)
			case 9:
				SendMessage(w, "Left Stick", nil)
			case 10:
				SendMessage(w, "Right Stick", nil)
			case 11:
				SendMessage(w, "Dpad Up", nil)
			case 12:
				SendMessage(w, "Dpad Left", nil)
			case 13:
				SendMessage(w, "Dpad Down", nil)
			case 14:
				SendMessage(w, "Dpad Right", nil)
			}
		}

	})

	//Adds a click event when the "Choose OK" message is sent by a controller
	Register(w.Id, "Choose OK", "Click on selected box", func(name, id string, args interface{}) {
		SendMessage(w.Id, "click", FindBox(w, CurrentWorld.Selected))
		b := FindBox(w, CurrentWorld.Selected)
		if b != nil {
			if b.Callback != nil {
				b.Callback(w, b, nil, "click", -1, -1)
			} else {
				DefaultCallback(w, b, nil, "click", -1, -1)
			}
		}
	})

	// Sends a "Select Next" when the "Dpad Right" message is sent by a controller
	Register(w.Id, "Dpad Right", "Translate to select next", func(name, id string, args interface{}) {
		SendMessage(w.Id, "Select Next", FindBox(w, CurrentWorld.Selected))
	})

	// Sends a "Select Previous" when the "Dpad Left" message is sent by a controller
	Register(w.Id, "Dpad Left", "Translate to select previous", func(name, id string, args interface{}) {
		SendMessage(w.Id, "Select Previous", FindBox(w, CurrentWorld.Selected))
	})
}

func init_worlds() {
	prompt := promptArg

	//_seed12509_g1.0_iter75.png
	//prompt := "alien scrapyard, outer space, unreal 5 render, studio ghibli, tim hildebrandt, digital art, octane render, beautiful composition, trending on artstation, award - winning photograph, masterpiece __devicemps"

	PicViewWorld := make_piclist_viewer_world()
	RegisterWorld(PicViewWorld)
	PicGridWorld := make_pic_grid(prompt)
	RegisterWorld(PicGridWorld)
	DebugWorld := make_debug_world()
	RegisterWorld(DebugWorld)
	PlayWorld := make_play_world()
	RegisterWorld(PlayWorld)
	VncWorld := make_vnc_world()
	RegisterWorld(VncWorld)


	Worlds = []*World_s{ PicViewWorld, PicGridWorld, DebugWorld, PlayWorld, VncWorld}
	

	if prompt != "" {
		CurrentWorld = PicGridWorld
	}

	StartPlaySystem()

}
