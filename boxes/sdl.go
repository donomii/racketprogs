//go:build sdl
// +build sdl

package boxes

import (
	"fmt"
	"image"
	"log"
	"os"
	"strings"
	"sync"
	"time"
	"unsafe"

	"github.com/donomii/glim"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

var (
	surface             *sdl.Surface
	oldSurface          *sdl.Surface
	window              *sdl.Window
	font                *ttf.Font
	text                *sdl.Surface
	WinWidth, WinHeight float64
	globalLock          sync.Mutex
)

func InitGraphics() {
	var err error
	if err := ttf.Init(); err != nil {
		return
	}
	// defer ttf.Quit()

	sdl.SetHint(sdl.HINT_RENDER_DRIVER, "opengl")
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	// defer sdl.Quit()

	window, err = sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		1600, 900, sdl.WINDOW_SHOWN|sdl.WINDOW_ALLOW_HIGHDPI|sdl.WINDOW_RESIZABLE|sdl.WINDOW_FULLSCREEN_DESKTOP)
	if err != nil {
		panic(err)
	}

	// defer window.Destroy()

	surface, err = window.GetSurface()
	if err != nil {
		panic(err)
	}

	// Load the font for our text
	if font, err = ttf.OpenFont(fontPath, DefaultFontSize); err != nil {
		log.Fatalf("Failed to load font because: %v\n", err)
		return
	}
	// defer font.Close()

}

func DrawWidth() float64 {
	return WinWidth
}

func DrawHeight() float64 {
	return WinHeight
}

func StartMain() {
	surface, err := window.GetSurface()
	if err != nil {
		panic(err)
	}
	oldSurface = surface
	w, h := window.GLGetDrawableSize()
	WinWidth = float64(w)
	WinHeight = float64(h)

	running := true
	var mouseX, mouseY int = 0, 0
	var action string

	var startZoomIndex int
	var cumDist float64
	var lastTime time.Time
	for running {

		

		// drawBox(0, 0, 200, 200, 0xffff0000)
		// rect := sdl.Rect{0, 0, 200, 200}
		// surface.FillRect(&rect, 0xffff0000)
		lapsed := time.Since(lastTime)

		// fmt.Printf("%v\n", lapsed
		if lapsed > time.Duration(2*lapsed.Milliseconds()) { //FIXME change max duration based on `battery` setting
			NeedRedraw()
		}
		if need_redraw {
			lastTime = time.Now()
			log.Printf("Update needed, updating now")
			w, h := window.GLGetDrawableSize()
			WinWidth = float64(w)
			WinHeight = float64(h)
			oldSurface = surface
			surface, err = window.GetSurface()
			if err != nil {
				panic(err)
			}
			surface.FillRect(nil, 0)
			drawText(CurrentWorld.Scale*float64(CurrentWorld.Top.X), CurrentWorld.Scale*float64(CurrentWorld.Top.Y), CurrentWorld.Top.Text, 18, CurrentTheme.TextColour[0], CurrentTheme.TextColour[1], CurrentTheme.TextColour[2], CurrentTheme.TextColour[3])
			draw(float64(mouseX), float64(mouseY), action)
			action = ""

			window.UpdateSurface()
		} else {

			time.Sleep(10 * time.Millisecond)

		}

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			//log.Printf("loop - %v", event.GetType())
			log.Printf("event: %+v\n", event)
			//log.Printf("type: %T\n", event)
			NeedRedraw()
			switch t := event.(type) {
			case *sdl.WindowEvent:
				if t.Event == sdl.WINDOWEVENT_RESIZED || t.Event == sdl.WINDOWEVENT_SIZE_CHANGED {
					globalLock.Lock()

					NeedRedraw()
					oldSurface = surface
					surface, err = window.GetSurface()
					if err != nil {
						panic(err)
					}
					globalLock.Unlock()

				}
			case *sdl.QuitEvent:
				println("Quit")
				running = false
				break
			case *sdl.MouseMotionEvent:
				mouseX = int(t.X)
				mouseY = int(t.Y)

				// fmt.Println("Mouse", t.Which, "moved by", t.XRel, t.YRel, "at", t.X, t.Y)
			case *sdl.MouseButtonEvent:
				mouseX = int(t.X)
				mouseY = int(t.Y)

				if t.State == sdl.PRESSED {
					action = "press"
					SendMessage(CurrentWorld.Id, "Mouse Press", "left")
					// fmt.Println("Mouse", t.Which, "button", t.Button, "pressed at", t.X, t.Y)
				} else {
					action = "release"
					SendMessage(CurrentWorld.Id, "Mouse Release", "left")
					// fmt.Println("Mouse", t.Which, "button", t.Button, "released at", t.X, t.Y)
				}
			case *sdl.TouchFingerEvent:
				if t.Type == sdl.FINGERDOWN {
					sdl.RecordGesture(-1)
					startZoomIndex = CurrentWorld.ZoomIndex
					fmt.Printf("Set startzoom to zoomindex(%v", CurrentWorld.ZoomIndex)
					cumDist = 0
				}
			case *sdl.MultiGestureEvent:
				log.Printf("SDL pinch: distance, %v, rotate: %v, fingers: %v", t.DDist, t.DTheta, t.NumFingers)
				cumDist = cumDist + float64(t.DDist)
				d := cumDist * 50
				i := int(d)
				log.Printf("Startzoomindex: %v, delta: %v, cumDist: %v", startZoomIndex, i, cumDist)
				SetZoomIndex(CurrentWorld, startZoomIndex + i)
				if CurrentWorld.ZoomIndex < 0 {
					CurrentWorld.ZoomIndex = 0
				}
				if CurrentWorld.ZoomIndex > len(zoomLevels)-1 {
					CurrentWorld.ZoomIndex = len(zoomLevels) - 1
				}

				log.Printf("Chose zoomindex: %v out of %v zoomLevels", CurrentWorld.ZoomIndex, len(zoomLevels))
				CurrentWorld.Scale = zoomLevels[CurrentWorld.ZoomIndex]
			case *sdl.MouseWheelEvent:
				mouseX = int(t.X)
				mouseY = int(t.Y)
				if mouseY > 0 {
					action = "wheel up"
				} else {
					action = "wheel down"
				}

				if t.X != 0 {
					// fmt.Println("Mouse", t.Which, "wheel scrolled horizontally by", t.X)
				} else {
					// fmt.Println("Mouse", t.Which, "wheel scrolled vertically by", t.Y)
				}

			case *sdl.KeyboardEvent:
				keysym := t.Keysym.Sym
				keys := ""

				BoxEvent := []string{"", "", ""}

				if (sdl.Keymod(t.Keysym.Mod) & sdl.KMOD_CTRL) != 0 {
					keys += "Ctrl+"
				}

				if (sdl.Keymod(t.Keysym.Mod) & sdl.KMOD_SHIFT) != 0 {
					keys += "Shift+"
				}

				if (sdl.Keymod(t.Keysym.Mod) & sdl.KMOD_ALT) != 0 {
					keys += "Alt+"
				}

				if (sdl.Keymod(t.Keysym.Mod) & sdl.KMOD_GUI) != 0 {
					keys += "GUI+"
				}

				if (sdl.Keymod(t.Keysym.Mod) & sdl.KMOD_CAPS) != 0 {
					keys += "Caps+"
				}

				if strings.HasSuffix(keys, "+") {
					keys = keys[:len(keys)-1]
				}

				BoxEvent[0] = keys

				switch keysym {
				case sdl.K_ESCAPE:
					os.Exit(0)
				case sdl.K_DELETE:
					dispatch("DELETE-RIGHT", CurrentWorld.CurrentEditor)
				case sdl.K_BACKSPACE:
					dispatch("DELETE-LEFT", CurrentWorld.CurrentEditor)
				case sdl.K_RETURN:

					fallthrough
				case sdl.K_RETURN2:
					ActiveBufferInsert(CurrentWorld.CurrentEditor, "")
					fmt.Println("Printing tree")
					PrintBoxTree(CurrentWorld.Top, 0, true)
				case sdl.K_LSHIFT:
					fallthrough
				case sdl.K_RSHIFT:
					// fmt.Println("Shift")
				case sdl.K_LCTRL:
					fallthrough
				case sdl.K_RCTRL:
					// fmt.Println("Ctrl")
				case sdl.K_LALT:
					fallthrough
				case sdl.K_RALT:
					// fmt.Println("Alt")
				default:
					if CurrentWorld.CurrentEditBox != nil {
						ActiveBufferInsert(CurrentWorld.CurrentEditor, string(keysym))
					}
				}

				if CurrentWorld.CurrentEditBox != nil {
					CurrentWorld.CurrentEditBox.Text = ActiveBufferText(CurrentWorld.CurrentEditor)
					if CurrentWorld.CurrentEditBox.AstNode != nil {
						CurrentWorld.CurrentEditBox.AstNode.Str = ActiveBufferText(CurrentWorld.CurrentEditor)
					}
				}

				if keys != "" {
					keys += " + "
				}

				switch keysym {
				case sdl.K_LCTRL:
					keys += "Left Ctrl"
				case sdl.K_RCTRL:
					keys += "Right Ctrl"
				case sdl.K_LSHIFT:
					keys += "Left Shift"
				case sdl.K_RSHIFT:
					keys += "Right Shift"
				case sdl.K_LALT:
					keys += "Left Alt"
				case sdl.K_RALT:
					keys += "Right Alt"
				case sdl.K_LGUI:
					keys += "Left GUI"
				case sdl.K_RGUI:
					keys += "Right GUI"
				case sdl.K_UP:
					keys += "Up"
				case sdl.K_DOWN:
					keys += "Down"
				case sdl.K_LEFT:
					keys += "Left"
				case sdl.K_RIGHT:
					keys += "Right"

				case sdl.K_BACKSPACE:
					keys += "Backspace"
				case sdl.K_TAB:
					keys += "Tab"
				case sdl.K_CLEAR:
					keys += "Clear"
				case sdl.K_RETURN:
					keys += "Return"
				case sdl.K_PAUSE:
					keys += "Pause"
				case sdl.K_ESCAPE:
					keys += "Escape"
				case sdl.K_SPACE:
					keys += "Space"
				case sdl.K_EXCLAIM:
					keys += "!"
				case sdl.K_QUOTEDBL:
					keys += "\""
				case sdl.K_HASH:
					keys += "#"
				case sdl.K_DOLLAR:
					keys += "$"
				default:
					keys += string(keysym)
				}

				// If the key is held down, this will fire
				if t.Repeat > 0 {
					keys += " repeating"
				} else {
					if t.State == 0 {
						keys += " released"
					} else if t.State == 1 {
						keys += " pressed"
					}
				}

				fmt.Printf("Key state: %+v\n", t)

				if keys != "" {
					//fmt.Println(keys)
					SendMessage(CurrentWorld.Id, keys, nil)
				}

				if keys == "Ctrl + Left Ctrl pressed" {
					EditMode = true
				}
				if keys == "Left Ctrl released" {
					EditMode = false
				}
			}
			NeedRedraw()
		}
	}
}

func drawBox(x, y, w, h float64, r, g, b, a uint8) {
	globalLock.Lock()
	defer globalLock.Unlock()
	rect := sdl.Rect{int32(x), int32(y), int32(w), int32(h)}
	color := sdl.Color{255, 255, b, 255}
	surface.FillRect(&rect, color.Uint32())
}

func drawText(x, y float64, str string, size float64, r, g, b, a uint8) {
	globalLock.Lock()
	defer globalLock.Unlock()
	// Load the font for our text
	var err error
	//FIXME: Cache this
	if font, err = ttf.OpenFont("test.ttf", int(size)); err != nil {
		return
	}
	defer font.Close()

	// Create a red text with the font
	if text, err = font.RenderUTF8Blended(str, sdl.Color{R: r, G: g, B: b, A: 128}); err != nil {
		log.Printf("Failed to render text because: %v\n", err)
		return
	}
	defer text.Free()

	// Draw the text around the center of the window
	if err = text.Blit(nil, surface, &sdl.Rect{X: int32(x), Y: int32(y), W: 0, H: 0}); err != nil {
		log.Printf("Failed to blit text because: %v\n", err)
		return
	}

	// Update the window surface with what we have drawn
	//window.UpdateSurface()
}

func drawImage(x, y float64, src *image.RGBA) {
	globalLock.Lock()
	defer globalLock.Unlock()

	pix, width, height := glim.GFormatToImage(src, nil, src.Rect.Dx(), src.Rect.Dy())
	sdlSurfacePic, err := sdl.CreateRGBSurfaceFrom(unsafe.Pointer(&pix[0]), int32(width), int32(height), 32, 4*width, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000)
	if err != nil {
		return
	}
	sdlSurfacePic.Blit(nil, surface, &sdl.Rect{X: int32(x), Y: int32(y), W: int32(width), H: int32(height)})

}
