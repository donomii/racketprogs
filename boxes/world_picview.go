package boxes

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"sort"
	"strings"

	"github.com/donomii/goof"
)

var currentDisplayDirectory string = "./"

func ForceSingleSlashAtEndCurrentDisplayDirectory() {
	if !strings.HasSuffix(currentDisplayDirectory, "/") {
		currentDisplayDirectory = currentDisplayDirectory + "/"
	}
}

func CreatePicList(PicViewWorld *World_s, grid *Box, directory string) {

	//Make an array of strings of the file names in the desktop directory
	files := []string{}
	file_stats, _ := ioutil.ReadDir(directory)

	//Sort the children by time added
	sort.Slice(file_stats, func(i, j int) bool {
		return file_stats[j].ModTime().Before(file_stats[i].ModTime())
	})

	for _, file_stat := range file_stats {
		files = append(files, file_stat.Name())
	}

	for index := 0; index < len(files); index++ {

		b := Box{Text: files[index],
			Id: fmt.Sprintf("picture_box_%v_%v",
				index, files[index]),
			X: 512, Y: float64(100.0 * index), W: 2000, H: 90,
			Scale:       1.0,
			PinToScreen: false,
			Colour:      [4]uint8{64, 64, 64, 255},
		}

		Add(PicViewWorld, grid, &b)
		SetFilePath(b.Id, files[index])
		Register(PicViewWorld.Id, b.Id, b.Id+" picture hover handler", pictureHandler)
	}
}

func pictureHandler(name, handler_id string, args interface{}) {
	//Parse message name
	p := strings.SplitN(name, ":", 2)
	box_id := p[1]
	world_id := p[0]
	//Find the current box in the world box list
	selected_box := FindBox(WorldOf(world_id), box_id)
	switch args.(string) {
	case "click":
		//If we found the box (some boxes aren't registered in the world list)
		if selected_box != nil {

			newDir := currentDisplayDirectory + FilePathOf(selected_box.Id)

			if goof.IsDir(newDir) {
				currentDisplayDirectory = newDir
				ForceSingleSlashAtEndCurrentDisplayDirectory()
				ListBox := FindBox(WorldOf(world_id), "picture_list")
				if ListBox != nil {
					//Delete all children of the list box
					for _, child := range ListBox.Children {
						RemoveBox(WorldOf(world_id), child)
					}
					ListBox.Children = []*Box{}
					CreatePicList(WorldOf(world_id), ListBox, currentDisplayDirectory)

				}

			}
		}

		fallthrough
	case "select":
		if selected_box != nil {

			//Move the world.top so that this box is in the center of the screen
			//Find the distance to the center of the screen
			diff := selected_box.ScreenY - float64(WinHeight/2)
			//Shift world.top by that distance, scaled by the zoom factor
			WorldOf(world_id).Top.Y -= diff / WorldOf(world_id).Scale
		}
		fallthrough
	case "hover":

		if selected_box != nil {

			newFilePath := currentDisplayDirectory + FilePathOf(selected_box.Id)
			oldFilePath := FilePathOf("picture_viewer_box")
			if newFilePath != oldFilePath {
				CurrentWorld.Top.Text = selected_box.Text
				//CurrentWorld.Top.Text = "Hovering over " + selected_box.Text
				setPictureCache("picture_viewer_box", nil)
				SetFilePath("picture_viewer_box", "")
				if strings.HasSuffix(selected_box.Text, ".png") {
					SetFilePath("picture_viewer_box", newFilePath)

					pb := FindBox(WorldOf(world_id), "picture_viewer_box")
					if pb != nil {
						pb.Data = nil
					} else {
						fmt.Printf("Couldn't find picture box\n")
					}
				}
			}
		}
	default:
	}
}
func make_piclist_viewer_world() *World_s {
	PicViewWorld := NewWorld()
	PicViewWorld.Id = "PicViewWorld"
	CurrentWorld = PicViewWorld
	CreateHalo(PicViewWorld) //FIXME
	currentDisplayDirectory = goof.Cwd()
	ForceSingleSlashAtEndCurrentDisplayDirectory()

	register_global_message_handlers(PicViewWorld)
	debug_message_display_box := Box{Text: "!!!debug_message_display", Id: "debug_message_display", X: 0, Y: 650, W: 100, H: 50, Scale: 1.0, PinToScreen: true,
		CallbackName: "DefaultCallback", Callback: DefaultCallback}

	NextWorld := Box{Text: "Next World", Id: "next_world", DefaultMessage: "Next World", X: 0, Y: 50, W: 100, H: 50, Scale: 1.0, PinToScreen: true}

	Add(PicViewWorld, PicViewWorld.Top, &debug_message_display_box)

	Add(PicViewWorld, PicViewWorld.Top, &NextWorld)

	Directory_up := Box{Text: "Directory up", Id: "directory_up", DefaultMessage: "Choose Cancel", X: 0, Y: 512, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(PicViewWorld, PicViewWorld.Top, &Directory_up)

	Keep_Picture := Box{Text: "Keep Picture", Id: "keep_picture", DefaultMessage: "", X: 0, Y: 576, W: 100, H: 50, Scale: 1.0, PinToScreen: true}
	Add(PicViewWorld, PicViewWorld.Top, &Keep_Picture)

	b := Box{Text: "", Id: "picture_viewer_box", X: 0, Y: 0, W: 512, H: 512, Scale: 1.0, PinToScreen: true}
	b.DrawCallback = RenderPicture
	b.DrawCallbackName = "RenderPicture"
	Add(PicViewWorld, PicViewWorld.Top, &b)

	grid := Box{Text: "Picture Grid", Id: "picture_list", X: 0, Y: 0, W: 1000, H: 1000, Scale: 1.0, PinToScreen: false, SplitLayout: "free", DynamicBox: true, Colour: [4]uint8{64, 64, 64, 255}}
	CreatePicList(PicViewWorld, &grid, currentDisplayDirectory)

	Add(PicViewWorld, PicViewWorld.Top, &grid)

	Register(PicViewWorld.Id, "Choose Cancel", "Directory Up", func(name, handler_id string, args interface{}) {
		fmt.Printf("Directory Up\n")
		pbits := strings.SplitN(name, ":", 2)

		world_id := pbits[0]
		//Trim the last directory off the currentDisplayDirectory
		ps := strings.Split(currentDisplayDirectory, "/")
		if len(ps) > 2 {
			currentDisplayDirectory = strings.Join(ps[:len(ps)-2], "/")
		} else {
			currentDisplayDirectory = "/"
		}
		ForceSingleSlashAtEndCurrentDisplayDirectory()

		newDir := currentDisplayDirectory

		if goof.IsDir(newDir) {

			ListBox := FindBox(WorldOf(world_id), "picture_list")
			if ListBox != nil {
				//Delete all children of the list box
				for _, child := range ListBox.Children {
					RemoveBox(WorldOf(world_id), child)
				}
				ListBox.Children = []*Box{}
				CreatePicList(WorldOf(world_id), ListBox, currentDisplayDirectory)

			}

		}
	})

	Register(PicViewWorld.Id, "keep_picture", "Keep Picture", func(name, handler_id string, args interface{}) {
		log.Printf("Handling keep_pic\n")
		if args.(string) == "click" {
			currentPicture := FilePathOf("picture_viewer_box")
			fname := filepath.Base(currentPicture)
			os.Rename(currentPicture, "../Interesting/"+fname)
			fmt.Printf("Moved %v to %v\n", currentPicture, "../Interesting/"+fname)
		}
	})

	return PicViewWorld
}
