package boxes

import (
	"io/ioutil"
	"log"
	"strings"

	autoparser "gitlab.com/donomii/racketprogs/autoparser"
)

func ParseAndDisplayFile(w *World_s, filename string) *Box {
	//FIXME Load go files on demand into an editor
	codeNode := AddBox(w, w.Top, 0, 0, "codenode", nil)
	if strings.HasSuffix(filename, ".go") {
		// Parse source into AST
		ast := fileToAst(filename)
		BoxTree(w, codeNode, 0, 0, ast, 0, true)
		return codeNode

	}
	return nil
}

// Load a code file
func fileToAst(filename string) []autoparser.Node {
	// Load entire file main.go into var
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println(err)
		return []autoparser.Node{}
	}
	// Convert to string
	source := string(data)
	// Parse source into AST
	ast := autoparser.ParseGo(source, filename)
	return ast
}

// Takes an AST from autoparser and creates a box tree from it
// Becoming increasingly obsolete
func BoxTree(w *World_s, b *Box, x, y float64, t []autoparser.Node, indent int, newlines bool) (float64, float64) {
	ox := x
	oy := y
	for _, v := range t {

		if v.List == nil {
			var dx float64 = 0
			if v.Str == "" {
				b = AddBox(w, b, x, y, v.Raw+" ! ", &v)
				dx = b.W
			} else {
				b = AddBox(w, b, x, y, "『"+v.Str+"』", &v)
				dx = b.W
			}
			x = x + dx
		} else {
			//List
			//if len(v.List) == 0 && (v.Note == "\n" || v.Note == "artifact") {
			//	y = y + 100
			//	x = x + 100
			//	continue
			//}
			// If the current expression contains 3 or more sub expressions, break it across lines
			if autoparser.ContainsLists(v.List, 3) {
				if autoparser.CountTree(v.List) > 50 {

					//y = y + 100
					x = float64((indent + 1) * 100)
				}
				//n := &Box{Id: "ast_" + getNextBoxId(), SplitLayout: "free", AstNode: &v, Scale: 1}
				//Add(w, b, x, y, n)
				n := AddBox(w, b, x, y, "(+", &v)
				// AddBox(n, "(", v)

				_, dy := BoxTree(w, n, 0, 0, v.List, indent+2, true)
				y = float64(y + 100 + dy)
				x = x + 100
				//AddBox(w, n, x, y, ")", &v)
				// AddBox(n, ")", v)
			} else {
				//n := &Box{Id: "ast_" + getNextBoxId(), SplitLayout: "free", AstNode: &v, Scale: 1}
				//Add(w, b, x, y, n)
				n := AddBox(w, b, x, y, "(-", &v)

				// AddBox(n, "(", v)
				_, dy := BoxTree(w, n, n.W, 0, v.List, indent+2, false)
				y = y + dy

				// AddBox(n, ")", v)
				//AddBox(w, n, x, y, ")", &v)

			}
		}
		//if newlines {
		//	y = y + 500
		//}
	}
	y = y + 100
	return x - ox, y - oy
}
