set -x GO111MODULE auto
set -x CGO_ENABLED 1
set -x GOOS darwin
set -x GOARCH arm64
rm boxes
rm boxes-sdl
rm boxes-webgpu
go mod init boxes
go mod tidy
go get github.com/0xcafed00d/joystick github.com/alphadose/haxmap github.com/amitbet/vnc2video github.com/donomii/sceneCamera github.com/veandco/go-sdl2 github.com/nfnt/resize
go get github.com/donomii/goof                                                        
go get github.com/go-gl/glfw/v3.3/glfw                                                 
go get gitlab.com/donomii/racketprogs/autoparser
go get github.com/atotto/clipboard
go get github.com/donomii/glim
go get github.com/go-gl/gl/v3.2-core/gl
#go build -o boxes-sdl -tags "sdl static" -ldflags "-s -w" .
rm boxes boxes-sdl boxes-webgpu
go build  -gcflags="all=-lang=go1.23" -o boxes  ./apps/test/
go build  -gcflags="all=-lang=go1.23" -o boxes-sdl -tags "sdl static"  ./apps/test/
go build  -gcflags="all=-lang=go1.23" -o boxes-webgpu -tags webgpu  ./apps/test/
