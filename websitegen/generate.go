package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/BurntSushi/toml"
	"github.com/donomii/goof"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
)

var obsceneWords = []string{"fuck", "shit", "piss", "arsehole", "retard"}
var verbose bool

type FrontMatter struct {
	Obscene bool
	Title   string
	Tags    []string
}

type PageData struct {
	Title string
	Tags  string
	Body  string
}

func main() {
	sourceDir := flag.String("source", "", "Source Directory")
	destDir := flag.String("destination", "", "Destination Directory")
	flag.BoolVar(&verbose, "verbose", false, "Print more details")
	flag.Parse()

	if *sourceDir == "" || *destDir == "" {
		fmt.Println("Both --source and --destination must be provided")
		return
	}

	err := filepath.Walk(*sourceDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}

		relPath, _ := filepath.Rel(*sourceDir, path)
		destPath := filepath.Join(*destDir, relPath)
		destPath = strings.Replace(destPath, ".md", ".html", -1)

		if info.IsDir() {
			//Check for an index.md or index.html
			if !goof.Exists(path+"/index.md") && !goof.Exists(path+"/index.html") {
				//Generate an index.html file that points to the .md files in the directory
				entriesRev, err := ioutil.ReadDir(path)
				if err != nil {
					fmt.Printf("Error reading directory %s: %v\n", path, err)
					return nil
				}

				entries := []fs.FileInfo{}
				destPath = filepath.Join(destPath, "index.html")

				for _, entry := range entriesRev {
					entries = append([]fs.FileInfo{entry}, entries...)
				}

				//if there are any .md files in the directory, generate an index.html file
				foundMd := false
				for _, entry := range entries {
					if !entry.IsDir() && strings.HasSuffix(entry.Name(), ".md") {
						foundMd = true
						break
					}
				}

				if foundMd {
					if verbose {
						fmt.Printf("Generating index for directory %s\n", path)
					}
					var buf bytes.Buffer
					buf.WriteString("<ul>")
					for _, entry := range entries {
						if !entry.IsDir() && strings.HasSuffix(entry.Name(), ".md") {

							relEntryPath := entry.Name()
							relEntryPath = strings.Replace(relEntryPath, ".md", ".html", -1)
							fmt.Printf("relEntryPath: %s\n", relEntryPath)
							buf.WriteString(fmt.Sprintf("<li><a href=\"%s\">%s</a></li>", relEntryPath, entry.Name()))
						}
					}
					buf.WriteString("</ul>")

					pageData := PageData{
						Title: path,
						Tags:  "",
						Body:  buf.String(),
					}

					shouldReturn, returnValue := writeTemplate(destPath, pageData)
					if shouldReturn {
						fmt.Printf("%s failed!\n", path)
						return returnValue
					}

				} else {
					if verbose {
						fmt.Printf("No .md files in directory %s, generating index for subdirs\n", path)
					}
					var buf bytes.Buffer
					buf.WriteString("<ul>")
					for _, entry := range entries {

						if entry.IsDir() {
							relEntryPath := entry.Name() + "/index.html"
							fmt.Printf("relEntryPath: %s\n", relEntryPath)
							buf.WriteString(fmt.Sprintf("<li><a href=\"%s\">%s</a></li>", relEntryPath, entry.Name()))
						}
					}
					buf.WriteString("</ul>")

					pageData := PageData{
						Title: path,
						Tags:  "",
						Body:  buf.String(),
					}

					shouldReturn, returnValue := writeTemplate(destPath, pageData)
					if shouldReturn {
						fmt.Printf("%s failed!\n", path)
						return returnValue
					}
				}

			}

		} else {
			//It's a file

			if strings.HasSuffix(info.Name(), ".md") {
				data, err := ioutil.ReadFile(path)
				if err != nil {
					fmt.Printf("Error reading file %s: %v\n", path, err)
					return nil
				}

				parts := bytes.SplitN(data, []byte("+++"), 3)
				if len(parts) < 3 {
					fmt.Printf("Incorrect format in file %s\n", path)
					return nil
				}

				frontMatter := FrontMatter{}
				if _, err := toml.Decode(string(parts[1]), &frontMatter); err != nil {
					fmt.Printf("Error decoding toml in file %s: %v\n", path, err)
					return nil
				}

				if containsObsceneWords(string(parts[2])) && !frontMatter.Obscene {
					fmt.Printf("Obscene content in file %s\n", path)
					return nil
				}

				tags := strings.Join(frontMatter.Tags, ", ")

				md := goldmark.New(
					goldmark.WithExtensions(extension.GFM),
					goldmark.WithParserOptions(
						parser.WithAutoHeadingID(),
					),
					goldmark.WithRendererOptions(
						html.WithUnsafe(),
					),
				)
				var buf bytes.Buffer
				if err := md.Convert(parts[2], &buf); err != nil {
					fmt.Printf("Error converting markdown in file %s: %v\n", path, err)
					return nil
				}

				body := buf.String()

				body = strings.Replace(body, "<img", "<img loading=\"lazy\"", -1)

				pageData := PageData{
					Title: frontMatter.Title,
					Tags:  tags,
					Body:  body,
				}

				shouldReturn, returnValue := writeTemplate(destPath, pageData)
				if shouldReturn {
					fmt.Printf("%s failed!\n", path)
					return returnValue
				}
				if verbose {
					fmt.Printf("%s => %s\n", path, destPath)
				}
			} else {
				destDir := filepath.Dir(destPath)
				os.MkdirAll(destDir, 0755)
				copyFile(path, destPath)
				if verbose {
					fmt.Printf("%s => %s\n", path, destPath)
				}
			}
		}

		return nil
	})
	if err != nil {
		fmt.Printf("Error walking the path %q: %v\n", *sourceDir, err)
	}
}

func writeTemplate(destPath string, pageData PageData) (bool, error) {
	htmlTemplate := genHTMLTemplate()
	tmpl, err := template.New("page").Parse(htmlTemplate)
	if err != nil {
		fmt.Printf("Error parsing template: %v\n", err)
		return true, nil
	}

	os.MkdirAll(filepath.Dir(destPath), 0755)
	outFile, err := os.Create(destPath)
	if err != nil {
		fmt.Printf("Error creating file %s: %v\n", destPath, err)
		return true, nil
	}
	defer outFile.Close()

	err = tmpl.Execute(outFile, pageData)
	if err != nil {
		fmt.Printf("Error executing template in file %s: %v\n", destPath, err)
		return true, nil
	}
	return false, nil
}

func genHTMLTemplate() string {
	htmlTemplate := `
			<!DOCTYPE html>
			<html lang="en">
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="color-scheme" content="light dark">
			<link rel="icon" type="image/png" href="data:image/png;base64,">
			<title>{{ .Title }}</title>
			<style>
				@media (prefers-color-scheme: dark){
					body {
						background:#000
					}
				}
				body{
					margin:1em auto;
					max-width:40em;
					padding:0 .62em 3.24em;
					font:1.2em/1.62 sans-serif
				}
				h1,h2,h3 {
					line-height:1.2
				}
				@media print{
					body{
						max-width:none
					}
				}
				img {
					width: 95%;
					height: auto;
				}
			</style>
			<article>
			<header>
			<p>File under: {{ .Tags }}</p>
			<h1>{{ .Title }}</h1>
			</header>
			{{ .Body }}
			</article>
			</html>
			`
	return htmlTemplate
}

func containsObsceneWords(s string) bool {
	for _, word := range obsceneWords {
		if strings.Contains(s, word) {
			return true
		}
	}
	return false
}

func copyFile(src, dst string) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		fmt.Printf("Error stating file %s: %v\n", src, err)
		return
	}

	if !sourceFileStat.Mode().IsRegular() {
		fmt.Printf("%s is not a regular file\n", src)
		return
	}

	source, err := os.Open(src)
	if err != nil {
		fmt.Printf("Error opening file %s: %v\n", src, err)
		return
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		fmt.Printf("Error creating file %s: %v\n", dst, err)
		return
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		fmt.Printf("Error copying file from %s to %s: %v\n", src, dst, err)
	}
}
