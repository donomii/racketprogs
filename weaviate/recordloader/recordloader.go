package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	//yaml
	"gopkg.in/yaml.v3"
)

var matchUrl = ""
var urlCh chan string

type Objects struct {
	Objects []Object
}

type Object struct {
	Class      string            "json:\"class\""
	Properties map[string]string "json:\"properties\""
}

// Import every _Packages file in /var/lib/apt/lists/
func loadAllDpkgStatus() {
	files, err := ioutil.ReadDir("/var/lib/apt/lists")
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), "_Packages") {
			loadDpkgStatus("/var/lib/apt/lists/" + f.Name())
		}
	}
}

// Load and parse dpkg status file from
func loadDpkgStatus(filename string) {

	//Read the file into memory
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	//Split the file into packages using the empty line as separator
	packages := strings.Split(string(file), "\n\n")

	for _, pkg := range packages {

		//Create a map to store the properties
		properties := map[string]string{}

		//Parse the package as YAML
		packageYaml := map[string]string{}
		yaml.Unmarshal([]byte(pkg), packageYaml)

		count := 0
		//Set the properties from the yaml
		//Loop over the yaml properties
		for key, value := range packageYaml {

			// Remove all non-alphanum characters from key
			key = strings.Map(func(r rune) rune {
				if r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z' || r >= '0' && r <= '9' {
					return r
				}
				return -1
			}, key)
			//Add the property to the map

			properties[strings.ToLower(key)] = value
			count = count + 1
		}
		if count > 0 {

			//create object
			insertObject("AptPackages", properties)
		}
	}

}

func insertObject(class string, properties map[string]string) {
	fmt.Printf("Inserting object %v, %v\n", class, properties)
	object := Object{

		Class:      class,
		Properties: properties,
	}

	url := "http://localhost:8080/v1/objects"
	json, err := json.Marshal(object)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(json))
	// Upload the objects using a HTTP POST request
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(json))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// Read the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(body))
}

func addProperty(class string, property string, description string) {
	url := fmt.Sprintf("http://localhost:8080/v1/schema/%v/properties", class)

	data := fmt.Sprintf(`
	{
		"dataType": [
		  "string"
		],
		"description": "%v",
		"moduleConfig": {
			"text2vec-contextionary": {
				"skip": false,
				"vectorizePropertyName": false
			}
		},
		"name": "%v",
		"indexInverted": true,
		"tokenization": "word"
	  }
		  `, description, property)

	fmt.Printf("Creating property %v, %v\n", property, data)

	// Upload the class definition
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(data)))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// Read the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return
	}

	fmt.Println(string(body))
}

// The class call requires at least one property
func createClass(name, property string) {

	url := "http://localhost:8080/v1/schema"

	data := fmt.Sprintf(`
		  {
			"class": "%v",
			"description": "A publication with an online source",
			"invertedIndexConfig": {
			  "bm25": {
				"b": 0.75,
				"k1": 1.2
			  },
			  "cleanupIntervalSeconds": 60,
			  "stopwords": {
				"additions": null,
				"preset": "en",
				"removals": null
			  }
			},
			"moduleConfig": {
			  "text2vec-contextionary": {
				"vectorizeClassName": false
			  },
			  "text2vec-transformers": {
				"vectorizeClassName": false
			  }
			},
			"properties": [
				{
					"dataType": [
						"string"
					],
					"description": "Name of the publication",
					"moduleConfig": {
						"text2vec-contextionary": {
							"skip": false,
							"vectorizePropertyName": false
						}
					},
					"name": "%v",
					"tokenization": "word"
				}
			]
		}
	`, name, property)

	fmt.Printf("Creating class %v, %v\n", name, data)

	// Upload the class definition
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(data)))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// Read the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return
	}

	fmt.Println(string(body))
}

func main() {

	//read class string from command line
	class := flag.String("class", "defaultclass", "class")
	importDpkg := flag.Bool("import-dpkg", false, "import dpkg status file")
	flag.Parse()
	if *importDpkg {
		loadAllDpkgStatus()
		return
	}

	properties := map[string]string{}

	//Loop over remaining command line flags
	for i := 0; i < flag.NArg(); i = i + 2 {
		key := flag.Arg(i)
		value := flag.Arg(i + 1)
		properties[key] = value
	}

	//create class
	//createClass(*class)

	//add properties
	for key, _ := range properties {
		addProperty(*class, key, "a description")
	}

	//create object
	insertObject(*class, properties)

}
