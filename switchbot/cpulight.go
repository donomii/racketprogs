package main

import (
"os/exec"
"strconv"
"strings"
"math"
"io/ioutil"
"bytes"
	"net/http"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"time"
	"github.com/google/uuid"
)

func main() {
	// Declare empty header map
	apiHeader := make(map[string]string)

	// Open token
	token := "c1386a16904ef6659306df4b1614132a12894b5657a2631343337df9e85a55d54d2b76ced73d7c95bfcced60b75d15f8" // copy and paste from the SwitchBot app V6.14 or later

	// Secret key
	secret := "9bab90d7f022ea6e4edb56dce851d174" // copy and paste from the SwitchBot app V6.14 or later

	nonce := uuid.New()
	t := time.Now().UnixNano() / int64(time.Millisecond)

	stringToSign := fmt.Sprintf("%s%d%s", token, t, nonce.String())

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(stringToSign))
	sign := base64.StdEncoding.EncodeToString(h.Sum(nil))

	fmt.Printf("Authorization: %s\n", token)
	fmt.Printf("t: %d\n", t)
	fmt.Printf("sign: %s\n", sign)
	fmt.Printf("nonce: %s\n", nonce.String())

	// Build api header JSON
	apiHeader["Authorization"] = token
	apiHeader["Content-Type"] = "application/json"
	apiHeader["charset"] = "utf8"
	apiHeader["t"] = fmt.Sprintf("%d", t)
	apiHeader["sign"] = sign
	apiHeader["nonce"] = nonce.String()

	apiHeaderJSON, _ := json.Marshal(apiHeader)
	fmt.Println(string(apiHeaderJSON))
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.switch-bot.com/v1.1/devices", nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	for key, value := range apiHeader {
		req.Header.Add(key, value)
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making request:", err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
		return
	}

	fmt.Println(string(body))



	deviceId := "6055F939232A"
	statusReq, err := http.NewRequest("GET", fmt.Sprintf("https://api.switch-bot.com/v1.1/devices/%s/status", deviceId), nil)
	if err != nil {
		fmt.Println("Error creating status request:", err)
		return
	}

	for key, value := range apiHeader {
		statusReq.Header.Add(key, value)
	}

	statusResp, err := client.Do(statusReq)
	if err != nil {
		fmt.Println("Error making status request:", err)
		return
	}
	defer statusResp.Body.Close()

	statusBody, err := ioutil.ReadAll(statusResp.Body)
	if err != nil {
		fmt.Println("Error reading status response body:", err)
		return
	}

	fmt.Println("Device Status Response:")
	fmt.Println(string(statusBody))


setColorBody := map[string]string{
		"command":     "setColor",
		"parameter":   "122:80:20", // yellow
		"commandType": "command",
	}
	setColorBodyJSON, err := json.Marshal(setColorBody)
	if err != nil {
		fmt.Println("Error marshalling setColor body:", err)
		return
	}
	colorReq, err := http.NewRequest("POST", "https://api.switch-bot.com/v1.1/devices/6055F939232A/commands", bytes.NewBuffer(setColorBodyJSON))
	if err != nil {
		fmt.Println("Error creating setColor request:", err)
		return
	}

	for key, value := range apiHeader {
		colorReq.Header.Add(key, value)
	}

	colorResp, err := client.Do(colorReq)
	if err != nil {
		fmt.Println("Error making setColor request:", err)
		return
	}
	defer colorResp.Body.Close()

	colorBody, err := ioutil.ReadAll(colorResp.Body)
	if err != nil {
		fmt.Println("Error reading setColor response body:", err)
		return
	}

	fmt.Println("Set Color Response:")
	fmt.Println(string(colorBody))





	for {
		// Get CPU load
		out, err := exec.Command("sh", "-c", "uptime | awk -F 'load averages: ' '{ print $2 }' | cut -d ' ' -f 1").Output()
		if err != nil {
			fmt.Println("Error getting CPU load:", err)
			return
		}

		load, err := strconv.ParseFloat(strings.TrimSpace(string(out)), 64)
		if err != nil {
			fmt.Printf("Error parsing CPU load %v:%v\n", out, err)
			return
		}

		// Convert load to color
		r := int(math.Min(255, load*255))
		g := int(math.Min(255, (1-load)*255))
		b := 255

		if load > 1 {
			r = 255
			g = int((2 - load) * 255)
			b = int((2 - load) * 255)
		}

		parameter := fmt.Sprintf("%d:%d:%d", r, g, b)

		setColorBody := map[string]string{
			"command":     "setColor",
			"parameter":   parameter,
			"commandType": "command",
		}
		setColorBodyJSON, err := json.Marshal(setColorBody)
		if err != nil {
			fmt.Println("Error marshalling setColor body:", err)
			return
		}

		colorReq, err := http.NewRequest("POST", "https://api.switch-bot.com/v1.1/devices/6055F939232A/commands", bytes.NewBuffer(setColorBodyJSON))
		if err != nil {
			fmt.Println("Error creating setColor request:", err)
			return
		}

		for key, value := range apiHeader {
			colorReq.Header.Add(key, value)
		}

		colorResp, err := client.Do(colorReq)
		if err != nil {
			fmt.Println("Error making setColor request:", err)
			return
		}
		defer colorResp.Body.Close()

		colorBody, err := ioutil.ReadAll(colorResp.Body)
		if err != nil {
			fmt.Println("Error reading setColor response body:", err)
			return
		}

		fmt.Println("Set Color Response:")
		fmt.Println(string(colorBody))

		time.Sleep(5 * time.Second)
	}

}

