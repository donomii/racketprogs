import os
import torch
#from torch import autocast
from diffusers import StableDiffusionPipeline
from transformers import AutoModel

from PIL import Image

steps = 100
seed = 1151799
guidance=20.0
model_path = "./stable-diffusion-v1-4"
model_id = "CompVis/stable-diffusion-v1-4"
#device="cpu"
#device = "mps"
device = "cuda"


torch.use_deterministic_algorithms(True)


def image_grid(imgs, rows, cols):
    print(len(images), rows, cols)
    assert len(imgs) == rows*cols

    w, h = imgs[0].size
    grid = Image.new('RGB', size=(cols*w, rows*h))
    grid_w, grid_h = grid.size
    
    for i, img in enumerate(imgs):
        grid.paste(img, box=(i%cols*w, i//cols*h))
    return grid




access_token = "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"

#16bit
pipe = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16, use_auth_token=access_token)
pipe = pipe.to(device)


#pipe = StableDiffusionPipeline.from_pretrained(model_path)
#pipe = pipe.to(device)



#make a directory called "results"
if not os.path.exists("output"):
    os.makedirs("output")


#Load an array of prompts from prompts.txt.  Each line is a prompt
prompts = []
with open("prompts.txt") as f:
	for line in f:
		prompts.append(line.strip())
		


i=seed
for z in range(seed, seed+1000):
	i = i + 1



	#25,40,50,75,100,125,150,200
	#[10, 15, 20, 25, 30, 40, 50, 75, 100, 125, 150, 200]:
	for steps in [ 20, 25, 30, 35,  40, 50, 75, 100, 125, 150, 200]:
		#Loop over prompts


		#with autocast(device):
			#Loop from 0 to seed
			
			#Loop guidance over array [3.0, 7.5, 20.0]
			#for guidance in [4.5, 7.5,10.0,12.5,15.0, 20.0, 30.0, 40.0]:
			for iii in range(0,15000,1000):
				guidance = iii/1000
			
				for prompt in prompts:

					


					g = torch.Generator("cuda")
					g.manual_seed(i)
					torch.manual_seed(i)
					
					#torch.set_printoptions(threshold=10_000)
					#print("rng state: ", torch.get_rng_state())
					#image = pipe(prompt, num_inference_steps=steps, generator=generator, guidance_scale=8.5 )["sample"][0]  
					fname="output/"+prompt+"__device"+device+"_seed"+str(i)+"_g"+str(guidance)+"_iter"+str(steps)+".png"
					if len(fname) > 220:
						#create shorter prompt, ending in "...", for the filename
						shortprompt = prompt[0:220-len(fname)]+"..."
						print("!!!!!! prompt too long, shortened:", shortprompt, "\n\n\n")
						fname="output/"+shortprompt+"__device"+device+"_seed"+str(i)+"_g"+str(guidance)+"_iter"+str(steps)+".png"
					# Skip if fname exists
					if os.path.isfile(fname):
						print("Picture already rendered: ", fname)
						continue
					print("Generating: ", fname)
					image = pipe(prompt, num_inference_steps=steps,  guidance_scale=guidance )["images"][0]  
					image.save(fname)
					print("Saved output/"+fname)

					#num_images = 9
					#iprompts = [prompt] * num_images
					#print(prompt)

					#images = pipe(iprompts, num_inference_steps=steps, generator=generator)["sample"]

					##loop over images and save each one with a different number
					#for j in range(num_images):
					#images[j].save("output/"+prompt+"_"+str(i)+"_"+str(j)+"_g7.5.png")

					#print("Saved "+str(num_images)+" as "+"output/"+prompt+"_"+str(i)+"_")
					##grid = image_grid(images, rows=3, cols=3)
					##grid.save("output/"+prompt+"_"+str(i)+".png")
