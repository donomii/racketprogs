import torch
import transformers
import os

# Check if GPU is available
if torch.cuda.is_available():
    device = torch.device("cuda")
    print("Using GPU for inference")
else:
    device = torch.device("cpu")
    print("Using CPU for inference")

device="cpu"

access_token = "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"
model_dir = "/internaldata/d/aimodels"

# Create the directory if it doesn't exist
if not os.path.exists(model_dir):
    os.makedirs(model_dir)


# Initialize the Bloom AI model
model = transformers.AutoModelForQuestionAnswering.from_pretrained("bigscience/bloomz-7b1",use_auth_token=access_token, cache_dir=model_dir)

# Move the model to the chosen device
model.to(device)

# Define the question and the context (the text you want to search for the answer)
question = "What is the GDP of the United States?"
context = "The GDP of the United States was $21.44 trillion in 2020."

# Encode the input
input_ids = transformers.tokenizer.encode(question, context)

# Move the input to the chosen device
input_ids = torch.tensor(input_ids).unsqueeze(0).to(device)

# Get the start and end indices of the answer
start_scores, end_scores = model(input_ids)

# Find the answer
answer = transformers.tokenizer.decode(input_ids.squeeze(0)[torch.argmax(start_scores) : torch.argmax(end_scores) + 1].tolist())

print(answer)
# Output: "21.44 trillion"
