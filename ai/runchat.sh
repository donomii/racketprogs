#!/bin/sh

python3 -m venv .env
. .env/bin/activate
cd /internaldata/d/git/racketprogs/ai

# Pass the shell args to bloomOne.py
.env/bin/python3 bloomOne.py $@
