import torch
from transformers import BloomTokenizerFast 
from petals import DistributedBloomForCausalLM

MODEL_NAME = "bigscience/bloom-petals"
models_dir = "/internaldata/d/aimodels"
tokenizer = BloomTokenizerFast.from_pretrained(MODEL_NAME, cache_dir=models_dir)
model = DistributedBloomForCausalLM.from_pretrained(MODEL_NAME, cache_dir=models_dir)
model = model.cuda()

inputs = tokenizer('A cat in French is "', return_tensors="pt")["input_ids"].cuda()
outputs = model.generate(inputs, max_new_tokens=3)
print(tokenizer.decode(outputs[0]))
