from transformers import AutoModelForCausalLM, AutoTokenizer

checkpoint = "bigscience/bloomz-7b1"
model_dir = "/internaldata/d/aimodels/"

tokenizer = AutoTokenizer.from_pretrained(checkpoint, cache_dir=model_dir)
model = AutoModelForCausalLM.from_pretrained(checkpoint, cache_dir=model_dir)





def post_process_sentence(input_sentence, generated_sentence):
    new_sentence = generated_sentence.replace(input_sentence, "")
    if "\n" not in new_sentence:
        return new_sentence.replace("  ", " ") + "\n- "
    else:
        return (new_sentence.split("\n")[0]).replace("  ", " ") + "\n- "


def question_bloom(input_sentence, max_length, temperature, do_sample=True, top_k=3, seed=42):

    inputs = tokenizer.encode(input_sentence, return_tensors="pt", cache_dir=model_dir)
    outputs = model.generate(inputs, max_length=max_length)#, temperature=temperature, do_sample=do_sample, top_k=top_k)
    answer = tokenizer.decode(outputs[0])
    post_processed_output = post_process_sentence(input_sentence, answer)
    for o in outputs:
        print (tokenizer.decode(outputs[0]))
    return post_processed_output.split("\n-")[-2]

#Read the command line arguments into a variable
import sys
args = sys.argv
#Combine the args into a sentence
input_sentence = " ".join(args[1:])
#Build a conversation
#conversation="Finish this conversation:\nQuestion: "+ input_sentence + "\nAnswer: "
conversation=input_sentence
#Run the AI
answer = question_bloom(input_sentence=conversation, max_length=512, top_k=3, seed=1234567, temperature=0.7)
#Print the output
print(answer)

