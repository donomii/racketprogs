import argparse
from transformers import pipeline, AutoTokenizer
import os

parser = argparse.ArgumentParser()
parser.add_argument("--access_token", type=str, required=True, help="Hugging face access token")
parser.add_argument("--model_name", type=str, required=True, help="name of the model to download")
parser.add_argument("--directory", type=str, required=True, help="directory to save the model in")

args = parser.parse_args()

# Authenticate with the Hugging Face API
pipeline(task='text-generation', model=args.model_name, tokenizer=args.model_name, config=args.model_name, download=True, download_dir=args.directory, use_cache=False, api_key=args.access_token)

