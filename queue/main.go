package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	pmoolib "../pmoolib"

	"github.com/philippgille/gokv/file"
)

var chans map[string]chan []byte
var mode string = "files"

func GetChan(key string) chan []byte {
	ch, ok := chans[key]
	if !ok {
		ch = make(chan []byte, 1000)
		chans[key] = ch
	}
	return ch
}

func main() {
	StartKVstore()
	chans = make(map[string]chan []byte)

	//Create http router

	http.HandleFunc("/subscribe/", func(w http.ResponseWriter, req *http.Request) {
		//Get the channel name from the request
		channel := req.URL.Path[len("/subscribe/"):]

		key := channel
		ch := GetChan(key)
		start := time.Now()
		msg := <-ch
		elapsed := time.Since(start)
		if elapsed.Seconds() > 20 {
			ch <- msg
			w.Write([]byte{})
			return
		}
		w.Write(msg)
	})
	http.HandleFunc("/publish/", func(w http.ResponseWriter, req *http.Request) {
		//Get the channel name from the request
		channel := req.URL.Path[len("/publish/"):]
		data, _ := ioutil.ReadAll(req.Body)
		key := channel
		ch := GetChan(key)
		ch <- data
	})

	http.HandleFunc("/store/", func(w http.ResponseWriter, req *http.Request) {
		//Get the channel name from the request
		channel := req.URL.Path[len("/store/"):]
		data, _ := ioutil.ReadAll(req.Body)
		key := channel
		StoreObject(key, data)
	})

	http.HandleFunc("/fetch/", func(w http.ResponseWriter, req *http.Request) {
		//Get the channel name from the request
		channel := req.URL.Path[len("/fetch/"):]

		key := channel
		w.Write(LoadObject(key))
	})

	http.HandleFunc("/find/:propname/:propval", func(w http.ResponseWriter, req *http.Request) {
		//Get the property name and value from the url
		tail := req.URL.Path[len("/find/"):]
		propname, propval := strings.Split(tail, "/")[0], strings.Split(tail, "/")[1]

		res := findObjects(propname, propval)
		out, _ := json.Marshal(res)
		w.Write(out)
	})

	http.HandleFunc("/operational", func(w http.ResponseWriter, req *http.Request) {

		w.Write([]byte("Database operational"))
	})

	http.ListenAndServe("0.0.0.0:8080", nil) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

var KVstore file.Store
var err error

func findObjects(propname, propval string) []string {
	res := pmoolib.SearchObjects(propname, propval)
	return res
}

func StartKVstore() {
	options := file.DefaultOptions // Address: "localhost:6379", Password: "", DB: 0
	options.Directory = "objects"
	// Create client
	KVstore, err = file.NewStore(options)
	if err != nil {
		panic(err)
	}

}

func StoreObject(id string, m []byte) {
	if mode == "files" {
		ioutil.WriteFile("objects/"+id+".json", m, 0644)
	} else {
		err := KVstore.Set(id, m)
		if err != nil {
			panic(err)
		}
	}
}

func LoadObject(id string) []byte {
	if mode == "files" {
		data, _ := ioutil.ReadFile("objects/" + id + ".json")
		return data
	} else {
		retrievedVal := new([]byte)
		found, err := KVstore.Get(id, retrievedVal)
		if err != nil {
			panic(err)
		}
		if !found {
			panic("Value not found")
		}
		return *retrievedVal
	}
}

func DeleteObject(id string) {
	if mode == "files" {
		os.Remove("objects/" + id + ".json")
	} else {
		err := KVstore.Delete(id)
		if err != nil {
			panic(err)
		}
	}
}
