# Project Directory Overview

## Table of Contents

- [Quonverter](./quonverter/README.md) - A tool to transpile code between programming languages.
- [TagExplorer](./desktopweaviate/macos/TagExplorer/README.md) - A web-based tool to search and group files.
- [esmf](./esmf/README.md) - The Extremely Simple Message Format library.
- [weaviate-client](./desktopweaviate/macos/weaviate-client/README.md) - An unofficial Go client for Weaviate.
- [Dungeon Crawl Bullet Hell](./dcbh/README.md) - A game using DCSS sprites
- [gitRemind](./gitremind/README.md) - Tracks git repositories and reminds you to sync them.
- [gorays](./gorays/README.md) - A ray-tracing engine written in Go.
- [grep UI](./grui/README.md) - A graphical interface for grep and csearch.
- [trigrammr](./trigrammr/README.md) - A tool for exploring trigram databases.
- [Nursemai](./nursemai/README.md) - A user-mode daemon manager.
- [dataJam](./DataJam/README.md) - A Perl-based data handling utility.
- [Stalk](./stalk/README.md) - A simplified version of SmallTalk.
- [The Helpful Editor](./the-helpful-editor/README.md) - An experimental source code browser.
- [XSH](./xsh/README.md) - A cross-platform scripting language.
- [jslayout](./jslayout/README.md) - An experimental JavaScript text layout engine.
- [goStringTable](./goStringTable/README.md) - A thread-safe string table for Go.
- [racketProgs](./atto/README.md) - A tiny functional programming language
- [3dtri](./3dtri/README.md) - A 3D rendering demo.
- [haskell-rtrm](./haskell-rtrm/README.md) - A ray-marching engine in Haskell.
- [PerlTalk](./PerlTalk/README.md) - SmallTalk interpreter written in Perl.
- [Stable Diffusion](./ai/stable-diffusion/README.md) - Stable Diffusion setup for Apple Silicon Macs.
- [TAGDB](./tagdb/README.md) - A text search engine with real-time searches.
- [IMAPbayes](./IMAPbayes/README.md) - Automatically categorizes email based on folders.
- [vid-ee-oh](./vid-ee-oh/README.md) - Real-time browser-based video mixing.
- [myvox](./myvox/README.md) - A voxel renderer.
- [svarmrgo](./svarmrgo/README.md) - Go libraries for svarmr.
- [clm-launcher](./clm-launcher/README.md) - A command-line launcher like Launchy.
- [racket-json](./racket-json/README.md) - JSON parser for Racket.
- [trafficLights](./trafficLights/README.md) - Displays traffic light timings.
- [racket-port-relay](./port-relay-racket/README.md) - Relays TCP connections from one port to another.
- [svarmr](./svarmr/README.md) - A networked control bus with useful modules.
- [Blockly Sample App](./blockly/query/README.md) - Sample app using Blockly and related tools.
- [pmoo](./pmoo/README.md) - A MOO (Multi-User Domain) written in Go.
- [pmoo2](./pmoo2/README.md) - The next version of MOO (Multi-User Domain) written in Go.
- [AlfaWiki](./wikialfa/README.md) - A personal wiki system.
- [Code Test](./code_test/README.md) - A programming test for a job application.
- [srt-collector](./srt-collector/README.md) - Downloads subtitle files from torrents.
- [inconstant](./inconstant/README.md) - Converts data between different formats.
- [The Tool](./tool/README.md) - A collection of simple cross-platform programs.
- [1bit Neural Network](./1bitneural/README.md) - An attempt to create a one-bit trainable neural network.



