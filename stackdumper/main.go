package stackdumper

import (
    "encoding/json"
    "fmt"
    "os"
    "runtime"
    "strconv"
    "strings"
    "sync"
    "time"
)

var (
    outputFile *os.File
    mu         sync.Mutex
    running    bool
    done       chan bool
)

func Start(filename string) error {
    mu.Lock()
    defer mu.Unlock()

    if running {
        return fmt.Errorf("stack dumper already running")
    }

    f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
        return fmt.Errorf("failed to open file: %v", err)
    }

    outputFile = f
    running = true
    done = make(chan bool)

    go dumpPeriodically()
    return nil
}

func Stop() {
    mu.Lock()
    defer mu.Unlock()

    if !running {
        return
    }

    done <- true
    outputFile.Close()
    running = false
}

func dumpPeriodically() {
    ticker := time.NewTicker(1)
    defer ticker.Stop()

    for {
        select {
        case <-ticker.C:
            dumpStacks()
        case <-done:
            return
        }
    }
}

func dumpStacks() {
    mu.Lock()
    defer mu.Unlock()

    buf := make([]byte, 32768)
    n := runtime.Stack(buf, true)
    rawStack := string(buf[:n])

    dump := processStackTrace(rawStack)
    
    jsonData, err := json.Marshal(dump)
    if err != nil {
        fmt.Fprintf(os.Stderr, "failed to marshal stack dump: %v\n", err)
        return
    }

    if _, err := outputFile.Write(append(jsonData, '\n')); err != nil {
        fmt.Fprintf(os.Stderr, "failed to write stack dump: %v\n", err)
    }
}

func processStackTrace(rawStack string) map[string]interface{} {
    dump := map[string]interface{}{
        "timestamp":  time.Now().Unix(),
        "goroutines": make([]map[string]interface{}, 0),
    }

    var currentGoroutine map[string]interface{}
    lines := strings.Split(rawStack, "\n")

    for i := 0; i < len(lines); i++ {
        line := lines[i]
        
        if strings.HasPrefix(line, "goroutine ") {
            if currentGoroutine != nil {
                dump["goroutines"] = append(dump["goroutines"].([]map[string]interface{}), currentGoroutine)
            }

            parts := strings.Split(strings.TrimSuffix(line, ":"), " ")
            id, _ := strconv.Atoi(parts[1])
            state := strings.Trim(parts[2], "[]")

            currentGoroutine = map[string]interface{}{
                "id":     id,
                "state":  state,
                "frames": make([]map[string]interface{}, 0),
            }
        } else if strings.HasPrefix(line, "\t") && i+1 < len(lines) {
            // Each frame consists of two lines:
            // \tfunction(args)
            // \t\tfile:line
            functionLine := strings.TrimPrefix(line, "\t")
            i++ // Move to next line
            fileLine := strings.TrimPrefix(lines[i], "\t\t")
            
            if frame := parseFrame(functionLine, fileLine); frame != nil {
                currentGoroutine["frames"] = append(
                    currentGoroutine["frames"].([]map[string]interface{}), 
                    frame,
                )
            }
        }
    }

    if currentGoroutine != nil {
        dump["goroutines"] = append(dump["goroutines"].([]map[string]interface{}), currentGoroutine)
    }

    return dump
}

func parseFrame(functionLine, fileLine string) map[string]interface{} {
    // Parse function name - everything before '('
    funcName := functionLine
    if idx := strings.Index(funcName, "("); idx != -1 {
        funcName = funcName[:idx]
    }
    
    // Parse file and line number
    fileLineParts := strings.Split(fileLine, ":")
    if len(fileLineParts) != 2 {
        return nil
    }

    lineNum, err := strconv.Atoi(fileLineParts[1])
    if err != nil {
        return nil
    }

    return map[string]interface{}{
        "function": funcName,
        "file":     fileLineParts[0],
        "line":     lineNum,
    }
}
