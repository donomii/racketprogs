package main

import (
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	//Get all command line arguments
	args := os.Args
	//Find the index of "then" in the array
	index := 0
	for i, v := range args {
		if v == "then" {
			index = i
		}
	}

	whenIndex := index
	//make two arrays for the arguments before and after "when"
	before := args[1:whenIndex]
	after := args[whenIndex+1:]

	log.Printf("When: %s, then: %s .", strings.Join(before, " "), strings.Join(after, " "))

	//loop continuously
	for {
		//Run before as a shell command and get the success value
		status := runCommand(before)
		//If the command was successful, run the after command
		if status == 0 {
			//log.Printf("Running command: %s", strings.Join(after, " "))
			runCommand(after)
		}

		//fmt.Println("Waiting for 1 seconds")
		//sleep 1 second
		time.Sleep(time.Second)
	}

}

func runCommand(command []string) int {
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if exitError, ok := err.(*exec.ExitError); ok {
		return exitError.ExitCode()
	}
	return 0

}
